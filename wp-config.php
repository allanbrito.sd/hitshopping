<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'hitshopping');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', '127.0.0.1');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');
define('WP_MEMORY_LIMIT', '2G');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'or59XsD7!xqT.)bxD=Fa*5?+ksmR,X7Rya8MwC~u!)e75+oM+:{D(tt$MJ)XPEI-');
define('SECURE_AUTH_KEY',  'ZVEVpu&cd1Rbju4wsYrR)dN(I?qOFr<@R82;Z(?2n$Hq`-tEp9op,Ju/lhp14%A4');
define('LOGGED_IN_KEY',    'sRnW3^LomLkuIwbv#PWI,&p{q+((PC7+<^43h@V-34=nzF5;2_A<F]$67dO~z7Q*');
define('NONCE_KEY',        '4Yh%M$)`lH(swKIW6)|#ts1dn-bzFUkSJ9oU{YqS8g;5AifHrB+K{C>gh,E?X]I4');
define('AUTH_SALT',        'fU!z+]|ghnkB|[3q#>UU0hk1X4&g%CwuU&xV5*^!TN(1#K`k`:R+S^)j?+@k.iIv');
define('SECURE_AUTH_SALT', '2;L*{thCu ]@gRQd<&lsiJqpY8BX-,,HM%*&aB TjdrE{N6w+4m2]tg=aSu#M>rs');
define('LOGGED_IN_SALT',   '?w_jCw5X[HHM/m`xM[^1xDe7y>igI.%^j&QczaVPdfx}?~3YL`c[+Z<z0l:*.i.G');
define('NONCE_SALT',       '7<^ky6J}Sl-.j.{2#LiR^3go!EYV.t6$uX]:rs1I#=HO#v7>?b/*0a`|QJBxs||u');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'hs_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
