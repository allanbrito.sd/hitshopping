#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Questions and Answers\n"
"POT-Creation-Date: 2018-05-10 15:45+0200\n"
"PO-Revision-Date: 2015-07-31 14:23+0100\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <plugins@hitoutlets.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.13\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;"
"_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../functions.php:94
msgid ""
"YITH WooCommerce Questions and Answers is enabled but not effective. It "
"requires WooCommerce in order to work."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:602
msgid "Search for a product&hellip;"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:653
msgid "Unanswered questions"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:659
msgid "Questions"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:665
msgid "Answers"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:776
msgid "Set appropriate"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:778
msgid "Set inappropriate"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:782
#: ../lib/class.yith-woocommerce-question-answer.php:336
msgid "Approve"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:784
#: ../lib/class.yith-woocommerce-question-answer.php:340
msgid "Unapprove"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:808
msgctxt "yith-woocommerce-questions-and-answers"
msgid "Inappropriate"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:813
#, php-format
msgid "Inappropriate content <span class=\"count\">(%s)</span>"
msgid_plural "Inappropriate content <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:819
msgctxt "yith-woocommerce-questions-and-answers"
msgid "Unapproved"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:824
#, php-format
msgid "Content not approved <span class=\"count\">(%s)</span>"
msgid_plural "Content not approved <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:849
msgid ""
"An error occurred. The requested resource does not exist or the content is "
"deprecated."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:867
msgid "The email address is not valid."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:895
msgid ""
"Unsubscription completed. You will not receive any more answering requests "
"for the selected products."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:903
msgid ""
"Please, confirm your email address to stop receiving answering requests for "
"the questions about products you have bought:"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:908
msgid "Email address"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:917
msgid "Unsubscribe"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:947
msgctxt "Page slug"
msgid "questions_and_answers_unsubscription"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:949
msgctxt "Page title"
msgid "Questions & Answers unsubscription"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1378
msgid "Product not found!"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1387
msgid "Product: "
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1399
msgid "Response to: "
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1415
#: ../lib/class.yith-woocommerce-question-answer.php:495
msgid "Author"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1416
msgid "Upvotes"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1417
msgid "Downvotes"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1418
msgid "Abuse reports"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1461
msgid "Thanks"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1462
msgid "Your content has been sent correctly."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1463
msgid "An error has occurred, your content has not been added correctly."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1464
msgid "Please write your text before submitting it."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1465
msgid "Please enter your name"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1466
msgid "Please enter your e-mail"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1468
msgid "Please confirm you are human solving the reCaptcha."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1577
#, php-format
msgid "%d vote"
msgid_plural "%d votes"
msgstr[0] ""
msgstr[1] ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1596
#, php-format
msgid "%d on %d found it useful. What do you think?"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1599
msgid "Do you think this answer is useful?"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1615
msgid "Upvote this question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1621
msgid "Downvote this question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1880
msgid ""
"An error occurred during the reCaptcha validation. Recaptcha token not set."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1891
msgid ""
"An error occurred during the reCaptcha validation. The token is not valid."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1930
#: ../lib/class.yith-woocommerce-question-answer-premium.php:2128
#: ../lib/class.yith-woocommerce-question-answer.php:225
#: ../lib/class.yith-woocommerce-question-answer.php:845
#: ../lib/class.yith-woocommerce-question-answer.php:911
#: ../lib/class.ywqa-discussion.php:97
msgid "Anonymous user"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:1959
#: ../lib/class.yith-woocommerce-question-answer-premium.php:2153
msgid ""
"Thanks for your post. It has been sent to site administrator for approval."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer-premium.php:2224
msgid "Thanks!"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:174
#: ../lib/class.yith-woocommerce-question-answer.php:175
msgid "Content updated."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:176
msgid "Content published."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:177
msgid "Content saved."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:178
msgid "Content submitted."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:276
msgid "Questions & Answers - Manage answers for this question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:278
msgid "Questions & Answers - Question information"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:300
msgid "UNAPPROVED"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:303
msgid "INAPPROPRIATE"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:310
#: ../templates/single-product/ywqa-single-answer.php:33
#, php-format
msgid "%s answered on %s"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:325
msgid "Are you sure you want to delete it?"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:329
msgid "Edit"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:331
msgid "Delete"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:360
msgid "Confirm"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:362
msgid "Cancel"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:376
msgid "Answers for this question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:386
msgid "There are no answers for this question yet"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:406
msgid "Select product"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:453
msgid "Anonymous"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:486
#: ../lib/class.yith-woocommerce-question-answer.php:543
msgid "Product"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:506
#: ../templates/single-product/ywqa-product-answers.php:70
msgid "Your answer"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:522
#: ../templates/single-product/ywqa-product-answers.php:116
msgid "Answer"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:552
msgid "Question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:556
msgid "Go to question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:759
msgctxt "Post Type General Name"
msgid "Questions & Answers"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:760
msgctxt "Post Type Singular Name"
msgid "Question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:761
#: ../lib/class.yith-woocommerce-question-answer.php:776
#: ../plugin-options/general-options.php:100
msgid "Questions & Answers"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:762
msgid "Parent discussion"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:763
msgid "All discussion"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:764
msgid "View discussions"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:765
msgid "Add new question"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:766
msgid "Add new"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:767
msgid "Edit discussion"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:768
msgid "Update discussion"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:769
msgid "Search discussion"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:770
msgid "Not found"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:771
msgid "Not found in the bin"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:777
msgid "YITH Questions and Answers"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:826
#: ../lib/class.yith-woocommerce-question-answer.php:899
msgid "Please retry submitting your question or answer."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:833
msgid "No product ID selected, the question will not be created."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:992
msgid "You need to write something!"
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:993
msgid "Answer correctly sent."
msgstr ""

#: ../lib/class.yith-woocommerce-question-answer.php:994
msgid "An error occurred, your answer has not been added."
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:124
msgid "General"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:125
msgid "Advanced"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:128
#: ../lib/class.ywqa-plugin-fw-loader.php:187
msgid "Premium Version"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:184
msgid "Settings"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:212
msgid "Plugin Documentation"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:225
msgid ""
"YITH WooCommerce Questions and Answers is available in an outstanding "
"PREMIUM version with many new options, discover it now."
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:226
msgid "Premium version"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:233
msgid "YITH WooCommerce Questions and Answers"
msgstr ""

#: ../lib/class.ywqa-plugin-fw-loader.php:234
msgid ""
"In YIT Plugins tab you can find YITH WooCommerce Questions and Answers "
"options.<br> From this menu you can access all settings of YITH plugins "
"activated."
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:28
msgid "YITH Q&A - ask answer to customers"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:31
msgid ""
"When a new question is submitted for a product, ask a feedback to the "
"customers that purchased the same product"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:34
msgid "Give your feedback"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:36
msgid "[{site_title}] A user need your feedback for [{product_title}]"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:135
#: ../lib/emails/class.ywqa-email-notify-answer.php:136
#: ../lib/emails/class.ywqa-email-notify-question.php:132
msgid "Enable/Disable"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:137
#: ../lib/emails/class.ywqa-email-notify-answer.php:138
#: ../lib/emails/class.ywqa-email-notify-question.php:134
msgid "Enable this email notification"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:141
#: ../lib/emails/class.ywqa-email-notify-answer.php:142
#: ../lib/emails/class.ywqa-email-notify-question.php:146
msgid "Subject"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:143
#: ../lib/emails/class.ywqa-email-notify-answer.php:144
#: ../lib/emails/class.ywqa-email-notify-question.php:148
#, php-format
msgid ""
"This controls the email subject line. Leave blank to use the default "
"subject: <code>%s</code>."
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:149
#: ../lib/emails/class.ywqa-email-notify-answer.php:150
#: ../lib/emails/class.ywqa-email-notify-question.php:154
msgid "Email Heading"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:151
#: ../lib/emails/class.ywqa-email-notify-answer.php:152
#: ../lib/emails/class.ywqa-email-notify-question.php:156
#, php-format
msgid ""
"This controls the main heading contained within the email notification. "
"Leave blank to use the default heading: <code>%s</code>."
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:157
#: ../lib/emails/class.ywqa-email-notify-answer.php:158
#: ../lib/emails/class.ywqa-email-notify-question.php:162
msgid "Email type"
msgstr ""

#: ../lib/emails/class.ywqa-email-ask-customers-answer.php:159
#: ../lib/emails/class.ywqa-email-notify-answer.php:160
#: ../lib/emails/class.ywqa-email-notify-question.php:164
msgid "Choose which format of email to send."
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-answer.php:29
msgid "YITH Q&A - New answer notification"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-answer.php:32
msgid ""
"When an answer is submitted, sent a notification to the user who submitted "
"the question"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-answer.php:35
msgid "You have a new answer for [{product_title}]"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-answer.php:36
msgid "[{site_title}] You have a new answer for [{product_title}]"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-question.php:29
msgid "YITH Q&A - New question notification"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-question.php:32
msgid ""
"New question emails are sent to chosen recipient(s) when a new question is "
"submitted for some product."
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-question.php:35
msgid "You have a new question for [{product_title}]"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-question.php:36
msgid "[{site_title}] You have a new question for [{product_title}]"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-question.php:138
msgid "Recipient(s)"
msgstr ""

#: ../lib/emails/class.ywqa-email-notify-question.php:140
#, php-format
msgid ""
"Enter recipients (comma separated) for this email. Defaults to <code>%s</"
"code>."
msgstr ""

#: ../plugin-options/advanced-options.php:19
msgid "Advanced settings"
msgstr ""

#: ../plugin-options/advanced-options.php:25
msgid "Vote question"
msgstr ""

#: ../plugin-options/advanced-options.php:27
msgid "Allow users to vote product questions."
msgstr ""

#: ../plugin-options/advanced-options.php:32
msgid "New question notification"
msgstr ""

#: ../plugin-options/advanced-options.php:33
msgid ""
"Send a notification email to the administrator when new questions are "
"available."
msgstr ""

#: ../plugin-options/advanced-options.php:36
#: ../plugin-options/advanced-options.php:49
msgid "Do not send notifications"
msgstr ""

#: ../plugin-options/advanced-options.php:38
#: ../plugin-options/advanced-options.php:51
msgid "Notification in HTML email"
msgstr ""

#: ../plugin-options/advanced-options.php:45
msgid "New answer notification"
msgstr ""

#: ../plugin-options/advanced-options.php:46
msgid ""
"Send a notification email to the administrator when new answers are "
"available."
msgstr ""

#: ../plugin-options/advanced-options.php:58
msgid "User notification"
msgstr ""

#: ../plugin-options/advanced-options.php:59
msgid ""
"Allow users to receive a notification when their questions receive an answer."
msgstr ""

#: ../plugin-options/advanced-options.php:66
msgid "Vote answers"
msgstr ""

#: ../plugin-options/advanced-options.php:68
msgid "Allow users to vote answers."
msgstr ""

#: ../plugin-options/advanced-options.php:73
msgid "Inappropriate content"
msgstr ""

#: ../plugin-options/advanced-options.php:75
msgid "Let users report an answer as inappropriate content."
msgstr ""

#: ../plugin-options/advanced-options.php:78
msgid "Not enabled"
msgstr ""

#: ../plugin-options/advanced-options.php:79
msgid "Only registered users can report an inappropriate content"
msgstr ""

#: ../plugin-options/advanced-options.php:80
msgid "Everyone can report an inappropriate content"
msgstr ""

#: ../plugin-options/advanced-options.php:85
msgid "Hiding threshold"
msgstr ""

#: ../plugin-options/advanced-options.php:87
msgid ""
"Hide temporarily an answer when a specific number of users has flagged it as "
"inappropriate. Set this value to 0 to never hide automatically the reviews."
msgstr ""

#: ../plugin-options/advanced-options.php:97
msgid "Answer excerpt"
msgstr ""

#: ../plugin-options/advanced-options.php:99
msgid ""
"Set max length for answers and show a \"Read more\" text for showing all "
"content."
msgstr ""

#: ../plugin-options/advanced-options.php:109
msgid "Anonymous mode"
msgstr ""

#: ../plugin-options/advanced-options.php:111
msgid ""
"Do not show the name of the users that have added a question or an answer."
msgstr ""

#: ../plugin-options/advanced-options.php:116
msgid "Ask customers for an answer"
msgstr ""

#: ../plugin-options/advanced-options.php:117
msgid ""
"Send an email to whoever purchased a product when a new question is "
"available."
msgstr ""

#: ../plugin-options/advanced-options.php:120
msgid "Do not send requests"
msgstr ""

#: ../plugin-options/advanced-options.php:121
msgid "Send an email to all customers"
msgstr ""

#: ../plugin-options/advanced-options.php:122
msgid "Send an email to a sample of customers"
msgstr ""

#: ../plugin-options/advanced-options.php:129
msgid "Survey sample size"
msgstr ""

#: ../plugin-options/advanced-options.php:131
msgid ""
"(%) Set the percentage of customers that have bought the product that you "
"want to contact to ask for an answer."
msgstr ""

#: ../plugin-options/advanced-options.php:142
msgid "reCaptcha"
msgstr ""

#: ../plugin-options/advanced-options.php:144
msgid "Enable reCaptcha on plugin forms."
msgstr ""

#: ../plugin-options/advanced-options.php:149
msgid "reCaptcha site key"
msgstr ""

#: ../plugin-options/advanced-options.php:151
msgid "Insert your reCaptcha site key."
msgstr ""

#: ../plugin-options/advanced-options.php:157
msgid "reCaptcha secret key"
msgstr ""

#: ../plugin-options/advanced-options.php:159
msgid "Insert your reCaptcha secret key."
msgstr ""

#: ../plugin-options/advanced-options.php:164
msgid "Enable search"
msgstr ""

#: ../plugin-options/advanced-options.php:166
msgid "Show a search bar for filtering questions and answers."
msgstr ""

#: ../plugin-options/advanced-options.php:171
msgid "Search result items"
msgstr ""

#: ../plugin-options/advanced-options.php:173
msgid "Choose the number of results matching the search string to shown."
msgstr ""

#: ../plugin-options/general-options.php:19
msgid "General settings"
msgstr ""

#: ../plugin-options/general-options.php:24
msgid "Shop name"
msgstr ""

#: ../plugin-options/general-options.php:25
msgid "The reference name for email notifications."
msgstr ""

#: ../plugin-options/general-options.php:30
msgid "Question paging"
msgstr ""

#: ../plugin-options/general-options.php:32
msgid ""
"Set how many questions you want to show for each product (set 0 to display "
"all)."
msgstr ""

#: ../plugin-options/general-options.php:42
msgid "Answer paging"
msgstr ""

#: ../plugin-options/general-options.php:44
msgid ""
"Set how many answers you want to show for each question (set 0 to display "
"all)."
msgstr ""

#: ../plugin-options/general-options.php:54
msgid "Question approval"
msgstr ""

#: ../plugin-options/general-options.php:56
msgid "The entered question has to be approved before it may be shown."
msgstr ""

#: ../plugin-options/general-options.php:61
msgid "Answer approval"
msgstr ""

#: ../plugin-options/general-options.php:63
msgid "The entered answer has to be approved before it may be shown."
msgstr ""

#: ../plugin-options/general-options.php:68
msgid "Allow guest users"
msgstr ""

#: ../plugin-options/general-options.php:70
msgid "Let guest user to enter questions or answers"
msgstr ""

#: ../plugin-options/general-options.php:75
msgid "Mandatory data for guest users"
msgstr ""

#: ../plugin-options/general-options.php:77
msgid ""
"Guest user that want to submit a question or answers must fill his name and "
"email"
msgstr ""

#: ../plugin-options/general-options.php:82
msgid "FAQ mode"
msgstr ""

#: ../plugin-options/general-options.php:84
msgid ""
"Don't allow users to add questions and answers, but let them read them in "
"FAQ mode."
msgstr ""

#: ../plugin-options/general-options.php:89
msgid "Show on product tabs"
msgstr ""

#: ../plugin-options/general-options.php:91
msgid ""
"Choose if the plugin output should be shown as a tab in the product tabs. "
"Uncheck it if you want to use the shortcode [ywqa_questions] in a custom "
"position"
msgstr ""

#: ../plugin-options/general-options.php:96
msgid "Tab label"
msgstr ""

#: ../plugin-options/general-options.php:98
msgid "Set the text for the label showed in single product page"
msgstr ""

#: ../plugin-options/general-options.php:103
msgid "Question section title"
msgstr ""

#: ../plugin-options/general-options.php:105
msgid "Set the title for the section \"Questions & Answers\""
msgstr ""

#: ../plugin-options/general-options.php:107
msgid "Questions and answers of the customers"
msgstr ""

#: ../templates/emails/ywqa-ask-customers.php:22
#, php-format
msgid ""
"A user submitted a question about the product <b>%s</b> that you have bought "
"recently."
msgstr ""

#: ../templates/emails/ywqa-ask-customers.php:29
msgid "The question"
msgstr ""

#: ../templates/emails/ywqa-ask-customers.php:33
msgid "Do you want to answer to the question?"
msgstr ""

#: ../templates/emails/ywqa-ask-customers.php:35
msgid "Go to the question"
msgstr ""

#: ../templates/emails/ywqa-ask-customers.php:45
msgid "Don't receive any more requests for this product"
msgstr ""

#: ../templates/emails/ywqa-ask-customers.php:51
msgid "Don't receive any more requests for any products"
msgstr ""

#: ../templates/emails/ywqa-notify-answer.php:24
msgid "There is a new answer for the question: "
msgstr ""

#: ../templates/emails/ywqa-notify-answer.php:32
msgid "The answer"
msgstr ""

#: ../templates/emails/ywqa-notify-answer.php:38
#: ../templates/emails/ywqa-notify-question.php:35
msgid "View product"
msgstr ""

#: ../templates/emails/ywqa-notify-question.php:23
#, php-format
msgid "There is a new question submitted about the product <b>%s</b>."
msgstr ""

#: ../templates/emails/ywqa-notify-question.php:29
msgid "The question "
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:27
#: ../templates/single-product/ywqa-single-question.php:42
msgid "Q"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:34
msgid "Back to questions"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:34
msgid "<<< Back to questions"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:40
#, php-format
msgid "Asked on %s"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:45
#, php-format
msgid "Asked by %s on %s"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:53
msgid "There are no answers for this question, be the first to respond."
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:76
msgid "Type your answer here"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:85
#: ../templates/single-product/ywqa-product-questions.php:91
msgid "Name"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:92
#: ../templates/single-product/ywqa-product-questions.php:98
msgid "Enter your name"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:97
#: ../templates/single-product/ywqa-product-questions.php:103
msgid "Email"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:105
#: ../templates/single-product/ywqa-product-questions.php:111
msgid "Enter your email"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:117
msgid "Answer now to the question"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:123
msgid "Only registered users are eligible to enter answers"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:134
#, php-format
msgid "%s answer shown"
msgid_plural "%s answers shown"
msgstr[0] ""
msgstr[1] ""

#: ../templates/single-product/ywqa-product-answers.php:137
msgid "Sort by:"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:138
msgid "Most useful"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:140
msgid "Most recent"
msgstr ""

#: ../templates/single-product/ywqa-product-answers.php:142
msgid "Oldest"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:43
msgid "No question has still received an answer."
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:58
#, php-format
msgid "Show all %d questions"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:58
#, php-format
msgid "Show all %d questions %s"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:58
#, php-format
msgid "(%d without an answer)"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:62
msgid ""
"There are no questions yet, be the first to ask something for this product."
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:81
msgid "Your question"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:84
msgid "Do you have any questions? Ask now!"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:124
msgid "Send me a notification for each new answer."
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:127
msgid "Ask"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:128
msgid "Ask your question"
msgstr ""

#: ../templates/single-product/ywqa-product-questions.php:135
msgid "Only registered users are eligible to enter questions"
msgstr ""

#: ../templates/single-product/ywqa-search-bar.php:16
msgid "Type here the text to search"
msgstr ""

#: ../templates/single-product/ywqa-single-answer.php:19
#: ../templates/single-product/ywqa-single-question.php:68
msgid "Read more"
msgstr ""

#: ../templates/single-product/ywqa-single-answer.php:31
#, php-format
msgid "Answered on %s"
msgstr ""

#: ../templates/single-product/ywqa-single-answer.php:55
msgid "YES"
msgstr ""

#: ../templates/single-product/ywqa-single-answer.php:57
msgid "NO"
msgstr ""

#: ../templates/single-product/ywqa-single-answer.php:64
msgid "Report an inappropriate content"
msgstr ""

#: ../templates/single-product/ywqa-single-answer.php:65
msgid "Report"
msgstr ""

#: ../templates/single-product/ywqa-single-question.php:45
msgid "answer now"
msgstr ""

#: ../templates/single-product/ywqa-single-question.php:59
msgid "Answered by the admin"
msgstr ""

#: ../templates/single-product/ywqa-single-question.php:62
msgid "A"
msgstr ""

#: ../templates/single-product/ywqa-single-question.php:76
msgid "There are no answers for this question yet."
msgstr ""

#: ../templates/single-product/ywqa-single-question.php:85
msgid "Answer now"
msgstr ""

#: ../templates/single-product/ywqa-single-question.php:98
#, php-format
msgid "Show all %s answers"
msgstr ""

#~ msgid "This review is closed for replies"
#~ msgstr "This review is closed to replies"

#~ msgid "This review is closed for comments"
#~ msgstr "This review is closed to comments"

#~ msgid "Product id is in invalid format"
#~ msgstr "Product id is an invalid format"

#~ msgid "Voting/Reviews settings"
#~ msgstr "Voting/Review settings"

#~ msgid ""
#~ "In the YIT Plugins tab you can find the YITH WooCommerce Advanced Reviews "
#~ "options. With this menu, you can access to all the settings of our "
#~ "plugins that you have activated."
#~ msgstr ""
#~ "In YIT Plugins tab you can find YITH WooCommerce Advanced Reviews "
#~ "options. From this menu you can access all settings of YITH plugins "
#~ "activated."

#~ msgid ""
#~ "From now on, you can find all the options of YITH WooCommerce Advanced "
#~ "Reviews under YIT Plugin -> Advanced Reviews instead of WooCommerce -> "
#~ "Settings -> Advanced Reviews, as in the previous version. When one of our "
#~ "plugins updates, a new voice will be added to this menu."
#~ msgstr ""
#~ "From now on, you can find all YITH WooCommerce Advanced Reviews options "
#~ "in YIT Plugin -> Advanced Reviews instead of WooCommerce -> Settings -> "
#~ "Advanced Reviews, as in the previous version. Any time one of our plugins "
#~ "is updated, a new entry will be added to this menu."

#~ msgid "The layout for this "
#~ msgstr "Layout for this "

#~ msgid ""
#~ "The word used for the URL of each project (the slug of post if empty)"
#~ msgstr ""
#~ "Univocal identification name in the URL for each product (slug from post "
#~ "if empty)"

#~ msgid "Label Singular"
#~ msgstr "Label in Singular"

#~ msgid ""
#~ "Set the label in singular to use for each label (the title of portfolio "
#~ "if empty)"
#~ msgstr "Set a label in singular (title of portfolio if empty)"

#~ msgid "Label Plural"
#~ msgstr "Label in Plural"

#~ msgid ""
#~ "Set the label in plural to use for each label (the title of portfolio if "
#~ "empty)"
#~ msgstr "Set a label in plural (title of portfolio if empty)"

#~ msgid ""
#~ "If you want to use a category section for the portfolio, set the name of "
#~ "taxonomy. Name should be in slug form (must not contain capital letters "
#~ "or spaces) and not more than 32 characters long (database structure "
#~ "restriction)."
#~ msgstr ""
#~ "If you want to use categories in the portfolio, set a name for taxonomy. "
#~ "Name should be a slug (must not contain capital letters nor spaces) and "
#~ "must not be more than 32 characters long (database structure restriction)."

#~ msgid "Set the word to use in the URL for each category page."
#~ msgstr "Set the word for each category page URL."

#~ msgid "The layout for single page of this portfolio"
#~ msgstr "Layout for single page of this portfolio"

#~ msgid "Show the frontend of the %s"
#~ msgstr "Show frontend of the %s"

#~ msgid ""
#~ "The changes you made will be lost if you navigate away from this page."
#~ msgstr "The changes you made will be lost if you leave this page."

#~ msgid ""
#~ "If you continue with this action, you will reset all options are in this "
#~ "page."
#~ msgstr ""
#~ "If you go on with this action, you will reset all options in this page."

#~ msgid ""
#~ "The element you have written is already exists. Please, add another name."
#~ msgstr "This element already exists. Please, enter a different name."

#~ msgid "An error encoured during during import. Please try again."
#~ msgstr "An error has occurred during import. Please try again."

#~ msgid "The file you have insert doesn't valid."
#~ msgstr "The file inserted is not valid."

#~ msgid "I'm sorry, the import featured is disabled."
#~ msgstr "Sorry, import is disabled."

#~ msgid "Sorting done correctly."
#~ msgstr "Sorting successful."

#~ msgid ""
#~ "From now on, you can find all the options of your plugins under the YIT "
#~ "Plugin menu voice.\n"
#~ "                                     For every installation of our new "
#~ "plugins, a new voice will be added to access to the customization "
#~ "settings."
#~ msgstr ""
#~ "From now on, you can find all plugin options in YIT Plugin menu.\n"
#~ "                                     For each plugin installed, "
#~ "customization settings will be available as a new entry in YIT Plugin "
#~ "menu."

#~ msgid ""
#~ "From now on, you can find all the options of your plugins under the YIT "
#~ "Plugin menu voice.\n"
#~ "                                    When one of our plugins updates, a "
#~ "new voice will be added to this menu.\n"
#~ "                                    For example, after the update, the "
#~ "options from the plugins (YITH WooCommerce Wishlist, YITH WooCommerce "
#~ "Ajax Search, etc.)\n"
#~ "                                    will be removed from the previous "
#~ "location and moved under the YIT Plugin tab."
#~ msgstr ""
#~ "From now on, you can find all the options of your plugins in YIT Plugin "
#~ "menu.\n"
#~ "                                    Any time one of our plugins is "
#~ "updated, a new entry will be added to this menu.\n"
#~ "                                    For example, after update, plugin "
#~ "options (such for YITH WooCommerce Wishlist, YITH WooCommerce Ajax "
#~ "Search, etc.)\n"
#~ "                                    will be moved from previous location "
#~ "to YIT Plugin tab."

#~ msgid ""
#~ "There is a new version of %1$s available. <a href=\"%2$s\" class="
#~ "\"thickbox yit-changelog-button\" title=\"%3$s\">View version %4$s "
#~ "details</a>. <em>You have to activate the plugin on a single site of the "
#~ "network to benefit from the automatic updates.</em>"
#~ msgstr ""
#~ "There is a new version of %1$s available. <a href=\"%2$s\" class="
#~ "\"thickbox yit-changelog-button\" title=\"%3$s\">View version %4$s "
#~ "details</a>. <em>You have to activate the plugin on a single site of the "
#~ "network to benefit from automatic updates.</em>"

#~ msgid "%field% field can not be empty"
#~ msgstr "%field% field cannot be empty"

#~ msgid "%field_1% and %field_2% fields can not be empty"
#~ msgstr "%field_1% and %field_2% fields cannot be empty"

#~ msgid "Software has been deactive"
#~ msgstr "Software has been deactivated"

#~ msgid "Licence key has be banned"
#~ msgstr "Licence key has been banned"

#~ msgid "To Active"
#~ msgstr "Activation"

#~ msgid "Insert the title of field."
#~ msgstr "Insert title for the field."

#~ msgid ""
#~ "REQUIRED: The identification name of this field, that you can insert into "
#~ "body email configuration. <strong>Note:</strong>Use only lowercase "
#~ "characters and underscores."
#~ msgstr ""
#~ "REQUIRED: Field identification name to be entered into email body. "
#~ "<strong>Note:</strong>Use only lowercase characters and underscores."

#~ msgid "Select the type of this field."
#~ msgstr "Select type for this field."

#~ msgid "Select this if it's the email where you can reply."
#~ msgstr "Select this if it is the email you can reply to."

#~ msgid ""
#~ "Insert an additional class(es) (separateb by comma) for more "
#~ "personalization."
#~ msgstr ""
#~ "Insert additional class(es) (separated by commas) for more "
#~ "personalization."

#~ msgid "Choose how much long will be the field."
#~ msgstr "Set field length."

#~ msgid "The content of the tab. (HTML is supported)"
#~ msgstr "Content of the tab. (HTML is supported)"

#~ msgid "Sidebar Left"
#~ msgstr "Left sidebar"

#~ msgid "Sidebar Right"
#~ msgstr "Right sidebar"

#~ msgid "Add a title field on reviews."
#~ msgstr "Add a title field to reviews."

#~ msgid "Set maximum number of attachments selectable (0 = no limit)."
#~ msgstr ""
#~ "Set maximum number of attachments that can be selected (0 = no limit)."

#~ msgid "Reviews summary options"
#~ msgstr "Review summary options"

#~ msgid "Fixed bars color"
#~ msgstr "Fixed bar color"

#~ msgid "Percentage bars color"
#~ msgstr "Percentage bar color"

#~ msgid "Show people's votes"
#~ msgstr "Show review votes"

#~ msgid "Add a string stating how many people's found the review useful."
#~ msgstr "Add a string stating how many people found the review useful."

#~ msgid "Customers reviews"
#~ msgstr "Customers' reviews"
