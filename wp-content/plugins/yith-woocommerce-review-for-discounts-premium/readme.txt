=== YITH WooCommerce Review For Discounts ===

Contributors: yithemes
Tags: woocommerce, products, themes, yit, yith, yithemes, e-commerce, shop, coupon, reward, discount, review, product review, woocommerce review
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.2.1 =

* New: French language (credits to Josselyn Jayant)
* Update: Dutch language
* Update: Italian language
* Update: Spanish language
* Fix: WPML support

= 1.2.0 =
* New: Support to Wordpress 4.9.6
* New: Support to WooCommerce 3.4.0
* New: Integration with YITH WooCommerce Account Funds Premium
* Update: plugin framework

= 1.1.9 =

* New: Support to WooCommerce 3.3.0
* New: Dutch language
* Update: plugin framework

= 1.1.8 =

* New: Support to WooCommerce 3.2.0
* Update: plugin framework

= 1.1.7 =

* Update: plugin framework

= 1.1.6 =

* Fix: expiration date on emails
* Update: plugin framework

= 1.1.5 =

* Tweak: Support for WooCommerce 3.0.0-RC 2

= 1.1.3 =

* Fix: notice on vendor dashboard even if disabled

= 1.1.2 =

* Fix: author name missing when using YITH WooCommerce Advanced Reviews

= 1.1.1 =

* Fix: plugin options panel not working if plugin is disabled

= 1.1.0 =

* Fix: spaces in coupon codes

= 1.0.9 =

* New: Italian Translation
* New: Spanish Translation
* New: Norwegian Translation (Credits to Rune Kristoffersen)

= 1.0.8 =

* Fix: coupon creation date

= 1.0.7 =

* Tweak: Tested with WooCommerce 2.6.0 beta 3

= 1.0.5 =

* New: missing language file
* New: minified versions of CSS files

= 1.0.4 =

* Fix: coupon expiry date based on website timezone

= 1.0.3 =

* Fix: email visualization error

= 1.0.2 =

* Tweak: compatibility with WooCommerce 2.5

= 1.0.1 =

* Fix: get send status on test email

= 1.0.0 =

* Initial release
