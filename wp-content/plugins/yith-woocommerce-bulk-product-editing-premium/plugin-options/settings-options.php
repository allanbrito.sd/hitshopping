<?php
// Exit if accessed directly
!defined( 'YITH_WCBEP' ) && exit();


$tab = array(
    'settings' => array(
        'general-options' => array(
            'title' => __( 'General Options', 'yith-woocommerce-bulk-product-editing' ),
            'type'  => 'title',
            'desc'  => '',
        ),

        'round-prices' => array(
            'id'        => 'yith-wcbep-round-prices',
            'name'      => __( 'Round Prices', 'yith-woocommerce-bulk-product-editing' ),
            'type'      => 'yith-field',
            'yith-type' => 'onoff',
            'default'   => 'no',
            'desc'      => __( 'If enabled, the prices will be rounded when bulk editing.' )
        ),

        'general-options-end' => array(
            'type' => 'sectionend'
        )
    )
);

return apply_filters( 'yith_wcbep_panel_settings_options', $tab );