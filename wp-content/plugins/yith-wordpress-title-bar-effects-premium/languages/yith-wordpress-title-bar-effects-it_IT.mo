��             +         �  @   �     
          4  
   D     O     c  "   s  8   �     �     �  	   �     �  	          K   (  �   t  M   	  ]   W     �      �      �                $     1     �     �      �  .   �  1     k  G  =   �	     �	     
     
     (
     6
     >
  #   R
  R   v
     �
     �
  	   �
     �
  	          _   1  �   �  c   J  C   �     �  -   �  -   -     [     s     z  �   �                2     S     e                       	             
                                                                                                            %1$sTitle displayed on the tab when the animation is applied%2$s %1$sTitle prefix%2$s %1$sTitle suffix%2$s Animation speed Change tab Delay time to start Effect duration Enable WordPress Title Bar Effects Enable effects when users switch to another browser tab. General Settings Intermittence Live demo Plugin documentation Scrolling Select animation Set delay time (milliseconds) to start the effect after the page is loaded. Set the animation speed (milliseconds). This is an inversely proportionate value, so, the smaller the value, the quicker the animation is performed. Set the duration of the effect (milliseconds). Count starts after delay time. Set the time (milliseconds) that has to pass between an animation cycle and the following one Settings Text to prefix to the page title Text to suffix to the page title Time between animation cycles Title Title prefix Title shown on the tab when the animation is applied. If empty, the animation effect will be applied on the default page title. Title suffix Typing YITH WordPress Title Bar Effects plugin name in admin WP menuTitle Bar Effects plugin name in admin page titleTitle Bar Effects Project-Id-Version: YITH WordPress Title Bar Effects
POT-Creation-Date: 2016-10-24 11:21+0200
PO-Revision-Date: 2018-04-13 22:29+0200
Last-Translator: 
Language-Team: Yithemes <plugins@yithemes.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n!=1;
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 %1$sTitolo mostrato sulla tab mentre durante l'animazione%2$s %1$sPrefisso%2$s %1$sSuffisso%2$s Velocità animazione Cambia scheda Ritardo Durata dell'effetto Abilita WordPress Title Bar Effects Abilita gli effetti quando gli utenti passano da una scheda del browser all'altra. Impostazioni Generali A intermittenza Live demo Documentazione plugin A discesa Selezione un'animazione Imposta un ritardo (millisecondi) per l'animazione dal momento in cui la pagina viene caricata. Imposta la velocità dell'animazione (millisecondi). Si tratta di un valore inversamente proporzionale, quindi, più piccolo è il valore inserito, più velocemente sarà l'animazione. Imposta la durata dell'effetto (millisecondi). Verrà calcolato dopo il tempo di ritardo impostato. Imposta il tempo che deve passare tra un'animazione e la successiva Impostazioni Prefisso da aggiungere al titolo della pagina Suffisso da aggiungere al titolo della pagina Tempo tra le animazioni Titolo Prefisso Titolo mostrato nella tab a cui si applica l'animazione. Se vuoto, l'effetto dell'animazione sarà applicato al titolo predefinito della pagina. Suffisso Macchina da scrivere YITH WordPress Title Bar Effects Title Bar Effects Title Bar Effects 