��             +         �  @   �     
          4  
   D     O     c  "   s  8   �     �     �  	   �     �  	          K   (  �   t  M   	  ]   W     �      �      �                $     1     �     �      �  .   �  1     r  G  O   �	     

     
     4
     F
     U
  
   r
  '   }
  P   �
     �
       	        "     6     >  _   Q  �   �  U   E  m   �     	  %     %   <     b     �     �  �   �     +     8      >     _     q                       	             
                                                                                                            %1$sTitle displayed on the tab when the animation is applied%2$s %1$sTitle prefix%2$s %1$sTitle suffix%2$s Animation speed Change tab Delay time to start Effect duration Enable WordPress Title Bar Effects Enable effects when users switch to another browser tab. General Settings Intermittence Live demo Plugin documentation Scrolling Select animation Set delay time (milliseconds) to start the effect after the page is loaded. Set the animation speed (milliseconds). This is an inversely proportionate value, so, the smaller the value, the quicker the animation is performed. Set the duration of the effect (milliseconds). Count starts after delay time. Set the time (milliseconds) that has to pass between an animation cycle and the following one Settings Text to prefix to the page title Text to suffix to the page title Time between animation cycles Title Title prefix Title shown on the tab when the animation is applied. If empty, the animation effect will be applied on the default page title. Title suffix Typing YITH WordPress Title Bar Effects plugin name in admin WP menuTitle Bar Effects plugin name in admin page titleTitle Bar Effects Project-Id-Version: YITH WooCommerce Email Templates
POT-Creation-Date: 2018-03-15 16:35+0000
PO-Revision-Date: 2018-03-15 17:28+0000
Last-Translator: 
Language-Team: Yithemes <plugins@yithemes.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 %1$sTitel die wordt weergegeven op het tabblad als de animatie is toegepast%2$s %1$sTitel prefix%2$s %1$sTitel suffix%2$s Animatie snelheid Wijzig tabblad Vertragingstijd voor starten Effectduur WordPress Title Bar Effects inschakelen Effecten inschakelen wanneer gebruikers wisselen naar een ander browser tabblad. Algemene Instellingen Verspringen Live demo Plugin Documentatie Rollend Selecteer animatie Stel de vertragingstijd in (milliseconden) om het effect te starten nadat de pagina is geladen. Stel de animatiesnelheid in (milliseconden). Dit is een omgekeerde waarde, dus, hoe kleiner de waarde, des te sneller wordt de animatie uitgevoerd. Stel de duur van het effect in (milliseconden). Aftellen start na de vertragingstijd. Stel de tijd in (milliseconden) die moet passeren tussen een animatie periode en de volgende animatie periode Instellingen Tekst voor prefix voor de paginatitel Tekst voor suffix voor de paginatitel Tijd tussen animatie periodes Titel Titel prefix Titel die wordt weergegeven in het tabblad als de animatie is toegepast. Indien leeg, wordt het animatie effect toegepast in de standaard pagina titel. Titel suffix Typen YITH WordPress Title Bar Effects Title Bar Effects Title Bar Effects 