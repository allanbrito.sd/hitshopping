== YITH WordPress Title Bar Effects ===

== Changelog ==

= 1.1.1 - Released: Sept 27, 2018 =

* Update: Core Framework 3.0.23

= 1.1.0 - Released: Apr 24, 2018 =

* New: plugin fw version 3.0.15
* New: Spanish translation
* New: Dutch translation
* New: Italian translation

= 1.0.1 - Released: Jan 12, 2017 =

* New: integration with YITH Desktop Notifications for WooCommerce

= 1.0.0 - Released: Oct 24, 2016 =

* Initial release