=== YITH WooCommerce Terms & Conditions Popup ===

Contributors: yithemes
Tags: terms, conditions, privacy, popup, woocommerce, checkout, themes, yit, e-commerce, shop
Requires at least: 4.0
Tested up to: 4.9.6
Stable tag: 1.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Documentation: http://hitoutlets.com/docs-plugins/yith-woocommerce-terms-conditions-popup/

== Changelog ==

= 1.2.1 - Released: 2018/05/28 =

* New: WooCommerce 3.4 compatibility
* New: WordPress 4.9.6 compatibility
* New: updated plugin framework to latest version
* New: GDPR compliance

= 1.2.0 - Released: 2018/02/05 =

* New: added WooCommerce 3.3.x support
* New: added WordPress 4.9.2 support
* New: updated plugin-fw
* New: added dutch translation
* New: added support to VC shortcodes into T&C popup content
* Tweak: fixed "Force to read" option when text is not scrollable
* Tweak: switched to Perfect Scrollbar to improve Browser/OS support
* Tweak: added CSS code to avoid strange appearance problem when closing popup

= 1.1.0 - Released: 2017/04/04 =

* New: Added WordPress 4.7.3 compatibility
* New: Added WooCommerce 3.0.0 RC2 compatibility
* New: Added italian - ITALY translation
* Tweak: using is_checkout() instead of checking page directly
* Tweak: to avoid problems with browsers, now only 95% of total scroll is required
* Fix: popup transitions

= 1.0.5 - Released: 2016/06/13 =

* Added: WooCommerce 2.6-RC1 support
* Added: spanish translation

= 1.0.4 - Released: 2016/02/09 =

* Added: WooCommerce 2.5.x compatibility
* Added: WordPress 4.4.x compatibility
* Tweak: Performance improved with new plugin core 2.0
* Tweak: Added filter the_content to "Terms & Conditions" and "Privacy" popup content
* Tweak: Added do_shortcode to "Terms & Conditions" and "Privacy" popup content
* Updated: jQuery ScrollBar to version 0.2.10
* Fixed: checkbox no more required on checkout (since WC 2.4)

= 1.0.3 - Released: 2015/08/13 =

* Added: Compatibility with WC 2.4.2
* Tweak: Updated internal plugin-fw
* Fixed: WPML compatibility

= 1.0.2 - Released: 2015/05/04 =

* Added: WP 4.2.1 support
* Fixed: "Plugin Documentation" appearing on all plugins
* Fixed: Wpml page id issue

= 1.0.1 - Released: 2015/04/20 =

* Added: link specific class
* Tweak: changed include_once in include for templates
* Tweak: changed js to use classes, instead of ids
* Tweak: changed background-color in background for a much stronger condition
* Fixed: popup close button style

= 1.0.0 - Released: 2015/03/11 =

* Initial release