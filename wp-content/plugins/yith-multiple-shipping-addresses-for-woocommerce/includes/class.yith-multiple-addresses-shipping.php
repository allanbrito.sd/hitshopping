<?php

if ( ! defined( 'YITH_WCMAS_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_Multiple_Addresses_Shipping' ) ) {
	/**
	 * Class YITH_Multiple_Addresses_Shipping
	 *
	 * @author Carlos Mora <carlos.eugenio@yourinspiration.it>
	 * @since 1.0.0
	 */
	class YITH_Multiple_Addresses_Shipping {

        /**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0
		 */
		protected $version = YITH_WCMAS_VERSION;

        /**
		 * Main Instance
		 *
		 * @var YITH_Advanced_Refund_System
		 * @since 1.0
		 * @access protected
		 */
		protected static $_instance = null;

		/**
		 * Main Admin Instance
		 *
		 * @var YITH_Multiple_Addresses_Shipping_Admin
		 * @since 1.0
		 */
		protected $admin = null;

        /**
         * Main Frontend Instance
         *
         * @var YITH_Multiple_Addresses_Shipping_Frontend
         * @since 1.0
         */
        public $frontend = null;

        /**
         * Construct
         *
         * @author Carlos Mora <carlos.eugenio@yourinspiration.it>
         * @since 1.0
         */
        protected function __construct() {
            add_filter( 'woocommerce_email_classes', array( $this, 'register_email_classes' ) );
	        // Load Plugin Framework
	        add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
	        add_action( 'plugins_loaded', array( $this, 'load_privacy' ), 20 );

	        $this->init_requires();
	        $this->init_classes();
        }

        /**
		 * Main plugin Instance
		 *
		 * @return YITH_Multiple_Addresses_Shipping Main instance
		 * @author Carlos Mora <carlos.eugenio@yourinspiration.it>
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		/**
		 * Load plugin framework
		 *
		 * @author Andrea Grillo <andrea.grillo@hitoutlets.com>
		 * @since  1.0.0
		 * @return void
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once( $plugin_fw_file );
				}
			}
		}

		/**
		 * Load privacy class
		 * @author Carlos Mora <carlos.eugenio@yourinspiration.it>
		 * @since 1.0.4
		 */
		public function load_privacy() {
			require_once( YITH_WCMAS_PATH . 'includes/class.yith-multiple-addresses-shipping-privacy.php' );
		}

		/**
		 * Require main files
		 *
		 * @author Carlos Mora <carlos.eugenio@yourinspiration.it>
		 * @since 1.0.0
		 */
		public function init_requires() {
			require_once( YITH_WCMAS_PATH . 'includes/functions.yith-mas.php' );
			require_once( YITH_WCMAS_PATH . 'includes/functions.yith-mas-gdpr.php' );
			require_once( YITH_WCMAS_PATH . 'includes/class.yith-multiple-addresses-shipping-admin.php' );
			require_once( YITH_WCMAS_PATH . 'includes/class.yith-multiple-addresses-shipping-frontend.php' );
			require_once( YITH_WCMAS_PATH . 'includes/tables/class.yith-wcmas-table-template.php' );
			require_once( YITH_WCMAS_PATH . 'includes/tables/class.yith-wcmas-excluded-products-table-options.php' );
			require_once( YITH_WCMAS_PATH . 'includes/tables/class.yith-wcmas-excluded-categories-table-options.php' );
		}

        /**
		 * Class Initialization
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Carlos Mora <carlos.eugenio@yourinspiration.it>
		 * @since  1.0.0
		 */
		public function init_classes() {
            if ( is_admin() ) {
				$this->admin = new YITH_Multiple_Addresses_Shipping_Admin();
	            YITH_WCMAS_Excluded_Products_Table_Options();
	            YITH_WCMAS_Excluded_Categories_Table_Options();
            }

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = new YITH_Multiple_Addresses_Shipping_Frontend();
			}
		}

		/*
		 * Register Email Classes
		 *
		 * Register the new email classes used by the plugin
		 * @param array $email_classes
		 *
		 * @return array
		 */
        function register_email_classes( $email_classes ) {
            $email_classes['YITH_MAS_Shipping_Status_Change_Email'] = include( 'emails/class.yith-mas-change-status-email.php' );
            return $email_classes;
        }

    }
}