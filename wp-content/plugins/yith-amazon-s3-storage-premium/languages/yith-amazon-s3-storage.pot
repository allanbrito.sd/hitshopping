#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH Amazon S3 Storage\n"
"POT-Creation-Date: 2017-09-27 16:37+0100\n"
"PO-Revision-Date: 2017-09-27 15:49+0100\n"
"Last-Translator: \n"
"Language-Team: YITH <plugins@hitoutlets.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.2\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;"
"_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin-fw\n"

#: includes/class.yith-wc-amazon-s3-storage-admin.php:215
#: templates/general_settings_tab.php:146
msgid "Copy files also to S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:237
#: templates/woocommerce_settings_tab.php:60
msgid "Private "
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:248
#: templates/woocommerce_settings_tab.php:69
msgid "Public "
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:273
#: templates/general_settings_tab.php:190
msgid "Remove from the server"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:282
msgid ""
"(after removing from the server the file is NOT going to be shown in the "
"media library)"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:583
msgid "Amazon S3 Storing"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:587
msgid "Connect to your S3 amazon account"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:592
msgid "Settings"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:594
msgid "WooCommerce settings for digital goods"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:293
msgid "Copied to S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:305
msgid "Removed from S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:316
msgid "Copied to server from S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:328
msgid "Removed from server"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:840
#: templates/connects3_tab.php:111
msgid "The connection with Amazon S3 Storage was done successfully"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:857
#: templates/connects3_tab.php:128
msgid ""
"There was an error while accessing, the credentials (access key or secret "
"key) are NOT correct"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:148
msgid "Choose a bucket"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:177
msgid ""
"There was an error while accessing, the credentials (access key and secret "
"key) are NOT correct"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:217
msgid "Private"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:221
msgid "Public"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:247
msgctxt "S3 File Manager"
msgid "Bucket"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:257
msgctxt "S3 File Manager"
msgid "Current folder"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:479
msgid "The bucket couldn't be created"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:170
msgctxt "yith-amazon-s3-storage"
msgid "Copying to S3"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:176
msgctxt "Button action mode grid"
msgid "Removing from S3"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:182
msgctxt "Button action mode grid"
msgid "Copying to server from S3"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:188
msgctxt "Button action mode grid"
msgid "Removing from server"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:290
msgid ""
"You have to configure correctly your Access Key and Secret Access Key in the "
"\"Connect to your s3 amazon account\" tab"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:319
msgid ""
"You have to choose a bucket in the \"Setting\" tab in the amazon s3 admin "
"panel"
msgstr ""

#: templates/connects3_tab.php:30 templates/general_settings_tab.php:60
#: templates/woocommerce_settings_tab.php:32
msgid "Settings saved."
msgstr ""

#: templates/connects3_tab.php:53
msgid "Access key"
msgstr ""

#: templates/connects3_tab.php:57
msgid "Set the access key"
msgstr ""

#: templates/connects3_tab.php:67
msgid "Secret access key"
msgstr ""

#: templates/connects3_tab.php:71
msgid "Set the secret access key"
msgstr ""

#: templates/connects3_tab.php:75
msgid "If you don't know where to search for your S3 credentials, "
msgstr ""

#: templates/connects3_tab.php:77
msgid "you can find it here"
msgstr ""

#: templates/connects3_tab.php:86 templates/general_settings_tab.php:202
#: templates/woocommerce_settings_tab.php:91
msgid "Save Changes"
msgstr ""

#: templates/general_settings_tab.php:86
msgid "Set a bucket name"
msgstr ""

#: templates/general_settings_tab.php:98
msgid "No Buckets Found!!"
msgstr ""

#: templates/general_settings_tab.php:125
msgid ""
"Select the bucket name of S3 where to upload all the files. If you don't "
"know how to create a bucket, "
msgstr ""

#: templates/general_settings_tab.php:127
msgid "you can click here"
msgstr ""

#: templates/general_settings_tab.php:140
msgid "Copy file to S3"
msgstr ""

#: templates/general_settings_tab.php:151
msgid ""
"The files which are being uploaded to the media library will be added "
"automatically to S3. The files which are already in the media library will "
"NOT be uploaded to S3"
msgstr ""

#: templates/general_settings_tab.php:162
msgid "Replace URL"
msgstr ""

#: templates/general_settings_tab.php:168
msgid "Files will be served by S3"
msgstr ""

#: templates/general_settings_tab.php:173
msgid ""
"The URL's of the files will be automatically replaced to be downloaded from "
"S3 and not from your wordpress installation"
msgstr ""

#: templates/general_settings_tab.php:184
msgid "Remove from server"
msgstr ""

#: templates/general_settings_tab.php:195
msgid "The files uploaded to S3 amazon will be deleted from your server"
msgstr ""

#: templates/woocommerce_settings_tab.php:53
msgid "Permissions"
msgstr ""

#: templates/woocommerce_settings_tab.php:73
msgid ""
"By setting the files public, those who know the S3 url will have completely "
"access "
msgstr ""

#: templates/woocommerce_settings_tab.php:81
msgid "URL Valid period"
msgstr ""

#: templates/woocommerce_settings_tab.php:85
msgid "Set the time in minutes the URL is valid for downloading"
msgstr ""
