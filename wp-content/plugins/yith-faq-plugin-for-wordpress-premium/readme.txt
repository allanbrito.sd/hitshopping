﻿=== YITH FAQ Plugin for WordPress Premium ===

Contributors: yithemes
Tags: faq, faqs, yit, yith, yithemes, e-commerce, shop, frequently asked questions
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.0.3 =

* Fix: scripts conflicting with other plugins scripts

= 1.0.2 =

* New: Italian translation
* Update: plugin framework

= 1.0.1 =

* New: Dutch translation
* Update: plugin framework
* Fix: pagination links behavior

= 1.0.0 =

* Initial release
