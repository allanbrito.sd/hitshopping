��    V      �     |      x  	   y     �     �     �     �     �     �  
   �     �     �          .  "   D     g  '   �  $   �     �     �     �     �     	      	     6	      Q	  "   r	     �	     �	     �	     �	     �	     �	     
     
     ,
     F
     T
  .   c
     �
     �
     �
     �
  $   �
     �
  )        1     >     F     K  	   Y  	   c     m     �     �     �     �     �     �     �     �                    /     I     b     �  
   �     �     �     �     �     �  	          +         H     i     �     �     �     �     �      �  #   �  %     z  7     �  4   �     �               3     P  	   c     m     ~     �     �  .   �  #   �  4   "  '   W          �     �     �     �  ,   �  @     ;   E  2   �  (   �     �     �          !     2  3   O  '   �  >   �     �       -        K  
   b     m  "   t  3   �  (   �  9   �     .     >  
   L     W     i     �  $   �     �     �     �     �     �          /     3  
   7     B     K  0   e  $   �  ;   �     �             "   ;     ^     k          �     �  G   �  	     	             &     9     N     a     h     l  	   p     J                            F   2   3             >                          K       A                       P   I   1   )   ,   6   U   E       @      B       S       %   0   ;   7              =   *           M   .          L          (       O   C       $   R       G   D       :                 8      -   
          9   +          "   !             V      H       <   '   &      ?              T             /   #       5                         	       N   4   Q        Accordion Add FAQ shortcode Add New Add New Category Add New FAQ Add or remove categories All Categories Categories Categories list Categories list navigation Categories to display Category Button Color Category Button Hover/Active Color Category Button Text Color Category Button Text Hover/Active Color Choose from the most used categories Choose the icon Choose the style Color Settings Copied! Copy FAQ Link Customize Category Button Colors Customize FAQ Icon Colors  Customize FAQ Link Button Colors Customize Navigation Button Colors Customize Search Button Colors Edit Category Edit FAQ FAQ Icon Background Color FAQ Icon Color FAQ Link Color FAQ Link Hover Color FAQ Link Icon Color FAQ Link Icon Hover Color FAQs per page Icon size (px) If left empty all categories will be displayed Insert Shortcode Left List Navigation Button Color Navigation Button Hover/Active Color Navigation Button Text Color Navigation Button Text Hover/Active Color New Category New FAQ Next No Categories Not Found Not found Not found in Trash Off Off/On Parent Category Parent Category: Plugin documentation Popular Categories Post Type General NameFAQs Post Type Singular NameFAQ Previous Right Search Button Color Search Button Hover Color Search Button Icon Color Search Button Icon Hover Color Search Categories Search FAQ Search FAQs Categories Separate items with commas Settings Shortcode Creation Show category filters Show icon Show search box Sorry, no matching results for your search. Taxonomy General NameCategories Taxonomy Singular NameCategory Toggle Update Category View Category View FAQ color tab nameColor plugin name in admin WP menuFAQ plugin name in admin page titleFAQ shortcode creation tab nameShortcode Project-Id-Version: YITH FAQ Plugin for WordPress
POT-Creation-Date: 2018-08-10 11:15+0200
PO-Revision-Date: 2018-09-07 10:35+0200
Last-Translator: 
Language-Team: YIThemes <plugins@yithemes.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Firsarmonica Aggiungi lo shortcode per le domande frequenti (FAQ) Aggiungi nuova Aggiungi nuova categoria Aggiungi nuova domanda Aggiungi o rimuovi categorie Tutte le categorie Categorie Elenco categorie Navigazione elenco categorie Categorie da mostrare Colore dei pulsanti categoria Colore dei pulsanti categoria attivi/mouseover Colore testo dei pulsanti categoria Colore testo dei pulsanti categoria attivi/mouseover Scegli fra le categorie più utilizzate Scegli l'icona Scegli lo stile Impostazioni colori Copiato! Copia link a questa domanda Personalizza i colori dei pulsanti categoria Personalizza i colori dell'icona per le Domande Frequenti (FAQ)  Personalizza i colori del pulsante con il link alla domanda Personalizza il colore dei pulsanti di navigazione Personalizza i colori del pulsante Cerca Modifica categoria Modifica domanda Colore di sfondo dell'icona FAQ Colore icona FAQ Colore del link alla domanda Colore del link alla domanda al passaggio del mouse Colore dell'icona del link alla domanda Colore dell'icona del link alla domanda al passaggio del mouse Numero di domande per pagina Dimensione icona (px) Se vuoto, saranno mostrate tutte le categorie Inserisci lo shortcode A sinistra Elenco Colore dei pulsanti di navigazione Colore dei pulsanti di navigazione attivi/mouseover Colore testo dei pulsanti di navigazione Colore testo dei pulsanti di navigazione attivi/mouseover Nuova categoria Nuova domanda Successivo Nessuna categoria Nessun risultato trovato Nessun risultato trovato Nessun risultato trovato nel Cestino Non mostrare Off/On Categoria genitore Categoria genitore: Documentazione del plugin Categorie più utilizzate FAQ FAQ Precedente A destra Colore del pulsante Cerca Colore del pulsante Cerca al passaggio del mouse Colore dell'icona del pulsante Cerca Colore dell'icona del pulsante Cerca al passaggio del mouse Cerca fra le categorie Cerca una domanda Cerca fra le categorie FAQ Separa gli articoli con la virgola Impostazioni Creazione shortcode Mostra filtri per categoria Mostra icona Mostra riquadro di ricerca Ci dispiace, non sono stati trovati che corrispondono alla tua ricerca. Categorie Categoria Toggle Aggiorna categoria Visualizza categoria Visualizza domanda Colori FAQ FAQ Shortcode 