��    X      �     �      �  	   �     �     �     �     �     �     �  
                  8     N  "   d     �  '   �  $   �     �     �     	     	     &	     A	     [	     o	     �	     �	     �	  
   �	     �	     �	     �	     �	     
     '
     A
     O
  .   ^
     �
     �
     �
     �
  $   �
     �
  )        ,     9     A     F  	   T  	   ^     h     {          �     �     �     �     �     �                    *     D     ]     |  
   �     �     �     �     �     �  	   �       +        C      R     s     �     �     �     �     �      �  #   �  %   !     G  w  \  	   �     �     �            %   3     Y     k     x     �     �     �  #   �       #     )   @     j     {     �     �  )   �     �     �  )     %   -     S  
   g     r     �     �     �     �     �     �     	       9   .     h     }     �     �  #   �     �  $   �       
        "     +     >     L     Z     v     z     �     �     �     �     �     �     �     �     �     �          (     D     V     _     v     �     �     �     �     �  0   �          0  	   =     G     N     a     u     �     �     �  	   �     �     J                          F   2   3             >                          K       A         X              Q   6   1   )   ,      V   E       @      B       M       %   0   ;   7               =   *   I       N      .                  (           C   L   $   S       G   D   T   :               '   8      -   
          9   +          "   !             W              <   H   &   P   ?             U             /   #       5                        	       O   4   R        Accordion Add FAQ shortcode Add New Add New Category Add New FAQ Add or remove categories All Categories Categories Categories list Categories list navigation Categories to display Category Button Color Category Button Hover/Active Color Category Button Text Color Category Button Text Hover/Active Color Choose from the most used categories Choose the icon Choose the style Copied! Copy FAQ Link Customize Category Buttons Customize FAQ Link Button Customize FAQ icons Customize Navigation Buttons Customize Search Button Edit Category Edit FAQ Enable FAQ FAQ Icon Background Color FAQ Icon Color FAQ Link Color FAQ Link Hover Color FAQ Link Icon Color FAQ Link Icon Hover Color FAQs per page Icon size (px) If left empty all categories will be displayed Insert Shortcode Left List Navigation Button Color Navigation Button Hover/Active Color Navigation Button Text Color Navigation Button Text Hover/Active Color New Category New FAQ Next No Categories Not Found Not found Not found in Trash Off Off/On Parent Category Parent Category: Plugin documentation Popular Categories Post Type General NameFAQs Post Type Singular NameFAQ Previous Right Search Button Color Search Button Hover Color Search Button Icon Color Search Button Icon Hover Color Search Categories Search FAQ Search FAQs Categories Separate items with commas Settings Shortcode Creation Show category filters Show icon Show search box Sorry, no results are matching your search. Style Settings Taxonomy General NameCategories Taxonomy Singular NameCategory Toggle Update Category View Category View FAQ metabox titleFAQ Settings plugin name in admin WP menuFAQ plugin name in admin page titleFAQ shortcode creation tab nameShortcode style tab nameStyle Project-Id-Version: YITH FAQ Plugin for WordPress
POT-Creation-Date: 2018-07-30 21:07+0200
PO-Revision-Date: 2018-07-30 21:22+0200
Language-Team: YIThemes <plugins@yithemes.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.13
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
Last-Translator: 
Language: nl
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Accordion Voeg FAQ shortcode toe Voeg nieuw toe Voeg nieuwe categorie toe Nieuwe FAQ toevoegen Categorieën toevoegen of verwijderen Alle categorieën Categorieën Categorieën lijst Categorie lijst navigatie Categorieën om weer te geven. Categorie tekstkleur knop Categorie knop hover /actieve kleur Categorie knop tekstkleur Categorie knop hover /actieve kleur Kies uit de meest gebruikte categorieën  Kies het symbool Kies de stijl Gekoopieërd FAQ Link kopieëren Pas de stijl aan van de categorie knoppen FAQ link knop aanpassen FAQ symbolen aanpassen Pas de stijl aan van de navigatie knoppen Pas de stijl aan van de zoek knop aan Categorie aanpassen Bewerk FAQ FAQ Inschakelen FAQ symbool achtergrondkleur FAQ symbool kleur FAQ link kleur FAQ link hover kleur FAQ link symbool kleur FAQ link symbool hover kleur FAQs per pagina Symbool grootte (px) Wanneer niet ingevuld  worden alle categorieën weergeven Voeg de shortcode in Links Lijst Tekstkleur navigatie knop Categorie knop hover /actieve kleur Tekstkleur navigatie knop Actieve tekstkleur op aanwijzer knop Nieuwe categorie Nieuwe FAQ Volgende Geen categorieën. Niet gevonden Niet gevonden Niet gevonden in prullenbak Uit Aan/uit Hoofd categorie Hoofd categorie: Plugin documentatie Populaire categorieën FAQs FAQ Vorige Rechts Kleur zoek knop Knop tekstkleur op aanwijzer Kleur icoon zoek knop Kleur hover icoon zoek knop Zoek categorieën Zoek FAQ Zoek FAQs categorieën Aparte items met komma's Instellingen Shortcode creatie Toon de categorie filters Symbool weergeven Weergeef zoek box Helaas, geen zoekresultaten voor uw zoekopdracht Stijl instellingen Categorieën Catogorie Toggle Werk categorie bij Categorie Weergeven FAQ weergeven FAQ Instellingen FAQ FAQ Shortcode Stijl 