#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH FAQ Plugin for WordPress\n"
"POT-Creation-Date: 2018-08-10 11:15+0200\n"
"PO-Revision-Date: 2016-02-08 17:21+0100\n"
"Last-Translator: \n"
"Language-Team: YIThemes <plugins@hitoutlets.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.9\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../class.yith-faq-plugin-for-wordpress.php:141
msgctxt "shortcode creation tab name"
msgid "Shortcode"
msgstr ""

#: ../class.yith-faq-plugin-for-wordpress.php:142
msgctxt "color tab name"
msgid "Color"
msgstr ""

#: ../class.yith-faq-plugin-for-wordpress.php:148
msgctxt "plugin name in admin page title"
msgid "FAQ"
msgstr ""

#: ../class.yith-faq-plugin-for-wordpress.php:149
msgctxt "plugin name in admin WP menu"
msgid "FAQ"
msgstr ""

#: ../class.yith-faq-plugin-for-wordpress.php:218
msgid "Settings"
msgstr ""

#: ../class.yith-faq-plugin-for-wordpress.php:242
msgid "Plugin documentation"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:109
msgctxt "Post Type General Name"
msgid "FAQs"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:110
msgctxt "Post Type Singular Name"
msgid "FAQ"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:111
msgid "Add New FAQ"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:112
msgid "Add New"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:113
msgid "New FAQ"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:114
msgid "Edit FAQ"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:115
msgid "View FAQ"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:116
#: ../includes/class-yith-faq-shortcode.php:369
msgid "Search FAQ"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:117
msgid "Not found"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:118
msgid "Not found in Trash"
msgstr ""

#: ../includes/class-yith-faq-post-type.php:174
msgid "Off/On"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:125
msgid "Shortcode Creation"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:130
msgid "Show search box"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:144
msgid "Show category filters"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:158
msgid "Choose the style"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:166
msgid "List"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:167
msgid "Accordion"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:168
msgid "Toggle"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:178
msgid "FAQs per page"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:197
msgid "Categories to display"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:206
msgid "Search FAQs Categories"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:213
msgid "If left empty all categories will be displayed"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:217
msgid "Show icon"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:225
msgid "Off"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:226
msgid "Left"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:227
msgid "Right"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:237
msgid "Icon size (px)"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:254
msgid "Choose the icon"
msgstr ""

#: ../includes/class-yith-faq-shortcode-panel.php:293
msgid "Insert Shortcode"
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:90
msgid "Add FAQ shortcode"
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:394
#: ../includes/class-yith-faq-taxonomy.php:71
msgid "All Categories"
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:407
msgid "Sorry, no matching results for your search."
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:434
msgid "Copy FAQ Link"
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:435
msgid "Copied!"
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:462
msgid "Previous"
msgstr ""

#: ../includes/class-yith-faq-shortcode.php:480
msgid "Next"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:68
msgctxt "Taxonomy General Name"
msgid "Categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:69
msgctxt "Taxonomy Singular Name"
msgid "Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:70
msgid "Categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:72
msgid "Parent Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:73
msgid "Parent Category:"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:74
msgid "New Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:75
msgid "Add New Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:76
msgid "Edit Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:77
msgid "Update Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:78
msgid "View Category"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:79
msgid "Separate items with commas"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:80
msgid "Add or remove categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:81
msgid "Choose from the most used categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:82
msgid "Popular Categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:83
msgid "Search Categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:84
msgid "Not Found"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:85
msgid "No Categories"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:86
msgid "Categories list"
msgstr ""

#: ../includes/class-yith-faq-taxonomy.php:87
msgid "Categories list navigation"
msgstr ""

#: ../plugin-options/color-options.php:22
msgid "Color Settings"
msgstr ""

#: ../plugin-options/color-options.php:34
msgid "Customize Search Button Colors"
msgstr ""

#: ../plugin-options/color-options.php:41
msgid "Search Button Color"
msgstr ""

#: ../plugin-options/color-options.php:52
msgid "Search Button Hover Color"
msgstr ""

#: ../plugin-options/color-options.php:63
msgid "Search Button Icon Color"
msgstr ""

#: ../plugin-options/color-options.php:74
msgid "Search Button Icon Hover Color"
msgstr ""

#: ../plugin-options/color-options.php:85
msgid "Customize Category Button Colors"
msgstr ""

#: ../plugin-options/color-options.php:92
msgid "Category Button Color"
msgstr ""

#: ../plugin-options/color-options.php:103
msgid "Category Button Hover/Active Color"
msgstr ""

#: ../plugin-options/color-options.php:114
msgid "Category Button Text Color"
msgstr ""

#: ../plugin-options/color-options.php:125
msgid "Category Button Text Hover/Active Color"
msgstr ""

#: ../plugin-options/color-options.php:136
msgid "Customize Navigation Button Colors"
msgstr ""

#: ../plugin-options/color-options.php:143
msgid "Navigation Button Color"
msgstr ""

#: ../plugin-options/color-options.php:154
msgid "Navigation Button Hover/Active Color"
msgstr ""

#: ../plugin-options/color-options.php:165
msgid "Navigation Button Text Color"
msgstr ""

#: ../plugin-options/color-options.php:176
msgid "Navigation Button Text Hover/Active Color"
msgstr ""

#: ../plugin-options/color-options.php:187
msgid "Customize FAQ Icon Colors "
msgstr ""

#: ../plugin-options/color-options.php:194
msgid "FAQ Icon Background Color"
msgstr ""

#: ../plugin-options/color-options.php:205
msgid "FAQ Icon Color"
msgstr ""

#: ../plugin-options/color-options.php:216
msgid "Customize FAQ Link Button Colors"
msgstr ""

#: ../plugin-options/color-options.php:223
msgid "FAQ Link Color"
msgstr ""

#: ../plugin-options/color-options.php:234
msgid "FAQ Link Hover Color"
msgstr ""

#: ../plugin-options/color-options.php:245
msgid "FAQ Link Icon Color"
msgstr ""

#: ../plugin-options/color-options.php:256
msgid "FAQ Link Icon Hover Color"
msgstr ""
