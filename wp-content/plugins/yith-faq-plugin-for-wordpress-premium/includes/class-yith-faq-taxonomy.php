<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_FAQ_Taxonomy' ) ) {


	/**
	 * Taxonomy class
	 *
	 * @class   YITH_FAQ_Taxonomy
	 * @since   1.0.0
	 * @author  Alberto Ruggiero
	 *
	 */
	class YITH_FAQ_Taxonomy {

		/**
		 * @var $post_type string post type name
		 */
		private $post_type = null;

		/**
		 * @var $taxonomy string taxonomy name
		 */
		private $taxonomy = null;

		/**
		 * Constructor
		 *
		 * @since   1.0.0
		 *
		 * @param   $taxonomy
		 * @param   $post_type
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function __construct( $post_type, $taxonomy ) {

			$this->post_type = $post_type;
			$this->taxonomy  = $taxonomy;

			add_action( 'init', array( $this, 'add_faq_taxonomy' ) );

		}

		/**
		 * Add faq taxonomy
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_faq_taxonomy() {

			$labels = array(
				'name'                       => _x( 'Categories', 'Taxonomy General Name', 'yith-faq-plugin-for-wordpress' ),
				'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'yith-faq-plugin-for-wordpress' ),
				'menu_name'                  => __( 'Categories', 'yith-faq-plugin-for-wordpress' ),
				'all_items'                  => __( 'All Categories', 'yith-faq-plugin-for-wordpress' ),
				'parent_item'                => __( 'Parent Category', 'yith-faq-plugin-for-wordpress' ),
				'parent_item_colon'          => __( 'Parent Category:', 'yith-faq-plugin-for-wordpress' ),
				'new_item_name'              => __( 'New Category', 'yith-faq-plugin-for-wordpress' ),
				'add_new_item'               => __( 'Add New Category', 'yith-faq-plugin-for-wordpress' ),
				'edit_item'                  => __( 'Edit Category', 'yith-faq-plugin-for-wordpress' ),
				'update_item'                => __( 'Update Category', 'yith-faq-plugin-for-wordpress' ),
				'view_item'                  => __( 'View Category', 'yith-faq-plugin-for-wordpress' ),
				'separate_items_with_commas' => __( 'Separate items with commas', 'yith-faq-plugin-for-wordpress' ),
				'add_or_remove_items'        => __( 'Add or remove categories', 'yith-faq-plugin-for-wordpress' ),
				'choose_from_most_used'      => __( 'Choose from the most used categories', 'yith-faq-plugin-for-wordpress' ),
				'popular_items'              => __( 'Popular Categories', 'yith-faq-plugin-for-wordpress' ),
				'search_items'               => __( 'Search Categories', 'yith-faq-plugin-for-wordpress' ),
				'not_found'                  => __( 'Not Found', 'yith-faq-plugin-for-wordpress' ),
				'no_terms'                   => __( 'No Categories', 'yith-faq-plugin-for-wordpress' ),
				'items_list'                 => __( 'Categories list', 'yith-faq-plugin-for-wordpress' ),
				'items_list_navigation'      => __( 'Categories list navigation', 'yith-faq-plugin-for-wordpress' ),
			);

			$args = array(
				'labels'            => $labels,
				'hierarchical'      => true,
				'public'            => true,
				'show_ui'           => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => false,
				'show_tagcloud'     => false,
			);

			register_taxonomy( $this->taxonomy, array( $this->post_type ), $args );

		}

	}

}




