<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Main class
 *
 * @class   YITH_WC_Product_Countdown_Premium
 * @package Yithemes
 * @since   1.0.0
 * @author  Your Inspiration Themes
 */

if ( ! class_exists( 'YITH_WC_Product_Countdown_Premium' ) ) {

	class YITH_WC_Product_Countdown_Premium extends YITH_WC_Product_Countdown {

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WC_Product_Countdown_Premium
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function __construct() {

			parent::__construct();

			// register plugin to licence/update system
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			$this->includes_premium();

			if ( is_admin() ) {

				add_action( 'woocommerce_admin_field_custom-radio', 'YWPC_Custom_Radio::output' );
				add_action( 'woocommerce_admin_field_custom-radio-topbar', 'YWPC_Custom_Radio_Topbar::output' );
				add_action( 'woocommerce_admin_field_custom-selector', 'YWPC_Custom_Select::output' );
				add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts_premium' ) );
				add_action( 'ywpc_bulk_operations', 'YWPC_Bulk_Operations::output' );
				add_action( 'wp_ajax_ywpc_json_search_product_categories', 'YWPC_Bulk_Operations::json_search_product_categories', 10 );
				add_action( 'manage_product_posts_custom_column', array( $this, 'render_ywpc_column' ), 3 );

				add_filter( 'manage_product_posts_columns', array( $this, 'add_ywpc_column' ), 11 );

			}

			if ( get_option( 'ywpc_enable_plugin' ) == 'yes' ) {

				add_action( 'widgets_init', array( $this, 'register_widget' ) );
				add_shortcode( 'ywpc_shortcode', array( $this, 'set_ywpc_shortcode' ) );

				if ( is_admin() ) {

					add_filter( 'ywpc_tab_class', array( $this, 'get_tab_class' ) );
					add_filter( 'ywpc_product_save_args', array( $this, 'save_countdown_tab_premium' ), 10, 2 );

					add_action( 'ywpc_countdown_tab_notice', array( $this, 'write_tab_options_notice' ) );
					add_action( 'ywpc_countdown_tab_options', array( $this, 'write_tab_options_premium' ) );
					add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'add_sale_options_product_variable' ), 10, 3 );
					add_action( 'woocommerce_save_product_variation', array( $this, 'save_sale_options_product_variable' ), 10, 2 );
					add_action( 'admin_action_ywpc_shortcodes_panel', array( $this, 'ywpc_add_shortcodes_panel' ) );
					add_action( 'admin_init', array( $this, 'ywpc_add_shortcodes_button' ) );
					add_action( 'media_buttons_context', array( &$this, 'ywpc_media_buttons_context' ) );

				} else {

					add_filter( 'ywpc_product_args', array( $this, 'get_product_args' ), 10, 3 );
					add_filter( 'ywpc_showing_position_product', array( $this, 'get_position_product' ) );
					add_filter( 'ywpc_timer_title', array( $this, 'get_timer_title' ), 10, 2 );
					add_filter( 'woocommerce_get_cart_item_from_session', array( $this, 'correct_quantity_product' ) );

					add_action( 'ywpc_single_product_page', array( $this, 'add_sales_bar_product' ), 15, 2 );
					add_action( 'ywpc_product_scripts', array( $this, 'add_script_product' ) );
					add_action( 'wp_enqueue_scripts', array( $this, 'get_custom_css' ), 11 );
					add_action( 'woocommerce_before_shop_loop_item', array( $this, 'check_show_ywpc_category' ) );
					add_action( 'ywpc_category_page', array( $this, 'add_timer_category' ), 10, 2 );
					add_action( 'ywpc_category_page', array( $this, 'add_sales_bar_category' ), 15, 2 );

					add_action( 'woocommerce_order_status_on-hold', array( $this, 'update_quantity_product_sold' ), 10, 2 );
					add_action( 'woocommerce_order_status_processing', array( $this, 'update_quantity_product_sold' ), 10, 2 );
					add_action( 'woocommerce_order_status_completed', array( $this, 'update_quantity_product_sold' ), 10, 2 );

					$end_sale = get_option( 'ywpc_end_sale' );

					if ( $end_sale == 'disable' ) {

						/** If on expired sale the product must be disabled */
						add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'avoid_add_to_cart' ), 10, 2 );

						add_action( 'wp_enqueue_scripts', array( $this, 'hide_add_to_cart_single' ), 15 );
						add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'hide_add_to_cart_loop' ), 5 );

					} elseif ( $end_sale == 'remove' ) {

						/** If on expired sale the product must be hidden */
						add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'avoid_add_to_cart' ), 10, 2 );
						add_filter( 'woocommerce_variation_is_visible', array( $this, 'hide_variations' ), 10, 2 );

						add_action( 'woocommerce_product_query', array( $this, 'hide_product_from_catalog' ), 10, 1 );
						add_action( 'woocommerce_shortcode_products_query', array( $this, 'hide_product_from_shortcodes' ), 10, 1 );
						add_action( 'template_redirect', array( $this, 'avoid_direct_access' ) );

					}

					add_action( 'init', array( $this, 'initialize_topbar' ) );

				}

			}

		}

		/**
		 * Include required core files
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function includes_premium() {

			require_once( 'includes/class-ywpc-widget.php' );

			if ( is_admin() ) {

				include_once( 'templates/admin/custom-select.php' );
				include_once( 'templates/admin/custom-radio.php' );
				include_once( 'templates/admin/custom-radio-topbar.php' );
				include_once( 'templates/admin/bulk-operations.php' );

			}

		}

		/**
		 * Register YWPC Widget
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function register_widget() {
			register_widget( 'YWPC_Widget' );
		}

		/**
		 * ADMIN FUNCTIONS
		 */

		/**
		 * Add the countdown column
		 *
		 * @since   1.0.0
		 *
		 * @param   $columns
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function add_ywpc_column( $columns ) {

			$columns['ywpc_status'] = __( 'Countdown', 'yith-woocommerce-product-countdown' );

			return $columns;

		}

		/**
		 * Render the order fraud risk column
		 *
		 * @since   1.0.0
		 *
		 * @param   $column
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function render_ywpc_column( $column ) {

			global $post;

			if ( 'ywpc_status' == $column ) {

				$args = $this->get_product_args( '', $post->ID );


				if ( empty( $args['items'] ) ) {
					return;
				}

				$extra_class = ( $args['class'] ) ? $args['class'] . '-' : '';

				foreach ( $args['items'] as $id => $item ) {

					if ( isset( $args['active_var'] ) && $args['active_var'] != $id ) {
						continue;
					}

					?>

					<?php if ( isset( $item['end_date'] ) ) : ?>

						<?php $date = ywpc_get_countdown( $item['end_date'] ); ?>

                        <div class="ywpc-countdown-admin ywpc-item-<?php echo $extra_class . $id; ?>">
                            <span class="ywpc-days">
                                <?php $days = ( ( is_rtl() ) ? strrev( $date['dd'] ) : $date['dd'] ); ?>
                                <span class="ywpc-char-0"><?php echo substr( $days, 0, 1 ); ?></span><span class="ywpc-char-1"><?php echo substr( $days, 1, 1 ); ?></span><span class="ywpc-char-2"><?php echo substr( $days, 2, 1 ); ?></span>
	                            <?php echo _nx( 'd', 'dd', $date['dd'], 'Abbreviation for Days', 'yith-woocommerce-product-countdown' ); ?>
                            </span>
                            <span class="ywpc-hours">
                                <?php $hours = ( ( is_rtl() ) ? strrev( $date['hh'] ) : $date['hh'] ); ?>
                                <span class="ywpc-char-1"><?php echo substr( $hours, 0, 1 ); ?></span><span class="ywpc-char-2"><?php echo substr( $hours, 1, 1 ); ?></span>
								<?php echo _nx( 'h', 'hh', $date['hh'], 'Abbreviation for Hours', 'yith-woocommerce-product-countdown' ); ?>
                            </span>
                            <span class="ywpc-minutes">
                                <?php $minutes = ( ( is_rtl() ) ? strrev( $date['mm'] ) : $date['mm'] ); ?>
                                <span class="ywpc-char-1"><?php echo substr( $minutes, 0, 1 ); ?></span><span class="ywpc-char-2"><?php echo substr( $minutes, 1, 1 ); ?></span>
								<?php echo _nx( 'm', 'mm', $date['mm'], 'Abbreviation for Minutes', 'yith-woocommerce-product-countdown' ); ?>
                            </span>
                            <span class="ywpc-seconds">
                                <?php $seconds = ( ( is_rtl() ) ? strrev( $date['ss'] ) : $date['ss'] ); ?>
                                <span class="ywpc-char-1"><?php echo substr( $seconds, 0, 1 ); ?></span><span class="ywpc-char-2"><?php echo substr( $seconds, 1, 1 ); ?></span>
								<?php echo _nx( 's', 'ss', $date['ss'], 'Abbreviation for Seconds', 'yith-woocommerce-product-countdown' ); ?>
                            </span>
                            <input type="hidden" value="<?php echo( date( 'Y', $date['to'] ) ) ?>.<?php echo( date( 'm', $date['to'] ) - 1 ) ?>.<?php echo( date( 'd', $date['to'] ) ) ?>.<?php echo( date( 'H', $date['to'] ) ) ?>.<?php echo( date( 'i', $date['to'] ) ) ?>">

                        </div>

					<?php endif; ?>

					<?php if ( isset( $item['sold_qty'] ) && isset( $item['discount_qty'] ) ): ?>

                        <div class="ywpc-label">
							<?php
							if ( ! is_rtl() ) {
								printf( __( '%d/%d Sold', 'yith-woocommerce-product-countdown' ), $item['sold_qty'], $item['discount_qty'] );
							} else {
								printf( __( '%d/%d Sold', 'yith-woocommerce-product-countdown' ), $item['discount_qty'], $item['sold_qty'] );
							}
							?>
                        </div>
					<?php endif; ?>

					<?php
				}

			}

		}

		/**
		 * Get shortcode panel
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywpc_add_shortcodes_panel() {
			global $name_tab;
			include( YWPC_TEMPLATE_PATH . '/admin/lightbox.php' );
		}

		/**
		 * Add shortcode button to TinyMCE editor, adding filter on mce_external_plugins
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywpc_add_shortcodes_button() {

			if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {

				return;

			}

			if ( get_user_option( 'rich_editing' ) == 'true' ) {

				add_filter( 'mce_external_plugins', array( &$this, 'ywpc_add_shortcodes_tinymce_plugin' ) );
				add_filter( 'mce_buttons', array( &$this, 'ywpc_register_shortcodes_button' ) );

			}

		}

		/**
		 * Add a script to TinyMCE script list
		 *
		 * @since   1.0.0
		 *
		 * @param   $plugin_array
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function ywpc_add_shortcodes_tinymce_plugin( $plugin_array ) {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			$plugin_array['ywpc_shortcode'] = YWPC_ASSETS_URL . '/js/ywpc-tinymce' . $suffix . '.js';

			return $plugin_array;

		}

		/**
		 * Make TinyMCE know a new button was included in its toolbar
		 *
		 * @since   1.0.0
		 *
		 * @param   $buttons
		 *
		 * @return  array()
		 * @author  Alberto Ruggiero
		 */
		public function ywpc_register_shortcodes_button( $buttons ) {

			array_push( $buttons, "|", "ywpc_shortcode" );

			return $buttons;

		}

		/**
		 * The markup of shortcode
		 *
		 * @since   1.0.0
		 *
		 * @param   $context
		 *
		 * @return  mixed
		 * @author  Alberto Ruggiero
		 */
		public function ywpc_media_buttons_context( $context ) {

			ob_start();

			?>

            <a id="ywpc_shortcode" style="display:none" href="#" class="hide-if-no-js" title="<?php _e( 'Add YITH WooCommerce Product Countdown shortcode', 'yith-woocommerce-product-countdown' ) ?>"></a>

			<?php

			$out = ob_get_clean();

			return $context . $out;

		}

		/**
		 * Enqueue admin script files
		 *
		 * @since   1.0.0
		 *
		 * @param   $hook
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function admin_scripts_premium( $hook ) {

			$screen = get_current_screen();
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			if ( $hook == 'widgets.php' ) {

				wp_enqueue_script( 'select2', WC()->plugin_url() . '/assets/js/select2/select2' . $suffix . '.js', array( 'jquery' ), '3.5.2' );
				if ( version_compare( WC()->version, '3.2.0', '<' ) ) {
					wp_enqueue_script( 'select2', WC()->plugin_url() . '/assets/js/select2/select2' . $suffix . '.js', array( 'jquery' ) );
					wp_enqueue_script( 'wc-enhanced-select', WC()->plugin_url() . '/assets/js/admin/wc-enhanced-select' . $suffix . '.js', array( 'jquery', 'select2' ), WC_VERSION );
				} else {
					wp_register_script( 'selectWoo', WC()->plugin_url() . '/assets/js/selectWoo/selectWoo.full' . $suffix . '.js', array( 'jquery' ) );
					wp_enqueue_script( 'wc-enhanced-select', WC()->plugin_url() . '/assets/js/admin/wc-enhanced-select' . $suffix . '.js', array( 'jquery', 'selectWoo' ), WC_VERSION );
				}
				wp_enqueue_script( 'ywpc-widget-panel-premium', YWPC_ASSETS_URL . '/js/ywpc-widget-panel-premium' . $suffix . '.js', array(
					'jquery',
					'wc-enhanced-select'
				), YWPC_VERSION, true );

			}

			if ( in_array( $screen->id, array( 'product', 'edit-product' ) ) ) {
				wp_enqueue_script( 'jquery-plugin', YWPC_ASSETS_URL . '/js/jquery.plugin' . $suffix . '.js', array( 'jquery' ) );
				wp_enqueue_script( 'jquery-countdown', YWPC_ASSETS_URL . '/js/jquery.countdown' . $suffix . '.js', array( 'jquery' ), '2.0.2' );
				wp_enqueue_script( 'ywpc-admin-premium', YWPC_ASSETS_URL . '/js/ywpc-admin-premium' . $suffix . '.js', array(
					'jquery',
					'jquery-ui-datetimepicker'
				), YWPC_VERSION, true );

				$js_vars = array(
					'gmt'    => get_option( 'gmt_offset' ),
					'is_rtl' => is_rtl(),
				);

				wp_localize_script( 'ywpc-admin-premium', 'ywpc_footer', $js_vars );
			}

			if ( $screen->id == 'yith-plugins_page_yith-wc-product-countdown' ) {

				$template        = get_option( 'ywpc_template', '1' );
				$template_topbar = get_option( 'ywpc_topbar_template', '1' );

				wp_enqueue_style( 'ywpc-google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:400,700' );
				wp_enqueue_style( 'ywpc-frontend', YWPC_ASSETS_URL . '/css/ywpc-style-' . $template . '.css' );
				wp_enqueue_style( 'ywpc-frontend-topbar', YWPC_ASSETS_URL . '/css/ywpc-bar-style-' . $template_topbar . '.css' );
				wp_enqueue_script( 'ywpc-admin-panel-footer', YWPC_ASSETS_URL . '/js/ywpc-admin-panel-premium' . $suffix . '.js', array( 'woocommerce_settings' ), YWPC_VERSION, true );
				wp_enqueue_script( 'jquery-ui-datetimepicker', YWPC_ASSETS_URL . '/js/timepicker' . $suffix . '.js', array( 'jquery' ), YWPC_VERSION, true );
				wp_enqueue_style( 'jquery-ui-datetimepicker-style', YWPC_ASSETS_URL . '/css/timepicker.css' );

				wp_enqueue_script( 'ywpc-admin-premium', YWPC_ASSETS_URL . '/js/ywpc-admin-premium' . $suffix . '.js', array(
					'jquery',
					'jquery-ui-datetimepicker'
				), YWPC_VERSION, true );

				$js_vars = array(
					'gmt'    => get_option( 'gmt_offset' ),
					'is_rtl' => is_rtl(),
				);

				wp_localize_script( 'ywpc-admin-premium', 'ywpc_footer', $js_vars );
			}

			wp_enqueue_script( 'ywpc-shortcode', YWPC_ASSETS_URL . '/js/ywpc-shortcode' . $suffix . '.js', array( 'jquery' ), YWPC_VERSION );

			global $post_ID, $temp_ID;

			$query_args = array(
				'action'    => 'ywpc_shortcodes_panel',
				'post_id'   => (int) ( 0 == $post_ID ? $temp_ID : $post_ID ),
				'KeepThis'  => true,
				'TB_iframe' => true
			);

			wp_localize_script( 'ywpc-shortcode', 'ywpc_shortcode', array(
				'lightbox_url'   => add_query_arg( $query_args, admin_url( 'admin.php' ) ),
				'lightbox_title' => __( 'Add YITH WooCommerce Product Countdown shortcode', 'yith-woocommerce-product-countdown' ),

			) );

		}

		/**
		 * Get css class for countdown tab
		 *
		 * @since   1.0.0
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_tab_class() {

			return 'hide_if_grouped hide_if_external';

		}

		/**
		 * Add variable product notice in countdown tab
		 *
		 * @since   1.0.0
		 *
		 * @param   $product
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function write_tab_options_notice( $product ) {

			if ( $product->is_type( 'variable' ) ) {

				woocommerce_wp_checkbox(
					array(
						'id'          => '_ywpc_variations_global_countdown',
						'label'       => __( 'General countdown', 'yith-woocommerce-product-countdown' ),
						'description' => __( 'Set a general countdown for all the variations rather than for each single variation', 'yith-woocommerce-product-countdown' ),
					)
				);

			}

		}

		/**
		 * Add option in product edit tab
		 *
		 * @since   1.0.0
		 *
		 * @param   $product
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function write_tab_options_premium( $product ) {

			woocommerce_wp_text_input(
				array(
					'id'                => '_ywpc_discount_qty',
					'label'             => __( 'Discounted products', 'yith-woocommerce-product-countdown' ),
					'placeholder'       => '',
					'desc_tip'          => 'true',
					'description'       => __( 'The number of discounted products.', 'yith-woocommerce-product-countdown' ),
					'default'           => '0',
					'type'              => 'number',
					'custom_attributes' => array(
						'step' => 'any',
						'min'  => '0'
					)
				)
			);
			woocommerce_wp_text_input(
				array(
					'id'                => '_ywpc_sold_qty',
					'label'             => __( 'Already sold products', 'yith-woocommerce-product-countdown' ),
					'placeholder'       => '',
					'desc_tip'          => 'true',
					'description'       => __( 'The number of already sold products.', 'yith-woocommerce-product-countdown' ),
					'type'              => 'number',
					'custom_attributes' => array(
						'step' => 'any',
						'min'  => '0'
					)
				)
			);


			if ( $product->is_type( 'variable' ) ) {

				?>

                <script type="text/javascript">

                    jQuery(function ($) {

                        $(window).load(function () {

                            $('#_ywpc_discount_qty').change(function () {

                                if (!$('#_ywpc_variations_global_countdown').is(':checked')) {

                                    $('.ywpc_discount_qty').val($(this).val());
                                    $('.woocommerce_variation').addClass('variation-needs-update');

                                }

                            });

                            $('#_ywpc_sold_qty').change(function () {

                                if (!$('#_ywpc_variations_global_countdown').is(':checked')) {

                                    $('.ywpc_sold_qty').val($(this).val());
                                    $('.woocommerce_variation').addClass('variation-needs-update');

                                }

                            });


                        });

                    });

                </script>

				<?php

			}

		}

		/**
		 * Save sales countdown tab options
		 *
		 * @since   1.0.0
		 *
		 * @param   $args
		 * @param   $product
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function save_countdown_tab_premium( $args, $product ) {

			$is_pre_order = isset( $_POST['_ywpo_preorder'] ) && ! is_array( $_POST['_ywpo_preorder'] ) ? 'yes' : 'no';

			if ( $is_pre_order == 'yes' ) {

				$new_sale_date = isset( $_POST['_ywpo_for_sale_date'] ) ? wc_clean( $_POST['_ywpo_for_sale_date'] ) : '';

				if ( ! empty( $new_sale_date ) ) {

					$new_sale_date                       = str_replace( '/', '-', $new_sale_date );
					$new_sale_date                       = $new_sale_date . ':00';
					$args['_ywpc_sale_price_dates_from'] = strtotime( date( 'Y-m-d' ) );
					$args['_ywpc_sale_price_dates_to']   = strtotime( $new_sale_date );
					$args['_ywpc_enabled']               = 'yes';

				}


			}

			$wc_sold_qty     = isset( $_POST['_ywpc_sold_qty'] ) ? $_POST['_ywpc_sold_qty'] : 0;
			$wc_stock_qty    = isset( $_POST['_stock'] ) ? $_POST['_stock'] : 0;
			$wc_manage_stock = isset( $_POST['_manage_stock'] ) ? $_POST['_manage_stock'] : 'no';
			$wc_discount_qty = isset( $_POST['_ywpc_discount_qty'] ) ? $_POST['_ywpc_discount_qty'] : 0;

			if ( $wc_manage_stock == 'yes' ) {

				switch ( true ) {

					case ( $wc_stock_qty < 1 ):
						$wc_discount_qty = 0;

						break;

					case ( $wc_discount_qty > $wc_stock_qty ):
						$wc_discount_qty = $wc_stock_qty;

						break;

				}

			}

			if ( $product->is_type( 'variable' ) ) {

				$override_variations                       = isset( $_POST['_ywpc_variations_global_countdown'] ) ? 'yes' : 'no';
				$args['_ywpc_variations_global_countdown'] = $override_variations;

				if ( $override_variations != 'yes' ) {

					$args['_ywpc_sold_qty']              = 0;
					$args['_ywpc_discount_qty']          = 0;
					$args['_ywpc_sale_price_dates_from'] = '';
					$args['_ywpc_sale_price_dates_to']   = '';

				}

			}

			$args['_ywpc_sold_qty']     = esc_attr( $wc_sold_qty );
			$args['_ywpc_discount_qty'] = esc_attr( $wc_discount_qty );

			return $args;

		}

		/**
		 * Add sales options to product variable
		 *
		 * @since   1.0.0
		 *
		 * @param   $loop
		 * @param   $variation_data
		 * @param   $variation
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_sale_options_product_variable( $loop, $variation_data, $variation ) {

			$variation_object      = wc_get_product( $variation->ID );
			$sale_price_dates_from = ( $date = yit_get_prop( $variation_object, '_ywpc_sale_price_dates_from' ) ) ? date_i18n( 'Y-m-d H:i', $date ) : '';
			$sale_price_dates_to   = ( $date = yit_get_prop( $variation_object, '_ywpc_sale_price_dates_to' ) ) ? date_i18n( 'Y-m-d H:i', $date ) : '';
			$discount_qty          = ( $dq = yit_get_prop( $variation_object, '_ywpc_discount_qty' ) ) ? $dq : '';
			$sold_qty              = ( $sq = yit_get_prop( $variation_object, '_ywpc_sold_qty' ) ) ? $sq : '';

			?>
            <div class="ywpc-dates">
                <p class="form-row form-row-first">
                    <label><?php _e( 'Countdown start date', 'yith-woocommerce-product-countdown' ); ?></label>
                    <input type="text" class="ywpc_sale_price_dates_from ywpc-variation-field" name="_ywpc_sale_price_dates_from_var[<?php echo $loop; ?>]" value="<?php echo $sale_price_dates_from; ?>" placeholder="<?php echo esc_attr_x( 'From&hellip;', 'placeholder', 'yith-woocommerce-product-countdown' ) ?> YYYY-MM-DD hh:mm" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01]) (([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])" />
                </p>

                <p class="form-row form-row-last">
                    <label><?php _e( 'Countdown end date', 'yith-woocommerce-product-countdown' ); ?></label>
                    <input type="text" class="ywpc_sale_price_dates_to ywpc-variation-field" name="_ywpc_sale_price_dates_to_var[<?php echo $loop; ?>]" value="<?php echo $sale_price_dates_to; ?>" placeholder="<?php echo esc_attr_x( 'To&hellip;', 'placeholder', 'yith-woocommerce-product-countdown' ) ?> YYYY-MM-DD hh:mm" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01]) (([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])" />
                </p>
            </div>
            <div class="ywpc-sales">
                <p class="form-row form-row-first">
                    <label><?php _e( 'Discounted products', 'yith-woocommerce-product-countdown' ); ?><?php echo wc_help_tip( __( 'The number of discounted products.', 'yith-woocommerce-product-countdown' ) ); ?></label>
                    <input type="number" class="ywpc_discount_qty ywpc-variation-field" name="_ywpc_discount_qty_var[<?php echo $loop; ?>]" value="<?php echo $discount_qty; ?>" step="any" min="0" />
                </p>

                <p class="form-row form-row-last">
                    <label><?php _e( 'Already sold products', 'yith-woocommerce-product-countdown' ); ?><?php echo wc_help_tip( __( 'Already sold products.', 'yith-woocommerce-product-countdown' ) ); ?></label>
                    <input type="number" class="ywpc_sold_qty ywpc-variation-field" name="_ywpc_sold_qty_var[<?php echo $loop; ?>]" value="<?php echo $sold_qty; ?>" step="any" min="0" />
                </p>
            </div>
			<?php

		}

		/**
		 * Save sales options of product variations
		 *
		 * @since   1.0.0
		 *
		 * @param   $variation_id
		 * @param   $loop
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function save_sale_options_product_variable( $variation_id, $loop ) {

			$variation_object    = wc_get_product( $variation_id );
			$is_pre_order        = isset( $_POST['_ywpo_preorder'][ $loop ] ) ? 'yes' : 'no';
			$override_variations = isset( $_POST['_ywpc_variations_global_countdown'] ) ? 'yes' : 'no';

			if ( $is_pre_order == 'yes' ) {
				$override_variations = 'no';
			}

			if ( $override_variations == 'yes' ) {

				$args = array(
					'_ywpc_sold_qty'              => 0,
					'_ywpc_discount_qty'          => 0,
					'_ywpc_sale_price_dates_from' => '',
					'_ywpc_sale_price_dates_to'   => ''
				);

			} else {

				$wc_start_date   = isset( $_POST['_ywpc_sale_price_dates_from_var'][ $loop ] ) ? $_POST['_ywpc_sale_price_dates_from_var'][ $loop ] : '';
				$wc_end_date     = isset( $_POST['_ywpc_sale_price_dates_to_var'][ $loop ] ) ? $_POST['_ywpc_sale_price_dates_to_var'][ $loop ] : '';
				$wc_sale_qty     = isset( $_POST['_ywpc_sold_qty_var'][ $loop ] ) ? $_POST['_ywpc_sold_qty_var'][ $loop ] : 0;
				$wc_discount_qty = isset( $_POST['_ywpc_discount_qty_var'][ $loop ] ) ? $_POST['_ywpc_discount_qty_var'][ $loop ] : 0;
				$wc_stock_qty    = isset( $_POST['variable_stock'][ $loop ] ) ? $_POST['variable_stock'][ $loop ] : 0;
				$wc_manage_stock = isset( $_POST['variable_manage_stock'][ $loop ] ) ? $_POST['variable_manage_stock'][ $loop ] : 'off';

				if ( $wc_manage_stock == 'on' ) {

					switch ( true ) {

						case ( $wc_stock_qty < 1 ):
							$wc_discount_qty = 0;

							break;

						case ( $wc_discount_qty > $wc_stock_qty ):
							$wc_discount_qty = $wc_stock_qty;

							break;

					}

				}

				if ( $wc_end_date && ! $wc_start_date ) {
					$wc_start_date = date( 'Y-m-d' );
				}

				if ( $is_pre_order == 'yes' ) {

					$wc_end_date = isset( $_POST['_ywpo_for_sale_date'][ $loop ] ) ? wc_clean( $_POST['_ywpo_for_sale_date'][ $loop ] ) : '';

					if ( ! empty( $wc_end_date ) ) {

						$wc_end_date   = str_replace( '/', '-', $wc_end_date );
						$wc_end_date   = $wc_end_date . ':00';
						$wc_start_date = date( 'Y-m-d' );

					}

				}

				$args = array(
					'_ywpc_sale_price_dates_from' => $wc_start_date ? strtotime( $wc_start_date ) : '',
					'_ywpc_sale_price_dates_to'   => $wc_end_date ? strtotime( $wc_end_date ) : '',
					'_ywpc_sold_qty'              => $wc_sale_qty,
					'_ywpc_discount_qty'          => $wc_discount_qty,
					'_ywpo_variation'             => $is_pre_order == 'yes' ? 'yes' : 'no',
				);

			}

			yit_save_prop( $variation_object, $args );

		}

		/**
		 * FRONTEND FUNCTIONS
		 */

		/**
		 * Check and correct quantity sold
		 *
		 * @since   1.0.0
		 *
		 * @param   $order_id
		 * @param   $order
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function update_quantity_product_sold( $order_id, $order ) {

			$processed = yit_get_prop( $order, '_ywpc_processed' );

			if ( $processed != 'yes' ) {

				$items = $order->get_items();

				foreach ( $items as $item ) {

					$product_id = $item['product_id'];

					global $sitepress;
					$has_wpml = ! empty( $sitepress ) ? true : false;

					if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
						$product_id = yit_wpml_object_id( $product_id, 'product', true, wpml_get_default_language() );
					}

					$product  = wc_get_product( $product_id );
					$has_ywpc = yit_get_prop( $product, '_ywpc_enabled' );

					if ( $has_ywpc == 'yes' ) {

						$time_from    = yit_get_prop( $product, '_ywpc_sale_price_dates_from' );
						$current_time = strtotime( current_time( "Y-m-d G:i:s" ) );

						if ( $time_from > $current_time ) {
							continue;
						}

						$variation_global = yit_get_prop( $product, '_ywpc_variations_global_countdown' );

						if ( $item['variation_id'] == 0 || $variation_global == 'yes' ) {

							$sold_qty = yit_get_prop( $product, '_ywpc_sold_qty' );
							$sold_qty += $item['qty'];
							yit_save_prop( $product, '_ywpc_sold_qty', esc_attr( $sold_qty ) );

						} else {

							$product_id = $item['variation_id'];

							global $sitepress;
							$has_wpml = ! empty( $sitepress ) ? true : false;

							if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
								$product_id = yit_wpml_object_id( $product_id, 'product', true, wpml_get_default_language() );
							}

							$variation = wc_get_product( $product_id );
							$sold_qty  = yit_get_prop( $variation, '_ywpc_sold_qty' );
							$sold_qty  += $item['qty'];
							yit_save_prop( $product, '_ywpc_sold_qty', esc_attr( $sold_qty ) );

						}

					}

				}

				yit_save_prop( $order, '_ywpc_processed', 'yes' );

			}

		}

		/**
		 * Check and correct quantity in sale
		 *
		 * @since   1.0.0
		 *
		 * @param   $data
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function correct_quantity_product( $data ) {

			if ( isset( $data['product_id'] ) ) {

				$product      = wc_get_product( $data['product_id'] );
				$discount_qty = yit_get_prop( $product, '_ywpc_discount_qty' );
				$sold_qty     = yit_get_prop( $product, '_ywpc_sold_qty' );
				$sold_qty     = $sold_qty ? $sold_qty : 0;

				if ( $discount_qty ) {

					$available_qty = $discount_qty - $sold_qty;

					if ( $available_qty > 0 && $available_qty < $data['quantity'] ) {

						$data['quantity'] = $available_qty;

					}

				}

			}

			return $data;

		}

		/**
		 * Get args for timer and sale bar
		 *
		 * @since   1.0.0
		 *
		 * @param   $value
		 * @param   $prod_id
		 * @param   $extra_class
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function get_product_args( $value, $prod_id = '', $extra_class = '' ) {

			global $post;

			$result = array(
				'items' => array(),
				'class' => $extra_class
			);

			$product_id = ( $prod_id != '' ) ? $prod_id : $post->ID;

			global $sitepress;
			$has_wpml = ! empty( $sitepress ) ? true : false;

			if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
				$product_id = yit_wpml_object_id( $product_id, 'product', true, wpml_get_default_language() );
			}

			$product          = wc_get_product( $product_id );
			$has_ywpc         = yit_get_prop( $product, '_ywpc_enabled', true );
			$current_time     = strtotime( current_time( "Y-m-d H:i:s" ) );
			$before_sale      = get_option( 'ywpc_before_sale_start' );
			$end_sale         = get_option( 'ywpc_end_sale' );
			$end_sale_summary = get_option( 'ywpc_end_sale_summary' );

			if ( $has_ywpc == 'yes' ) {

				$variation_global = yit_get_prop( $product, '_ywpc_variations_global_countdown' );

				if ( ( ! $product->is_type( 'variable' ) ) || ( $product->is_type( 'variable' ) && $variation_global == 'yes' ) ) {

					$stock_status = yit_get_prop( $product, '_stock_status' );
					$is_preorder  = yit_get_prop( $product, '_ywpo_preorder' );

					if ( ( $stock_status != 'outofstock' ) || ( $stock_status == 'outofstock' && $is_preorder == 'yes' ) ) {

						$expired = false;

						$sale_start   = yit_get_prop( $product, '_ywpc_sale_price_dates_from' );
						$sale_end     = yit_get_prop( $product, '_ywpc_sale_price_dates_to' );
						$discount_qty = yit_get_prop( $product, '_ywpc_discount_qty' );
						$sold_qty     = yit_get_prop( $product, '_ywpc_sold_qty' );

						$result['items'][ $product_id ]['show_bar'] = 'hide';
						$result['items'][ $product_id ]['expired']  = 'valid';

						if ( empty( $sold_qty ) ) {
							$sold_qty = 0;
						}

						if ( $sold_qty < $discount_qty ) {

							$result['items'][ $product_id ]['show_bar']     = 'show';
							$result['items'][ $product_id ]['sold_qty']     = $sold_qty;
							$result['items'][ $product_id ]['discount_qty'] = $discount_qty;
							$result['items'][ $product_id ]['percent']      = intval( $sold_qty / $discount_qty * 100 );

						} else {

							if ( $sold_qty != 0 && $discount_qty != 0 ) {

								$result['items'][ $product_id ]['show_bar'] = 'hide';

								$expired = true;

							}

						}

						if ( ! empty( $sale_end ) && ! empty( $sale_start ) ) {

							if ( $current_time < $sale_start && $before_sale == 'yes' ) {

								$result['items'][ $product_id ]['before']   = true;
								$result['items'][ $product_id ]['end_date'] = $sale_start;

							} elseif ( $current_time >= $sale_start && $current_time <= $sale_end ) {

								$result['items'][ $product_id ]['before']   = false;
								$result['items'][ $product_id ]['end_date'] = $sale_end;

							} elseif ( $current_time > $sale_end ) {

								$expired = true;

							}

						}

						if ( $expired ) {

							$result['items'][ $product_id ]['show_bar'] = 'hide';

							if ( $end_sale == 'disable' && $end_sale_summary == 'yes' && $discount_qty > 0 ) {

								$result['items'][ $product_id ]['show_bar']     = 'show';
								$result['items'][ $product_id ]['sold_qty']     = $sold_qty;
								$result['items'][ $product_id ]['discount_qty'] = $discount_qty;
								$result['items'][ $product_id ]['percent']      = intval( $sold_qty / $discount_qty * 100 );

							}

							$result['items'][ $product_id ]['expired'] = 'expired';

							do_action( 'ywpc_countdown_expiration', $product, $product_id );

						}

					}

				} else {
					$product_variables = $product->get_available_variations();

					if ( count( array_filter( $product_variables ) ) > 0 ) {
						$product_variables    = array_filter( $product_variables );
						$result['active_var'] = 0;
						$result['variable']   = true;
						$default_atts         = yit_get_prop( $product, '_default_attributes' );
						$variation_points     = array();

						foreach ( $product_variables as $product_variable ) {

							$variation    = wc_get_product( $product_variable['variation_id'] );
							$stock_status = yit_get_prop( $variation, '_stock_status' );
							$is_preorder  = yit_get_prop( $variation, '_ywpo_preorder' );

							if ( ( $stock_status != 'outofstock' ) || ( $stock_status == 'outofstock' && $is_preorder == 'yes' ) ) {

								$expired = false;

								$sale_start   = yit_get_prop( $variation, '_ywpc_sale_price_dates_from' );
								$sale_end     = yit_get_prop( $variation, '_ywpc_sale_price_dates_to' );
								$discount_qty = yit_get_prop( $variation, '_ywpc_discount_qty' );
								$sold_qty     = yit_get_prop( $variation, '_ywpc_sold_qty' );

								$discount_qty = ( empty( $discount_qty ) ? 0 : $discount_qty );
								$sold_qty     = ( empty( $sold_qty ) ? 0 : $sold_qty );

								$result['items'][ $product_variable['variation_id'] ]['show_bar'] = 'hide';
								$result['items'][ $product_variable['variation_id'] ]['expired']  = 'valid';

								if ( $sold_qty < $discount_qty ) {

									$result['items'][ $product_variable['variation_id'] ]['show_bar']     = 'show';
									$result['items'][ $product_variable['variation_id'] ]['sold_qty']     = $sold_qty;
									$result['items'][ $product_variable['variation_id'] ]['discount_qty'] = $discount_qty;
									$result['items'][ $product_variable['variation_id'] ]['percent']      = ( $sold_qty && $discount_qty ) ? intval( $sold_qty / $discount_qty * 100 ) : 0;

								} else {

									if ( $sold_qty != 0 && $discount_qty != 0 ) {

										$result['items'][ $product_variable['variation_id'] ]['show_bar'] = 'hide';

										$expired = true;

									}

								}

								if ( ! empty( $sale_end ) && ! empty( $sale_start ) ) {

									if ( $current_time < $sale_start && $before_sale == 'yes' ) {

										$result['items'][ $product_variable['variation_id'] ]['before']   = true;
										$result['items'][ $product_variable['variation_id'] ]['end_date'] = $sale_start;

									} elseif ( $current_time >= $sale_start && $current_time <= $sale_end ) {

										$result['items'][ $product_variable['variation_id'] ]['before']   = false;
										$result['items'][ $product_variable['variation_id'] ]['end_date'] = $sale_end;

									} elseif ( $current_time > $sale_end ) {

										$expired = true;

									}

								}

								if ( $expired ) {

									$result['items'][ $product_variable['variation_id'] ]['show_bar'] = 'hide';

									if ( $end_sale == 'disable' && $end_sale_summary == 'yes' ) {

										$result['items'][ $product_variable['variation_id'] ]['show_bar']     = 'show';
										$result['items'][ $product_variable['variation_id'] ]['sold_qty']     = $sold_qty;
										$result['items'][ $product_variable['variation_id'] ]['discount_qty'] = $discount_qty;
										$result['items'][ $product_variable['variation_id'] ]['percent']      = ( $sold_qty && $discount_qty ) ? intval( $sold_qty / $discount_qty * 100 ) : 0;

									}

									$result['items'][ $product_variable['variation_id'] ]['expired'] = 'expired';

									do_action( 'ywpc_countdown_expiration', $product, $product_variable['variation_id'] );

								}

								if ( $default_atts ) {

									foreach ( $default_atts as $key => $val ) {

										$variation_points[ $product_variable['variation_id'] ] = 0;

										if ( isset( $product_variable['attributes'][ 'attribute_' . $key ] ) && $product_variable['attributes'][ 'attribute_' . $key ] != '' ) {

											if ( $product_variable['attributes'][ 'attribute_' . $key ] == $val ) {

												$variation_points[ $product_variable['variation_id'] ] ++;

											}

										}

									}

								}

							}

						}

						if ( ! empty( $variation_points ) ) {

							$result['active_var'] = max( $variation_points ) > 0 ? array_search( max( $variation_points ), $variation_points ) : 0;

						}

					}

				}

			}

			return $result;

		}

		/**
		 * Get countdown e sale bar position in product page
		 *
		 * @since   1.0.0
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function get_position_product() {

			$position = get_option( 'ywpc_position_product' );

			switch ( $position ) {

				case '1':
					return array(
						'hook'     => 'single_product',
						'priority' => 15
					);
					break;

				case '2':
					return array(
						'hook'     => 'single_product',
						'priority' => 25
					);
					break;

				case '3':
					return array(
						'hook'     => 'after_single_product',
						'priority' => 5
					);
					break;

				case '4':
					return array(
						'hook'     => 'after_single_product',
						'priority' => 15
					);
					break;

				case '5':
					return array(
						'hook'     => 'after_single_product',
						'priority' => 25
					);
					break;

				default:
					return array(
						'hook'     => 'before_single_product',
						'priority' => 5
					);

			}

		}

		/**
		 * Get countdown e sale bar position in product page
		 *
		 * @since   1.0.0
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function get_position_category() {

			$position = get_option( 'ywpc_position_category' );

			switch ( $position ) {

				case '1':
					return array(
						'hook'     => 'after_shop_loop_item_title',
						'priority' => 9
					);
					break;

				case '2':
					return array(
						'hook'     => 'after_shop_loop_item',
						'priority' => 9
					);
					break;

				case '3':
					return array(
						'hook'     => 'after_shop_loop_item',
						'priority' => 15
					);
					break;

				default:
					return array(
						'hook'     => 'before_shop_loop_item_title',
						'priority' => 15
					);

			}

		}

		/**
		 * Get timer title
		 *
		 * @since   1.0.0
		 *
		 * @param   $value
		 * @param   $before
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_timer_title( $value, $before ) {

			$arg = '';

			if ( $before === true ) {
				$arg = '_before';
			}

			return get_option( 'ywpc_timer_title' . $arg );

		}

		/**
		 * Add countdown css to product page
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function get_custom_css() {

			$inline_css = '';

			if ( get_option( 'ywpc_timer_title' ) == '' ) {

				$inline_css .= '
				.ywpc-countdown > .ywpc-header,
				.ywpc-countdown-loop > .ywpc-header {
					display: none;
				}';

			}

			if ( get_option( 'ywpc_sale_bar_title' ) == '' ) {

				$inline_css .= '
				.ywpc-sale-bar > .ywpc-header,
				.ywpc-sale-bar-loop > .ywpc-header {
					display: none;
				}';

			}

			if ( get_option( 'ywpc_appearance' ) == 'cust' ) {

				$text_font_size       = get_option( 'ywpc_text_font_size', 25 );
				$text_font_size_loop  = get_option( 'ywpc_text_font_size_loop', 15 );
				$timer_font_size      = get_option( 'ywpc_timer_font_size', 28 );
				$timer_font_size_loop = get_option( 'ywpc_timer_font_size_loop', 15 );
				$text_color           = get_option( 'ywpc_text_color', '#a12418' );
				$border_color         = get_option( 'ywpc_border_color', '#dbd8d8' );
				$back_color           = get_option( 'ywpc_back_color', '#fafafa' );
				$timer_fore_color     = get_option( 'ywpc_timer_fore_color', '#3c3c3c' );
				$timer_back_color     = get_option( 'ywpc_timer_back_color', '#ffffff' );
				$bar_fore_color       = get_option( 'ywpc_bar_fore_color', '#a12418' );
				$bar_back_color       = get_option( 'ywpc_bar_back_color', '#e6e6e6' );

				$inline_css .= '
				.ywpc-countdown,
				.ywpc-sale-bar {
					background: ' . $back_color . ';
					border: 1px solid ' . $border_color . ';
				}

				.ywpc-countdown > .ywpc-header,
				.ywpc-sale-bar > .ywpc-header {
					color: ' . $text_color . ';
					font-size: ' . $text_font_size . 'px;
				}

				.ywpc-countdown-loop > .ywpc-header,
				.ywpc-sale-bar-loop > .ywpc-header {
					color: ' . $text_color . ';
					font-size: ' . $text_font_size_loop . 'px;
				}';

				if ( get_option( 'ywpc_template', '1' ) == '1' ) {

					$inline_css .= '
					.ywpc-countdown > .ywpc-timer > div > .ywpc-amount > span {
						background: ' . $timer_back_color . ';
						color: ' . $timer_fore_color . ';
						font-size: ' . $timer_font_size . 'px;
					}
		
					.ywpc-countdown-loop > .ywpc-timer > div > .ywpc-amount > span {
						background: ' . $timer_back_color . ';
						color: ' . $timer_fore_color . ';
						font-size: ' . $timer_font_size_loop . 'px;
					}';

				} else {

					$inline_css .= '
					.ywpc-countdown > .ywpc-timer > div > .ywpc-amount,
					.ywpc-countdown-loop > .ywpc-timer > div > .ywpc-amount {
						background: ' . $timer_back_color . ';
					}
		
					.ywpc-countdown > .ywpc-timer > div > .ywpc-amount > span {
						color: ' . $timer_fore_color . ';
						font-size: ' . $timer_font_size . 'px;
					}
		
					.ywpc-countdown-loop > .ywpc-timer > div > .ywpc-amount > span {
						color: ' . $timer_fore_color . ';
						font-size: ' . $timer_font_size_loop . 'px;
					}';

				}

				$inline_css .= '
				.ywpc-sale-bar > .ywpc-bar > .ywpc-back,
				.ywpc-sale-bar-loop > .ywpc-bar > .ywpc-back {
					background: ' . $bar_back_color . ';
				}
	
				.ywpc-sale-bar > .ywpc-bar > .ywpc-back > .ywpc-fore,
				.ywpc-sale-bar-loop > .ywpc-bar > .ywpc-back > .ywpc-fore {
					background: ' . $bar_fore_color . ';
				}';

			}

			if ( get_option( 'ywpc_topbar_appearance' ) == 'cust' && get_option( 'ywpc_topbar_enable' ) == 'yes' ) {

				$topbar_text_font_size   = get_option( 'ywpc_topbar_text_font_size', 30 );
				$topbar_timer_font_size  = get_option( 'ywpc_topbar_timer_font_size', 18 );
				$topbar_text_color       = get_option( 'ywpc_topbar_text_color', '#a12418' );
				$topbar_label_color      = get_option( 'ywpc_topbar_text_label_color', '#232323' );
				$topbar_back_color       = get_option( 'ywpc_topbar_back_color', '#ffba00' );
				$topbar_timer_text_color = get_option( 'ywpc_topbar_timer_text_color', '#363636' );
				$topbar_timer_back_color = get_option( 'ywpc_topbar_timer_back_color', '#ffffff' );
				$topbar_border_color     = get_option( 'ywpc_topbar_timer_border_color', '#ff8a00' );

				$inline_css .= '
				.ywpc-countdown-topbar {
					background: ' . $topbar_back_color . ';
				}

				.ywpc-countdown-topbar > .ywpc-header {
					color: ' . $topbar_text_color . ';
					font-size: ' . $topbar_text_font_size . 'px;
				}

				.ywpc-countdown-topbar > .ywpc-timer > div > .ywpc-label {
					color: ' . $topbar_label_color . ';
				}';

				if ( get_option( 'ywpc_topbar_template', '2' ) == '2' ) {

					$inline_css .= '
					.ywpc-countdown-topbar > .ywpc-timer > div > .ywpc-amount > span {
						background: ' . $topbar_timer_back_color . ';
						color: ' . $topbar_timer_text_color . ';
						font-size: ' . $topbar_timer_font_size . 'px;
					}
					
					.ywpc-countdown-topbar > .ywpc-timer > div > .ywpc-amount {
						background: ' . $topbar_border_color . ';
					}';

				} else {

					$inline_css .= '
					.ywpc-countdown-topbar > .ywpc-timer > div > .ywpc-amount {
						background: ' . $topbar_timer_back_color . ';
						border: 1px solid ' . $topbar_border_color . ';
					}
					
					.ywpc-countdown-topbar > .ywpc-timer > div > .ywpc-amount > span {
						color: ' . $topbar_timer_text_color . ';
						font-size: ' . $topbar_timer_font_size . ' px;
					}';

				}

			}

			if ( $inline_css ) {

				wp_add_inline_style( 'ywpc-frontend', $inline_css );

			}

		}

		/**
		 * Add sale bar to product page
		 *
		 * @since   1.0.0
		 *
		 * @param   $what_show
		 * @param   $args
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_sales_bar_product( $what_show, $args ) {

			if ( $what_show == 'bar' || $what_show == 'both' ) {

				wc_get_template( '/frontend/single-product-bar.php', array( 'args' => $args ), '', YWPC_TEMPLATE_PATH );

			}

		}

		/**
		 * Add scripts to product page
		 *
		 * @since   1.0.0
		 *
		 * @param   $args
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_script_product( $args ) {

			if ( isset( $args['variable'] ) && $args['variable'] == true ) {

				ob_start();

				?>

                jQuery(function ($) {

                $(window).load(function () {
                $('input[name=\"variation_id\"]').change(function () {
                var pv_id = parseInt($(this).val());
                $('.ywpc-countdown').hide();
                $('.ywpc-sale-bar').hide();

                if (pv_id) {
                $('.ywpc-item-' + pv_id).show();
                }
                });
                });

                });

				<?php

				$inline_js = ob_get_clean();
				wp_add_inline_script( 'ywpc-footer', $inline_js );

			}

		}

		/**
		 * Check if product/variation is valid or expired
		 *
		 * @since   1.0.0
		 *
		 * @param   $id
		 *
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function check_ywpc_expiration( $id ) {

			global $sitepress;
			$has_wpml = ! empty( $sitepress ) ? true : false;

			if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
				$id = yit_wpml_object_id( $id, 'product', true, wpml_get_default_language() );
			}

			$product = wc_get_product( $id );

			if ( ! $product ) {
				return false;
			}

			$current_time       = strtotime( current_time( "Y-m-d G:i:s" ) );
			$before_sale        = get_option( 'ywpc_before_sale_start' );
			$before_sale_status = get_option( 'ywpc_before_sale_start_status' );
			$sale_start         = yit_get_prop( $product, '_ywpc_sale_price_dates_from' );
			$sale_end           = yit_get_prop( $product, '_ywpc_sale_price_dates_to' );
			$discount_qty       = yit_get_prop( $product, '_ywpc_discount_qty' );
			$sold_qty           = yit_get_prop( $product, '_ywpc_sold_qty' );
			$expired            = false;

			if ( ! empty( $sale_start ) && ! empty( $sale_end ) ) {

				switch ( true ) {

					case ( $current_time < $sale_start && $before_sale == 'yes' ):
						$expired = ( $before_sale_status == 'yes' );
						break;

					case ( $current_time >= $sale_start && $current_time <= $sale_end ):
						$expired = false;
						break;

					case ( $current_time > $sale_end ) :
						$expired = true;
						break;

				}

			}

			if ( ! $expired ) {

				if ( $sold_qty < $discount_qty ) {

					$expired = false;

				} else {

					if ( $sold_qty == 0 && $discount_qty == 0 || $discount_qty < $sold_qty ) {

						$expired = false;

					} else {

						$expired = true;

					}

				}

			}

			if ( defined( 'YWCTM_PREMIUM' ) && YWCTM_PREMIUM && get_option( 'ywpc_end_sale' ) == 'disable' ) {

				if ( get_option( 'ywctm_enable_plugin' ) == 'yes' && YITH_WCTM()->check_user_admin_enable() ) {

					if ( YITH_WCTM()->disable_shop() ) {

						$expired = true;

					} else {

						$hide_add_to_cart_single = apply_filters( 'ywctm_get_vendor_option', get_option( 'ywctm_hide_add_to_cart_single' ), $id, 'ywctm_hide_add_to_cart_single' );
						$hide_add_to_cart_loop   = apply_filters( 'ywctm_get_vendor_option', get_option( 'ywctm_hide_add_to_cart_loop' ), $id, 'ywctm_hide_add_to_cart_loop' );
						$hide_price              = apply_filters( 'ywctm_get_vendor_option', get_option( 'ywctm_hide_price' ), $id, 'ywctm_hide_price' );

						if ( $hide_add_to_cart_single == 'yes' || $hide_add_to_cart_loop == 'yes' || $hide_price == 'yes' ) {

							if ( YITH_WCTM()->apply_catalog_mode( $id ) ) {

								$enable_exclusion = apply_filters( 'ywctm_get_vendor_option', get_option( 'ywctm_exclude_hide_add_to_cart' ), $id, 'ywctm_exclude_hide_add_to_cart' );
								$exclude_catalog  = apply_filters( 'ywctm_get_exclusion', yit_get_prop( $product, '_ywctm_exclude_catalog_mode' ), $id, '_ywctm_exclude_catalog_mode' );

								$expired = ( $enable_exclusion != 'yes' ? true : ( $exclude_catalog != 'yes' ? true : false ) );

								$reverse_criteria = apply_filters( 'ywctm_get_vendor_option', get_option( 'ywctm_exclude_hide_add_to_cart_reverse' ), $id, 'ywctm_exclude_hide_add_to_cart_reverse' );

								if ( $enable_exclusion == 'yes' && $reverse_criteria == 'yes' ) {

									$expired = ! $expired;

								}

							}

						}

						if ( apply_filters( 'ywctm_check_price_hidden', false, $id ) ) {

							$expired = true;

						}

					}

				}


			}

			return $expired;

		}

		/**
		 * Check if ywpc needs to be showed in category page
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function check_show_ywpc_category() {

			global $post, $ywpc_loop;

			$product_id = $post->ID;

			global $sitepress;
			$has_wpml = ! empty( $sitepress ) ? true : false;

			if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
				$product_id = yit_wpml_object_id( $post->ID, $post->post_type, true, wpml_get_default_language() );
			}

			$product        = wc_get_product( $product_id );
			$has_countdown  = yit_get_prop( $product, '_ywpc_enabled', true );
			$show_countdown = get_option( 'ywpc_where_show' );

			if ( $has_countdown == 'yes' && ( ( $show_countdown == 'both' || $show_countdown == 'loop' ) || ( $show_countdown == 'code' && $ywpc_loop != '' ) ) ) {

				$args   = $this->get_position_category();
				$action = apply_filters( 'ywpc_override_standard_position_loop', 'woocommerce_' . $args['hook'] );

				add_action( $action, array( $this, 'add_ywpc_category' ), $args['priority'] );

			}

		}

		/**
		 * Add sales countdown to category page
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_ywpc_category() {

			global $post;

			$product_id = $post->ID;

			global $sitepress;
			$has_wpml = ! empty( $sitepress ) ? true : false;

			if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
				$product_id = yit_wpml_object_id( $post->ID, $post->post_type, true, wpml_get_default_language() );
			}

			$product   = wc_get_product( $product_id );
			$what_show = get_option( 'ywpc_what_show' );
			$args      = apply_filters( 'ywpc_product_args', array(
				'items' => array(
					$post->ID => yit_get_prop( $product, '_ywpc_sale_price_dates_to', true )
				)
			), '', ''
			);

			do_action( 'ywpc_category_page', $what_show, $args );

		}

		/**
		 * Add sale bar to category
		 *
		 * @since   1.2.0
		 *
		 * @param   $what_show
		 * @param   $args
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_timer_category( $what_show, $args ) {

			if ( $what_show != 'bar' || $what_show == 'both' ) {

				wc_get_template( '/frontend/category-timer.php', array( 'args' => $args ), '', YWPC_TEMPLATE_PATH );

			}

		}

		/**
		 * Add sale bar to category
		 *
		 * @since   1.2.0
		 *
		 * @param   $what_show
		 * @param   $args
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function add_sales_bar_category( $what_show, $args ) {

			if ( $what_show == 'bar' || $what_show == 'both' ) {

				wc_get_template( '/frontend/category-bar.php', array( 'args' => $args ), '', YWPC_TEMPLATE_PATH );

			}

		}

		/**
		 * Set ywpc shortcode
		 *
		 * @since   1.0.0
		 *
		 * @param   $atts
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function set_ywpc_shortcode( $atts ) {

			shortcode_atts( array(
				                'id'   => '',
				                'type' => ''
			                ), $atts );

			add_filter( 'widget_text', 'shortcode_unautop' );
			add_filter( 'widget_text', 'do_shortcode' );

			$ids = '';

			if ( isset( $atts['id'] ) ) {
				$ids = explode( ',', str_replace( ' ', '', $atts['id'] ) );
				$ids = array_map( 'trim', $ids );
			}

			if ( isset( $atts['type'] ) && $atts['type'] == 'single' ) {

				if ( $ids == '' || $ids[0] == 'null' ) {
					global $post;
					$product_id = apply_filters( 'ywpc_manipulate_current_post', $post->ID );

				} else {
					$product_id = $ids[0];
				}

				$what_show         = get_option( 'ywpc_what_show' );
				$args              = $this->get_product_args( '', $product_id );
				$args['shortcode'] = true;

				ob_start();

				do_action( 'ywpc_single_product_page', $what_show, $args );

				$shortcode = ob_get_clean();

			} else {

				$options = array(
					'show_title'     => get_option( 'ywpc_shortcode_title', 'yes' ),
					'show_rating'    => get_option( 'ywpc_shortcode_rating', 'yes' ),
					'show_price'     => get_option( 'ywpc_shortcode_price', 'yes' ),
					'show_image'     => get_option( 'ywpc_shortcode_image', 'yes' ),
					'show_addtocart' => get_option( 'ywpc_shortcode_addtocart', 'yes' ),
				);

				ob_start();

				$this->get_ywpc_custom_loop( $ids, 'shortcode', $options );

				$shortcode = ob_get_clean();

			}

			return $shortcode;

		}

		/**
		 * Get custom loop for widget and shortcode
		 *
		 * @since   1.0.0
		 *
		 * @param   $ids
		 * @param   $type
		 * @param   $options
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function get_ywpc_custom_loop( $ids, $type, $options = array() ) {

			global $ywpc_loop;

			$ywpc_loop = 'ywpc_' . $type;

			if ( $ids ) {

				$query_args = array(
					'posts_per_page' => - 1,
					'no_found_rows'  => 1,
					'post_status'    => 'publish',
					'post_type'      => 'product',
					'post__in'       => $ids,
				);

			} else {

				$query_args = array(
					'posts_per_page' => - 1,
					'no_found_rows'  => 1,
					'post_status'    => 'publish',
					'post_type'      => 'product',
					'meta_query'     => array(
						'relation' => 'AND',
						array(
							'key'   => '_ywpc_enabled',
							'value' => 'yes',
						),
						array(
							'key'     => '_ywpc_sale_price_dates_from',
							'value'   => strtotime( 'NOW', current_time( 'timestamp' ) ),
							'compare' => '<=',
							'type'    => 'NUMERIC'
						),
						array(
							'key'     => '_ywpc_sale_price_dates_to',
							'value'   => strtotime( 'NOW', current_time( 'timestamp' ) ),
							'compare' => '>=',
							'type'    => 'NUMERIC'
						)
					)
				);

			}
			$products = new WP_Query( $query_args );

			if ( $products->have_posts() ) {

				wc_get_template( '/frontend/widget-shortcode-loop.php', array(
					'type'     => $type,
					'products' => $products,
					'options'  => $options
				), '', YWPC_TEMPLATE_PATH );

			}

			wp_reset_query();

			$ywpc_loop = false;

		}

		/**
		 * Function for Widget and Shortcode to hide loop elements
		 *
		 * @since   1.0.0
		 *
		 * @param    $value
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function ywpc_loop_hide_filter( $value ) {

			global $ywpc_loop;

			return ( ( $ywpc_loop == 'ywpc_shortcode' || $ywpc_loop == 'ywpc_widget' ) ? '' : $value );

		}

		/**
		 * Hides "Add to cart" button from loop page (if option is enabled)
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function hide_add_to_cart_loop() {

			$ywpc_alt_loop_hook = apply_filters( 'ywpc_alternative_loop_hook', true );
			$result             = $this->hide_add_to_cart_check();

			if ( ! empty( $result ) ) {

				if ( $result['type'] == 'simple' ) {

					if ( $ywpc_alt_loop_hook ) {
						remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
					}
					add_filter( 'woocommerce_loop_add_to_cart_link', '__return_empty_string', 10 );
				}

			} else {

				if ( $ywpc_alt_loop_hook ) {
					add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
				}

				remove_filter( 'woocommerce_loop_add_to_cart_link', '__return_empty_string', 10 );

			}

		}

		/**
		 * Hides "Add to cart" button from single product page (if option is enabled)
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function hide_add_to_cart_single() {

			$result = $this->hide_add_to_cart_check();

			if ( ! empty( $result ) ) {

				$args = array();

				if ( ! class_exists( 'YITH_YWRAQ_Frontend' ) ) {

					$args[] = 'form.cart .quantity';

				}

				if ( $result['type'] == 'simple' || $result['type'] == 'variable-all' ) {

					$args[] = 'form.cart button.single_add_to_cart_button';
					$args[] = 'form.cart .woocommerce-variation-add-to-cart';

				} else {

					$expired = ( ( isset( $result['ids'] ) && ! empty( $result['ids'] ) ) ? implode( ', ', $result['ids'] ) : '' );

					ob_start();

					?>

                    jQuery(function ($) {

                    var expired = [ <?php echo $expired ?> ],
                    value = parseInt($('.single_variation_wrap .variation_id, .single_variation_wrap input[name="variation_id"]').val());

                    if (expired.length > 0 && $.inArray(value, expired) != -1) {
                    $('.single_variation_wrap .variations_button').hide()
                    }

                    $(document).on('woocommerce_variation_has_changed', hide_variations);
                    $(document).on('found_variation', hide_variations);


                    function hide_variations () {
                    value = parseInt($('.single_variation_wrap .variation_id, .single_variation_wrap input[name="variation_id"]').val());

                    if (expired.length > 0) {
                    if ($.inArray(value, expired) == -1) {
                    $('.single_variation_wrap .variations_button').show();
                    } else {
                    $('.single_variation_wrap .variations_button').hide();
                    }
                    }

                    }

                    });

					<?php

					$inline_js = ob_get_clean();
					wp_add_inline_script( 'ywpc-footer', $inline_js );

				}

				$classes = implode( ', ', apply_filters( 'ywpc_hide_classes', $args ) );

				ob_start();
				?>
				<?php echo $classes; ?>
                {
                display: none<?php echo( $result['type'] == 'simple' ? ' !important' : '' ); ?>;
                }
				<?php

				$inline_css = ob_get_clean();
				wp_add_inline_style( 'ywpc-frontend', $inline_css );

			}

		}

		/**
		 * Check if "Add to cart" needs to be hidden (if option is enabled)
		 *
		 * @since   1.0.0
		 *
		 * @param   $product_id
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function hide_add_to_cart_check( $product_id = false ) {

			global $post;

			$id = ( $product_id ) ? $product_id : ( $post ? $post->ID : 0 );

			global $sitepress;
			$has_wpml = ! empty( $sitepress ) ? true : false;

			if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
				$id = yit_wpml_object_id( $id, 'product', true, wpml_get_default_language() );
			}

			$product = wc_get_product( $id );

			if ( ! $product ) {
				return array();
			}

			$has_ywpc = yit_get_prop( $product, '_ywpc_enabled' );
			$result   = array();

			if ( $has_ywpc == 'yes' ) {

				$variation_global = yit_get_prop( $product, '_ywpc_variations_global_countdown' );

				if ( ( ! $product->is_type( 'variable' ) ) || ( $product->is_type( 'variable' ) && $variation_global == 'yes' ) ) {

					if ( $this->check_ywpc_expiration( $id ) ) {

						$result['type'] = 'simple';

					}

				} else {

					$product_variables = $product->get_available_variations();

					if ( count( array_filter( $product_variables ) ) > 0 ) {

						$product_variables = array_filter( $product_variables );
						$result['type']    = 'variable';
						$count             = 0;

						foreach ( $product_variables as $product_variable ) {

							if ( $this->check_ywpc_expiration( $product_variable['variation_id'] ) ) {

								$result['ids'][] = $product_variable['variation_id'];
								$count ++;

							}

						}

						if ( $count == count( $product_variables ) ) {
							$result['type'] = 'variable-all';
						}

					}
				}

			}

			return $result;

		}

		/**
		 * Avoid "Add to cart" action (if option is enabled)
		 *
		 * @since   1.0.0
		 *
		 * @param   $passed
		 * @param   $product_id
		 *
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function avoid_add_to_cart( $passed, $product_id ) {

			$result = $this->hide_add_to_cart_check( $product_id );

			if ( ! empty( $result ) ) {
				if ( $result['type'] == 'simple' ) {
					$passed = false;
				}
			}

			return $passed;
		}

		/**
		 * Hide product form the shop
		 *
		 * @since    1.0.0
		 *
		 * @param    $query
		 *
		 * @return   void
		 * @author   Alberto Ruggiero
		 */
		public function hide_product_from_catalog( $query ) {

			$products_list = $this->get_expired_products();

			if ( ! empty( $products_list ) ) {

				$query->set( 'post__not_in', $products_list );

			}

		}

		/**
		 * Hide product form the shop
		 *
		 * @since    1.2.1
		 *
		 * @param    $query_args
		 *
		 * @return   array
		 * @author   Alberto Ruggiero
		 */
		public function hide_product_from_shortcodes( $query_args ) {

			$products_list = $this->get_expired_products();

			if ( ! empty( $products_list ) ) {

				$query_args['post__not_in'] = $products_list;

			}

			return $query_args;

		}

		/**
		 * Get expired products
		 *
		 * @since    1.2.1
		 * @return   array
		 * @author   Alberto Ruggiero
		 */
		public function get_expired_products() {

			$products_list = array();
			$product_args  = array(
				'post_type'      => 'product',
				'post_status'    => 'publish',
				'posts_per_page' => - 1,
				'meta_key'       => '_ywpc_enabled',
				'meta_value'     => 'yes'
			);

			wp_reset_query();

			$expired = new WP_Query( $product_args );

			if ( $expired->have_posts() ) {

				while ( $expired->have_posts() ) {

					$expired->the_post();

					$product          = wc_get_product( $expired->post->ID );
					$variation_global = yit_get_prop( $product, '_ywpc_variations_global_countdown' );

					if ( $this->check_ywpc_expiration( $expired->post->ID ) && ( ! $product->is_type( 'variable' ) || ( $product->is_type( 'variable' ) && $variation_global == 'yes' ) ) ) {

						$products_list[] = $expired->post->ID;

					}

				}

			}

			wp_reset_postdata();

			return $products_list;

		}

		/**
		 * Hide variation
		 *
		 * @since    1.0.0
		 *
		 * @param    $visible
		 * @param    $variation_id
		 *
		 * @return   bool
		 * @author   Alberto Ruggiero
		 */
		public function hide_variations( $visible, $variation_id ) {

			if ( $this->check_ywpc_expiration( $variation_id ) ) {
				error_log( 'here' );
				$visible = false;

			}

			return $visible;

		}

		/**
		 * Avoid direct access to disabled products
		 *
		 * @since    1.0.0
		 * @return   string
		 * @author   Alberto Ruggiero
		 */
		public function avoid_direct_access() {

			global $post;

			if ( is_singular( 'product' ) ) {

				$product  = wc_get_product( $post->ID );
				$has_ywpc = yit_get_prop( $product, '_ywpc_enabled' );

				if ( $has_ywpc == 'yes' ) {

					$variation_global = yit_get_prop( $product, '_ywpc_variations_global_countdown' );

					if ( $this->check_ywpc_expiration( $post->ID ) && ( ! $product->is_type( 'variable' ) || ( $product->is_type( 'variable' ) && $variation_global == 'yes' ) ) ) {

						include( get_query_template( '404' ) );
						exit;

					}

				}

			}

		}

		/**
		 * Initialize topbar
		 *
		 * @since   1.1.5
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function initialize_topbar() {

			if ( get_option( 'ywpc_topbar_enable' ) == 'yes' ) {

				add_filter( 'body_class', array( $this, 'add_body_classes' ) );
				add_action( 'wp_footer', array( $this, 'print_bar_countdown' ), 999 );
				add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts_premium' ), 9 );

			}

		}

		/**
		 * Add countdown all pages
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function print_bar_countdown() {

			$prod_id = get_option( 'ywpc_topbar_product' );

			global $sitepress;
			$has_wpml = ! empty( $sitepress ) ? true : false;

			if ( $has_wpml && apply_filters( 'ywpc_wpml_use_default_language_settings', false ) ) {
				$prod_id = yit_wpml_object_id( $prod_id, 'product', true, wpml_get_default_language() );
			}

			$product  = wc_get_product( $prod_id );
			$has_ywpc = yit_get_prop( $product, '_ywpc_enabled' );
			$args     = array();

			if ( $has_ywpc == 'yes' ) {

				$current_time = strtotime( current_time( "Y-m-d G:i:s" ) );

				$variation_global = yit_get_prop( $product, '_ywpc_variations_global_countdown' );

				if ( ( ! $product->is_type( 'variable' ) ) || ( $product->is_type( 'variable' ) && $variation_global == 'yes' ) ) {

					$sale_start = yit_get_prop( $product, '_ywpc_sale_price_dates_from' );
					$sale_end   = yit_get_prop( $product, '_ywpc_sale_price_dates_to' );

					if ( $current_time >= $sale_start && $current_time <= $sale_end ) {

						$args['id']       = $prod_id;
						$args['end_date'] = $sale_end;
						$args['type']     = 'simple';
					}

				} else {

					$product_variables = $product->get_available_variations();

					if ( count( array_filter( $product_variables ) ) > 0 ) {

						$product_variables = array_filter( $product_variables );
						$check_default     = yit_get_prop( $product, '_default_attributes' );

						foreach ( $product_variables as $product_variable ) {

							if ( $check_default && is_array( $check_default ) ) {

								$variation          = wc_get_product( $product_variable['variation_id'] );
								$key_select_default = key( $check_default );
								$check_default      = $check_default[ $key_select_default ];
								$key_attr           = str_replace( 'attribute_', '', key( $product_variable['attributes'] ) );
								$data_attr          = $product_variable['attributes'][ 'attribute_' . $key_attr ];

								if ( $key_attr == $key_select_default && $check_default == $data_attr ) {

									$sale_start = yit_get_prop( $variation, '_ywpc_sale_price_dates_from' );
									$sale_end   = yit_get_prop( $variation, '_ywpc_sale_price_dates_to' );

									if ( ! empty( $sale_end ) && ! empty( $sale_start ) ) {

										if ( $current_time >= $sale_start && $current_time <= $sale_end ) {

											$args['id']       = $prod_id;
											$args['end_date'] = $sale_end;
											$args['type']     = 'variable';
											$args['url']      = '?attribute_' . $key_attr . '=' . $data_attr;

										}

									}

								}

							}

						}

					}

				}

				if ( ! empty( $args ) ) {
					wc_get_template( '/frontend/bar-timer.php', array( 'args' => $args ), '', YWPC_TEMPLATE_PATH );
				}

			}

		}

		/**
		 * Enqueue frontend script files
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function frontend_scripts_premium() {

			$template = get_option( 'ywpc_topbar_template', '1' );
			$suffix   = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			wp_enqueue_style( 'ywpc-frontend-topbar', YWPC_ASSETS_URL . '/css/ywpc-bar-style-' . $template . $suffix . '.css' );

			if ( apply_filters( 'ywpc_force_two_cypher_days', false ) && '2' == $template ) {

				$css = '.ywpc-countdown-topbar > .ywpc-timer > .ywpc-days > .ywpc-amount { width: 72px }';

				wp_add_inline_style( 'ywpc-frontend-topbar', $css );

			}

		}

		/**
		 * Add classes to body
		 *
		 * @since   1.0.0
		 *
		 * @param   $classes
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function add_body_classes( $classes ) {

			$position = get_option( 'ywpc_topbar_position', 'top' );

			$classes[] = 'ywpc-' . $position;

			if ( is_admin_bar_showing() && $position == 'top' ) {
				$classes[] = 'ywpc-admin';
			}

			return $classes;

		}

		/**
		 * YITH FRAMEWORK
		 */

		/**
		 * Register plugins for activation tab
		 *
		 * @return  void
		 * @since   2.0.0
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 */
		public function register_plugin_for_activation() {
			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once 'plugin-fw/licence/lib/yit-licence.php';
				require_once 'plugin-fw/licence/lib/yit-plugin-licence.php';
			}
			YIT_Plugin_Licence()->register( YWPC_INIT, YWPC_SECRET_KEY, YWPC_SLUG );
		}

		/**
		 * Register plugins for update tab
		 *
		 * @return  void
		 * @since   2.0.0
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 */
		public function register_plugin_for_updates() {
			if ( ! class_exists( 'YIT_Upgrade' ) ) {
				require_once( 'plugin-fw/lib/yit-upgrade.php' );
			}
			YIT_Upgrade()->register( YWPC_SLUG, YWPC_INIT );
		}

		/**
		 * Plugin row meta
		 *
		 * add the action links to plugin admin page
		 *
		 * @since   1.0.0
		 *
		 * @param   $new_row_meta_args
		 * @param   $plugin_meta
		 * @param   $plugin_file
		 * @param   $plugin_data
		 * @param   $status
		 * @param   $init_file
		 *
		 * @return  array
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 * @use     plugin_row_meta
		 */
		public function plugin_row_meta( $new_row_meta_args, $plugin_meta, $plugin_file, $plugin_data, $status, $init_file = 'YWPC_INIT' ) {
			$new_row_meta_args = parent::plugin_row_meta( $new_row_meta_args, $plugin_meta, $plugin_file, $plugin_data, $status, $init_file );

			if ( defined( $init_file ) && constant( $init_file ) == $plugin_file ){
				$new_row_meta_args['is_premium'] = true;
			}

			return $new_row_meta_args;
		}

		/**
		 * Action Links
		 *
		 * add the action links to plugin admin page
		 * @since   1.0.0
		 *
		 * @param   $links | links plugin array
		 *
		 * @return  mixed
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			$links = yith_add_action_links( $links, $this->_panel_page, true );
			return $links;
		}

	}

}
