=== YITH WooCommerce Product Countdown ===

Contributors: yithemes
Tags: woocommerce, products, themes, yit, yith, e-commerce, shop, count down, countdown, countdown clock, countdown timer, countdown widget, counter, counter clock, date countdown, date time, date timer, datetime, days until
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.2.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.2.8 =

* Update: plugin framework

= 1.2.7 =

* New: Support to Wordpress 4.9.6
* New: Support to WooCommerce 3.4.0
* Update: plugin framework

= 1.2.6 =

* New: Support to WooCommerce 3.3.0

= 1.2.5 =

* New: Dutch translation
* Update: countdown library
* Update: plugin framework
* Tweak: script loading moved to footer
* Fix: shortcode lightbox not working

= 1.2.4 =

* New: counter supporting countdown over 100 days
* New: Support to WooCommerce 3.2.0 RC2
* Update: plugin framework
* Fix: countdown not displayed id Pre-Order item in out of stock

= 1.2.3 =

* Fix: minified JS files not updated

= 1.2.2 =

* New: set the time for start and end date of the countdown
* New: integration with YITH Pre-Order for WooCommerce Premium
* Tweak: shortcode loop template overwritable from theme

= 1.2.1 =

* Fix: countdown with WPML active
* Fix: expired products visibile on shortcodes
* Fix: sold quantity update for variable products
* Fix: topbar styling
* Tweak: added "ywpc_override_standard_position_loop" filter

= 1.2.0 =

* Tweak: Support for WooCommerce 2.7.0-RC 1

= 1.1.5 =

* Fix: general countdown for product variations from bulk actions

= 1.1.4 =

* New: general countdown for product variations
* Tweak: Tested with WooCommerce 2.6.0 RC1

= 1.1.3 =

* New: compatibility with Wordpress 4.5

= 1.1.2 =

* Fix: minor bug

= 1.1.1 =

* Fix: wrong "ywpc-style-3.min.css" file

= 1.1.0 =

* New: minified version of CSS files
* New: option to show the countdown only on the widget / shortcode
* Update: plugin core framework

= 1.0.9 =

* New: single product shortcode
* New: compatibility with YITH WooCommerce Catalog Mode premium

= 1.0.8 =

* Fix: issues while saving variations
* Tweak: countdown dates on dedicated post meta

= 1.0.7 =

* New: compatibility with WooCommerce 2.5
* New: column showing active countdowns in product page

= 1.0.6 =

* Tweak: Shortcode without specified ids shows all products with active countdown
* Update: plugin core framework

= 1.0.5 =

* Update: changed text domain from ywpc to yith-woocommerce-product-countdown
* Update: changed all language file for the new text domain

= 1.0.4 =

* Update: plugin core framework

= 1.0.3 =

* Fix: Variation options save

= 1.0.2 =

* New: Support for RTL languages

= 1.0.1 =

* Update: language file

= 1.0.0 =

* Initial release