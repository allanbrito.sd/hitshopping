<?php
if( !defined('ABSPATH' ) )
    exit;

$endpoints = array(

    'endpoints-settings' => array(

        'endpoints-section-start' => array(
            'type' => 'title',
            'name' => __('Funds endpoints','yith-woocommerce-account-funds'),
           
        ),
        'fund-make-a-deposit-endpoint-title' => array(
            'name' => __('"Make a deposit" endpoint title','yith-woocommerce-account-funds'),
            'type' => 'text',
            'default' => __('Make a deposit', 'yith-woocommerce-account-funds'),
            'id' => 'ywf_make_a_deposit'
        ),

        'fund-make-a-deposit-endpoint-slug' => array(
	        'name' => __('"Make a deposit" endpoint slug','yith-woocommerce-account-funds'),
	        'type' => 'text',
	        'default' => 'make-a-deposit',
	        'id' => 'ywf_make_a_deposit_slug'
        ),

        'fund-view-income-expenditure-history-endpoint-title' => array(
            'name' => __('Income/Expenditure History endpoint title', 'yith-woocommerce-account-funds'),
            'type' => 'text',
            'default' =>__('Income/Expenditure History', 'yith-woocommerce-account-funds'),
            'id' => 'ywf_view_income_expenditure_history'
        ),

        'fund-view-income-expenditure-history-endpoint-slug' => array(
	        'name' => __('Income/Expenditure History endpoint slug', 'yith-woocommerce-account-funds'),
	        'type' => 'text',
	        'default' =>'view-history',
	        'id' => 'ywf_view_income_expenditure_history_slug'
        ),
        'endpoint-section-end' => array(
            'type'  => 'sectionend',
           
        )
    )
);

return apply_filters( 'ywf_endpoints_settings', $endpoints );