﻿=== YITH WooCommerce Account Funds Premium ===

Contributors: yithemes
Tags: woocommerce, ecommerce, deposit, account funds, widget,shop, yith, yit, yithemes
Requires at least: 3.5.1
Tested up to: 4.9.8
Stable tag: 1.0.25
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

= Update : Plugin Framework
= 1.0.25 =
* Update: Plugin Framework
* Fix: Conversion issues during refund process using WooCommerce Multilingual
= 1.0.24 =
* New: Add a column in Log table to show the fund editor
* Update: Plugin Framework
* Update: Italian language
* Fix: Conversion issues with funds using WooCommerce Multilingual
  
= 1.0.23 =
* New: Support to WooCommerce 3.4.0
* Tweak: GDPR support
* Update: Plugin Framework
* Update: Spanish translation

= 1.0.22 =
* New: Support to WooCommerce 3.4.0 RC-1
* New: Support to WordPress 4.9.6
* New: Support to GDPR compliance
* Update: Italian Language
* Update: Plugin Framework

= 1.0.21 =
* New: Support to WooCommerce 3.3.0 RC2
* New: Support to WordPress 4.9.2
* Update: Plugin Framework

= 1.0.20 =
* Upload: Plugin Framework 3.0.5
* Fix: Wrong text domain in some strings
* Fix: Minor bugs

= 1.0.19 =
* New: Choose the amount of increments of the deposit amount
* New: Support to WooCommerce 3.2.6
* New: Support to WordPress 4.9.1
* Update: Plugin Framework 3.0.1
* Update: Language Files
* Fix: translation of WPML endpoints
* Fix: Check if order exist before calling member function get_currency

= 1.0.18 =
* New: Support to WordPress 4.8.2
* New: Support to WooCommerce 3.2.0-RC2
* Update: Plugin Framework

= 1.0.17 =
* New: Support to WooCommerce 3.0.8
* Fix: Pagination issue on transaction history
* Update: Plugin Framework

= 1.0.16 =
* New: Support to WooCommerce 3.0.0-RC2
* Fix: Funds not credited when a deposit order is completed
* Update: Plugin Framework


= 1.0.15 =
* Fix: Issue with old deposit order
* Update: Plugin Framework

= 1.0.14 =
* Fix: Funds not credited when a deposit order is completed
* Update: Plugin Framework

= 1.0.13 =
* New: Support to WooCommerce 2.7.0-RC1
* Update: Plugin Framework

= 1.0.12 =
* New Support to WordPress 4.7
* New: Support to WooCommerce 2.6.11
* Update: Plugin Framework

= 1.0.11 =

* New: Compatibility with WooCommerce 2.6.8
* Update: Plugin Framework
* Fix: undue extra funds given for gateway fees


= 1.0.10 =

* New : Compatibility with Aelia Currency Switcher

= 1.0.9 =

* Fix: Endpoints conflicting with specific themes
* Update: Plugin Framework

= 1.0.8 =

* New: Option to choose the payment gateway for deposit orders
* Update: Language files
* Update: Plugin Framework

= 1.0.7 =

* New: Shortcode to display deposit form
* Fix: Layout issue
* Update : Plugin Framework

= 1.0.6 =

* Fix:  Layout issue when is activated YITH WooCommerce Customize My Account Page
* Update: Plugin Framework

= 1.0.5 =

* New: Compatibility with YITH WooCommerce Customize My Account Page

= 1.0.4 =

* New: Compatibility with WooCommerce 2.6 RC1
* Update : Italian Language File
* Update: Spanish Language file
* Update: Plugin Framework

= 1.0.3 =

* New: Italian Language file
* Update: Plugin Framework

= 1.0.2 =
* New: Spanish Language file

= 1.0.1 =

* Fix: Order status including a deposit doesn’t turn into “completed” after payment

= 1.0.0 =

* Initial release
