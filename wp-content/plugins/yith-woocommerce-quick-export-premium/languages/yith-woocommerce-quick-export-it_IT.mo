��    I      d  a   �      0     1     9     ?     L     S  D   i  I   �  )   �     "     *     2  	   K     U     [     l     s     �     �  V   �  4     /   7  ;   g  (   �     �  	   �     �     �     	     	     	     %	     6	     S	  �   V	     �	     
     
     '
     3
     8
     =
     D
  %   P
  #   v
     �
     �
     �
     �
  
   �
  
   �
     �
     
        M   4     �     �  ,   �  )   �  �     .  �     �  '   �  .   �  ;     e   S     �     �  ]   �     9     @  t   ^  e   �  �  9     �     �     �     �     �  Q     O   a  )   �     �     �     �       
             -     5     G     c  c   l  ;   �  0     F   =  .   �     �  
   �     �     �                    '  #   =     a  �   d                    3     F     K     S     Z  5   o  8   �     �     �            
   /  
   :     E     Z     s  T   �     �     �  9     7   S  �   �  >  -     l  /   y  :   �  B   �  f   '     �     �  l   �          (  w   F  d   �               	          <      E      7      5      9   1      >   3   :      .         F      G          B   /                             #   %                 8       &           !   @   '   (      6      2          4      =   0       H           ?      -      "           $          *       )       
   ,   A       D   ;   +                C          I    Add job Apply Bulk Actions Cancel Choose data to export Choose the ending date. Leave blank to end with the last item found. Choose the starting date. Leave blank to start from the first item found. Choose when the exportation should start. Confirm Coupons Create new scheduled job Customers Daily Data Exportation Delete Disable Dropbox Discover The Advanced Features Download Dropbox backup is currently active for <b>%s</b>. <b>%s</b> used over <b>%s</b> total. Dropbox backup: unable to disable authorization  ->  Dropbox backup: unable to get access token  ->  Dropbox backup: unable to retrieve account information  ->  Dropbox backup: unable to send file  ->  Export data Export on Exportation job name Exportation time Filename format From General General Settings Get Support and Pro Features ID In the YIT Plugins tab, you can find the YITH WooCommerce Quick Export options.<br> From this menu you can have access to all settings of the activated YITH plugins. Login to Dropbox Monthly Monthly (30 days) Move to Bin Name None Orders Path format Please enter a valid folder/path name Please enter a valid schedule time. Plugin Documentation Premium Version Premium version Processed task list Recurrence Recurrency Schedule history Scheduled exportation Scheduled task list See YITH WooCommerce Quick Export plugin with full premium features in action Select bulk action Send documents to Dropbox Set a univocal name for this exportation job Set automatic document backup to Dropbox. Set filename format for invoice documents. Use {{year}}, {{month}}, {{day}}, {{hours}}, {{minutes}}, {{seconds}} as placeholders. Set the path where you want to store the documents. Use {{year}}, {{month}}, {{day}} as placeholders. For example: "Backup/{{year}}/{{month}}" will create a paths like Backup/2015/05, Backup/2015/06 for invoices stored per year and month; alternatively, leave blank for storing files in the root folder Settings Start exportation at specific date/time Starting date must be earlier than ending date Starting date must be earlier than/the same of ending date. The authentication token provided is no longer valid, please repeat the Dropbox authentication steps. To Upgrade to the PREMIUM VERSION Upgrade to the PREMIUM VERSION of YITH WooCommerce Quick Export to benefit from all features! Weekly YITH WooCommerce Quick Export YITH WooCommerce Quick Export is available in an outstanding PREMIUM version with many new options, discover it now. YITH WooCommerce Quick Export is enabled but not effective. It requires WooCommerce in order to work. Project-Id-Version: YITH WooCommerce Quick Export
POT-Creation-Date: 2016-09-01 10:28+0200
PO-Revision-Date: 2016-09-01 10:28+0200
Last-Translator: 
Language-Team: Your Inspiration Themes <plugins@yithemes.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __ ;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Aggiungi task Applica  Azioni di massa Annulla Scegli i dati da esportare Scegli la data di fine. Lascia vuoto per terminare con l'ultimo articolo trovato. Scegli la data di inizio. Lascia vuoto per iniziare dal primo articolo trovato. Scegliere quando iniziare l'esportazione. Conferma Coupon  Crea nuovo task programmato Clienti Quotidiano Esportazione dati Elimina Disattiva Dropbox Scopri le funzioni avanzate Download Il backup di Dropbox è al momento attivo per <b>%s</b>. <b>%s</b> utilizzato sul <b>%s</b> totale. Backup Dropbox: impossibile disattivare l'autorizzazione -> Backup Dropbox: impossibile accedere al token -> Backup Dropbox: impossibile recuperare le informazioni dell'account -> Backup Dropbox: impossibile inviare il file -> Esporta dati Esporta il Nome del task di esportazione Orario dell'esportazione Format del nome del file Da Generale Impostazioni generali Ottieni supporto e funzioni premium ID Puoi trovare le opzioni di YITH WooCommerce Quick Export nella tab YIT Plugins.<br>Da questo menu puoi accedere a tutte le impostazioni dei plugin YITH attivati. Accedi a Dropbox Mensile Mensile (30 giorni) Sposta nel cestino Nome Nessuno Ordini Formato del percorso Per favore inserisci un nome cartella/percorso valido Per favore inserisci un orario di programmazione valido. Documentazione plugin Versione premium Versione premium Lista dei task elaborati Ricorrenza Ricorrenza Programma lo storico Esportazione programmata Lista dei task programmati Vedi il plugin YITH WooCommerce Quick Export con tutte le funzioni premium in azione Seleziona azione di massa Invia i documenti a Dropbox Imposta un nome univoco per questo lavoro di esportazione Imposta il backup automatico dei documenti per Dropbox. Imposta il format del nome del file per i documenti di fatturazione. Utilizza  {{year}}, {{month}}, {{day}}, {{hours}}, {{minutes}}, {{seconds}} come segnaposto. Imposta il percorso in cui vuoi archiviare i documenti. Utilizza  {{year}}, {{month}}, {{day}} come segnaposto. Ad esempio: "Backup/{{year}}/{{month}}" creerà un percorso del tipo Backup/2015/05, Backup/2015/06 per le fatture archiviate per anno e mese; in alternativa, lascia vuoto per archiviare nella cartella root Impostazioni Inizia l'esportazione in una data/ora specifica La data di inizio deve essere precedente alla data di fine La data di inizio deve essere precedente/uguale alla data di fine. Il token di verifica fornito non è più valido, per favore ripeti i passaggi per la verifica Dropbox. A Aggiorna alla VERSIONE PREMIUM Aggiorna alla VERSIONE PREMIUM di YITH WooCommerce Quick Export per usufruire di tutte le sue funzionalità! Settimanale YITH WooCommerce Quick Export YITH WooCommerce Quick Export è disponibile in un' eccezionale versione PREMIUM con molte nuove opzioni, scoprila ora. YITH WooCommerce Quick Export è attivo ma non operativo. Il suo funzionamento richiede WooCommerce. 