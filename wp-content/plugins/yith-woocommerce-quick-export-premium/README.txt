=== YITH WooCommerce Quick Export Premium ===

Contributors: yithemes
Tags: export, data export, order export, customer export, coupon export, scheduled export, order backup, csv, customer backup, dropbox, dropbox backup, export dropbox
Requires at least: 3.5.1
Tested up to: 4.9.x
Stable tag: 1.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= Version 1.2.1 - Released: Jan 31, 2018 =

* Update: plugin framework 3.0.11
* New: support to WooCommerce 3.0.0


= Version 1.2.0 - Released: Dec 22, 2017 =

* Update: plugin framework 3.0
* Fix: cart discount not showing
* New: ducth translation

= Version 1.1.0 - Released: Nov 30, 2017 =

* New: support to WordPress 4.9.1
* New: support to WooCommerce 3.2.5
* New: support to Dropbox API v2 support

= Version 1.0.10 - Released: Aug 01, 2017 =

* Tweak: support to PHP 7

= Version 1.0.9 - Released: Jul 10, 2017 =

* Tweak: order export


= Version 1.0.8 - Released: Jul 06, 2017 =

* New: support for WooCommerce 3.1.
* New: tested up to WordPress 4.8.
* Update: YITH Plugin Framework.

= Version 1.0.7 - Released: May 19, 2017 =

* Update: Dropbox app keys.
* Fix: Dropbox conflicts with third party plugin.

= Version 1.0.6 - Released: Mar 24, 2017 =

* New:  Support to WooCommerce 3.0
* Update: YITH Plugin Framework
* Fix: YITH Plugin Framework initialization

= Version 1.0.5 - Released: Feb 02, 2017 =

* New: filter the order statuses to export

= Version 1.0.4 - Released: Dec 07, 2016 =

* New: ready for WordPress 4.7

= Version 1.0.3 - Released: Oct 19, 2016 =

* Fix: conflict with Gravity Forms Dropbox Add-On

= Version 1.0.2 - Released: Jan 28, 2016 =

* Updated: change text-domain from ywqe to yith-woocommerce-quick-export
* Updated: YITH plugin FW to latest release
* Updated: WP up to 4.4.1
* Fix: datepicker conflict with the CSS class selector
* Fix: plugin pot file in /languages folder

= Version 1.0.1 - Released: Aug 12, 2015 =

* Tweak: update YITH Plugin framework.

= Version 1.0.0 - Released: May 27 , 2015 =

Initial release
