msgid ""
msgstr ""
"Project-Id-Version: YIT WooCommerce Uploads\n"
"POT-Creation-Date: 2017-06-28 14:17+0200\n"
"PO-Revision-Date: 2017-06-28 14:18+0200\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <plugins@hitoutlets.com>\n"
"Language: it_IT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.12\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;"
"__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;"
"_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../init.php:42
msgid ""
"YITH WooCommerce Uploads is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""
"YITH WooCommerce Uploads è abilitato ma non in funzione. Devi aver "
"installato WooCommerce perché possa funzionare correttamente."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:441
msgid "Uploaded elements"
msgstr "Elementi caricati"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:514
#: ../plugin-options/general-options.php:197
msgid "You can customize your order attaching files to the current order."
msgstr ""
"Puoi personalizzare il tuo ordine allegando i files all'ordine corrente."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:781
msgid "Send a message to customer"
msgstr "Invia un messaggio al cliente"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:785
#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:891
msgid "Insert here the message you want to send to the customer"
msgstr "Inserisci qui il messaggio da inviare al cliente"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:787
msgid "Send"
msgstr "Invia"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:829
msgid "Rule label:"
msgstr "Etichetta regola:"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:854
msgid "User notes: "
msgstr "Note utente: "

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:856
msgid "There are no user notes"
msgstr "Non sono presenti note degli utenti"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:876
msgid "Accept"
msgstr "Accetta"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:880
msgid "Reject"
msgstr "Rifiuta"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:882
msgid "File will be <b>accepted</b>: please remember to save the order."
msgstr "Il file sarà <b>accettato</b>: ricorda di salvare l’ordine."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:884
msgid "File will be <b>rejected</b>: please remember to save the order."
msgstr "Il file sarà <b>rifiutato</b>: ricorda di salvare l’ordine."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:887
msgid "Cancel"
msgstr "Annulla"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:895
#, php-format
msgid "The file has been accepted on %s."
msgstr "Il file è stato accettato in data %s."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:898
#, php-format
msgid "The file has been rejected on %s."
msgstr "Il file è stato rifiutato in data %s."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:900
#, php-format
msgid " You have sent the following notification: %s."
msgstr " Hai inviato la seguente notifica: %s."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:907
#: ../templates/emails/ywau-order-file-actions.php:88
#: ../templates/emails/ywau-order-file-actions.php:91
msgid "There are no associated files."
msgstr "Non sono presenti file associati."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:954
msgid "Remove file"
msgstr "Rimuovi file"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1129
msgid "Disable the upload"
msgstr "Disabilita caricamento file"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1495
#, php-format
msgid "Allowed extension: %s."
msgstr "Estensione file consentita: %s."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1498
#, php-format
msgid "Max allowed size: %s MB."
msgstr "Dimensioni massime consentite: %s MB."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1510
msgid "The file you sent has been verified and accepted"
msgstr "Il file che hai inviato è stato verificato e accettato"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1520
msgid "The file you sent has been rejected."
msgstr "Il file che hai inviato è stato rifiutato."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1541
#: ../lib/class.yith-woocommerce-additional-uploads.php:591
msgid "Select file"
msgstr "Seleziona file"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1549
msgid "Notes:"
msgstr "Note:"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1562
msgid "Submit"
msgstr "Invia"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1747
#, php-format
msgid "Upload %s/%s file"
msgid_plural "Upload %s/%s files"
msgstr[0] "Carica %s/%s file"
msgstr[1] "Carica %s/%s file"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1751
#, php-format
msgid "Modify %s file"
msgid_plural "Modify %s files"
msgstr[0] "Modifica %s file"
msgstr[1] "Modifica %s file"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1826
#: ../lib/class.yith-woocommerce-additional-uploads.php:267
#, php-format
msgid "The following error occurred during the upload of %s: %s"
msgstr "Si è verificato questo errore durante il caricamento di %s: %s"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1842
#, php-format
msgid "The format of the %s file is not accepted."
msgstr "Il formato del file %s non è accettato."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1854
#: ../lib/class.yith-woocommerce-additional-uploads.php:294
#, php-format
msgid "The %s file has not been accepted, the maximum dimension is %s MB."
msgstr ""
"Il file %s non è stato accettato, le dimensioni massime consentite sono %s "
"MB."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:1868
#, php-format
msgid ""
"The <b>%s</b> file has been included in the current order. Your order is now "
"being processed."
msgstr ""
"Il file <b>%s</b> è stato incluso nell'ordine corrente. Il tuo ordine è ora "
"in fase di elaborazione."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:2045
msgid "Upload"
msgstr "Carica"

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:2303
msgid "This order contains file waiting for approval."
msgstr "Questo ordine contiene un file in attesa di approvazione."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:2305
msgid "All requested files have been received and accepted."
msgstr "Tutti i file richiesti sono stati ricevuti ed approvati."

#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:2326
#: ../lib/class.yith-woocommerce-additional-uploads-premium.php:2358
msgid "Upload rules to display"
msgstr "Regole di caricamento da mostrare"

#: ../lib/class.yith-woocommerce-additional-uploads.php:217
msgid "Upload file"
msgstr "Carica file"

#: ../lib/class.yith-woocommerce-additional-uploads.php:254
#, php-format
msgid ""
"You have already sent a file for the current order, it is not possible to "
"add the new %s file."
msgstr ""
"Hai già inviato un file con il presente ordine, non è possibile aggiungere "
"un altro file %s."

#: ../lib/class.yith-woocommerce-additional-uploads.php:260
#, php-format
msgid "The name of the file %s has not been accepted."
msgstr "Il nome del file %s non è stato accettato."

#: ../lib/class.yith-woocommerce-additional-uploads.php:284
#, php-format
msgid "The format of the %s file is not valid. The allowed extensions are: %s."
msgstr ""
"Il formato del file %s non è valido. Le estensioni consentite sono: %s."

#: ../lib/class.yith-woocommerce-additional-uploads.php:315
#: ../lib/class.yith-woocommerce-additional-uploads.php:570
#, php-format
msgid ""
"The %s file has been included in the current order. Your order is now being "
"processed."
msgstr ""
"Il file %s è stato incluso nella cartella corrente. Adesso il tuo ordine "
"verrà elaborato."

#: ../lib/class.yith-woocommerce-additional-uploads.php:397
msgid "The customer has sent a file."
msgstr "Il cliente ha inviato un file."

#: ../lib/class.yith-woocommerce-additional-uploads.php:398
msgid "Download"
msgstr "Scarica"

#: ../lib/class.yith-woocommerce-additional-uploads.php:401
msgid "There are no files attached to the order."
msgstr "Non sono presenti file in allegato all’ordine."

#: ../lib/class.yith-woocommerce-additional-uploads.php:424
msgid "The file could not be deleted"
msgstr "Non è stato possibile cancellare il file"

#: ../lib/class.yith-woocommerce-additional-uploads.php:455
msgid "The message has been sent to the email address of the customer."
msgstr "Il messaggio è stato inviato all’indirizzo email del cliente."

#: ../lib/class.yith-woocommerce-additional-uploads.php:456
msgid "An error occurred, the message has not been sent."
msgstr "Si è verificato un errore, il messaggio non è stato inviato."

#: ../lib/class.yith-woocommerce-additional-uploads.php:550
msgid "An error occurred, the file has not been accepted."
msgstr "Si è verificato un errore, il file non è stato accettato."

#: ../lib/class.yith-woocommerce-additional-uploads.php:580
msgid "You can customize your order sending a file."
msgstr "Puoi personalizzare il tuo ordine inviando un file."

#: ../lib/class.yith-woocommerce-additional-uploads.php:582
#, php-format
msgid "Choose one of the following formats: %s"
msgstr "Seleziona uno tra i seguenti formati: %s"

#: ../lib/class.ywau-plugin-fw-loader.php:125
msgid "General"
msgstr "Generali"

#: ../lib/class.ywau-plugin-fw-loader.php:128
#: ../lib/class.ywau-plugin-fw-loader.php:188
msgid "Premium Version"
msgstr "Versione premium"

#: ../lib/class.ywau-plugin-fw-loader.php:184
msgid "Settings"
msgstr "Impostazioni"

#: ../lib/class.ywau-plugin-fw-loader.php:217
msgid "Plugin Documentation"
msgstr "Documentazione plugin"

#: ../lib/class.ywau-plugin-fw-loader.php:230
msgid ""
"YITH WooCommerce Uploads is available in an outstanding PREMIUM version with "
"many new options, discover it now."
msgstr ""
"Un’eccezionale versione PREMIUM di YITH WooCommerce Uploads è disponibile "
"con molte nuove opzioni. Scoprila adesso."

#: ../lib/class.ywau-plugin-fw-loader.php:231
msgid "Premium version"
msgstr "Versione premium"

#: ../lib/class.ywau-plugin-fw-loader.php:238
msgid "YITH WooCommerce Uploads"
msgstr "YITH WooCommerce Uploads"

#: ../lib/class.ywau-plugin-fw-loader.php:239
msgid ""
"In YIT Plugins tab, you can find the YITH WooCommerce Uploads options.<br> "
"From this menu, you can access all settings of the activated YITH plugins."
msgstr ""
"Puoi trovare il pannello opzioni di YITH WooCommerce Uploads nella tab YIT "
"Plugins.<br> Da questo menu, potrai accedere a tutte le impostazioni dei "
"plugin YITH finora attivati."

#: ../lib/emails/class.ywau-order-admin-message.php:38
msgid "Send a message to the customer regarding the order"
msgstr "Invia al cliente un messaggio relativo al suo ordine"

#: ../lib/emails/class.ywau-order-admin-message.php:41
#: ../lib/emails/class.ywau-order-file-actions.php:36
#: ../lib/emails/class.ywau-order-file-deleted.php:35
msgid "Your order has been updated"
msgstr "Il tuo ordine è stato aggiornato"

#: ../lib/emails/class.ywau-order-admin-message.php:42
msgid ""
"[{site_title}] Your order ({order_number}) - {order_date} has been updated"
msgstr ""
"[{site_title}] Il tuo ordine ({order_number}) - {order_date} è stato "
"aggiornato"

#: ../lib/emails/class.ywau-order-admin-message.php:174
#: ../lib/emails/class.ywau-order-file-actions.php:164
#: ../lib/emails/class.ywau-order-file-deleted.php:165
#: ../lib/emails/class.ywau-order-file-uploaded.php:170
msgid "Enable/Disable"
msgstr "Abilita/Disabilita"

#: ../lib/emails/class.ywau-order-admin-message.php:176
#: ../lib/emails/class.ywau-order-file-actions.php:166
#: ../lib/emails/class.ywau-order-file-deleted.php:167
#: ../lib/emails/class.ywau-order-file-uploaded.php:172
msgid "Enable this email notification"
msgstr "Abilita notifiche a questa email"

#: ../lib/emails/class.ywau-order-admin-message.php:180
#: ../lib/emails/class.ywau-order-file-actions.php:170
#: ../lib/emails/class.ywau-order-file-deleted.php:178
#: ../lib/emails/class.ywau-order-file-uploaded.php:183
msgid "Subject"
msgstr "Oggetto"

#: ../lib/emails/class.ywau-order-admin-message.php:182
#: ../lib/emails/class.ywau-order-admin-message.php:189
#: ../lib/emails/class.ywau-order-file-actions.php:172
#: ../lib/emails/class.ywau-order-file-actions.php:179
#: ../lib/emails/class.ywau-order-file-deleted.php:180
#: ../lib/emails/class.ywau-order-file-deleted.php:187
#: ../lib/emails/class.ywau-order-file-uploaded.php:185
#: ../lib/emails/class.ywau-order-file-uploaded.php:192
#, php-format
msgid "Defaults to <code>%s</code>"
msgstr "Di default <code>%s</code>"

#: ../lib/emails/class.ywau-order-admin-message.php:187
#: ../lib/emails/class.ywau-order-file-actions.php:177
#: ../lib/emails/class.ywau-order-file-deleted.php:185
#: ../lib/emails/class.ywau-order-file-uploaded.php:190
msgid "Email Heading"
msgstr "Intestazione email"

#: ../lib/emails/class.ywau-order-admin-message.php:194
#: ../lib/emails/class.ywau-order-file-actions.php:184
#: ../lib/emails/class.ywau-order-file-deleted.php:192
#: ../lib/emails/class.ywau-order-file-uploaded.php:197
msgid "Email type"
msgstr "Tipo di email"

#: ../lib/emails/class.ywau-order-admin-message.php:196
#: ../lib/emails/class.ywau-order-file-actions.php:186
#: ../lib/emails/class.ywau-order-file-deleted.php:194
#: ../lib/emails/class.ywau-order-file-uploaded.php:199
msgid "Choose which format of email to send."
msgstr "Scegli il formato per l’email da inviare"

#: ../lib/emails/class.ywau-order-file-actions.php:34
msgid ""
"When a file uploaded by customers is accepted or rejected, they will receive "
"an email"
msgstr ""
"Il cliente riceverà un’email quando un file da lui caricato viene accettato "
"o rifiutato"

#: ../lib/emails/class.ywau-order-file-actions.php:37
msgid "Your {site_title} order from {order_date} has been updated"
msgstr "Il tuo ordine {site_title} del {order_date} è stato aggiornato"

#: ../lib/emails/class.ywau-order-file-deleted.php:33
msgid "Be notified when the customer delete a file from the order"
msgstr "Notificami quando il cliente cancella un file dal suo ordine"

#: ../lib/emails/class.ywau-order-file-deleted.php:36
msgid "[{site_title}] - Order {order_number}, customer deleted a file"
msgstr "[{site_title}] - Ordine {order_number}, il cliente ha rimosso un file"

#: ../lib/emails/class.ywau-order-file-deleted.php:171
#: ../lib/emails/class.ywau-order-file-uploaded.php:176
msgid "Recipient(s)"
msgstr "Destinatari"

#: ../lib/emails/class.ywau-order-file-deleted.php:173
#: ../lib/emails/class.ywau-order-file-uploaded.php:178
#, php-format
msgid ""
"Enter recipients (comma separated) for this email. Defaults to <code>%s</"
"code>."
msgstr ""
"Inserisci i destinatari (separati da virgole) di questa email. Di default a "
"<code>%s</code>."

#: ../lib/emails/class.ywau-order-file-uploaded.php:38
msgid "Be notified when the customer upload a file for the order"
msgstr "Notificami quando il cliente carica un file per il suo ordine"

#: ../lib/emails/class.ywau-order-file-uploaded.php:40
msgid "A file has been uploaded by the customer"
msgstr "Il cliente ha caricato un file"

#: ../lib/emails/class.ywau-order-file-uploaded.php:41
msgid "[{site_title}] - Order {order_number}, a new file has been uploaded"
msgstr "[{site_title}] - Ordine {order_number}, è stato caricato un nuovo file"

#: ../plugin-options/general-options.php:20
msgid "General settings"
msgstr "Impostazioni generali"

#: ../plugin-options/general-options.php:25
msgid "Thumbnail width"
msgstr "Larghezza miniatura"

#: ../plugin-options/general-options.php:27
msgid "Set the width of the thumbnails in pixel."
msgstr "Imposta la larghezza delle miniatura in pixel."

#: ../plugin-options/general-options.php:37
msgid "Thumbnail height"
msgstr "Altezza miniatura"

#: ../plugin-options/general-options.php:39
msgid "Set the height of the thumbnails in pixel."
msgstr "Imposta l’altezza delle miniatura in pixel."

#: ../plugin-options/general-options.php:49
msgid "Thumbnail quality"
msgstr "Qualità miniatura"

#: ../plugin-options/general-options.php:51
msgid "Set the quality (in %) of the thumbnails."
msgstr "Imposta la qualità (in %) per le miniature."

#: ../plugin-options/general-options.php:62
msgid "Allow on cart"
msgstr "Abilita nel carrello"

#: ../plugin-options/general-options.php:63
msgid "Use this option to allow users to attach a file even from the cart"
msgstr ""
"Utilizza questa opzione per permettere agli utenti di allegare un file anche "
"dal carrello"

#: ../plugin-options/general-options.php:69
msgid "Allow on checkout you page"
msgstr "Abilita nella pagina checkout"

#: ../plugin-options/general-options.php:70
msgid "Use this option to allow users to attach a file from the checkout page"
msgstr ""
"Utilizza questa opzione per permettere agli utenti di allegare file nella "
"pagina Checkout"

#: ../plugin-options/general-options.php:76
msgid "Allow on thank you page"
msgstr "Abilita nella pagina di conferma acquisto"

#: ../plugin-options/general-options.php:77
msgid "Use this option to allow users to attach a file from the thankyou page"
msgstr ""
"Abilitando questa opzione permetti agli utenti di allegare un file anche "
"dalla pagina di conferma acquisto"

#: ../plugin-options/general-options.php:83
msgid "Allow on myaccount"
msgstr "Consenti su my account"

#: ../plugin-options/general-options.php:84
msgid ""
"Use this option to allow users to attach a file to an order from myaccount "
"page"
msgstr ""
"Utilizza questa opzione per permettere agli utenti di allegare un file ad un "
"ordine dalla pagina myaccount"

#: ../plugin-options/general-options.php:110
msgid "Allow the file upload when the order status is:"
msgstr "Consenti il caricamento del file quando lo stato dell’ordine è:"

#: ../plugin-options/general-options.php:134
msgid "Allow the file deleting when the order status is:"
msgstr "Consenti la rimozione del file quando lo stato dell’ordine è:"

#: ../plugin-options/general-options.php:144
msgid "Upload folder"
msgstr "Cartella caricamenti"

#: ../plugin-options/general-options.php:146
msgid ""
"Set a folder in which saving the files that are uploaded by users. The "
"folder will be create in wp-content/uploads/yith-additional-uploads."
msgstr ""
"Seleziona una cartella in cui salvare i file caricati dagli utenti. La "
"cartella sarà creata in wp-content/uploads/yith-additional-uploads."

#: ../plugin-options/general-options.php:152
msgid "Storing mode"
msgstr "Modalità di salvataggio"

#: ../plugin-options/general-options.php:154
msgid ""
"Choose whether to use the ID or the order number as name of the folder where "
"you want to store the files linked to a specific order."
msgstr ""
"Scegli se utilizzare l’ID or il numero dell’ordine come nome della cartella "
"in cui verranno salvati i file relativi ad un ordine specifico."

#: ../plugin-options/general-options.php:157
msgid "Order ID"
msgstr "ID ordine"

#: ../plugin-options/general-options.php:158
msgid "Order number"
msgstr "Numero ordine"

#: ../plugin-options/general-options.php:164
msgid "Split products on cart"
msgstr "Dividi i prodotti nel carrello"

#: ../plugin-options/general-options.php:166
msgid "Learn more"
msgstr "Per saperne di più"

#: ../plugin-options/general-options.php:166
msgid ""
"Choose whether to use standard WooCommerce behaviour that groups more items "
"of the same products in one line, giving users the possibility to  upload "
"the same files for all the items (e.g. 3 exact copies of the same \"Fashion "
"Calendar\") or to add to cart one item for each line and allow users to "
"upload different files for each item (e.g. 3 items of \"Fashion Calendar\" "
"with different pictures each). <a href=\"http://hitoutlets.com/docs-plugins/"
"yith-woocommerce-uploads/04-uploads-rules.html\" title=\""
msgstr ""
"Scegli se usare il comportamento standard di WooCommerce che raggruppa in "
"una riga più elementi degli stessi prodotti, dando agli utenti la "
"possibilità di caricare gli stessi file per tutti gli elementi (ad esempio: "
"3 copie esatte dello stesso \"Calendario Fashion\") o di aggiungere al "
"carrello un elemento per ogni riga e permettere agli utenti di caricare file "
"diversi per ogni elemento (ad esempio: 3 elementi di \"Calendario Fashion\" "
"ognuno con immagini diverse). <a href=\"http://hitoutlets.com/docs-plugins/"
"yith-woocommerce-uploads/04-uploads-rules.html\" title=\""

#: ../plugin-options/general-options.php:172
msgid "Product Upload rules"
msgstr "Regole di caricamento prodotto"

#: ../plugin-options/general-options.php:178
msgid "Enable uploads for products"
msgstr "Attiva i caricamenti per i prodotti"

#: ../plugin-options/general-options.php:179
msgid ""
"Enable the upload rules for products, the customer could attach files to the "
"products, according to the upload rules"
msgstr ""
"Attiva le regole di caricamento, il cliente potrà allegare i file ai "
"prodotti in base alle regole di caricamento"

#: ../plugin-options/general-options.php:186
msgid "Enable uploads for orders"
msgstr "Attiva i caricamenti per gli ordini"

#: ../plugin-options/general-options.php:187
msgid ""
"Enable the upload rules for orders, the customer could attach files to the "
"while order, according to the upload rules"
msgstr ""
"Attiva le regole di caricamento per gli ordini, il cliente potrà allegare i "
"file all'intero ordine, in base alle regole di caricamento"

#: ../plugin-options/general-options.php:194
msgid "Order upload text"
msgstr "Testo per caricamento file associato agli ordini"

#: ../plugin-options/general-options.php:195
msgid ""
"Set the message to show on cart and checkout pages for uploads attached to "
"the whole order."
msgstr ""
"Imposta il messaggio da mostrae nelle pagine carrello e cassa per "
"evidenziare il caricamento di files associati all'ordine."

#: ../templates/admin/file-upload-rules.php:51
msgid "Label:"
msgstr "Etichetta:"

#: ../templates/admin/file-upload-rules.php:55
msgid "Allowed extensions:"
msgstr "Estensioni file consentite:"

#: ../templates/admin/file-upload-rules.php:57
msgid ""
"Add the allowed file formats, writing the extensions divided by comma (e.g., "
"jpg, png, gif)."
msgstr ""
"Aggiungi formati consentiti scrivendone l’estensione e separandoli con "
"virgole (e.g., jpg, png, gif)."

#: ../templates/admin/file-upload-rules.php:60
msgid "Max size:"
msgstr "Dimensione massima:"

#: ../templates/admin/file-upload-rules.php:62
msgid "Maximum allowed size in MB"
msgstr "Dimensione massima consentita in MB"

#: ../templates/admin/file-upload-rules.php:65
msgid "Allow notes:"
msgstr "Abilita note:"

#: ../templates/admin/file-upload-rules.php:70
msgid "Delete"
msgstr "Elimina"

#: ../templates/admin/file-upload-rules.php:89
msgid "Add new rule"
msgstr "Aggiungi nuova regola"

#: ../templates/emails/plain/ywau-notify-order-admin-message.php:16
#: ../templates/emails/plain/ywau-order-admin-message.php:16
#: ../templates/emails/plain/ywau-order-file-actions.php:16
#, php-format
msgid "You have received an order from %s."
msgstr "Hai ricevuto un ordine da %s."

#: ../templates/emails/plain/ywau-notify-order-admin-message.php:22
#: ../templates/emails/plain/ywau-order-admin-message.php:22
#: ../templates/emails/plain/ywau-order-file-actions.php:22
#, php-format
msgid "Order number: %s"
msgstr "Numero ordine: %s"

#: ../templates/emails/plain/ywau-notify-order-admin-message.php:23
#: ../templates/emails/plain/ywau-order-admin-message.php:23
#: ../templates/emails/plain/ywau-order-file-actions.php:23
msgid "jS F Y"
msgstr "jS F Y"

#: ../templates/emails/plain/ywau-notify-order-admin-message.php:37
#: ../templates/emails/plain/ywau-order-admin-message.php:37
#: ../templates/emails/plain/ywau-order-file-actions.php:37
#, php-format
msgid "View order: %s"
msgstr "Visualizza ordine: %s"

#: ../templates/emails/ywau-notify-order-admin-message.php:21
#: ../templates/emails/ywau-order-admin-message.php:21
#: ../templates/emails/ywau-order-file-actions.php:18
#: ../templates/emails/ywau-order-file-deleted.php:20
#: ../templates/emails/ywau-order-file-uploaded.php:22
#, php-format
msgid "Order #%s"
msgstr "Ordine #%s"

#: ../templates/emails/ywau-notify-order-admin-message.php:30
#: ../templates/emails/ywau-order-admin-message.php:30
#: ../templates/emails/ywau-order-file-actions.php:27
#: ../templates/emails/ywau-order-file-deleted.php:29
#: ../templates/emails/ywau-order-file-uploaded.php:31
msgid "Product"
msgstr "Prodotto"

#: ../templates/emails/ywau-notify-order-admin-message.php:32
#: ../templates/emails/ywau-order-admin-message.php:32
#: ../templates/emails/ywau-order-file-actions.php:29
#: ../templates/emails/ywau-order-file-deleted.php:31
#: ../templates/emails/ywau-order-file-uploaded.php:33
msgid "Quantity"
msgstr "Quantità"

#: ../templates/emails/ywau-notify-order-admin-message.php:34
#: ../templates/emails/ywau-order-admin-message.php:34
#: ../templates/emails/ywau-order-file-actions.php:31
#: ../templates/emails/ywau-order-file-deleted.php:33
#: ../templates/emails/ywau-order-file-uploaded.php:35
msgid "Price"
msgstr "Prezzo"

#: ../templates/emails/ywau-notify-order-admin-message.php:81
#: ../templates/emails/ywau-order-admin-message.php:83
msgid "Regarding the current order, we would like to inform you that:"
msgstr "Relativamente al presente ordine, vorremmo informarla che:"

#: ../templates/emails/ywau-order-file-actions.php:74
#, php-format
msgid "The %s file has been accepted"
msgstr "Il file %s è stato accettato"

#: ../templates/emails/ywau-order-file-actions.php:76
#, php-format
msgid "The %s file has been rejected."
msgstr "Il file %s è stato rifiutato."

#: ../templates/emails/ywau-order-file-actions.php:79
#, php-format
msgid "This is the reason why: %s"
msgstr "Questo il motivo: %s"

#: ../templates/emails/ywau-order-file-actions.php:84
msgid "The file you sent is waiting for verification."
msgstr "Il file inviato è in attesa di verifica."

#: ../templates/emails/ywau-order-file-actions.php:86
#, php-format
msgid ""
"This file you sent was not compliant with the required specifications. (Max "
"size: %s MB, allowed extensions: %s"
msgstr ""
"Il file inviato non è conforme alle specifiche richieste (Dimensione "
"massima: %s MB, estensioni consentite: %s"

#: ../templates/emails/ywau-order-file-deleted.php:15
#, php-format
msgid "The customer %s deleted a file from the order."
msgstr "Il cliente %s ha rimosso un file dall’ordine."

#: ../templates/emails/ywau-order-file-uploaded.php:17
msgid "The customer has uploaded a file to the order."
msgstr "Il cliente ha caricato un file all'ordine."

#~ msgid "Upload rules"
#~ msgstr "Regole di caricamento"

#~ msgid "The customer %s has uploaded a file to the order."
#~ msgstr "Il cliente %s ha allegato un file all’ordine."

#~ msgid "You order has been updated"
#~ msgstr "Il tuo ordine è stato aggiornato"

#~ msgid "This review is closed for replies"
#~ msgstr "This review is closed to replies"

#~ msgid "This review is closed for comments"
#~ msgstr "This review is closed to comments"

#~ msgid "Product id is in invalid format"
#~ msgstr "Product id is an invalid format"

#~ msgid "Voting/Reviews settings"
#~ msgstr "Voting/Review settings"

#~ msgid ""
#~ "In the YIT Plugins tab you can find the YITH WooCommerce Advanced Reviews "
#~ "options. With this menu, you can access to all the settings of our "
#~ "plugins that you have activated."
#~ msgstr ""
#~ "In YIT Plugins tab you can find YITH WooCommerce Advanced Reviews "
#~ "options. From this menu you can access all settings of YITH plugins "
#~ "activated."

#~ msgid ""
#~ "From now on, you can find all the options of YITH WooCommerce Advanced "
#~ "Reviews under YIT Plugin -> Advanced Reviews instead of WooCommerce -> "
#~ "Settings -> Advanced Reviews, as in the previous version. When one of our "
#~ "plugins updates, a new voice will be added to this menu."
#~ msgstr ""
#~ "From now on, you can find all YITH WooCommerce Advanced Reviews options "
#~ "in YIT Plugin -> Advanced Reviews instead of WooCommerce -> Settings -> "
#~ "Advanced Reviews, as in the previous version. Any time one of our plugins "
#~ "is updated, a new entry will be added to this menu."

#~ msgid "The layout for this "
#~ msgstr "Layout for this "

#~ msgid ""
#~ "The word used for the URL of each project (the slug of post if empty)"
#~ msgstr ""
#~ "Univocal identification name in the URL for each product (slug from post "
#~ "if empty)"

#~ msgid "Label Singular"
#~ msgstr "Label in Singular"

#~ msgid ""
#~ "Set the label in singular to use for each label (the title of portfolio "
#~ "if empty)"
#~ msgstr "Set a label in singular (title of portfolio if empty)"

#~ msgid "Label Plural"
#~ msgstr "Label in Plural"

#~ msgid ""
#~ "Set the label in plural to use for each label (the title of portfolio if "
#~ "empty)"
#~ msgstr "Set a label in plural (title of portfolio if empty)"

#~ msgid ""
#~ "If you want to use a category section for the portfolio, set the name of "
#~ "taxonomy. Name should be in slug form (must not contain capital letters "
#~ "or spaces) and not more than 32 characters long (database structure "
#~ "restriction)."
#~ msgstr ""
#~ "If you want to use categories in the portfolio, set a name for taxonomy. "
#~ "Name should be a slug (must not contain capital letters nor spaces) and "
#~ "must not be more than 32 characters long (database structure restriction)."

#~ msgid "Set the word to use in the URL for each category page."
#~ msgstr "Set the word for each category page URL."

#~ msgid "The layout for single page of this portfolio"
#~ msgstr "Layout for single page of this portfolio"

#~ msgid "Show the frontend of the %s"
#~ msgstr "Show frontend of the %s"

#~ msgid ""
#~ "The changes you made will be lost if you navigate away from this page."
#~ msgstr "The changes you made will be lost if you leave this page."

#~ msgid ""
#~ "If you continue with this action, you will reset all options are in this "
#~ "page."
#~ msgstr ""
#~ "If you go on with this action, you will reset all options in this page."

#~ msgid ""
#~ "The element you have written is already exists. Please, add another name."
#~ msgstr "This element already exists. Please, enter a different name."

#~ msgid "An error encoured during during import. Please try again."
#~ msgstr "An error has occurred during import. Please try again."

#~ msgid "The file you have insert doesn't valid."
#~ msgstr "The file inserted is not valid."

#~ msgid "I'm sorry, the import featured is disabled."
#~ msgstr "Sorry, import is disabled."

#~ msgid "Sorting done correctly."
#~ msgstr "Sorting successful."

#~ msgid ""
#~ "From now on, you can find all the options of your plugins under the YIT "
#~ "Plugin menu voice.\n"
#~ "                                     For every installation of our new "
#~ "plugins, a new voice will be added to access to the customization "
#~ "settings."
#~ msgstr ""
#~ "From now on, you can find all plugin options in YIT Plugin menu.\n"
#~ "                                     For each plugin installed, "
#~ "customization settings will be available as a new entry in YIT Plugin "
#~ "menu."

#~ msgid ""
#~ "From now on, you can find all the options of your plugins under the YIT "
#~ "Plugin menu voice.\n"
#~ "                                    When one of our plugins updates, a "
#~ "new voice will be added to this menu.\n"
#~ "                                    For example, after the update, the "
#~ "options from the plugins (YITH WooCommerce Wishlist, YITH WooCommerce "
#~ "Ajax Search, etc.)\n"
#~ "                                    will be removed from the previous "
#~ "location and moved under the YIT Plugin tab."
#~ msgstr ""
#~ "From now on, you can find all the options of your plugins in YIT Plugin "
#~ "menu.\n"
#~ "                                    Any time one of our plugins is "
#~ "updated, a new entry will be added to this menu.\n"
#~ "                                    For example, after update, plugin "
#~ "options (such for YITH WooCommerce Wishlist, YITH WooCommerce Ajax "
#~ "Search, etc.)\n"
#~ "                                    will be moved from previous location "
#~ "to YIT Plugin tab."

#~ msgid ""
#~ "There is a new version of %1$s available. <a href=\"%2$s\" class="
#~ "\"thickbox yit-changelog-button\" title=\"%3$s\">View version %4$s "
#~ "details</a>. <em>You have to activate the plugin on a single site of the "
#~ "network to benefit from the automatic updates.</em>"
#~ msgstr ""
#~ "There is a new version of %1$s available. <a href=\"%2$s\" class="
#~ "\"thickbox yit-changelog-button\" title=\"%3$s\">View version %4$s "
#~ "details</a>. <em>You have to activate the plugin on a single site of the "
#~ "network to benefit from automatic updates.</em>"

#~ msgid "%field% field can not be empty"
#~ msgstr "%field% field cannot be empty"

#~ msgid "%field_1% and %field_2% fields can not be empty"
#~ msgstr "%field_1% and %field_2% fields cannot be empty"

#~ msgid "Software has been deactive"
#~ msgstr "Software has been deactivated"

#~ msgid "Licence key has be banned"
#~ msgstr "Licence key has been banned"

#~ msgid "To Active"
#~ msgstr "Activation"

#~ msgid "Insert the title of field."
#~ msgstr "Insert title for the field."

#~ msgid ""
#~ "REQUIRED: The identification name of this field, that you can insert into "
#~ "body email configuration. <strong>Note:</strong>Use only lowercase "
#~ "characters and underscores."
#~ msgstr ""
#~ "REQUIRED: Field identification name to be entered into email body. "
#~ "<strong>Note:</strong>Use only lowercase characters and underscores."

#~ msgid "Select the type of this field."
#~ msgstr "Select type for this field."

#~ msgid "Select this if it's the email where you can reply."
#~ msgstr "Select this if it is the email you can reply to."

#~ msgid ""
#~ "Insert an additional class(es) (separateb by comma) for more "
#~ "personalization."
#~ msgstr ""
#~ "Insert additional class(es) (separated by commas) for more "
#~ "personalization."

#~ msgid "Choose how much long will be the field."
#~ msgstr "Set field length."

#~ msgid "The content of the tab. (HTML is supported)"
#~ msgstr "Content of the tab. (HTML is supported)"

#~ msgid "Sidebar Left"
#~ msgstr "Left sidebar"

#~ msgid "Sidebar Right"
#~ msgstr "Right sidebar"

#~ msgid "Add a title field on reviews."
#~ msgstr "Add a title field to reviews."

#~ msgid "Set maximum number of attachments selectable (0 = no limit)."
#~ msgstr ""
#~ "Set maximum number of attachments that can be selected (0 = no limit)."

#~ msgid "Reviews summary options"
#~ msgstr "Review summary options"

#~ msgid "Fixed bars color"
#~ msgstr "Fixed bar color"

#~ msgid "Percentage bars color"
#~ msgstr "Percentage bar color"

#~ msgid "Show people's votes"
#~ msgstr "Show review votes"

#~ msgid "Add a string stating how many people's found the review useful."
#~ msgstr "Add a string stating how many people found the review useful."

#~ msgid "Customers reviews"
#~ msgstr "Customers' reviews"
