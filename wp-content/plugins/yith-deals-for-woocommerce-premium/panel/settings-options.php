<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */


return array(

    'settings' => apply_filters( 'yith_wcdls_settings_options', array(

            //////////////////////////////////////////////////////

            'yith_wcdls_settings_options_start'    => array(
                'type' => 'sectionstart',
                'id'   => 'yith_wcdls_settings_tab_start'
            ),

            'yith_wcdls_settings_options_title'    => array(
                'title' => _x( 'General settings', 'Panel: page title', 'yith-deals-for-woocommerce' ),
                'type'  => 'title',
                'desc'  => '',
                'id'    => 'yith_wcdls_settings_tab_general_settings'
            ),
            'yith_wcdls_settings_box_size' => array(
                'title'   => _x( 'Popup Size', 'Admin option: Popup Size', 'yith-deals-for-woocommerce' ),
                'type'    => 'yith_wcdls_box_size',
                'default'   => array(
                    'width'     => '700',
                    'height'    => '700'
                ),
                'desc'    => _x( 'Set popup size.', 'Admin option description: Set popup size.', 'yith-deals-for-woocommerce' ),
                'id'      => 'yith-wcdls-box-size-pixel',
            ),

            'yith_wcdls_settings_options_end'      => array(
                'type' => 'sectionend',
                'id'   => 'yith_wcdls_settings_tab_end'
            ),

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        )
    )
);
