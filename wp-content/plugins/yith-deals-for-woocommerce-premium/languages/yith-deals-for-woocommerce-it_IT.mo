��    i      d  �   �       	     	     	     +	     ?	     L	     T	     b	     q	     �	  9   �	  #   �	  �   

     �
     �
     �
     �
  	   �
  3   �
  <   �
     <     B     G     M     b     p     w     �     �     �     �  .   �     �          
  
             %     .     E     Y     a     h     x     �     �     �     �     �     �     �  	   �            	   1     ;     K     d  
   i     t     �  "   �     �     �     �     �     �                       	   1  !   ;     ]  #   o     �     �     �     �     �     �     �  #   �          !     6     =     N     R     r     �     �     �     �  B   �     �  
   �  #     '   +  "   S  Q   v     �  "   �  %   �  
   !    ,     �     �     �     �               5  $   G     l      y     �  �   �     7     =  $   S  	   x  	   �  6   �  >   �          	               2     D     L     g          �  
   �     �     �     �     �     �     �     �            
   &     1     6     K     W     n     �  &   �     �     �     �  	   �     �               *  "   A     d     l     y     �     �     �     �     �     �     �     �                 
   /  %   :     `  $   q     �  
   �     �  	   �     �     �     �  *   �     )     6     P     V     p     t     �     �     �     �     �  B   �            &   1  *   X     �  R   �     �                    O           
       ;   #   _      L       /      d           S   g   :   G   \           P             0       B       b   ,      U   C   %   @   R       >   F              6             *   Z   e   ^   7   	                     Q   $      X              c       Y       D           !              2   [       T         V   f   K       H   <   I       =   9              -       "   E         )   ?   M          h       a           W   8              '      4   N      i   `   3   1      5       &           (      ]   +                A               .      J    + Add new condition - Add product options Accept Offer button Accept offer Add new Add new offer Add product(s) Add these products to the cart Admin menu nameYITH Offers Admin option description: Set popup size.Set popup size. Admin option: Popup SizePopup Size Alert Message: WooCommerce requiresYITH Deals for WooCommerce is enabled but not effective. It requires WooCommerce in order to work. All All YITH Offers Back to Deals list Category Changelog Check this option if you want to disable this offer Choose the offer's layout that will be seen by your customer Close Deal Deals Decline Offer button Decline offer Delete Delete %s permanently Delete Permanently Delete permanently Disable: Disabled Do
                                    nothing Does not contain Edit Edit %s Edit Offer Enabled Equal to Fixed product discount Fixed product price Forever From:  Geolocalization Greater than Greater than or equal to Help Center Hide offer:  In line with page content Include all Include at least one of Layout Less than Less than or equal to Move %s to the Trash New Offer No Offers found No Offers found in trash None Offer name Offer settings Only this time Panel: page titleGeneral settings Parent Offers Percentage product discount Plugin documentation Popover Popup Premium Version Price Product Product discount Published Remove all products from the cart Remove product(s) Remove these products from the cart Repeat offer? Restore Restore %s from the Trash Restriction by: Role Search Offers Select layout Select the offer availability dates Settings Show another offer:  Status Support platform Tag This is where deals are stored. Time-based offer To:  Trash Type of discount Type of restriction: Use these rules to decide when your offer is going to be displayed User View Offer What to do if the offer is accepted What to do if the offer is not accepted When will your offer be displayed? You didn't create any other offer. Please create another offer to use this option current version plugin name in admin WP menuDeals plugin name in admin page titleDeals verbTrash Project-Id-Version: YITH Deals for WooCommerce
POT-Creation-Date: 2018-01-04 22:41+0100
PO-Revision-Date: 2018-01-04 22:43+0100
Last-Translator: 
Language-Team: YITH <plugins@yithemes.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n!=1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + Aggiungi nuova condizione - Aggiungi opzioni prodotto Pulsante Accetta offerta Accetta l'offerta Aggiungi nuovo Aggiungi nuova offerta Aggiungi prodotti Aggiungi questi prodotti al carrello YITH Offerte Imposta le dimensioni del popup. Dimensione popup YITH Deals for WooCommerce è abilitato ma non in funzione. Devi aver installato WooCommerce perché questo possa funzionare correttamente. Tutti Tutte le offerte YITH Torna all'elenco di tutte le offerte Categoria Changelog Seleziona quest'opzione per disabilitare quest'offerta Scegli quale layout mostrare ai tuoi clienti per quest'offerta Chiudi Offerta Offerte Pulsante Rifiuta offerta Rifiuta l'offerta Elimina Elimina %s definitivamente Elimina definitivamente Elimina definitivamente Disabilita: Disabilita Non fare nulla Non contiene Modifica Modifica %s Modifica offerta Abilita Uguale a Sconto di importo fisso Prezzo fisso Per sempre Da:  Posizione geografica Maggiore di Maggiore di o uguale a Centro assistenza Nascondi l'offerta:  In linea con il contenuto della pagina Includi tutti Includi almeno uno di Layout Minore di Minore di o uguale a Sposta %s nel cestino Nuova offerta Nessun'offerta trovata Nessun'offerta trovata nel cestino Nessuno Nome offerta Impostazioni offerte Solo questa volta Impostazioni generali Offerte genitore Sconto percentuale Documentazione plugin Popover Popup Versione premium Prezzo Prodotto Sconto prodotto Pubblicate Rimuovi tutti i prodotti dal carrello Rimuovi prodotti Rimuovi questi prodotti dal carrello Ripetere l'offerta? Ripristina Ripristina %s dal cestino Limita a: Ruolo Cerca offerte Seleziona layout Seleziona le date di inizio e fine offerta Impostazioni Mostra un'altra offerta:  Stato Piattaforma di assistenza Tag Qui vengono salvate le offerte. Offerta a tempo A:  Cestino Tipo di sconto Tipo di limitazione: Usa queste regole per stabilire quando visualizzare la tua offerta Utente Visualizza offerta Cosa fare se l'offerta viene accettata Cosa fare se l'offerta non viene accettata Quando mostrare la tua offerta? Non hai creato nessun'altra offerta. Creane una per poter utilizzare quest'opzione versione attuale Deals Deals Cestina 