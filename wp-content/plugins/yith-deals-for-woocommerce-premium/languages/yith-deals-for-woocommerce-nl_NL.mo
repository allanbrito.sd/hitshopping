��    i      d  �   �       	     	     	     +	     ?	     L	     T	     b	     q	     �	  9   �	  #   �	  �   

     �
     �
     �
     �
  	   �
  3   �
  <   �
     <     B     G     M     b     p     w     �     �     �     �  .   �     �          
  
             %     .     E     Y     a     h     x     �     �     �     �     �     �     �  	   �            	   1     ;     K     d  
   i     t     �  "   �     �     �     �     �     �                       	   1  !   ;     ]  #   o     �     �     �     �     �     �     �  #   �          !     6     =     N     R     r     �     �     �     �  B   �     �  
   �  #     '   +  "   S  Q   v     �  "   �  %   �  
   !  �  ,     �     �     �               -     H  *   ^     �     �     �  g   �     ,     1     H  	   _     i  <   |  @   �     �                    &     :     F     ]     q     �     �  -   �  
   �     �  	   �     �     �  
             ,     ?     F     L  
   [     f          �     �  
   �     �     �  
   �     �          )     ;  (   V          �     �  	   �     �     �     �                    &     5     ;     C     R  +   _     �  +   �     �  
   �     �     
               0  0   B     s     �     �     �     �     �     �     �  
   �            ?      	   `     j  0   |  -   �  "   �  e   �     d     s     y          O           
       ;   #   _      L       /      d           S   g   :   G   \           P             0       B       b   ,      U   C   %   @   R       >   F              6             *   Z   e   ^   7   	                     Q   $      X              c       Y       D           !              2   [       T         V   f   K       H   <   I       =   9              -       "   E         )   ?   M          h       a           W   8              '      4   N      i   `   3   1      5       &           (      ]   +                A               .      J    + Add new condition - Add product options Accept Offer button Accept offer Add new Add new offer Add product(s) Add these products to the cart Admin menu nameYITH Offers Admin option description: Set popup size.Set popup size. Admin option: Popup SizePopup Size Alert Message: WooCommerce requiresYITH Deals for WooCommerce is enabled but not effective. It requires WooCommerce in order to work. All All YITH Offers Back to Deals list Category Changelog Check this option if you want to disable this offer Choose the offer's layout that will be seen by your customer Close Deal Deals Decline Offer button Decline offer Delete Delete %s permanently Delete Permanently Delete permanently Disable: Disabled Do
                                    nothing Does not contain Edit Edit %s Edit Offer Enabled Equal to Fixed product discount Fixed product price Forever From:  Geolocalization Greater than Greater than or equal to Help Center Hide offer:  In line with page content Include all Include at least one of Layout Less than Less than or equal to Move %s to the Trash New Offer No Offers found No Offers found in trash None Offer name Offer settings Only this time Panel: page titleGeneral settings Parent Offers Percentage product discount Plugin documentation Popover Popup Premium Version Price Product Product discount Published Remove all products from the cart Remove product(s) Remove these products from the cart Repeat offer? Restore Restore %s from the Trash Restriction by: Role Search Offers Select layout Select the offer availability dates Settings Show another offer:  Status Support platform Tag This is where deals are stored. Time-based offer To:  Trash Type of discount Type of restriction: Use these rules to decide when your offer is going to be displayed User View Offer What to do if the offer is accepted What to do if the offer is not accepted When will your offer be displayed? You didn't create any other offer. Please create another offer to use this option current version plugin name in admin WP menuDeals plugin name in admin page titleDeals verbTrash Project-Id-Version: YITH Deals for WooCommerce
POT-Creation-Date: 2018-01-05 08:24+0000
PO-Revision-Date: 2018-01-05 08:41+0000
Last-Translator: 
Language-Team: YITH <plugins@yithemes.com>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + Nieuwe voorwaarden toevoegen - Product opties toevoegen Aanbieding accepteren knop Aanbieding accepteren Nieuwe toevoegen Voeg nieuwe aanbieding toe Product(en) toevoegen Voeg deze producten toe aan de winkelwagen YITH Aanbiedingen Pop-up formaat instellen. Pop-up formaat YITH Deals for WooCommerce is ingeschakeld maar niet werkend. Het heeft WooCommerce nodig om te werken. Alle Alle YITH Aanbiedingen Terug naar deals lijst Categorie Wijzigingenlogboek Selecteer deze optie als u deze aanbieding wilt uitschakelen Kies de lay-out van de aanbieding die wordt getoond aan de klant Sluiten Deal Deals Aanbieding afwijzen knop Aanbieding afwijzen Verwijderen Verwijder %s permanent Verwijder permanent Permanent verwijderen Uitschakelen: Uitgeschakeld Doe
                                    niets Bevat niet Bewerk Bewerk %s Bewerk aanbieding Ingeschakeld Gelijk aan Vaste productkorting Vaste productprijs Altijd Van:  Geolokalisatie Groter dan Groter dan of gelijk aan Helpcentrum Aanbieding verbergen: In de inhoud van de pagina Bevat alle Bevat ten minste een van Lay-out Minder dan Minder dan of gelijk aan Verplaats %s naar prullenbak Nieuwe aanbieding Geen aanbiedingen gevonden Geen aanbiedingen gevonden in prullenbak Geen Naam aanbieding Aanbieding instellingen Alleen nu Algemene Instellingen Bovenliggende aanbiedingen Procentuele productkorting Plugin documentatie Pop-over Pop-up Premium versie Prijs Product Productkorting Gepubliceerd Verwijder alle producten uit de winkelwagen Verwijder product(en) Verwijder deze producten uit de winkelwagen Aanbieding herhalen? Herstellen Herstel %s van de prullenbak Beperking door: Rol Zoek aanbiedingen Selecteer lay-out Selecteer de beschikbare dagen van de aanbieding Instellingen Toon andere aanbieding:  Status Support platform Tag Hier worden alle deals bewaart Op tijd gebaseerde aanbieding Tot:  Prullenbak Type korting Type beperking: Gebruik deze regels om te bepalen of uw offer wordt weergegeven Gebruiker Bekijk aanbieding Wat gebeurt er als de aanbieding is geaccepteerd Wat gebeurt er als de aanbieding is afgewezen Wanneer wordt u offer weergegeven? U heeft geen andere aanbieding gemaakt. Maak een andere aanbieding, zodat u deze optie kunt gebruiken huidige versie Deals Deals Verplaatsen naar prullenbak 