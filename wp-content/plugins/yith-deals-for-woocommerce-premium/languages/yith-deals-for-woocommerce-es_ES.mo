��    i      d  �   �       	     	     	     +	     ?	     L	     T	     b	     q	     �	  9   �	  #   �	  �   

     �
     �
     �
     �
  	   �
  3   �
  <   �
     <     B     G     M     b     p     w     �     �     �     �  .   �     �          
  
             %     .     E     Y     a     h     x     �     �     �     �     �     �     �  	   �            	   1     ;     K     d  
   i     t     �  "   �     �     �     �     �     �                       	   1  !   ;     ]  #   o     �     �     �     �     �     �     �  #   �          !     6     =     N     R     r     �     �     �     �  B   �     �  
   �  #     '   +  "   S  Q   v     �  "   �  %   �  
   !  �  ,     �     �          -     <     J     _  "   s     �  -   �     �  d   �     S     Y     p  
   �     �  .   �  2   �                    $     >     N     U     o     �  
   �  	   �  1   �     �     �  	   �                         =     V     ^     f  	   w     �     �     �  '   �     �     �                    (     ?     L     [     y     ~     �     �     �     �  &   �             	   &     0     A     H     Q  
   j  &   u     �  "   �     �  	   �     �               %     4  )   E     o     w     �     �     �  (   �     �     �     �            @   0     q  
   y  !   �  $   �      �  P   �     =     M     U     ]     O           
       ;   #   _      L       /      d           S   g   :   G   \           P             0       B       b   ,      U   C   %   @   R       >   F              6             *   Z   e   ^   7   	                     Q   $      X              c       Y       D           !              2   [       T         V   f   K       H   <   I       =   9              -       "   E         )   ?   M          h       a           W   8              '      4   N      i   `   3   1      5       &           (      ]   +                A               .      J    + Add new condition - Add product options Accept Offer button Accept offer Add new Add new offer Add product(s) Add these products to the cart Admin menu nameYITH Offers Admin option description: Set popup size.Set popup size. Admin option: Popup SizePopup Size Alert Message: WooCommerce requiresYITH Deals for WooCommerce is enabled but not effective. It requires WooCommerce in order to work. All All YITH Offers Back to Deals list Category Changelog Check this option if you want to disable this offer Choose the offer's layout that will be seen by your customer Close Deal Deals Decline Offer button Decline offer Delete Delete %s permanently Delete Permanently Delete permanently Disable: Disabled Do
                                    nothing Does not contain Edit Edit %s Edit Offer Enabled Equal to Fixed product discount Fixed product price Forever From:  Geolocalization Greater than Greater than or equal to Help Center Hide offer:  In line with page content Include all Include at least one of Layout Less than Less than or equal to Move %s to the Trash New Offer No Offers found No Offers found in trash None Offer name Offer settings Only this time Panel: page titleGeneral settings Parent Offers Percentage product discount Plugin documentation Popover Popup Premium Version Price Product Product discount Published Remove all products from the cart Remove product(s) Remove these products from the cart Repeat offer? Restore Restore %s from the Trash Restriction by: Role Search Offers Select layout Select the offer availability dates Settings Show another offer:  Status Support platform Tag This is where deals are stored. Time-based offer To:  Trash Type of discount Type of restriction: Use these rules to decide when your offer is going to be displayed User View Offer What to do if the offer is accepted What to do if the offer is not accepted When will your offer be displayed? You didn't create any other offer. Please create another offer to use this option current version plugin name in admin WP menuDeals plugin name in admin page titleDeals verbTrash Project-Id-Version: YITH Deals for WooCommerce
POT-Creation-Date: 2018-01-24 11:50+0000
PO-Revision-Date: 2018-01-24 11:53+0000
Last-Translator: Fernando Tellado <fernando@tellado.es>
Language-Team: YITH <plugins@yithemes.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + Añadir nueva condición - Añadir opciones del producto Botón de aceptar oferta Aceptar oferta Añadir nueva Añadir nueva oferta Añadir producto(s) Añadir estos productos al carrito Ofertas YITH Establece el tamaño de la ventana emergente. Tamaño de ventana emergente YITH Deals for WooCommerce está activo pero no es efectivo. Requiere WooCommerce para que funcione. Todas Todas las ofertas YITH Volver a la lista de ofertas Categoría Registro de cambios Marca esta opción para desactivar esta oferta Elige el diseño de la oferta que verá tu cliente Cerrar Oferta Ofertas Botón de declinar oferta Declinar oferta Borrar Borrar %s permanentemente Borrar permanentemente Borrar permanentemente Inactivar: Inactivas No hacer
                                    nada No contiene Editar Editar %s Editar oferta Activas Igual a Descuento fijo en el producto Precio fijo del producto Siempre Desde:  Geolocalización Mayor que Mayor que o igual a Centro de ayuda Ocultar oferta:  En linea con el contenido de la página Incluye todos Incluye al menos uno de Diseño Menos de Menos de o igual a Mover %s a la papelera Nueva oferta No hay ofertas No hay ofertas en la papelera Nada Nombre de la oferta Ajustes de ofertas Sólo esta vez Ajustes generales Ofertas superiores Descuento en porcentaje en el producto Documentación del plugin Superpuesta Emergente Versión premium Precio Producto Descuento en el producto Publicadas Quitar todos los productos del carrito Eliminar producto(s) Quitar estos productos del carrito ¿Repetir oferta? Restaurar Restaurar %s de la papelera Restricción por: Perfil Buscar ofertas Elige el diseño Elige las fechas disponibles de la oferta Ajustes Mostrar otra oferta:  Estado Plataforma de soporte Etiqueta Aquí es donde se almacenan las ofertas. Oferta por tiempo limitado Hasta:  Papelera Tipo de descuento Tipo de restricción: Utiliza estas reglas para decidir cuándo se mostrará tu oferta Usuario Ver oferta Qué hacer si se acepta la oferta Qué hacer si no se acepta la oferta ¿Cuando se mostrará tu oferta? No has creado ninguna oferta. Por favor, crea otra oferta para usar esta opción versión actual Ofertas Ofertas Papelera 