<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( !defined( 'YITH_WCDLS_VERSION' ) ) {
    exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_Deals_Frontend_Premium
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author     Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
 *
 */
if ( !class_exists( 'YITH_Deals_Frontend_Premium' ) ) {
    /**
     * Class YITH_Deals_Frontend_Premium
     *
     * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
     */
    class YITH_Deals_Frontend_Premium extends YITH_Deals_Frontend
    {

        /**
         * Construct
         *
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         * @since 1.0
         */
        public function __construct()
        {
            add_filter( 'woocommerce_add_cart_item', array($this,'change_cart_item'), 10, 2 );
            add_filter( 'woocommerce_get_cart_item_from_session', array($this, 'change_cart_item_from_session'), 10, 3 );

            parent::__construct();
        }

        /**
         * Enqueue Scripts
         *
         * Register and enqueue scripts for Frontend
         *
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         * @since 1.0.0
         * @return void
         */
        public function enqueue_scripts()
        {
            //Publicar popup y pasar parámetros
            if( is_checkout() ) {
                wp_register_style('yith-wcdls-frontend-css', YITH_WCDLS_ASSETS_URL . 'css/wcdls-frontend.css');

                wp_register_script('yith-wcdls-frontend-premium', YITH_WCDLS_ASSETS_URL . 'js/wcdls-frontend-premium.js', array('jquery', 'jquery-ui-datepicker'), '1.0.0', 'true');
                wp_localize_script('yith-wcdls-frontend-premium', 'yith_wcdls', array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'popup_size' => get_option( 'yith-wcdls-box-size-pixel' ),

                ));
                wp_enqueue_style('yith-wcdls-frontend-css');
                wp_enqueue_script('yith-wcdls-frontend-premium');

                do_action('yith_wcdls_enqueue_fontend_scripts');


                add_action( 'wp_footer', array( $this, 'load_template' ) );
            }
        }

        /**
         * Load template
         *
         * Get offer
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         * @since 1.0.0
         * @return void
         */
        public function load_template() {

            if(!did_action('woocommerce_checkout_process') && !is_order_received_page()) {

                $offer = $this->get_offer();
                if ($offer) {
                    $deals_offer = get_post_meta($offer->ID, 'yith_wcdls_offer', true);

                    $args = apply_filters('yith_wcdls_popup_template_args', array(
                        'animation' => $deals_offer['type_layout'],
                        'content' => do_shortcode($offer->post_content),
                        'offer_id' => $offer->ID,
                    ));

                    ?>
                    <div class="yith-wcdls-deals-offer">
                        <?php
                        wc_get_template('yith-deals-popup.php', $args, '', YITH_WCDLS_TEMPLATE_PATH . 'frontend/');
                        ?>
                    </div>
                    <?php
                }
            }

        }

        /**
         * Get Offer
         *
         * Get offer
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         * @since 1.0.0
         * @return
         */
        public function get_offer() {
            $deals = get_deals();
            $function = YITH_Deals()->functions;
            $user = wp_get_current_user();
            $offer = false;
            foreach ($deals as $deal) {
                //delete_post_meta($deal->ID,'yith_wcdls_user_list');
                 $show_deal = $function->check_deal_to_show($deal,$user);
                 if($show_deal) {
                     $offer = $deal;
                     break;
                 }
            }

            if( $offer ) {
                return $offer;
            }

            return false;
        }

        public function change_cart_item($cart_item_data, $cart_item_key) {
            if ( isset( $cart_item_data[ 'yith_wcdls_type_offer' ] ) && isset( $cart_item_data['yith_wcdls_offer_value'] ) ) {
                $type = $cart_item_data[ 'yith_wcdls_type_offer' ];
                $value = $cart_item_data[ 'yith_wcdls_offer_value' ];

                switch ($type) {

                    case 'fixed_product_discount' :

                        $product = $cart_item_data[ 'data' ];
                        $price   = $product->get_price();
                        $price   = (float)$price - (float)$value;
                        if($price < 0) {
                            $price = 0;
                        }
                        $product->set_price( $price );
                        $cart_item_data[ 'data' ] = $product;

                        break;
                    case 'percentage_product_discount' :
                        $product = $cart_item_data[ 'data' ];
                        $price   = $product->get_price();
                        $percentage_discount = ((float)$price * (float)$value)/100;
                        $price = (float)$price - (float)$percentage_discount;
                        $product->set_price( $price );
                        $cart_item_data[ 'data' ] = $product;

                        break;
                    case 'fixed_product_price' :

                        $product = $cart_item_data[ 'data' ];
                        $product->set_price( (float)$value );
                        $cart_item_data[ 'data' ] = $product;

                        break;
                }
            }

            return $cart_item_data;
        }


        public function change_cart_item_from_session( $session_data, $cart_item, $cart_item_key ) {
            if ( isset( $session_data[ 'yith_wcdls_type_offer' ] ) && isset( $session_data['yith_wcdls_offer_value'] ) ) {

                $type = $session_data[ 'yith_wcdls_type_offer' ];
                $value = $session_data[ 'yith_wcdls_offer_value' ];

                switch ($type) {

                    case 'fixed_product_discount' :

                        $product = $session_data[ 'data' ];
                        $price   = $product->get_price();
                        $price   = (float)$price - (float)$value;
                        if($price < 0) {
                            $price = 0;
                        }
                        $product->set_price( (float)$price );
                        $session_data[ 'data' ] = $product;

                        break;

                    case 'percentage_product_discount' :

                        $product = $session_data[ 'data' ];
                        $price   = $product->get_price();
                        $percentage_discount = ((float)$price * (float)$value)/100;
                        $price = (float)$price - (float)$percentage_discount;
                        $product->set_price( (float)$price );
                        $session_data[ 'data' ] = $product;

                        break;

                    case 'fixed_product_price' :

                        $product = $session_data[ 'data' ];
                        $product->set_price( (float)$value );
                        $session_data[ 'data' ] = $product;

                        break;
                }
            }

            return $session_data;
        }
    }
}