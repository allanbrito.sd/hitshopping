=== YITH Deals for WooCommerce ===

== Changelog ==

= Version 1.0.2 - Released: Feb 19, 2018 =

* Fix: Javascript selector problem on admin side

= Version 1.0.1 - Released: Jan 31, 2018 =

* New: support to WordPress 4.9.2
* New: support to WooCommerce 3.3.0
* New: Spanish translation
* New: Italian translation
* New: Dutch translation
* Fix: add a product to the cart and no apply any rules
* Update: plugin core

= Version 1.0.0 - Released: Nov 22, 2017 =

* First release

== Suggestions ==

If you have suggestions about how to improve YITH Deals for WooCommerce, you can [write us](mailto:plugins@hitoutlets.com "Your Inspiration Themes") so we can bundle them into the next release of the plugin.

== Translators ==

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://hitoutlets.com/contact/ "Your Inspiration Themes") so we can bundle it into YITH Deals for WooCommerce languages.

 = Available Languages =
 * English
