<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('CWG_Product_Visibility_Settings')) :

    class CWG_Product_Visibility_Settings extends WC_Settings_Page {

        /**
         * Constructor for Class CWG_Product_Visibility_Settings
         */
        public function __construct() {
            $this->id = 'woocommerce_hide_products';
            $this->label = __('WooCommerce Show/Hide Products', 'cwghideproducts');
            $this->slug = "woocommerce_hide_products";
            add_filter('woocommerce_settings_tabs_array', array($this, 'add_settings_page'), 999);
            add_filter('woocommerce_product_visibility_settings', array($this, 'extend_add_options'));
            //add_action('woocommerce_settings_tabs_' . $this->slug, array($this, 'register_add_options'));
            add_action('woocommerce_update_options_' . $this->slug, array($this, 'register_update_options'));

            add_action('woocommerce_admin_field_cwg_product_search', array($this, 'product_search'));
            add_action('admin_head', array($this, 'add_chosen_to_product_category'));
            add_action('woocommerce_admin_field_cwg-accordion-div-start', array($this, 'accordion_div_start'));
            add_action('woocommerce_admin_field_cwg-accordion-div-end', array($this, 'accordion_div_end'));
            add_action('woocommerce_admin_field_cwg_title', array($this, 'custom_title'));
            add_action('woocommerce_sections_' . $this->id, array($this, 'output_sections'));
            add_action('woocommerce_settings_' . $this->id, array($this, 'output'));
            add_action('woocommerce_admin_field_toggle_link', array($this, 'add_function_toggle_link'));
            add_action('admin_init', array($this, 'set_default_values'), 999);
            add_action('woocommerce_admin_field_cwg_reset_option', array($this, 'add_reset_option'));
        }

        /**
         * @param array $settings list of settings tab in array
         * @return array altered array to include custom settings tab
         */
        public function add_settings_page($pages) {
            return parent::add_settings_page($pages);
        }

        public function get_sections() {
            $sections = array(
                'global' => __('Global Visibility', 'cwghideproducts'),
                '' => __('Visibility by User Roles', 'cwghideproducts'),
                'advanced' => __('Advanced', 'cwghideproducts'),
            );
            return apply_filters('woocommerce_get_sections_' . $this->id, $sections);
        }

        public function get_settings($current_section = '') {
            $settings = array();

            if ('' === $current_section) {
                $settings = $this->add_options();
            }
            return apply_filters('woocommerce_get_settings_' . $this->id, $settings, $current_section);
        }

        /**
         *
         * @global type $current_section
         */
        public function output() {
            global $current_section;
            $this->register_add_options();
            if ($current_section == '') {
                $settings = $this->get_settings();
            } elseif ($current_section == 'global') {
                $settings = $this->add_global_option();
            } elseif ($current_section == 'advanced') {
                $settings = $this->add_advanced_option();
            }
            $reset = array(array('type' => 'cwg_reset_option'));
            $merge_data = array_merge($settings, $reset);


            WC_Admin_Settings::output_fields($merge_data);
        }

        /**
         *
         * @return array it has the settings page options array
         */
        public function add_options() {
            return apply_filters('woocommerce_product_visibility_settings', array(
                array(
                    'name' => __('Show/Hide Products by User Roles', 'cwghideproducts'),
                    'type' => 'title',
                    'desc' => '',
                    'id' => '_woo_hide_products_maincontent'
                ),
                array(
                    'name' => __('Enable Multiple User Roles Support', 'cwghideproducts'),
                    'desc' => __('Enable this option if your site users have more than one user roles support. Once you enable this option Role Priority Settings will visible in each role settings see below,  except guest role. If lower the number higher the priority.', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'cwg_visibility_products_enable_multiple_roles',
                    'css' => 'background:none;',
                    'type' => 'checkbox',
                    'default' => 'no',
                    'std' => 'no',
                ),
                array(
                    'name' => 'Expand all',
                    'type' => 'toggle_link',
                    'id' => 'cwg_toggle_accordion_link',
                ),
                array('type' => 'sectionend', 'id' => '_woo_hide_products_maincontent'),
            ));
        }

        public function add_global_option() {
            $array = array('allusers' => __('All Users including guests', 'cwghideproducts'));
            $product_visibility_object = new CWG_Product_Visibility_API();
            $available_categories = $product_visibility_object->get_product_terms();
            $updated_settings[] = array(
                'name' => __('Global Settings', 'cwghideproducts'),
                'type' => 'title',
                'id' => 'cwg_global_settings_heading',
                'desc' => __('It helps to configure Show/Hide settings for all users including guests, <strong> Please Note: High Priority will be Visibility by User Roles Section Settings, If any of the role settings is inactive then it consider global settings(if it is active).</strong>', 'cwghideproducts'),
            );
            $updated_settings[] = array(
                'type' => 'sectionend', 'id' => 'cwg_global_settings_heading',
            );
            $updated_settings[] = array('id' => 'cwg-accordion-div-start', 'type' => 'cwg-accordion-div-start');
            foreach ($array as $data => $key) {
                $updated_settings[] = array(
                    'name' => __('Show/Hide Products for ' . $key, 'cwghideproducts'),
                    'type' => 'cwg_title',
                    'role' => $data,
                    'id' => '_woo_hide_products_' . $data,
                );


                $updated_settings[] = array(
                    'name' => __('Show/Hide by', 'cwghideproducts'),
                    'desc' => __('Show/Hide Products by category or certain products', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_toggle_products_by_type_' . $data,
                    'css' => '',
                    'std' => '1',
                    'defaultvalue' => 'woo_toggle_products_by_type_' . $data,
                    'type' => 'select',
                    'options' => array('1' => __('Products', 'cwghideproducts'), '2' => __('Categories', 'cwghideproducts'), '3' => __('Both Products and Categories', 'cwghideproducts')),
                    'desc_tip' => true,
                );

                $updated_settings[] = array(
                    'name' => __('If it is by Products then ', 'cwghideproducts'),
                    'desc' => __('Show/Hide All Products or Selected Products', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_toggle_products_selection_type_' . $data,
                    'css' => '',
                    'std' => '2',
                    'defaultvalue' => 'woo_toggle_products_selection_type_' . $data,
                    'type' => 'select',
                    'options' => array('1' => __('All Products', 'cwghideproducts'), '2' => __('Selected Products', 'cwghideproducts')),
                    'desc_tip' => true,
                );

                $updated_settings[] = array(
                    'name' => __('Select Product(s)', 'cwghideproducts'),
                    'desc' => __('Select Product which will be show/hide it in site wide', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_select_products_' . $data,
                    'css' => '',
                    'std' => '',
                    'placeholder' => __('Select Products', 'cwghideproducts'),
                    'type' => 'cwg_product_search',
                );
                $updated_settings[] = array(
                    'name' => __('If it is by Categories then ', 'cwghideproducts'),
                    'desc' => __('Show/Hide All Categories or Selected Categories', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_toggle_category_selection_type_' . $data,
                    'css' => '',
                    'std' => '2',
                    'defaultvalue' => 'woo_toggle_category_selection_type_' . $data,
                    'type' => 'select',
                    'options' => array('1' => __('All Categories', 'cwghideproducts'), '2' => __('Selected Categories', 'cwghideproducts')),
                    'desc_tip' => true,
                );
                $updated_settings[] = array(
                    'name' => __('Select Categories', 'cwghideproducts'),
                    'desc' => __('Want to show/hide products based on categories in a bulk way then using this option', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_toggle_type_category_' . $data,
                    'css' => '',
                    'std' => '',
                    'defaultvalue' => 'woo_toggle_type_category_' . $data,
                    'type' => 'multiselect',
                    'options' => $available_categories,
                );
                $updated_settings[] = array(
                    'name' => __('Consider child categories', 'cwghideproducts'),
                    'desc' => __('select this option if you want to consider child categories of selected categories', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_toggle_type_child_category_' . $data,
                    'css' => '',
                    'type' => 'checkbox',
                    'default' => 'no',
                    'std' => 'no',
                );

                $updated_settings[] = array(
                    'name' => __('Show/Hide Product(s) or Categories', 'cwghideproducts'),
                    'desc' => __('If it is Hide then selected products/category products will be hide, other than selected products will be visible and if it is show option selected products will be shown other products will be hidden', 'cwghideproducts'),
                    'tip' => '',
                    'id' => 'woo_include_exclude_products_' . $data,
                    'css' => '',
                    'std' => '2',
                    'defaultvalue' => 'woo_include_exclude_products_' . $data,
                    'type' => 'radio',
                    'options' => array('1' => __('Show Selected', 'cwghideproducts'), '2' => __('Hide Selected', 'cwghideproducts')), 'desc_tip' => true,
                    'desc_tip' => true,
                );

                $updated_settings[] = array(
                    'type' => 'sectionend', 'id' => '_woo_hide_products_' . $data,
                );
            }
            $updated_settings[] = array('id' => 'cwg-accordion-div-end', 'type' => 'cwg-accordion-div-end');

            return $updated_settings;
        }

        public function add_advanced_option() {
            $updated_settings[] = array(
                'name' => __('Advanced Settings', 'cwghideproducts'),
                'type' => 'title',
            );

            $updated_settings[] = array(
                'name' => __('Allow Search Engine to Index Hide Products', 'cwghideproducts'),
                'desc' => __('Allow Search Engine to Index the Hide Products but it won\'t affect user role visibility', 'cwghideproducts'),
                'tip' => '',
                'id' => 'cwg_hide_products_search_engine_index',
                'css' => '',
                'type' => 'select',
                'options' => array(
                    '1' => __('Allow', 'cwghideproducts'), '2' => __('Disallow', 'cwghideproducts'),
                ),
                'default' => '2',
                'std' => '2',
            );

            $updated_settings[] = array(
                'name' => __('Hide/Remove Category Products Count', 'cwghideproducts'),
                'desc' => __('Due to the performance issue we cannot recount the products for category after hide the products, and hence remove the category products count', 'cwghideproducts'),
                'tip' => '',
                'id' => 'cwg_hide_products_remove_category_count',
                'css' => '',
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
            );

            $updated_settings[] = array(
                'name' => __('Exclude Specific Users from Category Level', 'cwghideproducts'),
                'desc' => __('Once enabled you can see the exclude specific users option in each categories - using that option you can exclude specific users from role settings. For example User A and User B belongs to Customer Role, where two categories(Let say Cat A, Cat B) selected in customer role visibility settings. Now admin wants User A to view Category A not B, and User B to view Category B not A, so admin has to edit categories and find "Exclude Users from Visibility Role Settings" for Category B exclude User A and Category A exclude User B. (Previously it was common to both users)', 'cwghideproducts'),
                'tip' => '',
                'id' => 'cwg_hide_products_exclude_specific_users_in_category_level',
                'css' => '',
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
            );

            $updated_settings[] = array(
                'type' => 'sectionend'
            );
            return $updated_settings;
        }

        public function add_function_toggle_link($settings) {
            ?>
            <tr valign="top" style="background:#f1f1f1">
                <th class="titledesc" scope="row">

                </th>
                <td class="forminp forminp-select">
                    <a style="float:right;" href="#" id="<?php echo $settings['id']; ?>"><?php echo $settings['name']; ?></a>
                </td>
            </tr>
            <?php
        }

        public function add_reset_option($settings) {
            global $current_section;
            ?>
            <p style="float:right;" class="submit"><input type="submit" class="button-secondary" name="cwg_reset_option" value="<?php _e('Reset', 'cwghideproducts'); ?>"</p>
            <input type="hidden" name="cwg_current_section" value="<?php echo $current_section; ?>"/>
            <?php
        }

        /**
         *
         * @param array $settings
         * @return array $updated_settings
         *
         */
        public function extend_add_options($settings) {
            $updated_settings = array();
            $product_visibility_object = new CWG_Product_Visibility_API();
            $available_categories = $product_visibility_object->get_product_terms();
            $updated_settings[] = array('id' => 'cwg-accordion-div-start', 'type' => 'cwg-accordion-div-start');
            foreach ($settings as $section) {
                if (isset($section['id']) && '_woo_hide_products_maincontent' == $section['id'] && isset($section['type']) && 'sectionend' == $section['type']) {
                    $get_roles = $product_visibility_object->get_all_user_roles();
                    foreach ($get_roles as $data => $key) {
                        $updated_settings[] = array(
                            'name' => __('Show/Hide Products for ' . $key . ' Role', 'cwghideproducts'),
                            'type' => 'cwg_title',
                            'role' => $data,
                            'id' => '_woo_hide_products_' . $data,
                        );
                        if ($data != 'guest') {
                            $updated_settings[] = array(
                                'name' => __('Role Priority', 'cwghideproducts'),
                                'desc' => __('For Multiple User Role Support, If lower the number higher the priority', 'cwghideproducts'),
                                'tip' => '',
                                'id' => 'cwg_multiple_user_role_priority_' . $data,
                                'class' => 'cwg_multiple_user_role_priority',
                                'css' => '',
                                'std' => '10',
                                'defaultvalue' => 'cwg_mutiple_user_role_priority_' . $data,
                                'type' => 'number',
                                'desc_tip' => false,
                            );
                        }
                        $updated_settings[] = array(
                            'name' => __('Show/Hide by', 'cwghideproducts'),
                            'desc' => __('Show/Hide Products by category or certain products', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_toggle_products_by_type_' . $data,
                            'css' => '',
                            'std' => '1',
                            'defaultvalue' => 'woo_toggle_products_by_type_' . $data,
                            'type' => 'select',
                            'options' => array('1' => __('Products', 'cwghideproducts'), '2' => __('Categories', 'cwghideproducts'), '3' => __('Both Products and Categories', 'cwghideproducts')),
                            'desc_tip' => true,
                        );

                        $updated_settings[] = array(
                            'name' => __('If it is by Products then ', 'cwghideproducts'),
                            'desc' => __('Show/Hide All Products or Selected Products', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_toggle_products_selection_type_' . $data,
                            'css' => '',
                            'std' => '2',
                            'defaultvalue' => 'woo_toggle_products_selection_type_' . $data,
                            'type' => 'select',
                            'options' => array('1' => __('All Products', 'cwghideproducts'), '2' => __('Selected Products', 'cwghideproducts')),
                            'desc_tip' => true,
                        );

                        $updated_settings[] = array(
                            'name' => __('Select Product(s)', 'cwghideproducts'),
                            'desc' => __('Select Product which will be show/hide it in site wide', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_select_products_' . $data,
                            'css' => '',
                            'std' => '',
                            'placeholder' => __('Select Products', 'cwghideproducts'),
                            'type' => 'cwg_product_search',
                        );
                        $updated_settings[] = array(
                            'name' => __('If it is by Categories then ', 'cwghideproducts'),
                            'desc' => __('Show/Hide All Categories or Selected Categories', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_toggle_category_selection_type_' . $data,
                            'css' => '',
                            'std' => '2',
                            'defaultvalue' => 'woo_toggle_category_selection_type_' . $data,
                            'type' => 'select',
                            'options' => array('1' => __('All Categories', 'cwghideproducts'), '2' => __('Selected Categories', 'cwghideproducts')),
                            'desc_tip' => true,
                        );
                        $updated_settings[] = array(
                            'name' => __('Select Categories', 'cwghideproducts'),
                            'desc' => __('Want to show/hide products based on categories in a bulk way then using this option', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_toggle_type_category_' . $data,
                            'css' => '',
                            'std' => '',
                            'defaultvalue' => 'woo_toggle_type_category_' . $data,
                            'type' => 'multiselect',
                            'options' => $available_categories,
                        );
                        $updated_settings[] = array(
                            'name' => __('Consider child categories', 'cwghideproducts'),
                            'desc' => __('select this option if you want to consider child categories of selected categories', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_toggle_type_child_category_' . $data,
                            'css' => '',
                            'type' => 'checkbox',
                            'default' => 'no',
                            'std' => 'no',
                        );

                        $updated_settings[] = array(
                            'name' => __('Show/Hide Product(s) or Categories', 'cwghideproducts'),
                            'desc' => __('If it is Hide then selected products/category products will be hide, other than selected products will be visible and if it is show option selected products will be shown other products will be hidden', 'cwghideproducts'),
                            'tip' => '',
                            'id' => 'woo_include_exclude_products_' . $data,
                            'css' => '',
                            'std' => '2',
                            'defaultvalue' => 'woo_include_exclude_products_' . $data,
                            'type' => 'radio',
                            'options' => array('1' => __('Show Selected', 'cwghideproducts'), '2' => __('Hide Selected', 'cwghideproducts')), 'desc_tip' => true,
                            'desc_tip' => true,
                        );

                        $updated_settings[] = array(
                            'type' => 'sectionend', 'id' => '_woo_hide_products_' . $data,
                        );
                    }
                }
            }
            $updated_settings[] = array('id' => 'cwg-accordion-div-end', 'type' => 'cwg-accordion-div-end');

            $merge_array = array_merge($settings, $updated_settings);

            return $merge_array;
        }

        /**
         *
         * @return bool true/false
         */
        public function register_add_options() {
            global $current_section;
            wp_enqueue_script('jquery-ui-accordion');
            wp_register_script('cwg-ui-accordion', CWG_HIDE_PRODUCTS_PLUGIN_URL . 'js/script.js');
            $localize_array = array(
                'slug' => $this->slug,
            );

            if ($current_section == 'global') {
                $localize_array['hiddenname'] = 'cwg_accordion_current_index_global';
                $localize_array['accordion_state'] = get_option('cwg_accordion_current_index_global', false);
            } else {
                $localize_array['hiddenname'] = 'cwg_accordion_current_index';
                $localize_array['accordion_state'] = get_option('cwg_accordion_current_index', false);
            }
            wp_localize_script('cwg-ui-accordion', 'accordionobj', $localize_array);
            wp_enqueue_script('cwg-ui-accordion');
            //woocommerce_admin_fields($this->add_options());
        }

        /**
         *
         * @return null
         */
        public function register_update_options() {
            global $current_section;
            if (isset($_POST['cwg_reset_option'])) {
                if ($current_section == '') {
                    foreach ($this->add_options() as $settings) {
                        if (isset($settings['id']) && isset($settings['std'])) {
                            delete_option($settings['id']);
                            add_option($settings['id'], $settings['std']);
                            delete_option('cwg_accordion_current_index');
                        }
                    }
                } elseif ($current_section == 'global') {
                    // for global
                    foreach ($this->add_global_option() as $settings) {
                        if (isset($settings['id']) && isset($settings['std'])) {
                            delete_option($settings['id']);
                            add_option($settings['id'], $settings['std']);
                            delete_option('cwg_accordion_current_index_global');
                        }
                    }
                } elseif ($current_section == 'advanced') {

                    // for advanced settings

                    foreach ($this->add_advanced_option() as $settings) {
                        if (isset($settings['id']) && isset($settings['std'])) {
                            delete_option($settings['id']);
                            add_option($settings['id'], $settings['std']);
                        }
                    }
                }
            } else {
                if ($current_section == '') {
                    woocommerce_update_options($this->add_options());
                    foreach ($this->add_options() as $settings) {
                        $check_value = isset($_POST[$settings['id']]) ? $_POST[$settings['id']] : array();
                        if ($settings['type'] != 'checkbox') {
                            update_option($settings['id'], $check_value);
                        }

                        if (isset($_POST['cwg_accordion_current_index'])) {
                            update_option('cwg_accordion_current_index', $_POST['cwg_accordion_current_index']);
                        }
                    }
                } elseif ($current_section == 'global') {
                    woocommerce_update_options($this->add_global_option());
                    foreach ($this->add_global_option() as $settings) {
                        $check_value = isset($_POST[$settings['id']]) ? $_POST[$settings['id']] : array();
                        if ($settings['type'] != 'checkbox') {
                            update_option($settings['id'], $check_value);
                        }

                        if (isset($_POST['cwg_accordion_current_index_global'])) {
                            update_option('cwg_accordion_current_index_global', $_POST['cwg_accordion_current_index_global']);
                        }
                    }
                } elseif ($current_section == 'advanced') {
                    woocommerce_update_options($this->add_advanced_option());
                }

                if (function_exists('w3tc_flush_all')) {
                    w3tc_flush_all();
                }
            }
        }

        /**
         * @return null
         */
        public function set_default_values() {
            foreach ($this->add_options() as $settings) {
                if (isset($settings['id']) && ($settings['std'])) {
                    add_option($settings['id'], $settings['std']);
                }
            }
            // for global
            foreach ($this->add_global_option() as $settings) {
                if (isset($settings['id']) && ($settings['std'])) {
                    add_option($settings['id'], $settings['std']);
                }
            }

            // for advanced settings

            foreach ($this->add_advanced_option() as $settings) {
                if (isset($settings['id']) && ($settings['std'])) {
                    add_option($settings['id'], $settings['std']);
                }
            }
        }

        /**
         *
         */
        public function product_search($value) {
            global $woocommerce;
            // For the WooCommerce Latest Version
            ?>

            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
                </th>
                <td class="forminp forminp-select">
                    <?php if (WC_VERSION >= (float) ('3.0.0')) {
                        ?>
                                        <!-- <input type="hidden" class="wc-product-search" id="<?php echo $value['id']; ?>"  style="width:330px;" name="<?php echo $value['id']; ?>" data-placeholder="<?php echo $value['placeholder']; ?>" data-action="woocommerce_json_search_products" data-multiple="true"  -->
                        <select class="wc-product-search" id="<?php echo $value['id']; ?>" style="width:330px;" name="<?php echo $value['id']; ?>[]" data-placeholder="<?php echo $value['placeholder']; ?>" data-action="woocommerce_json_search_products" multiple="multiple"> <?php
                            $get_datas = array();
                            $available_data = get_option($value['id']);
                            if ($available_data) {
                                $ids = is_array($available_data) ? array_unique(array_filter(array_map('absint', (array) $available_data))) : array_unique(array_filter(array_map('absint', (array) explode(',', $available_data))));
                                foreach ($ids as $eachid) {
                                    $getproductdata = wc_get_product($eachid);
                                    if ($getproductdata) {
                                        $get_datas[$eachid] = wp_kses_post($getproductdata->get_formatted_name());
                                        ?>
                                        <option value="<?php echo $eachid; ?>" selected="selected"><?php echo wp_kses_post($getproductdata->get_formatted_name()); ?></option>
                                        <?php
                                    }
                                }
                            } else {
                                ?>
                                <option value=" "></option>
                            <?php }
                            ?> </select>
                        <?php
                    } elseif (WC_VERSION > (float) ('2.2.0') && WC_VERSION < (float) ('3.0.0')) {
                        ?>
                        <input type="hidden" class="wc-product-search" id="<?php echo $value['id']; ?>"  style="width:330px;" name="<?php echo $value['id']; ?>" data-placeholder="<?php echo $value['placeholder']; ?>" data-action="woocommerce_json_search_products" data-multiple="true" data-selected="<?php
                        $get_datas = array();
                        $available_data = get_option($value['id']);
                        if ($available_data) {
                            $ids = is_array($available_data) ? array_unique(array_filter(array_map('absint', (array) $available_data))) : array_unique(array_filter(array_map('absint', (array) explode(',', $available_data))));
                            foreach ($ids as $eachid) {
                                $getproductdata = wc_get_product($eachid);
                                if ($getproductdata) {
                                    $get_datas[$eachid] = wp_kses_post($getproductdata->get_formatted_name());
                                }
                            }
                            echo esc_attr(json_encode($get_datas));
                        }
                        ?>" value="<?php echo implode(',', array_keys($get_datas)); ?>"/>
                               <?php
                           } else {
                               ?>

                        <select multiple="multiple" name="<?php echo $value['id']; ?>[]" id="<?php echo $value['id']; ?>" class="<?php echo $value['id']; ?>" style="width:330px;" >
                            <?php
                            $get_datas = array();
                            $available_data = get_option($value['id']);
                            if ($available_data) {
                                $ids = is_array($available_data) ? array_unique(array_filter(array_map('absint', (array) $available_data))) : array_unique(array_filter(array_map('absint', (array) explode(',', $available_data))));
                                foreach ($ids as $eachid) {
                                    ?>
                                    <option value="<?php echo $eachid; ?>" selected="selected"><?php echo '#' . $eachid . ' &ndash; ' . get_the_title($eachid); ?></option>
                                    <?php
                                }
                            } else {
                                ?>
                                <option value=""></option>
                            <?php }
                            ?>
                        </select>

                        <script type="text/javascript">

                            jQuery(function () {
                                jQuery("select.<?php echo $value['id']; ?>").ajaxChosen({
                                    method: 'GET',
                                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                    dataType: 'json',
                                    afterTypeDelay: 100,
                                    data: {
                                        action: 'woocommerce_json_search_products',
                                        security: '<?php echo wp_create_nonce("search-products"); ?>'
                                    }
                                }, function (data) {
                                    var terms = {};
                                    jQuery.each(data, function (i, val) {
                                        terms[i] = val;
                                    });
                                    return terms;
                                });
                            });
                        </script>
                    <?php }
                    ?>
                </td>
            </tr>
            <?php
        }

        public function add_chosen_to_product_category() {
            if (isset($_GET['tab'])) {
                if ($_GET['tab'] == 'woocommerce_hide_products') {
                    $visibility_obj = new CWG_Product_Visibility_API();
                    $get_roles = $visibility_obj->get_all_user_roles();

                    $merge_array = array('allusers' => __('All Users including guests', 'cwghideproducts'));
                    $merge_data = array_merge($get_roles, $merge_array);
                    $get_roles = $merge_data;
                    ?>
                    <style type="text/css">
                        table.form-table {
                            background:#fff;
                        }
                        .woocommerce table.form-table th {
                            padding:10px;
                        }

                        h2 {
                            margin-top:20px;
                            letter-spacing: 1px;
                        }
                        table {
                            height: auto !important;
                        }

                        h3.h3_woocommerce_hide_products {
                            font-weight:700;
                            background: #fefefe;
                            letter-spacing: 1px;


                        }
                        h3.h3_woocommerce_hide_products:hover {
                            background: #C3CAC6;
                        }
                        h3.h3_woocommerce_hide_products.ui-accordion-header-active.ui-state-active {
                            background: #C3CAC6;
                        }
                        .span_woocommerce_hide_products {
                            float:right;
                        }
                        .span_woocommerce_hide_products.active {
                            color:#11bd06;
                        }


                    </style>
                    <script type="text/javascript">

                        function show_or_hide(value, element) {

                            if (value === '1') {
                                // Show products
                                jQuery('#woo_toggle_products_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().hide();
                                jQuery('#woo_select_products_' + element).parent().parent().show();

                                var get_product_value = jQuery('#woo_toggle_products_selection_type_' + element).val();

                                show_or_hide_internal_section_products(value, get_product_value, element);

                            } else if (value === '2') {
                                // show categories
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().show();
                                jQuery('#woo_select_products_' + element).parent().parent().hide();


                                var get_category_value = jQuery('#woo_toggle_category_selection_type_' + element).val();
                                show_or_hide_internal_section_categories(value, '0', get_category_value, element);

                            } else {
                                //show both
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().show();
                                jQuery('#woo_select_products_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().show();
                                jQuery('#woo_toggle_products_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().show();

                                var get_product_value = jQuery('#woo_toggle_products_selection_type_' + element).val();

                                show_or_hide_internal_section_products(value, get_product_value, element);
                                var get_category_value = jQuery('#woo_toggle_category_selection_type_' + element).val();
                                show_or_hide_internal_section_categories(value, get_product_value, get_category_value, element);

                            }

                        }

                        function show_or_hide_internal_section_products(parentvalue, value, element) {
                            // all products/selected products
                            if (parentvalue == '1' && value == '1') {
                                // if it is all hide selected products, selected categories, child categories and category type
                                jQuery('#woo_select_products_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().hide();

                            } else if (parentvalue == '1' && value == '2') {
                                jQuery('#woo_select_products_' + element).parent().parent().show();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().hide();
                            } else if (parentvalue == '3' && value == '1') {

                                jQuery('#woo_select_products_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().hide();
                            } else if (parentvalue == '3' && value == '2') {

                                jQuery('#woo_select_products_' + element).parent().parent().show();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().show();
                                var get_category_value = jQuery('#woo_toggle_category_selection_type_' + element).val();
                                show_or_hide_internal_section_categories(parentvalue, value, get_category_value, element);


                            }
                        }

                        function show_or_hide_internal_section_categories(parentvalue, productvalue, value, element) {
                            //all categories/selected categories
                            if (parentvalue == '3' && productvalue == '2' && value == '1') {
                                //console.log('parentvalue' + parentvalue, 'productvalue' + productvalue, 'categoryvalue' + value);
                                // all categories in both category
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().hide();

                            } else if (parentvalue == '3' && productvalue == '2' && value == '2') {
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().show();

                            } else if (parentvalue == '2' && value == '2') {

                                jQuery('#woo_toggle_products_selection_type_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().show();

                            } else if (parentvalue == '2' && value == '1') {
                                jQuery('#woo_toggle_products_selection_type_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_category_selection_type_' + element).parent().parent().show();
                                jQuery('#woo_toggle_type_category_' + element).parent().parent().hide();
                                jQuery('#woo_toggle_type_child_category_' + element).parent().parent().parent().parent().hide();

                            }
                        }

                        jQuery(function () {

                            //jQuery('#mainform').find('h2').css('font-size', '20px');
                            //jQuery('#mainform').find('h2').first().css('font-size', '25px');
                    <?php
                    foreach ($get_roles as $key => $value) {
                        if (WC_VERSION >= (float) ('3.0.0')) {
                            ?>
                                    jQuery('#woo_toggle_type_category_<?php echo $key; ?>').select2();
                            <?php
                        } else {
                            ?>
                                    jQuery('#woo_toggle_type_category_<?php echo $key; ?>').chosen();
                        <?php }
                        ?>
                                var get_value = jQuery('#woo_toggle_products_by_type_<?php echo $key; ?>').val();
                                show_or_hide(get_value, '<?php echo $key; ?>');

                                jQuery('#woo_toggle_products_by_type_<?php echo $key; ?>').change(function () {
                                    var current_value = jQuery(this).val();
                                    show_or_hide(current_value, '<?php echo $key; ?>');
                                });

                                jQuery('#woo_toggle_products_selection_type_<?php echo $key; ?>').change(function () {
                                    var parentvalue = jQuery('#woo_toggle_products_by_type_<?php echo $key; ?>').val();
                                    var currentvalue = jQuery(this).val();
                                    show_or_hide_internal_section_products(parentvalue, currentvalue, '<?php echo $key; ?>');
                                });

                                jQuery('#woo_toggle_category_selection_type_<?php echo $key; ?>').change(function () {
                                    var newparentvalue = jQuery('#woo_toggle_products_by_type_<?php echo $key; ?>').val();
                                    if (newparentvalue == '1' || newparentvalue == '3') {
                                        var newproductvalue = jQuery('#woo_toggle_products_selection_type_<?php echo $key; ?>').val();
                                    } else {
                                        var newproductvalue = "0";
                                    }
                                    var newcurrentvalue = jQuery(this).val();

                                    show_or_hide_internal_section_categories(newparentvalue, newproductvalue, newcurrentvalue, '<?php echo $key; ?>');

                                });

                    <?php }
                    ?>

                        });
                    </script>
                    <?php
                }
            }
        }

        /**
         *
         * @param type $settings
         */
        public function accordion_div_start($settings) {
            echo "<div id='$this->slug'>";
        }

        /**
         *
         * @param type $settings
         */
        public function accordion_div_end($settings) {
            global $current_section;
            echo "</div>";
            if ($current_section == '') {
                echo "<input type='hidden' name='cwg_accordion_current_index' id='cwg_accordion_current_index' value='' />";
            } elseif ($current_section == 'global') {
                echo "<input type='hidden' name='cwg_accordion_current_index_global' id='cwg_accordion_current_index_global' value='' />";
            }
        }

        /**
         *
         * @param type $settings
         */
        public function custom_title($settings) {
            $get_role = $settings['role'];
            $get_status = $this->get_rule_status($get_role);
            $strtolower = strtolower($get_status);
            echo "<h3 class='h3_" . $this->slug . "'>" . $settings['name'] . "<span class='span_$this->slug $strtolower'>" . $get_status . "</span></h3>";
            echo '<table class="form-table">';
        }

        public function get_rule_status($role) {
            $obj = new CWG_Product_Visibility_API();
            $get_settings = $obj->get_data_from_settings($role);
            $check = false;
            if ($get_settings['type'] == 'products') {
                $check = $this->check_product_level_active($get_settings);
                if ($check) {
                    return __('Active', 'cwghideproducts');
                } else {
                    return $this->get_global_status();
                }
            } elseif ($get_settings['type'] == 'categories') {
                $check = $this->check_category_level_active($get_settings);
                if ($check) {
                    return __('Active', 'cwghideproducts');
                } else {
                    return $this->get_global_status();
                }
            } else {
                //both
                $check_product = $this->check_product_level_active($get_settings);
                $check_category = $this->check_category_level_active($get_settings);

                if ($check_product || $check_category) {
                    return __('Active', 'cwghideproducts');
                } else {
                    return $this->get_global_status();
                }
            }
        }

        public function get_global_status($role = 'allusers') {
            $obj = new CWG_Product_Visibility_API();
            $get_settings = $obj->get_data_from_settings($role);
            $check = false;
            if ($get_settings['type'] == 'products') {
                $check = $this->check_product_level_active($get_settings);
                if ($check) {
                    return __('Global Level Active', 'cwghideproducts');
                } else {
                    return __('Inactive', 'cwghideproducts');
                }
            } elseif ($get_settings['type'] == 'categories') {
                $check = $this->check_category_level_active($get_settings);
                if ($check) {
                    return __('Global Level Active', 'cwghideproducts');
                } else {
                    return __('Inactive', 'cwghideproducts');
                }
            } else {
                //both
                $check_product = $this->check_product_level_active($get_settings);
                $check_category = $this->check_category_level_active($get_settings);

                if ($check_product || $check_category) {
                    return __('Global Level Active', 'cwghideproducts');
                } else {
                    return __('Inactive', 'cwghideproducts');
                }
            }
        }

        public function check_product_level_active($settings) {
            $selected_type = $settings['producttype'];
            if ($selected_type == 'all') {
                return true;
            } else {
                if ($selected_type == 'selected') {
                    $getids = $settings['productids'];
                    if (is_array($getids) && !empty($getids)) {
                        return true;
                    }
                }
            }
            return false;
        }

        public function check_category_level_active($settings) {
            $selected_type = $settings['categorytype'];
            if ($selected_type == 'all') {
                return true;
            } else {
                if ($selected_type == 'selected') {
                    $getids = $settings['categoryids'];
                    if (is_array($getids) && !empty($getids)) {
                        return true;
                    }
                }
            }
            return false;
        }

    }

    endif;


return new CWG_Product_Visibility_Settings();
