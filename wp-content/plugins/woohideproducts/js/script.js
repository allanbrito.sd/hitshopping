var id = accordionobj.slug;
var state = accordionobj.accordion_state;
var updatedstate = state == 'false' ? false : parseInt(state);
var sortable_div = accordionobj.sortable_div_id;
var hiddenname = accordionobj.hiddenname;
jQuery(function () {

    jQuery('#' + id).accordion({
        collapsible: true,
        active: updatedstate
    });


    jQuery('#cwg_toggle_accordion_link').on('click', function () {
        jQuery('.ui-accordion-header:not(.ui-state-active)').next().slideToggle();
        jQuery(this).text(jQuery(this).text() == 'Expand all' ? 'Collapse all' : 'Expand all');
        jQuery('.ui-accordion-header').toggleClass('collapse');
        return false;
    });

    jQuery('#' + id).on('click', function () {

        var currentindex = jQuery("#" + id).accordion("option", "active");
        jQuery('#' + hiddenname).val(currentindex);
    });



jQuery('#cwg_visibility_products_enable_multiple_roles').on('click',function() {
   
   if(jQuery(this).is(':checked')) {
       jQuery('.cwg_multiple_user_role_priority').parent().parent().show();
   }else {
       jQuery('.cwg_multiple_user_role_priority').parent().parent().hide();
   }
    
});

    if (jQuery('#cwg_visibility_products_enable_multiple_roles').is(":checked")) {
       
        jQuery('.cwg_multiple_user_role_priority').parent().parent().show();
    } else {
        jQuery('.cwg_multiple_user_role_priority').parent().parent().hide();
    }



});


