<?php

/*
 * Plugin Name: WooCommerce Show/Hide Products or Categories by User Roles
 * Plugin URI: http://codewoogeek.com
 * Description: Show/Hide Products by Specific User Roles
 * Version: 5.9
 * Author: CodeWooGeek
 * Author URI: http://codewoogeek.com
 *
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (class_exists('WooCommerce')) {

    if (!class_exists('CWG_Hide_Products')):

        /**
         * Main CWG_Hide_Products Class.
         *
         * @class CWG_Hide_Products
         * @version	6.0
         */
        class CWG_Hide_Products {

            /**
             * Hide Products version.
             *
             * @var string
             */
            public $version = '5.0';

            /**
             * The single instance of the class.
             *
             */
            protected static $_instance = null;

            /**
             *
             * @static
             * @see CWG_Product_Visibility()
             * @return WooCommerce - Main instance.
             */
            public static function instance() {
                if (is_null(self::$_instance)) {
                    self::$_instance = new self();
                }
                return self::$_instance;
            }

            /**
             * Cloning is forbidden.
             */
            public function __clone() {
                wc_doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'cwghideproducts'), '6.0');
            }

            /**
             * Unserializing instances of this class is forbidden.
             */
            public function __wakeup() {
                wc_doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'cwghideproducts'), '6.0');
            }

            /**
             *
             * CWG_Hide_Products Constructor
             */
            public function __construct() {
                $this->include_files();
                $this->define_constant();
                $this->load_plugin_textdomain();
                add_filter('woocommerce_get_settings_pages', array($this, 'add_settings_page'));
            }

            /**
             * Include files for admin and frontend should goes here
             */
            public function include_files() {
                if (!function_exists('get_plugins')) {
                    require_once ABSPATH . 'wp-admin/includes/plugin.php';
                }
                include('updates/github.php');
                include_once('includes/class-product-visibility-api.php');
                $enable_category = get_option('cwg_hide_products_exclude_specific_users_in_category_level', 'no');
                if ($enable_category == 'yes') {
                    include_once('includes/class-product-category-field.php');
                }
                $bot = $this->check_is_bot();
                if (!$this->check_is_bot()) {
                    include_once('includes/class-product-visibility-core.php');
                }
            }

            /**
             *
             * @return bool
             */
            public function load_plugin_textdomain() {

                $domain = 'cwghideproducts';
                $dir = untrailingslashit(WP_LANG_DIR);
                $locale = apply_filters('plugin_locale', get_locale(), $domain);

                if ($exists = load_textdomain($domain, $dir . '/plugins/' . $domain . '-' . $locale . '.mo')) {
                    return $exists;
                } else {
                    load_plugin_textdomain($domain, FALSE, basename(dirname(__FILE__)) . '/languages/');
                }
            }

            /**
             * Define Constant
             */
            public function define_constant() {
                define('CWG_HIDE_PRODUCTS_PLUGIN_URL', plugins_url('/', __FILE__));
            }

            /**
             *
             * @param array $settings
             *
             */
            public function add_settings_page($settings) {
                $settings[] = include('admin/class-product-visibility-settings.php');

                return $settings;
            }

            public function check_is_bot() {
                $option_check = get_option('cwg_hide_products_search_engine_index', '2');
                if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT']) && ($option_check == '1')) {
                    return true;
                }
                return false;
            }

        }

        endif;

    function CWG_Product_Visibility() {
        return CWG_Hide_Products::instance();
    }

    CWG_Product_Visibility();
}