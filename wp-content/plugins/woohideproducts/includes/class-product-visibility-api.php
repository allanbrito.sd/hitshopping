<?php

/*
 * For Common Functions to be used internally
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('CWG_Product_Visibility_API')) {

    class CWG_Product_Visibility_API {

        /**
         * Constructor for CWG_Product_Visibility_API
         *
         * @param string $user_id user id will be used if it is member otherwise it is 0
         */
        public function __construct($user_id = 0) {
            $this->user_id = $user_id;
        }

        /**
         * Return Single User Role of First Element in Array
         *
         * @param null
         * @return string return first element user role from array
         */
        public function get_user_role_from_user_id() {
            $user_id = $this->user_id;
            if ($user_id > 0) {
                $get_user_data = get_userdata($user_id);

                // check if multiple user roles is enabled
                $check_multiple_is_enabled = get_option('cwg_visibility_products_enable_multiple_roles', 'no');


                if ($check_multiple_is_enabled == 'yes') {
                    $multiple_roles = $get_user_data->roles;

                    $priority_level = $this->get_the_priority_from_role();
                    if (is_array($multiple_roles) && !empty($multiple_roles)) {
                        $finalize_user_role_priority = array();
                        foreach ($multiple_roles as $each_key => $each_role) {
                            $finalize_user_role_priority[$each_role] = $priority_level[$each_role];
                        }
                        asort($finalize_user_role_priority);
                        $array_keys = array_keys($finalize_user_role_priority)[0];
                        return $array_keys;
                    }
                } else {
                    return $get_user_data->roles[0];
                }
            } else {
                return "guest";
            }
        }

        // get the priority from role

        public function get_the_priority_from_role() {
            //$option_name = cwg_multiple_user_role_priority_
            global $wp_roles;
            if (!isset($wp_roles)) {
                $wp_roles = new WP_Roles();
            }
            $roles = $wp_roles->get_names();
            $priority_array = array();
            foreach ($roles as $key => $value) {
                $priority_array[$key] = get_option('cwg_multiple_user_role_priority_' . $key, '10');
            }
            return $priority_array;
        }

        /**
         * @param null
         * @return array It returns all roles associated with user id
         */
        public function get_user_roles_from_user_id() {
            $user_id = $this->user_id;
            $get_user_data = get_userdata($user_id);
            return $get_user_data->roles;
        }

        /**
         *
         * @param string $role pass the role to get the configured settings
         * @return array all the saved information about the corresponding role
         */
        public function get_data_from_settings($role) {
            $user_id = $this->user_id;
            // $role = $this->get_rule_status($role) ? $role : 'allusers';

            $type = get_option('woo_toggle_products_by_type_' . $role);
            if ($type == '1') {
                $type = 'products';
            } elseif ($type == '2') {
                $type = 'categories';
            } else {
                $type = 'both';
            }

            $categoryids = get_option('woo_toggle_type_category_' . $role, array());
            if ($user_id > 0) {
                $exclude_categories = $this->get_excluded_categories_from_user_id($user_id, $categoryids);
                if (is_array($exclude_categories) && !empty($exclude_categories)) {
                    $filter_data = array_unique(array_filter($exclude_categories));
                    $categoryids = array_diff($categoryids, $filter_data);
                }
            }


            $array = array(
                'type' => $type,
                'visibility' => get_option('woo_include_exclude_products_' . $role) == '1' ? 'include' : 'exclude',
                'producttype' => get_option('woo_toggle_products_selection_type_' . $role) == '1' ? 'all' : 'selected',
                'categorytype' => get_option('woo_toggle_category_selection_type_' . $role) == '1' ? 'all' : 'selected',
                'productids' => get_option('woo_select_products_' . $role),
                'categoryids' => $categoryids,
                'childcategory' => get_option('woo_toggle_type_child_category_' . $role),
            );
            return $array;
        }

        /**
         *
         * @param array $ids array of ids
         * @param string $type denote either product type/product category
         * @return array list of translated ids from parent id
         */
        public function get_translated_ids_from_wpml($ids, $type) {
            $get_translated_ids = array();
            //run only when wpml is enabled
            if (function_exists('icl_get_languages')) {
                $all_languages = icl_get_languages();
                if (is_array($ids) && !empty($ids)) {
                    foreach ($ids as $each_id) {
                        foreach ($all_languages as $lang => $row) {
                            $get_translated_ids[] = icl_object_id($each_id, $type, false, $lang);
                        }
                    }
                }
            }
            return $get_translated_ids;
        }

        public function get_excluded_categories_from_user_id($user_id, $categories) {
            $excluded_categories = array();
            $check_exclude_category = get_option('cwg_hide_products_exclude_specific_users_in_category_level', 'no');

            if ($user_id > 0 && $check_exclude_category == 'yes') {
                if (is_array($categories) && !empty($categories)) {
                    foreach ($categories as $each_id) {
                        $get_excluded_data = (array) get_term_meta($each_id, 'cwg_exclude_users_from_visibility', true);
                        if (in_array($user_id, $get_excluded_data)) {
                            $excluded_categories[] = $each_id;
                        }
                    }
                }
            }
            return $excluded_categories;
        }

        /**
         * @param null
         * @return array get all categories with respect to products
         */
        public function get_product_terms() {
            $get_product_terms = get_terms('product_cat', array('hide_empty' => 'false'));
            $available_categories = array();
            if (!empty($get_product_terms) && is_array($get_product_terms)) {
                foreach ($get_product_terms as $each_term) {
                    $available_categories[$each_term->term_id] = $each_term->name . "(" . $each_term->count . ")";
                }
            }
            return $available_categories;
        }

        /**
         *
         * @global WP_Roles $wp_roles
         * @return array list of user roles in a array
         */
        public function get_all_user_roles() {
            global $wp_roles;
            if (!isset($wp_roles)) {
                $wp_roles = new WP_Roles();
            }
            $guest_role = array('guest' => __('Guest', 'cwghideproducts'));
            $get_roles = $wp_roles->get_names();
            $merge = array_merge($get_roles, $guest_role);
            return $merge;
        }

        public function get_rule_status($role) {

            $get_settings = $this->get_data_from_settings($role);
            $check = false;
            if ($get_settings['type'] == 'products') {
                $check = $this->check_product_level_active($get_settings);
                if ($check) {
                    return true;
                } else {
                    return false;
                }
            } elseif ($get_settings['type'] == 'categories') {
                $check = $this->check_category_level_active($get_settings);
                if ($check) {
                    return true;
                } else {
                    return false;
                }
            } else {
                //both
                $check_product = $this->check_product_level_active($get_settings);
                $check_category = $this->check_category_level_active($get_settings);

                if ($check_product || $check_category) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        public function check_product_level_active($settings) {
            $selected_type = $settings['producttype'];
            if ($selected_type == 'all') {
                return true;
            } else {
                if ($selected_type == 'selected') {
                    $getids = $settings['productids'];
                    if (is_array($getids) && !empty($getids)) {
                        return true;
                    }
                }
            }
            return false;
        }

        public function check_category_level_active($settings) {
            $selected_type = $settings['categorytype'];
            if ($selected_type == 'all') {
                return true;
            } else {
                if ($selected_type == 'selected') {
                    $getids = $settings['categoryids'];
                    if (is_array($getids) && !empty($getids)) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         *
         * @global object $wpdb
         * @return array
         */
        public function get_all_term_products() {
            global $wpdb;
            $user_id = $this->user_id;
            $get_all_term_ids = $this->get_all_term_ids(FALSE);
            $exclude_data = '';
            $where = '';
            if ($user_id > 0) {
                $excluded_categories = $this->get_excluded_categories_from_user_id($user_id, $get_all_term_ids);
                if (is_array($excluded_categories) && !empty($excluded_categories)) {
                    $filter_data = array_unique(array_filter($excluded_categories));
                    $arrayofids = join(',', $filter_data);
                    $where = " AND tt.term_taxonomy_id NOT IN ($arrayofids)";
                }
            }

            $query = $wpdb->get_results("SELECT DISTINCT object_id FROM {$wpdb->term_taxonomy} as tt INNER JOIN {$wpdb->term_relationships} AS tr ON tt.term_taxonomy_id=tr.term_taxonomy_id WHERE tt.taxonomy='product_cat' $where");
            $extract_data = $this->extract_objects($query);
            return $extract_data;
        }

        /**
         *
         * @global object $wpdb
         * @return array
         */
        public function get_all_term_ids($exclude = TRUE) {
            global $wpdb;
            $user_id = $this->user_id;

            $query = $wpdb->get_results("SELECT DISTINCT term_id as object_id FROM {$wpdb->term_taxonomy} as tt WHERE tt.taxonomy='product_cat'");
            $extract_data = $this->extract_category_objects($query);
            if ($exclude != FALSE && $user_id > 0) {
                $excluded_categories = $this->get_excluded_categories_from_user_id($user_id, $extract_data);
                if (is_array($excluded_categories) && !empty($excluded_categories)) {
                    $filter_data = array_filter($excluded_categories);
                    $difference = array_diff($extract_data, $filter_data);
                    return $difference;
                }
            }
            return $extract_data;
        }

        /**
         *
         * @global object $wpdb
         * @param object $data which has settings data
         * @return array of product ids
         */
        public function get_list_of_product_ids($data) {


            global $wpdb;
            if (isset($data['type'])) {
                $type = $data['type'];
                if ($type == 'products') {
                    $selection = $data['producttype'];
                    if ($selection == 'selected') {
                        $list_of_ids = array_filter((array) $data['productids']);
                        if (!empty($list_of_ids)) {
                            $get_ids = $this->get_translated_ids_from_wpml($list_of_ids, 'product');
                            $merge_data = array_unique(array_filter(array_merge($list_of_ids, $get_ids)));
                            return $merge_data;
                        }
                    } else {
                        return array(0);
                    }
                } elseif ($type == 'categories') {
                    $category_selection = $data['categorytype'];

                    if ($category_selection == 'selected') {
                        $list_of_ids = array_filter((array) $data['categoryids']);
                        if ($data['childcategory'] == 'yes' && $list_of_ids) {
                            /* ------------------------------------------------------------------------- */
                            $get_child_terms = $this->get_child_term_ids($list_of_ids);
                            $list_of_ids = array_unique(array_filter(array_merge($list_of_ids, $get_child_terms)));
                            /* -------------------------------------------------------------------------- */
                        }
                        if (!empty($list_of_ids)) {
                            $get_ids = $this->get_translated_ids_from_wpml($list_of_ids, 'product_cat');
                            $merge_data = array_unique(array_filter(array_merge($list_of_ids, $get_ids)));
                            $array_of_category_ids = join(',', $merge_data);
                            $get_product_ids_from_taxonomy = $wpdb->get_results("SELECT object_id FROM {$wpdb->term_relationships} WHERE term_taxonomy_id IN ($array_of_category_ids)");
                            $merge_taxonomy_data = $this->extract_objects($get_product_ids_from_taxonomy);
                            //var_dump($merge_taxonomy_data);
                            return $merge_taxonomy_data;
                        }
                    } else {
                        $get_product_ids_from_taxonomy = $wpdb->get_results("SELECT object_id FROM {$wpdb->term_relationships}");
                        $merge_taxonomy_data = $this->extract_objects($get_product_ids_from_taxonomy);
                        return $merge_taxonomy_data;
                    }
                } else {
                    //for both scenario
                    $merge_product_data = array();
                    $merge_taxonomy_data = array();

                    $get_stored_producttype = $data['producttype'];
                    $get_stored_categorytype = $data['categorytype'];

                    $get_stored_product_ids = array_filter((array) $data['productids']);
                    $get_stored_taxonomy_ids = array_filter((array) $data['categoryids']);

                    if ($get_stored_producttype == 'selected') {
                        if (!empty($get_stored_product_ids)) {
                            //for stored product ids
                            $get_ids = $this->get_translated_ids_from_wpml($get_stored_product_ids, 'product');
                            $merge_product_data = array_unique(array_filter(array_merge($get_stored_product_ids, $get_ids)));
                        }
                    } else {
                        $merge_product_data = array(0);
                        return $merge_product_data;
                    }

                    if ($get_stored_categorytype == 'selected') {
                        if (!empty($get_stored_taxonomy_ids)) {
                            if ($data['childcategory'] == 'yes' && $get_stored_taxonomy_ids) {
                                /* ------------------------------------------- */
                                $get_child_terms = $this->get_child_term_ids($get_stored_taxonomy_ids);
                                $get_stored_taxonomy_ids = array_unique(array_filter(array_merge($get_stored_taxonomy_ids, $get_child_terms)));
                                /* --------------------------------------------- */
                            }
                            $get_taxonomy_ids = $this->get_translated_ids_from_wpml($get_stored_taxonomy_ids, 'product_cat');
                            $list_tax_ids = array_unique(array_filter(array_merge($get_stored_taxonomy_ids, $get_taxonomy_ids)));
                            $array_of_category_ids = join(',', $list_tax_ids);
                            $get_product_ids_from_taxonomy = $wpdb->get_results("SELECT object_id FROM {$wpdb->term_relationships} WHERE term_taxonomy_id IN ($array_of_category_ids)");
                            $merge_taxonomy_data = $this->extract_objects($get_product_ids_from_taxonomy);
                        }
                    } else {
                        if ($get_stored_producttype == 'selected') {
                            //one products with all categories
                            $merge_taxonomy_data = $this->get_all_term_products();
                        } else {
                            // all products means including categories too
                            $merge_taxonomy_data = array(0);
                        }
                    }
                    $final_merge_data = array_unique(array_filter(array_merge($merge_product_data, $merge_taxonomy_data)));
                    return $final_merge_data;
                }
            }
            return false;
        }

        /**
         *
         * @param array $array_of_objects
         * @return array
         */
        public function extract_objects($array_of_objects) {
            $get_extracted_data = array();
            if (is_array($array_of_objects) && !empty($array_of_objects)) {
                foreach ($array_of_objects as $each_obj) {

                    $productid = $each_obj->object_id;
                    $gather_available_variations = $this->get_variation_id_from_variable($productid);
                    foreach ($gather_available_variations as $eachid) {
                        $get_extracted_data[] = $eachid;
                    }
                }
            }

            return $get_extracted_data;
        }

        public function extract_category_objects($category_object) {
            $get_extracted_data = array();
            if (is_array($category_object) && !empty($category_object)) {
                foreach ($category_object as $each_obj) {

                    $productid = $each_obj->object_id;
                    $get_extracted_data[] = $productid;
                }
            }

            return $get_extracted_data;
        }

        public function get_product_object($id) {
            if (function_exists('wc_get_product')) {
                return wc_get_product($id);
            } else {
                return get_product($id);
            }
        }

        public function get_variation_id_from_variable($id) {

            $prod_obj = $this->get_product_object($id);

            $get_children = array();
            if ($prod_obj) {
                $get_type = $prod_obj->get_type();
                if ($get_type == 'variable') {
                    $get_children = $prod_obj->get_children();
                }
            }
            $merge_array = !empty($get_children) ? (array_merge(array($id), $get_children) ) : array($id);


            return $merge_array;
        }

        /**
         *
         * @global object $wpdb
         * @param array $term_ids
         * @return array
         */
        public function get_child_term_ids($term_ids) {
            $user_id = $this->user_id;
            global $wpdb;
            $extract_data = array();
            $new_extract_data = array();
            if ($term_ids) {
                $list_tax_ids = array_unique(array_filter($term_ids));

                // new revamp
                if (is_array($list_tax_ids) && !empty($list_tax_ids)) {
                    foreach ($list_tax_ids as $each_term_id) {
                        $taxonomy_name = 'product_cat';
                        $get_children = get_term_children($each_term_id, $taxonomy_name);
                        $new_extract_data = array_unique(array_filter(array_merge($new_extract_data, $get_children)));
                    }
                }

                //var_dump($new_extract_data);

                if ($user_id > 0) {
                    $excluded_categories = $this->get_excluded_categories_from_user_id($user_id, $new_extract_data);
                    //var_dump($excluded_categories);
                    if (is_array($excluded_categories) && !empty($excluded_categories)) {
                        $filter_data = array_filter($excluded_categories);
                        $difference = array_diff($extract_data, $filter_data);
                        return $difference;
                    }
                }
            }
            return $new_extract_data;
        }

    }

}
