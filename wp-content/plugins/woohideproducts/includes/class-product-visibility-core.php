<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('CWG_Product_Visibility_Main')):

    class CWG_Product_Visibility_Main {

        public function __construct() {
            add_filter('posts_where_paged', array($this, 'hide_products'), 10, 2);
            add_filter('terms_clauses', array($this, 'hide_terms'), 999, 3);
            add_filter('woocommerce_product_is_visible', array($this, 'visibility_products'), 1, 2);
            add_filter('wp_get_nav_menu_items', array($this, 'show_or_hide_from_menu'), 1, 3);
            add_filter('woocommerce_is_purchasable', array($this, 'restrict_purchase_product'), 1, 2);
            add_filter('woocommerce_add_to_cart_validation', array($this, 'visibility_products'), 1, 2);
            add_filter('woocommerce_subcategory_count_html', array($this, 'remove_category_products_count'), 10, 2);
            add_action('woocommerce_product_query', array($this, 'hide_terms_query'), 10, 2);
        }

        public function restrict_purchase_product($bool, $prod_obj) {
            $get_id = $prod_obj->get_id();

            $bool = $this->visibility_products($bool, $get_id);

            return $bool;
        }

        public function hide_terms_query($q, $obj) {

            $tax_query = (array) $q->get('tax_query');
            // if (!is_admin() && in_array('product_cat', $taxonomy)) {

            $user_id = is_user_logged_in() ? get_current_user_id() : 0;

            $visibility_obj = new CWG_Product_Visibility_API($user_id);
            $get_role = $visibility_obj->get_user_role_from_user_id();
            $check = $visibility_obj->get_rule_status($get_role);

            $get_role = $check ? $get_role : 'allusers';
            $get_settings = $visibility_obj->get_data_from_settings($get_role);

            $visibility = $get_settings['visibility'];


            if ($get_settings['type'] == 'categories' || $get_settings['type'] == 'both') {

                $product_type = $get_settings['producttype'];
                $category_type = $get_settings['categorytype'];

                if (($get_settings['type'] == 'both' && $product_type == 'selected') || $get_settings['type'] == 'categories') {
                    if ($category_type == 'selected') {
                        $array_of_category_ids = array_filter((array) $get_settings['categoryids']);
                        if ($get_settings['childcategory'] == 'yes' && $array_of_category_ids) {
                            /* ------------------------------------------- */
                            $get_child_terms = $visibility_obj->get_child_term_ids($array_of_category_ids);
                            $array_of_category_ids = array_unique(array_filter(array_merge($array_of_category_ids, $get_child_terms)));
                            /* --------------------------------------------- */
                        }

                        if (!empty($array_of_category_ids)) {
                            $get_ids = $visibility_obj->get_translated_ids_from_wpml($array_of_category_ids, 'product_cat');
                            $merge_data = array_unique(array_filter(array_merge($array_of_category_ids, $get_ids)));


                            if ($visibility == 'include') {
                                $tax_query[] = array(
                                    'taxonomy' => 'product_cat',
                                    'field' => 'term_id',
                                    'terms' => $merge_data, // set product categories here
                                    'operator' => 'IN'
                                );
                            } else {
                                $tax_query[] = array(
                                    'taxonomy' => 'product_cat',
                                    'field' => 'term_id',
                                    'terms' => $merge_data, // set product categories here
                                    'operator' => 'NOT IN'
                                );
                            }
                        }
                    } else {
                        // for all categories
                        $get_ids = $visibility_obj->get_all_term_ids();
                        if ($get_ids) {
                            $merge_data = (array) array_unique(array_filter($get_ids));

                            if ($visibility == 'exclude') {
                                $tax_query[] = array(
                                    'taxonomy' => 'product_cat',
                                    'field' => 'term_id',
                                    'terms' => $merge_data, // set product categories here
                                    'operator' => 'NOT IN'
                                );
                            }
                        }
                    }
                } else {
                    // If it is all products then hide all categories also
                    if ($visibility == 'exclude') {
                        $get_ids = $visibility_obj->get_all_term_ids();
                        if ($get_ids) {
                            $merge_data = (array) array_unique(array_filter($get_ids));
                            $tax_query[] = array(
                                'taxonomy' => 'product_cat',
                                'field' => 'term_id',
                                'terms' => $merge_data, // set product categories here
                                'operator' => 'NOT IN'
                            );
                        }
                    }
                }
            } else {
                if ($get_settings['type'] == 'products' && $get_settings['producttype'] == 'all') {
                    if ($visibility == 'exclude') {
                        $get_ids = $visibility_obj->get_all_term_ids();
                        if ($get_ids) {
                            $merge_data = $get_ids;
                            if ($merge_data) {
                                $tax_query[] = array(
                                    'taxonomy' => 'product_cat',
                                    'field' => 'term_id',
                                    'terms' => $merge_data, // set product categories here
                                    'operator' => 'NOT IN'
                                );
                            }
                        }
                    }
                }
            }

            $q->set('tax_query', $tax_query);
        }

        /**
         * @param bool $bool Return either true/false
         * @param int $id ID of the product
         * @return bool it return either true or false
         */
        public function visibility_products($bool, $id) {


            $user_id = is_user_logged_in() ? get_current_user_id() : 0;

            $visibility_obj = new CWG_Product_Visibility_API($user_id);

            $get_role = $visibility_obj->get_user_role_from_user_id();

            $check = $visibility_obj->get_rule_status($get_role);

            $get_role = $check ? $get_role : 'allusers';

            $get_settings = $visibility_obj->get_data_from_settings($get_role);

            $get_product_ids = $visibility_obj->get_list_of_product_ids($get_settings);



            //var_dump($get_product_ids);

            if (is_array($get_product_ids) && !empty($get_product_ids)) {
                $visibility = $get_settings['visibility'];

                if (in_array(0, $get_product_ids)) {

                    if ($visibility == 'exclude') {
                        return false;
                    }
                } else {
                    if ($visibility == 'include' && !in_array($id, $get_product_ids)) {
                        return false;
                    } elseif ($visibility == 'exclude' && in_array($id, $get_product_ids)) {
                        return false;
                    }
                }
            }
            return $bool;
        }

        /**
         * @param string $where contains sql data
         * @param object $query contains post object information
         * @return string serialized sql
         */
        public function hide_products($where, $query) {
            global $wpdb;

            if (!is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
                $product = $query->query_vars['post_type'];
                $product_taxonomy = isset($query->query_vars['taxonomy']) ? $query->query_vars['taxonomy'] : null;
                if ($product == 'product' || (isset($product_taxonomy) && ($product_taxonomy == 'product_cat' || $product_taxonomy == 'product_tag')) || is_search()) {
                    $user_id = is_user_logged_in() ? get_current_user_id() : 0;

                    $visibility_obj = new CWG_Product_Visibility_API($user_id);
                    $get_role = $visibility_obj->get_user_role_from_user_id();
                    $check = $visibility_obj->get_rule_status($get_role);


                    $get_role = $check ? $get_role : 'allusers';
                    $get_settings = $visibility_obj->get_data_from_settings($get_role);


                    $get_product_ids = $visibility_obj->get_list_of_product_ids($get_settings);



                    if (is_array($get_product_ids) && !empty($get_product_ids)) {
                        $visibility = $get_settings['visibility'];
                        $type = $get_settings['type'];
                        $product_type = $get_settings['producttype'];
                        $category_type = $get_settings['categorytype'];

                        $merge_data = array_unique(array_filter($get_product_ids));

                        $arrayofids = join(',', $merge_data);

                        $where .= " AND `post_type`='product'";


                        if (in_array(0, $get_product_ids)) {

                            $search_query = join(',', $get_product_ids);
                            if ($visibility == 'exclude') {
                                //hide
                                $where .= " AND `ID` IN ($search_query)";
                            }
                        } else {
                            if ($visibility == 'include') {
                                //show
                                $where .= " AND `ID` IN ($arrayofids)";
                            } else {
                                //hide
                                $where .= " AND `ID` NOT IN ($arrayofids)";
                            }
                        }
                    }
                }
            }
            return $where;
        }

        /**
         *
         * @param string $pieces
         * @param string $taxonomy
         * @param array $args
         * @return array
         */
        public function hide_terms($pieces, $taxonomy, $args) {

            //var_dump($taxonomy, $args);
            global $wpdb;

            if (!is_admin() && in_array('product_cat', $taxonomy)) {

                $user_id = is_user_logged_in() ? get_current_user_id() : 0;

                $visibility_obj = new CWG_Product_Visibility_API($user_id);
                $get_role = $visibility_obj->get_user_role_from_user_id();
                $check = $visibility_obj->get_rule_status($get_role);

                $get_role = $check ? $get_role : 'allusers';
                $get_settings = $visibility_obj->get_data_from_settings($get_role);

                $visibility = $get_settings['visibility'];


                if ($get_settings['type'] == 'categories' || $get_settings['type'] == 'both') {

                    $product_type = $get_settings['producttype'];
                    $category_type = $get_settings['categorytype'];

                    if (($get_settings['type'] == 'both' && $product_type == 'selected') || $get_settings['type'] == 'categories') {
                        if ($category_type == 'selected') {
                            $array_of_category_ids = array_filter((array) $get_settings['categoryids']);
                            if ($get_settings['childcategory'] == 'yes' && $array_of_category_ids) {
                                /* ------------------------------------------- */
                                $get_child_terms = $visibility_obj->get_child_term_ids($array_of_category_ids);
                                $array_of_category_ids = array_unique(array_filter(array_merge($array_of_category_ids, $get_child_terms)));
                                /* --------------------------------------------- */
                            }

                            if (!empty($array_of_category_ids)) {

                                $get_ids = $visibility_obj->get_translated_ids_from_wpml($array_of_category_ids, 'product_cat');
                                $merge_data = array_unique(array_filter(array_merge($array_of_category_ids, $get_ids)));
                                $get_all_ids = $visibility_obj->get_all_term_ids(FALSE);

                                $difference = array_diff($get_all_ids, $merge_data);

                                $array_of_category_ids = join(',', $merge_data);


                                $append_where = $pieces['where'];

                                if ($visibility == 'include') {
                                    if ($difference) {
                                        $inclusive_category_ids = join(',', $difference);
                                        //IN

                                        $append_where .= " AND t.term_id NOT IN ($inclusive_category_ids)";
                                        $pieces['where'] = $append_where;
                                    }
                                } else {
                                    //NOT IN
                                    // hide
                                    $append_where .= " AND t.term_id NOT IN ($array_of_category_ids)";
                                    $pieces['where'] = $append_where;
                                }
                            }
                        } else {
                            // for all categories
                            $get_ids = $visibility_obj->get_all_term_ids();
                            if ($get_ids) {
                                $merge_data = (array) array_unique(array_filter($get_ids));
                                $array_of_category_ids = join(',', $merge_data);
                                $append_where = $pieces['where'];
                                if ($visibility == 'exclude') {
                                    //NOT IN
                                    // hide
                                    $append_where .= " AND t.term_id NOT IN ($array_of_category_ids)";
                                    $pieces['where'] = $append_where;
                                }
                            }
                        }
                    } else {
                        // If it is all products then hide all categories also
                        if ($visibility == 'exclude') {
                            $get_ids = $visibility_obj->get_all_term_ids();
                            if ($get_ids) {
                                $merge_data = (array) array_unique(array_filter($get_ids));
                                $array_of_category_ids = join(',', $merge_data);
                                $append_where = $pieces['where'];

                                $append_where .= " AND t.term_id NOT IN ($array_of_category_ids)";
                                $pieces['where'] = $append_where;
                            }
                        }
                    }
                } else {
                    if ($get_settings['type'] == 'products' && $get_settings['producttype'] == 'all') {
                        if ($visibility == 'exclude') {
                            $get_ids = $visibility_obj->get_all_term_ids();
                            if ($get_ids) {
                                $merge_data = $get_ids;
                                if ($merge_data) {
                                    $array_of_category_ids = join(',', $merge_data);

                                    $append_where .= " AND t.term_id NOT IN ($array_of_category_ids)";
                                    $pieces['where'] = $append_where;
                                }
                            }
                        }
                    }
                }
            }
            // var_dump($pieces);

            return $pieces;
        }

        /**
         *
         * @param type $items
         * @param type $menu
         * @param type $args
         * @return type
         */
        public function show_or_hide_from_menu($items, $menu, $args) {
            if (!is_admin()) {
                $user_id = is_user_logged_in() ? get_current_user_id() : 0;
                $visibility_obj = new CWG_Product_Visibility_API($user_id);
                $get_role = $visibility_obj->get_user_role_from_user_id();
                // check the role level either product/global
                $check = $visibility_obj->get_rule_status($get_role);

                $get_role = $check ? $get_role : 'allusers';

                $get_settings = $visibility_obj->get_data_from_settings($get_role);

                $product_ids = $visibility_obj->get_list_of_product_ids($get_settings);

                $product_type = $get_settings['producttype'];
                $category_type = $get_settings['categorytype'];

                $get_type = $get_settings['type'];

                if ($get_type == 'products') {
                    $product_ids = $visibility_obj->get_list_of_product_ids($get_settings);
                    $category_ids = false;
                } elseif ($get_type == 'categories') {
                    $product_ids = $visibility_obj->get_list_of_product_ids($get_settings);
                    if ($category_type == 'selected') {
                        $array_of_category_ids = array_filter((array) $get_settings['categoryids']);
                        if ($get_settings['childcategory'] == 'yes' && $array_of_category_ids) {
                            /* ------------------------------------------- */
                            $get_child_terms = (array) $visibility_obj->get_child_term_ids($array_of_category_ids);
                            $array_of_category_ids = array_unique(array_filter(array_merge($array_of_category_ids, $get_child_terms)));
                            /* --------------------------------------------- */
                        }
                        if (!empty($array_of_category_ids)) {
                            $get_ids = $visibility_obj->get_translated_ids_from_wpml($array_of_category_ids, 'product_cat');
                            $array_of_category_ids = array_unique(array_filter(array_merge($array_of_category_ids, $get_ids)));
                        }
                        $category_ids = $array_of_category_ids;
                    } else {
                        $category_ids = $visibility_obj->get_all_term_ids();
                    }
                } elseif ($get_type == 'both') {
                    $product_ids = $visibility_obj->get_list_of_product_ids($get_settings);

                    if ($category_type == 'selected') {
                        $array_of_category_ids = array_filter((array) $get_settings['categoryids']);
                        if ($get_settings['childcategory'] == 'yes' && $array_of_category_ids) {
                            /* ------------------------------------------- */
                            $get_child_terms = (array) $visibility_obj->get_child_term_ids($array_of_category_ids);
                            $array_of_category_ids = array_unique(array_filter(array_merge($array_of_category_ids, $get_child_terms)));
                            /* --------------------------------------------- */
                        }
                        if (!empty($array_of_category_ids)) {
                            $get_ids = $visibility_obj->get_translated_ids_from_wpml($array_of_category_ids, 'product_cat');
                            $array_of_category_ids = array_unique(array_filter(array_merge($array_of_category_ids, $get_ids)));
                        }
                        $category_ids = $array_of_category_ids;
                    } else {
                        $category_ids = $visibility_obj->get_all_term_ids();
                    }
                }

                $visibility_type = $get_settings['visibility'];
                foreach ($items as $key => $item) {

                    if (isset($item->object) && $item->object == 'product') {
                        $menu_product_id = $item->object_id;
                        if ($product_ids && $menu_product_id > 0) {
                            //Hide the Product
                            if ($visibility_type == 'exclude') {
                                if (in_array($menu_product_id, $product_ids)) {
                                    unset($items[$key]);
                                }
                            }
                        }
                    } elseif (isset($item->object) && $item->object == 'product_cat') {
                        $menu_category_id = $item->object_id;

                        if ($category_ids && $menu_category_id > 0) {
                            if ($visibility_type == 'exclude') {
                                if (in_array($menu_category_id, $category_ids)) {
                                    unset($items[$key]);
                                }
                            }
                        }
                    }
                }
            }

            return $items;
        }

        public function remove_category_products_count($html, $category) {
            $get_option = get_option('cwg_hide_products_remove_category_count', 'no');
            if ($get_option == 'yes') {
                return;
            } else {
                return $html;
            }
        }

    }

    endif;

return new CWG_Product_Visibility_Main();
