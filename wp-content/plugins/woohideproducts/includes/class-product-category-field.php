<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

class CWG_Add_Product_Category_Field {

    public function __construct() {
        add_action('product_cat_edit_form_fields', array($this, 'edit_term_data'), 10);
        add_action('edited_product_cat', array($this, 'save_term_data'));
    }

    public function edit_term_data($term) {
        $term_id = $term->term_id;

        $cwg_exclude_users_from_visibility = get_term_meta($term_id, 'cwg_exclude_users_from_visibility', true);
        if (WC_VERSION >= (float) ('3.0.0')) {
            ?>
            <tr class="form-field">
                <th scope="row" valign="top"><label for="cwg_exclude_users_from_visibility"><?php _e('Exclude Users from Visibility Role Settings', 'cwghideproducts'); ?></label></th>
                <td>
                    <?php
                    $value = array('css' => "", 'id' => 'cwg_exclude_users_from_visibility', 'placeholder' => __('Search Users', 'cwghideproducts'));
                    $saved_data = $cwg_exclude_users_from_visibility;
                    echo $this->customer_search($value, $saved_data);
                    ?>
                    <p class="description"><?php _e('If you want to exclude some of the users from specific role then this option might help. For Example Category A and Category B was selected in Visibiliity settings for Customer Role, User A and User B is also belongs to Customer Role, but admin wants User A to view Category A not B and User B to view Category B Not A, so using this option Edit Category A - Exclude User B and Edit Category B - Exclude User A', 'cwghideproducts'); ?></p>
                </td>
            </tr>
            <?php
        }
    }

    public function save_term_data($term_id) {
        if (isset($_POST['cwg_exclude_users_from_visibility'])) {
            update_term_meta($term_id, 'cwg_exclude_users_from_visibility', $_POST['cwg_exclude_users_from_visibility']);
        } else {
            update_term_meta($term_id, 'cwg_exclude_users_from_visibility', array());
        }
    }

    public function customer_search($value, $saved_data = array()) {
        ob_start();
        $user_id = '';
        $user_string = '';
        ?>
        <select class="wc-customer-search" id="<?php echo $value['id']; ?>" style="width:330px;" name="<?php echo $value['id']; ?>[]" data-placeholder="<?php echo $value['placeholder']; ?>" multiple="multiple"> <?php
            $get_datas = array();
            $available_data = $saved_data;
            if ($available_data) {
                $ids = is_array($available_data) ? array_unique(array_filter(array_map('absint', (array) $available_data))) : array_unique(array_filter(array_map('absint', (array) explode(',', $available_data))));
                foreach ($ids as $eachid) {

                    $user_id = $eachid;

                    $user = get_user_by('id', $user_id);
                    $user_string = sprintf(
                            esc_html__('%1$s (#%2$s &ndash; %3$s)', 'woocommerce'), $user->display_name, absint($user->ID), $user->user_email
                    );
                    ?>
                    <option value="<?php echo esc_attr($user_id); ?>" selected="selected"><?php echo htmlspecialchars($user_string); ?></option>
                    <?php
                }
            } else {
                ?>
                <option value=" "></option>
            <?php }
            ?> </select>
        <?php
        return ob_get_clean();
    }

}

new CWG_Add_Product_Category_Field();
