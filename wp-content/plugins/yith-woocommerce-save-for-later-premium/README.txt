=== YITH WooCommerce Save For Later ===

Contributors: yithemes
Requires at least: 3.5.1
Tested up to: 4.9.6
Stable tag: 1.0.10
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Documentation:

== Changelog ==

= 1.0.10 =

* New: Support to WooCommerce 3.4.0
* Update: Plugin Framework
* Tweak: GDPR compliance

= 1.0.9 =
* Update: Plugin Framework
* Fix: Products not added to the list

= 1.0.8 =
* New: Support to WordPress 4.9.6 RC1
* New: Support to WooCommerce 3.4.0 RC 1
* New: Support to GDPR compliance
* Update: Plugin Framework
* Update: Language files
* Update: Italian language

= 1.0.7 =
* New: Support to WooCommerce 3.3.0
* New: Support to WordPress 4.9.2
* New: Dutch Language file
* Update: Plugin Framework 3.0
* Fix: Product not added in save for later list for guest users

= 1.0.6 =
* New: Save for later button in single product page
* New: Support to WooCommerce 3.0.0-RC2
* Update: Language file
* Update: Plugin Framework

= 1.0.5 =

* New: Spanish language file
* Update: Plugin Framework

= 1.0.4 =

* Fix: Add To Cart button redirect in a blank page
* Update : Plugin Framework

= 1.0.3 =

* New: Products no longer available are automatically removed from "Save for Later" list.
* Update: Plugin Framework

= 1.0.2 =

* Fix : Hide "Save for Later" link in mini-cart

= 1.0.1 =

* New: Support to WooCommerce 2.4
* New: variable product support
* Update: Plugin core framework

= 1.0.0 =

* Initial release