��    $      <  5   \      0     1     G     ]  +   m     �     �  s   �  3   $     X     a  
   z     �     �     �     �     �  !   �     �  #        <     O     `     o     �     �  (   �     �     �  1   �       j   6  X   �  o   �  3   j     �  X  �     
     %
     <
  8   T
     �
     �
  �   �
  L   -     z  #   �     �     �     �     �     �            +   9  $   e     �     �     �     �     �     �  1   �     +     8  C   Q     �  p   �  a   %  n   �  8   �     /                     #   $      !                                  "   	                                     
                                                                            "Save for Later" text 1 Product %s Products Add button text Button text to remove product from the list Free! General Settings If YITH WooCommerce Wishlist is installed, you can display the "Add to Wishlist" link in your "Save for Later" list If checked, the button shows on single product page In Stock No Products in save list No product Out of Stock Plugin Documentation Premium Version Product Settings Product added Product already in Save for later Product correctly added to cart Product deleted from Save for later Remove button text Remove from list Save for Later Save for Later page Save for later Saved for later  Set the text for "save for later" button Settings Show "Add to Wishlist" Show Save For Later button on single product page Single Product Settings This page contains the [yith_wsfl_saveforlater] shortcode.<br> You can use this shortcode in other pages!. We are sorry, but this feature is available only if cookies are enabled in your browser. YITH WooCommerce Save for Later Premium is enabled but not effective. It requires WooCommerce in order to work. You can set the text for your "Save for Later" link page_slugsaveforlater Project-Id-Version: YITH WooCommerce Save for Later
POT-Creation-Date: 2018-05-12 12:22+0200
PO-Revision-Date: 2018-05-12 12:25+0200
Last-Translator: 
Language-Team: Yithemes <plugins@yithemes.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.13
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Testo "Save for Later" 1 prodotto %s prodotti Aggiungi testo pulsante Testo del pulsante per rimuovere il prodotto dalla lista Gratis! Impostazioni Generali Se hai installato YITH WooCommerce Wishlist, è possibile visualizzare il link "Add to Wishlist" nella tua lista "Save for Later" Se selezionato, il pulsante viene mostrato sulla pagina del singolo prodotto Disponibile Nessun prodotto nella lista salvati Nessun prodotto Non disponibile Documentazione plugin Versione Premium Impostazioni prodotto Prodotto aggiunto Prodotto già in Save for later Prodotto aggiunto correttamente al carrello Prodotto eliminato da Save for later Testo pulsante Rimuovi Rimuovi dalla lista Save for Later Pagina Save for Later Save for later Saved for later Impostare il testo per il pulsante Salva per dopo Impostazioni Mostra "Add to Wishlist" Mostra il pulsante Salva per dopo nella pagina del singolo prodotto Impostazione singolo prodotto  Questa pagina contiene lo shortcode  [yith_wsfl_saveforlater]. <br> Puoi usare questo shortcode in altre pagine! Ci dispiace, questa funzionalità è disponibile solo se i cookies nel tuo browser sono attivati. YITH WooCommerce Save for Later Premium è attivo ma non operativo. Il suo funzionamento richiede WooCommerce. Puoi impostare il testo per il tuo link "Save for Later" saveforlater 