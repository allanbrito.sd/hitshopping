��    $      <  5   \      0     1     G     ]  +   m     �     �  s   �  3   $     X     a  
   z     �     �     �     �     �  !   �     �  #        <     O     `     o     �     �  (   �     �     �  1   �       j   6  X   �  o   �  3   j     �  W  �     
     (
     ?
  5   S
     �
     �
  �   �
  H   2  
   {     �     �     �     �     �     �     �  ,     (   <  )   e     �     �     �     �     �     �  .        <  !   I  5   k     �  r   �  [   0  t   �  9        ;                     #   $      !                                  "   	                                     
                                                                            "Save for Later" text 1 Product %s Products Add button text Button text to remove product from the list Free! General Settings If YITH WooCommerce Wishlist is installed, you can display the "Add to Wishlist" link in your "Save for Later" list If checked, the button shows on single product page In Stock No Products in save list No product Out of Stock Plugin Documentation Premium Version Product Settings Product added Product already in Save for later Product correctly added to cart Product deleted from Save for later Remove button text Remove from list Save for Later Save for Later page Save for later Saved for later  Set the text for "save for later" button Settings Show "Add to Wishlist" Show Save For Later button on single product page Single Product Settings This page contains the [yith_wsfl_saveforlater] shortcode.<br> You can use this shortcode in other pages!. We are sorry, but this feature is available only if cookies are enabled in your browser. YITH WooCommerce Save for Later Premium is enabled but not effective. It requires WooCommerce in order to work. You can set the text for your "Save for Later" link page_slugsaveforlater Project-Id-Version: YITH WooCommerce Save for Later
POT-Creation-Date: 2018-01-29 19:35+0000
PO-Revision-Date: 2018-01-29 19:43+0000
Last-Translator: 
Language-Team: Yithemes <plugins@yithemes.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 "Bewaren voor later" tekst 1 Product %s Producten Voeg knop tekst toe Knop tekst om het product uit de lijst te verwijderen Gratis! Algemene instellingen Indien YITH WooCommerce Wishlist is geïnstalleerd, kunt u de "toevoegen aan wensenlijst" link weergeven in uw "Bewaren voor later" lijst  Indien aangevinkt, wordt de knop weergegeven op de single product pagina Op vooraad Geen producten in bewaren lijst Geen product Niet op voorraad Plugin Documentatie Premium Versie Product instellingen Product toegevoegd Product al toegevoegd aan bewaren voor later Product juist toegevoegd aan winkelwagen Product verwijderd uit bewaren voor later Verwijder knop tekst Verwijderen van lijst Bewaren voor later Bewaren voor later pagina Bewaren voor later Bewaren voor later Bepaal de tekst voor "Bewaren voor later" knop Instellingen Toon "Toegevoegd aan wensenlijst" Toon bewaren voor later knop op single product pagina Single product instellingen Deze pagina bevat de [yith_wsfl_saveforlater] shortcode. <br> U kunt deze shortcode gebruiken in andere pagina's!. Sorry, deze functie is uitsluitend beschikbaar als cookies zijn ingeschakeld in je browser. YITH WooCommerce Save for Later Premium is ingeschakeld maar niet werkend. Het heeft WooCommerce nodig om te werken. U kunt de tekst bepalen voor uw "Bewaren voor later" link saveforlater 