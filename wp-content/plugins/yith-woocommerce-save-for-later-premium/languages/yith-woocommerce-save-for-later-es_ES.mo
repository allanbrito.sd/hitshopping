��          �   %   �      P     Q     g     }     �  s   �       
             )     >     N  !   \     ~  #   �     �     �     �     �            j   %  X   �  o   �  3   Y     �  W  �  $   �     !  	   9     C  �   U     �     �     �     
	     $	     5	     G	  *   e	  "   �	     �	     �	     �	     �	     

  #   
  q   6
  p   �
  p     F   �     �                	                                                                                               
              "Save for Later" text 1 Product %s Products Free! General Settings If YITH WooCommerce Wishlist is installed, you can display the "Add to Wishlist" link in your "Save for Later" list In Stock No product Out of Stock Plugin Documentation Premium Version Product added Product already in Save for later Product correctly added to cart Product deleted from Save for later Save for Later Save for Later page Save for later Saved for later  Settings Show "Add to Wishlist" This page contains the [yith_wsfl_saveforlater] shortcode.<br> You can use this shortcode in other pages!. We are sorry, but this feature is available only if cookies are enabled in your browser. YITH WooCommerce Save for Later Premium is enabled but not effective. It requires WooCommerce in order to work. You can set the text for your "Save for Later" link page_slugsaveforlater Project-Id-Version: YITH WooCommerce Save for Later
POT-Creation-Date: 2015-11-09 14:05+0100
PO-Revision-Date: 2016-06-30 11:50+0100
Last-Translator: 
Language-Team: Yithemes <plugins@yithemes.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Texto para "Guardar para más tarde" 1 Producto %s Productos ¡Gratis! Ajustes Generales Si YITH WooCommerce Wishlist está instalado, puedes mostrar el enlace "Añadir a la Lista de deseos" en tu lista "Guardar para más tarde" En Stock Ningún producto Fuera de Stock Documentación del Plugin Versión Premium Producto añadido Producto ya en Save for later Producto añadido correctamente al carrito Producto borrado de Save for later Guardar para más tarde Página Save for Later Save for later Guardado para más tarde Ajustes Mostrar "Añadir a Lista de deseos" Esta página contiene el shortcode  [yith_wsfl_saveforlater].<br> ¡Puedes usar este shortcode en otras páginas! Lo sentimos, pero esta característica sólo está disponible si las cookies están habilitadas en tu navegador. YITH WooCommerce Save for Later Premium está activado pero no es efectivo. Requiere WooCommerce para funcionar. Puedes establecer el texto para tu enlace de "Guardar para más tarde" saveforlater 