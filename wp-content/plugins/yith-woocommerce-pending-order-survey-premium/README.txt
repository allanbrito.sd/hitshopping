== YITH WooCommerce Pending Order Survey  ==

= 1.0.5 =

* New: Support to WooCommerce 3.4.0
* New: Support to WordPress 4.9.6
* New: Support to GDPR compliance
* Update: Italian language
* Update: Spanish language
* Update: Plugin Framework

= 1.0.4
* New: Spanish language
* New: Support to WooCommerce 3.3.5
* New: Support to WordPress 4.9.5
* Update: Plugin Framework

= 1.0.3 =
* New: Support to WooCommerce 3.3.0
* New: Support to WordPress 4.9.2
* Update: Plugin Framework 3.0
= 1.0.2 =
* New: Support to WooCommerce 3.2.0
* New: Italian language file
* Update Plugin Framework

= 1.0.1 =
* New: Support to WooCommerce 3.0-RC2
* Update: Plugin Framework


= 1.0.0 =

* Initial release
