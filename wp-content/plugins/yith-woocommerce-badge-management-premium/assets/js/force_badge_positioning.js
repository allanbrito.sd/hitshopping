/* global yith_wcbm_params */
jQuery( function ( $ ) {
    var $clonedBadgeContainer   = $( '#yith-wcbm-cloned-badges' ),
        $badges                 = $( '.yith-wcbm-badge:not(.yith-wcbm-badge-clone):visible' ),
        $body                   = $( 'body' ),
        force_badge_positioning = function () {
            if ( $clonedBadgeContainer.length < 1 ) {
                $clonedBadgeContainer = $( '<div id="yith-wcbm-cloned-badges"></div>' );
                $( 'body' ).append( $clonedBadgeContainer );
            } else {
                $clonedBadgeContainer.html( '' );
            }

            $badges.show();

            $badges.each( function () {
                var $badge        = $( this ),
                    $badgeClone   = $badge.clone().addClass( 'yith-wcbm-badge-clone' ),
                    relative_body = $body.css( 'position' ) !== 'static',
                    top_offset    = relative_body ? ( $badge.offset().top - $body.position().top ) + 'px' : $badge.offset().top + 'px',
                    left_offset   = relative_body ? ( $badge.offset().left - $body.position().left ) + 'px' : $badge.offset().left + 'px',
                    cssOptions    = {
                        top   : top_offset,
                        left  : left_offset,
                        bottom: 'auto',
                        right : 'auto'
                    };

                $badgeClone.css( cssOptions );
                $badge.hide();

                $clonedBadgeContainer.append( $badgeClone );
                $badgeClone.show();
            } );
        };

    setTimeout( force_badge_positioning, yith_wcbm_params.timeout );

    $( window ).resize( force_badge_positioning );

    if ( yith_wcbm_params.is_mobile === 'yes' && yith_wcbm_params.on_scroll_mobile ) {
        $( window ).scroll( force_badge_positioning );
    }
} );