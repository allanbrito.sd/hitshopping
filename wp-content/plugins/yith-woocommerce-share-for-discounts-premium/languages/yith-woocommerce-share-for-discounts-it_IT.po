msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Save For Discounts\n"
"POT-Creation-Date: 2016-10-18 10:25+0200\n"
"PO-Revision-Date: 2016-10-18 10:25+0200\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <plugins@hitoutlets.com>\n"
"Language: it_IT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../class.yith-wc-share-for-discounts-premium.php:498
msgid "You need to add a Linkedin Client ID"
msgstr "È necessario aggiungere il Client ID di Linkedin"

#: ../class.yith-wc-share-for-discounts-premium.php:504
msgid "You need to add a Linkedin Client Secret"
msgstr "È necessario aggiungere la Client Secret di Linkedin"

#: ../class.yith-wc-share-for-discounts-premium.php:738
#: ../class.yith-wc-share-for-discounts.php:309
msgid ""
"You closed the parent window. The authorization process has been suspended."
msgstr ""
"Hai chiuso la finestra principale. Il processo di autorizzazione è stato "
"sospeso."

#: ../class.yith-wc-share-for-discounts-premium.php:739
#: ../class.yith-wc-share-for-discounts.php:310
msgid "The authorization process has failed."
msgstr "Il processo di autorizzazione è fallito."

#: ../class.yith-wc-share-for-discounts.php:179
#: ../class.yith-wc-share-for-discounts.php:183
msgid "General Settings"
msgstr "Impostazioni generali"

#: ../class.yith-wc-share-for-discounts.php:180
#: ../plugin-options/coupon-options.php:18
msgid "Coupon Settings"
msgstr "Impostazioni coupon"

#: ../class.yith-wc-share-for-discounts.php:181
#: ../plugin-options/share-options.php:19
msgid "Sharing Settings"
msgstr "Impostazioni condivisione"

#: ../class.yith-wc-share-for-discounts.php:184
#: ../class.yith-wc-share-for-discounts.php:1001
msgid "Premium Version"
msgstr "Versione premium"

#: ../class.yith-wc-share-for-discounts.php:190
#: ../class.yith-wc-share-for-discounts.php:191
#: ../includes/class-ywsfd-share-email.php:47
msgid "Share For Discounts"
msgstr "Share For Discounts"

#: ../class.yith-wc-share-for-discounts.php:217
msgid "You need to add a Facebook App ID"
msgstr "È necessario aggiungere un App ID Facebook"

#: ../class.yith-wc-share-for-discounts.php:222
msgid "You need to add a Twitter Consumer Key (API Key)"
msgstr "È necessario aggiungere la Consumer Key (API Key) di Twitter"

#: ../class.yith-wc-share-for-discounts.php:228
msgid "You need to add a Twitter Consumer Secret (API Secret)"
msgstr "È necessario aggiungere la Consumer Secret (API Secret) di Twitter"

#: ../class.yith-wc-share-for-discounts.php:240
msgid "YITH WooCommerce Share For Discounts"
msgstr "YITH WooCommerce Share For Discounts"

#: ../class.yith-wc-share-for-discounts.php:290
#: ../class.yith-wc-share-for-discounts.php:391
msgid "via"
msgstr "tramite"

#: ../class.yith-wc-share-for-discounts.php:378
#, php-format
msgid "Share the product and get 10% discount!"
msgstr "Condividi il prodotto e ottieni il 10% di sconto!"

#: ../class.yith-wc-share-for-discounts.php:413
#: ../plugin-options/share-options.php:33
msgid "Thank you for sharing!"
msgstr "Grazie per aver condiviso!"

#: ../class.yith-wc-share-for-discounts.php:417
#: ../plugin-options/share-options.php:41
msgid "Your discount has been activated and applied to your shopping cart."
msgstr "Lo sconto è stato attivato e applicato al tuo carrello."

#: ../class.yith-wc-share-for-discounts.php:622
msgid "Guest"
msgstr "Ospite"

#: ../class.yith-wc-share-for-discounts.php:754
#: ../plugin-options/coupon-options.php:25
#, php-format
msgid "10% off for the shared product"
msgstr "10% di sconto per il prodotto condiviso"

#: ../class.yith-wc-share-for-discounts.php:998
msgid "Settings"
msgstr "Impostazioni"

#: ../class.yith-wc-share-for-discounts.php:1028
msgid "Plugin Documentation"
msgstr "Documentazione plugin"

#: ../class.yith-wc-share-for-discounts.php:1081
msgid "To use the coupon, you need to add the product to the cart"
msgstr "Per utilizzare il coupon, devi aggiungere il prodotto al carrello"

#: ../includes/class-ywsfd-ajax-premium.php:86
#: ../includes/class-ywsfd-ajax-premium.php:90
#: ../includes/class-ywsfd-ajax.php:85
msgid "We were unable to process your request, please try again."
msgstr "Non è stato possibile soddisfare la tua richiesta, riprova di nuovo."

#: ../includes/class-ywsfd-ajax-premium.php:97
#: ../templates/frontend/mail-form-premium.php:4
msgid "Your friend email"
msgstr "Email di un tuo amico"

#: ../includes/class-ywsfd-ajax-premium.php:102
#: ../templates/frontend/mail-form-premium.php:11
msgid "Your email"
msgstr "La tua email"

#: ../includes/class-ywsfd-ajax-premium.php:107
msgid "Your message"
msgstr "Il tuo messaggio"

#: ../includes/class-ywsfd-ajax-premium.php:124
msgid "is a required field."
msgstr "è un campo necessario."

#: ../includes/class-ywsfd-ajax-premium.php:130
msgid "is not a valid email address."
msgstr "non è un indirizzo email valido."

#: ../includes/class-ywsfd-ajax-premium.php:143
#, php-format
msgid "%s wants to share something with you on %s site"
msgstr "%s vuole condividere qualcosa con te sul sito %s"

#: ../includes/class-ywsfd-ajax-premium.php:152
msgid "There was an error while sending the email, please try again."
msgstr "Si è verificato un errore durante l'invio dell'email, riprova."

#: ../includes/class-ywsfd-ajax-premium.php:268
#: ../includes/class-ywsfd-ajax.php:188 ../includes/class-ywsfd-ajax.php:228
msgid "An error occurred"
msgstr "Si è verificato un errore"

#: ../includes/class-ywsfd-ajax-premium.php:313
#, php-format
msgid "Cancellation completed. %d coupon deleted."
msgid_plural "Cancellation completed. %d coupons deleted."
msgstr[0] "Cancellazione completata. %d coupon cancellato."
msgstr[1] "Cancellazione completata. %d coupon cancellati."

#: ../includes/class-ywsfd-ajax-premium.php:318
#, php-format
msgid "An error has occurred: %s"
msgstr "Si è verificato un errore: %s"

#: ../includes/class-ywsfd-ajax.php:204
msgid "The message field cannot be empty"
msgstr "Il campo messaggio non può essere lasciato vuoto"

#: ../includes/class-ywsfd-share-email.php:46
msgid ""
"YITH WooCommerce Share For Discounts gives you the perfect tool to reward "
"your users when they share the products they are going to purchase."
msgstr ""
"YITH WooCommerce Share For Discounts ti offre lo strumento perfetto per "
"ricompensare i tuoi utenti quando condividono i prodotti che acquisteranno. "

#: ../init.php:23
msgid ""
"YITH WooCommerce Share For Discounts is enabled but not effective. It "
"requires WooCommerce in order to work."
msgstr ""
"YITH WooCommerce Share For Discounts è abilitato ma non in funzione. Devi "
"installare WooCommerce affinché questo possa funzionare correttamente."

#: ../plugin-options/coupon-options.php:22
msgid "Coupon settings"
msgstr "Impostazioni coupon"

#: ../plugin-options/coupon-options.php:34
msgid "Expired Coupon Clearing"
msgstr "Rimozione Coupon Scaduti"

#: ../plugin-options/coupon-options.php:36
msgid ""
"Automatically deletes expired coupons (Only those created by this plugin)"
msgstr ""
"Cancella automaticamente i coupon scaduti (solo quelli creati da questo "
"plugin)"

#: ../plugin-options/general-options.php:22
#: ../plugin-options/premium-general-options.php:22
#: ../plugin-options/premium-general-options.php:38
msgid "or"
msgstr "o"

#: ../plugin-options/general-options.php:37
#: ../plugin-options/premium-general-options.php:53
msgid "Share For Discounts settings"
msgstr "Impostazioni Share For Discounts"

#: ../plugin-options/general-options.php:41
#: ../plugin-options/premium-general-options.php:57
msgid "Enable YITH WooCommerce Share For Discounts"
msgstr "Attiva YITH WooCommerce Share For Discounts"

#: ../plugin-options/general-options.php:52
#: ../plugin-options/premium-general-options.php:68
msgid "Facebook"
msgstr "Facebook"

#: ../plugin-options/general-options.php:56
#: ../plugin-options/premium-general-options.php:72
msgid "Enable Facebook sharing"
msgstr "Attiva condivisione Facebook"

#: ../plugin-options/general-options.php:63
#: ../plugin-options/premium-general-options.php:79
msgid "Facebook App ID"
msgstr "App ID Facebook"

#: ../plugin-options/general-options.php:70
#: ../plugin-options/premium-general-options.php:86
msgid "Facebook Button Type"
msgstr "Tipo pulsante Facebook"

#: ../plugin-options/general-options.php:72
#: ../plugin-options/premium-general-options.php:88
msgid "Select the type of button you want to show for Facebook"
msgstr "Scegli il tipo di pulsante che vuoi mostrare per Facebook"

#: ../plugin-options/general-options.php:75
#: ../plugin-options/premium-general-options.php:91
msgid "Like and Share Buttons"
msgstr "Pulsanti \"Mi piace\" e \"Condividi\""

#: ../plugin-options/general-options.php:76
#: ../plugin-options/premium-general-options.php:92
msgid "Like Button Only"
msgstr "Pulsante \"Mi piace\""

#: ../plugin-options/general-options.php:77
#: ../plugin-options/general-options.php:144
#: ../plugin-options/premium-general-options.php:93
#: ../plugin-options/premium-general-options.php:160
msgid "Share Button Only"
msgstr "Pulsante \"Condividi\""

#: ../plugin-options/general-options.php:86
#: ../plugin-options/premium-general-options.php:102
msgid "Twitter"
msgstr "Twitter"

#: ../plugin-options/general-options.php:90
#: ../plugin-options/premium-general-options.php:106
msgid "Enable Twitter sharing"
msgstr "Attiva condivisione Twitter"

#: ../plugin-options/general-options.php:96
#: ../plugin-options/premium-general-options.php:112
msgid "Twitter username"
msgstr "Nome utente Twitter"

#: ../plugin-options/general-options.php:98
#: ../plugin-options/premium-general-options.php:114
msgid ""
"Set this option if you want to include \"via @YourUsername\" to your tweets"
msgstr ""
"Seleziona questa opzione se vuoi includere \"via @IlTuoNomeUtente\" ai tuoi "
"tweet"

#: ../plugin-options/general-options.php:103
#: ../plugin-options/premium-general-options.php:119
msgid "Twitter Consumer Key (API Key)"
msgstr "Consumer Key (API Key) di Twitter"

#: ../plugin-options/general-options.php:109
#: ../plugin-options/premium-general-options.php:125
msgid "Twitter Consumer Secret (API Secret)"
msgstr "Consumer Secret (API Secret) di Twitter"

#: ../plugin-options/general-options.php:115
#: ../plugin-options/premium-general-options.php:131
#: ../plugin-options/premium-general-options.php:191
msgid "Callback URL"
msgstr "URL di callback"

#: ../plugin-options/general-options.php:117
#: ../plugin-options/premium-general-options.php:133
msgid ""
"Copy this text string into the \"Callback URL\" field of your Twitter App"
msgstr ""
"Copia questo testo nel campo \"URL di callback\" della tua applicazione "
"Twitter"

#: ../plugin-options/general-options.php:125
#: ../plugin-options/premium-general-options.php:141
msgid "Google+"
msgstr "Google+"

#: ../plugin-options/general-options.php:129
#: ../plugin-options/premium-general-options.php:145
msgid "Enable Google+ sharing"
msgstr "Attiva condivisione Google+"

#: ../plugin-options/general-options.php:136
#: ../plugin-options/premium-general-options.php:152
msgid "Google+ Button Type"
msgstr "Tipo pulsante Google+"

#: ../plugin-options/general-options.php:138
#: ../plugin-options/premium-general-options.php:154
msgid "Select the type of button you want to show for Google+."
msgstr "Seleziona il tipo di pulsante che vuoi mostrare per Google+."

#: ../plugin-options/general-options.php:142
#: ../plugin-options/premium-general-options.php:158
msgid "+1 and Share Buttons"
msgstr "Pulsanti \"+1\" e \"Condividi\""

#: ../plugin-options/general-options.php:143
#: ../plugin-options/premium-general-options.php:159
msgid "+1 Button Only"
msgstr "Pulsante \"+1\""

#: ../plugin-options/premium-general-options.php:169
msgid "Linkedin"
msgstr "Linkedin"

#: ../plugin-options/premium-general-options.php:173
msgid "Enable Linkedin sharing"
msgstr "Attiva condivisione Linkedin"

#: ../plugin-options/premium-general-options.php:179
msgid "Linkedin Client ID"
msgstr "Client ID di Linkedin"

#: ../plugin-options/premium-general-options.php:185
msgid "Linkedin Client Secret"
msgstr "Client Secret di Linkedin"

#: ../plugin-options/premium-general-options.php:193
msgid ""
"Copy this text string into the \"Authorized Redirect URLs\" field of your "
"Linkedin App"
msgstr ""
"Copia questo testo nel campo \"URL reindirizzamento autorizzati\" della tua "
"applicazione Linkedin"

#: ../plugin-options/premium-general-options.php:201
msgid "Email to a friend"
msgstr "Invia via email a un amico"

#: ../plugin-options/premium-general-options.php:205
msgid "Enable email sharing"
msgstr "Attiva condivisione via email"

#: ../plugin-options/share-options.php:23
msgid "Box title before sharing"
msgstr "Titolo del box precedente alla condivisione"

#: ../plugin-options/share-options.php:25
msgid "Share and get your discount!"
msgstr "Condividi e ottieni il tuo sconto!"

#: ../plugin-options/share-options.php:28
msgid "Title showed above the sharing buttons"
msgstr "Titolo mostrato sopra ai pulsanti di condivisione"

#: ../plugin-options/share-options.php:31
msgid "Box title after sharing"
msgstr "Titolo del box successivo alla condivisione"

#: ../plugin-options/share-options.php:36
msgid "Title showed after the sharing"
msgstr "Titolo mostrato dopo la condivisione"

#: ../plugin-options/share-options.php:39
msgid "Message after sharing"
msgstr "Messaggio successivo alla condivisione"

#: ../plugin-options/share-options.php:44
msgid "Text that replaces the buttons after the sharing"
msgstr "Testo che sostituisce i pulsanti dopo la condivisione"

#: ../plugin-options/share-options.php:47
msgid "Show on product page"
msgstr "Mostra sulla pagina del prodotto"

#: ../plugin-options/share-options.php:57
msgid "Product page position"
msgstr "Posizione pagina prodotto"

#: ../plugin-options/share-options.php:62
msgid ""
"The position where the sharing buttons are showed in product detail pages."
msgstr ""
"La posizione dove i pulsanti di condivisione sono mostrati nelle pagine "
"dettaglio dei prodotti."

#: ../plugin-options/share-options.php:64
msgid "Before title"
msgstr "Prima del titolo"

#: ../plugin-options/share-options.php:65
msgid "After price"
msgstr "Dopo il prezzo"

#: ../plugin-options/share-options.php:66
msgid "Before \"Add to cart\""
msgstr "Prima di \"Aggiungi al carrello\""

#: ../plugin-options/share-options.php:67
msgid "Before tabs"
msgstr "Prima delle tab"

#: ../plugin-options/share-options.php:68
msgid "Between tabs and related products"
msgstr "Tra le tab e i prodotti correlati"

#: ../plugin-options/share-options.php:69
msgid "After related products"
msgstr "Dopo i prodotti correlati"

#: ../plugin-options/share-options.php:73
msgid "Enable on on-sale products"
msgstr "Abilita sui prodotti in saldo"

#: ../plugin-options/share-options.php:83
msgid "Show on checkout page"
msgstr "Mostra nella pagina pagamento"

#: ../plugin-options/share-options.php:93
msgid "Checkout position"
msgstr "Posizione nella pagina pagamento"

#: ../plugin-options/share-options.php:98
msgid "The position where share buttons are showed in checkout page."
msgstr ""
"La posizione dove i pulsanti di condivisione sono mostrati nella pagina di "
"pagamento."

#: ../plugin-options/share-options.php:100
msgid "Before customer details"
msgstr "Prima delle informazioni del cliente"

#: ../plugin-options/share-options.php:101
msgid "After customer details"
msgstr "Dopo  delle informazioni del cliente"

#: ../plugin-options/share-options.php:105
msgid "Show on cart page"
msgstr "Mostra nella pagina carrello"

#: ../plugin-options/share-options.php:115
msgid "Cart page position"
msgstr "Posizione nella pagina carrello"

#: ../plugin-options/share-options.php:120
msgid "The position where share buttons are showed in cart page."
msgstr ""
"La posizione dove i pulsanti di condivisione sono mostrati nella pagina "
"carrello."

#: ../plugin-options/share-options.php:122
msgid "Before cart"
msgstr "Prima del carrello"

#: ../plugin-options/share-options.php:123
msgid "Cart collaterals"
msgstr "Accanto il carrello"

#: ../plugin-options/share-options.php:124
msgid "After cart"
msgstr "Dopo il carrello"

#: ../plugin-options/share-options.php:132
msgid "Custom Sharing Settings"
msgstr "Impostazioni di condivisione personalizzate"

#: ../plugin-options/share-options.php:136
msgid "URL to share"
msgstr "URL da condividere"

#: ../plugin-options/share-options.php:141
msgid "If not specified, the page URL will be used"
msgstr "Se non specificato, verrà utilizzato l'URL della pagina"

#: ../plugin-options/share-options.php:144
msgid "Custom message"
msgstr "Messaggio personalizzato"

#: ../plugin-options/share-options.php:149
msgid ""
"If not specified, the page name will be used (available only for Twitter & "
"Email sharing)"
msgstr ""
"Se non specificato, verrà utilizzato il nome della pagina (disponibile solo "
"per la condivisione su Twitter e per email)"

#: ../plugin-options/share-options.php:156
msgid "Facebook OpenGraph Meta Defaults"
msgstr "OpenGraph Meta di Facebook predefiniti"

#: ../plugin-options/share-options.php:160
msgid "Default Title"
msgstr "Titolo Predefinito"

#: ../plugin-options/share-options.php:166
msgid "Default Description"
msgstr "Descrizione Predefinita"

#: ../plugin-options/share-options.php:172
msgid "Default Image"
msgstr "Immagine Predefinita"

#: ../plugin-options/share-options.php:175
msgid "Image size must be at least 200x200px"
msgstr "Le dimensioni dell'immagine devono essere almeno 200x200px"

#: ../templates/admin/class-ywsfd-custom-coupon-purge.php:110
msgid "Remove expired coupons"
msgstr "Rimuovi coupon scaduti"

#: ../templates/admin/class-ywsfd-custom-coupon-purge.php:111
msgid "Removal of expired coupons in progress..."
msgstr "Rimozione dei coupon scaduti in corso..."

#: ../templates/admin/class-ywsfd-custom-image-upload.php:97
msgid "Upload"
msgstr "Carica"

#: ../templates/admin/custom-coupon.php:51
msgid "Description"
msgstr "Descrizione"

#: ../templates/admin/custom-coupon.php:62
msgid "Discount type"
msgstr "Tipo sconto"

#: ../templates/admin/custom-coupon.php:91
msgid "Coupon amount"
msgstr "Valore coupon"

#: ../templates/admin/custom-coupon.php:105
msgid "Number of days after which the coupon expires"
msgstr "Numero di giorni dopo i quali il coupon scade"

#: ../templates/admin/custom-coupon.php:113
msgid "No expiration"
msgstr "Nessuna scadenza"

#: ../templates/admin/custom-coupon.php:129
msgid "Allow free shipping"
msgstr "Permetti spedizioni gratuite"

#: ../templates/frontend/fb-share-btn.php:3
#: ../templates/frontend/lnk-share-btn-premium.php:4
#: ../templates/frontend/lnk-share-form-premium.php:13
msgid "Share"
msgstr "Condividi"

#: ../templates/frontend/lnk-share-form-premium.php:4
msgid "Your Message"
msgstr "Il tuo messaggio"

#: ../templates/frontend/lnk-share-form-premium.php:12
#: ../templates/frontend/tw-tweet-form.php:13
msgid "Cancel"
msgstr "Annulla"

#: ../templates/frontend/mail-btn-premium.php:4
msgid "Send to a friend"
msgstr "Invia a un amico"

#: ../templates/frontend/mail-form-premium.php:18
msgid "Message"
msgstr "Messaggio"

#: ../templates/frontend/mail-form-premium.php:28
msgid "Send email"
msgstr "Invia email"

#: ../templates/frontend/tw-tweet-btn.php:4
#: ../templates/frontend/tw-tweet-form.php:14
msgid "Tweet"
msgstr "Tweet"

#: ../templates/frontend/tw-tweet-form.php:4
msgid "Your Tweet"
msgstr "Il tuo tweet"

#: ../templates/frontend/tw-tweet-form.php:8
msgid "Remaining characters"
msgstr "Caratteri rimasti"

#~ msgid ""
#~ "Copy this text string into the \"Authorized Redirect URLs:\" field of "
#~ "your Linkedin App"
#~ msgstr "Il tuo messaggio"

#~ msgid "If not specified, it will be used the URL of the page"
#~ msgstr "Se non specificato, verrà utilizzato l'URL della pagina"

#~ msgid ""
#~ "Select the type of button you want to show for Google+. Note: because of "
#~ "a bug unresolved by Google, the \"Share\" button could not generate the "
#~ "coupon correctly. For more information about the bug, please click here:"
#~ msgstr ""
#~ "Scegli il tipo di pulsante che vuoi mostrare per Google+. Attenzione: a "
#~ "causa di un bug non risolto da parte di Google, il pulsante \"Condividi\" "
#~ "potrebbe non generare il coupon correttamente. Per maggiori informazioni "
#~ "sul bug, visita questa pagina:"

#~ msgid ""
#~ "Note: due to a change made by twitter, you can not be sure that the "
#~ "tweets are published, so the coupons will be given even if the tweet is "
#~ "canceled. For more information about this change, please click here:"
#~ msgstr ""
#~ "Nota: a causa di un cambiamento fatto da Twitter, non si può avere la "
#~ "certezza se un tweet è stato pubblicato, quindi i coupon saranno dati "
#~ "anche se il tweet viene annullato. Per maggiori informazioni su questo "
#~ "cambiamento, clicca qui:"
