��    M      �  g   �      �     �     �     �     �  Y   �     "      >     _  	   p     z  
   �     �     �     �     �  *   �     �  0     
   2     =     E  %   V     |     �  #   �     �     �  
   �     �     �     	  ,   )	  +   V	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
      
     &
     .
     @
     N
     _
     k
  K   �
     �
     �
  !   �
  9     2   H     {     �     �     �     �     �  4   �  W     
   `  A   k  U   �  <     
   @     K     X     d     s     x     �  �  �     -     F     a     |  c   �  .   �  )        @     ^  &   |  	   �     �     �     �     �  )   �       :        Q     ]     f  ,   |     �  "   �  "   �     �          !     -  6   6     m  2   �  1   �  $   �               0     9     L     `     q     �  	   �     �  
   �     �     �     �     �  
          O   ,     |     �  #   �  @   �  9   �     9     Q     b     x     �     �  3   �  W   �     1  k   =  j   �  7        L     Y     h     v  
   �     �     �     -   ?      H   /      E      
                8   4              *   B          "                 F         <              0       &   C          .      +   (   D       2         L   1   6         ;              9   #   $   ,   '       %   M              	       3             7   G   !      >   :          @   K          I                              =   A   )      5   J        AVG Product Cost AVG Product Margin AVG Product Prices Actions Add a custom field to the report table (Separate them with commas for different columns). Add a tag column to report. Add custom columns to the report Add custom field All stock Apply costs to previous orders Categories Cost Cost of Goods Currency Settings Custom: Display the currency symbol in the reports Edit Error during updating the product Cost of Goods. Export CSV General General settings Import Cost of Goods from WooCommerce In stock Include shipping total cost Include taxes cost for each product Include total fees Last 7 Days Last Month Margin No cost is set for this product No products found. Number of items per page in the Report table Number of items per page in the Stock table Order Status Settings Out of stock Potential Profit Product Product Cost Product Price Product Search Product Total Cost Product Total Price Quantity Reports Reset Revenue Sales by category Sales by date Sales by product Search for: Select categories&hellip; Select the minimum order status required to display the data in the reports Set Cost of Goods Set cost Set the pagination to the reports Shipping costs will be included in the total product cost Show a tag column for products in the report table Showing reports for: Stock Reports Stock by category Stock by product Stock status Tags Tax costs will be included in the total product cost The cost related to the payment gateway used will be included in the total product cost This Month This will apply costs to previous orders if the cost was not set. This will apply costs to previous orders, overriding the previous cost if it was set. This will import the costs of the products from WooCommerce. Total Cost Total Margin Total Price Total Quantity View YITH Cost of Good Year Project-Id-Version: YITH Cost of Goods for WooCommerce
POT-Creation-Date: 2018-06-07 22:16+0200
PO-Revision-Date: 2018-06-07 22:20+0200
Last-Translator: 
Language-Team: Your Inspiration Themes <plugins@yithemes.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=n!=1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Media costo del prodotto Media margine del prodotto Media prezzi dei prodotti  Azioni Aggiungi un campo personalizzato alla tabella report (Separali con la virgola per colonne diverse). Aggiungi una colonna personalizzata al report. Aggiungi colonne personalizzate al report Aggiungi campo personalizzato Tutte le riserve in magazzino Applica i costi agli ordini precedenti Categorie Costo Cost of Goods Impostazioni valuta Personalizza: Mostra il simbolo della valuta nei report Modifica Errore durante l'aggiornamento del prodotto Cost of Goods. Esporta CSV Generale Impostazioni generali Importa il costo dei prodotti da WooCommerce In magazzino Includi costi totali di spedizione Includi le tasse per ogni prodotto Includi spese totali Ultimi 7 giorni Ultimo mese Margine  Non è stato impostato alcun costo per questo prodotto Nessun prodotto trovato. Numero di articoli per pagina nella tabella Report Numero di articoli per pagina nella tabella Stock Impostazioni dello stato dell'ordine Esaurito Previsione di guadagno Prodotto Costo del prodotto Prezzo del prodotto Ricerca prodotto Costo totale dei prodotti Prezzo totale dei prodotti Quantità Report Ripristina Ricavo Vendite per categoria Vendite per data Vendite per prodotto Cerca per: Seleziona categorie&hellip; Seleziona lo stato dell'ordine minimo necessario per mostrare i dati nei report Imposta i costi Imposta costa Imposta la paginazione per i report I costi di spedizione sono inclusi nel costo totale del prodotto Mostra un tag colonna per i prodotti nella tabella report Mostra i resoconti per: Report magazzino Riserve per categoria Riserve per prodotto Stato del magazzino Tag Le tasse sono incluse nel costo totale del prodotto Il costo del metodo di pagamento utilizzato viene incluso nel costo totale del prodotto Questo mese Quest'opzione permette di applicare i costi agli ordini precedenti se non è stato selezionato alcun costo. Quest'opzione permette di applicare i costi agli ordini precedenti sovrascrivendo il costo già impostato. Questo importerà i costi dei prodotti da WooCommerce.  Costo totale Margine totale Prezzo totale Quantità totale Visualizza YITH Cost of Goods Anno 