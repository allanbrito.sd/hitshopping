��    E      D  a   l      �     �  Y   �     S      o     �  	   �     �  
   �     �     �     �     �  0   �  
   &     1     9  %   J     p     y  #   �     �     �  
   �     �       ,     +   C     o     |     �     �     �     �     �     �     �     �     	     	     	     $	     2	     C	     O	     i	     {	  !   �	  9   �	  2   �	     
     (
     6
     H
     Y
     f
  4   k
  W   �
  
   �
  A     U   E  <   �  
   �     �     �     �                 �  $     �  ^   �      -  (   N     w     �  )   �     �     �     �  
   �  	   �  N   �     N     \     e  '   {     �     �  -   �     �          (  .   8     g  *   �  ,   �     �     �     �                    /     <     K     `     t  
   }     �     �     �     �     �  !   �       '     8   G  4   �     �     �     �     �            B   #  r   f  
   �  T   �  �   9  4   �     �     �            	   (     2     E               &   %              )       3      @   ?               2            8      :   !   ,   1       
               D   <                 ;   4   $       #       B   0      C   '   7   *           (          A      >   -       +         9                 	   /                       "      5                      .      E               6   =            Actions Add a custom field to the report table (Separate them with commas for different columns). Add a tag column to report. Add custom columns to the report Add custom field All stock Apply costs to previous orders Categories Cost Cost of Goods Custom: Edit Error during updating the product Cost of Goods. Export CSV General General settings Import Cost of Goods from WooCommerce In stock Include shipping total cost Include taxes cost for each product Include total fees Last 7 Days Last Month No cost is set for this product No products found. Number of items per page in the Report table Number of items per page in the Stock table Out of stock Potential Profit Product Product Cost Product Price Product Prices Product Profit Product Search Product Total Cost Product Total Price Reports Reset Sales by category Sales by date Sales by product Search for: Select categories&hellip; Set Cost of Goods Set cost Set the pagination to the reports Shipping costs will be included in the total product cost Show a tag column for products in the report table Showing reports for: Stock Reports Stock by category Stock by product Stock status Tags Tax costs will be included in the total product cost The cost related to the payment gateway used will be included in the total product cost This Month This will apply costs to previous orders if the cost was not set. This will apply costs to previous orders, overriding the previous cost if it was set. This will import the costs of the products from WooCommerce. Total Cost Total Price Total Profit Total Sales View YITH Cost of Good Year Project-Id-Version: YITH Cost of Goods for WooCommerce
POT-Creation-Date: 2018-05-10 14:46+0100
PO-Revision-Date: 2018-05-10 14:51+0100
Last-Translator: 
Language-Team: Your Inspiration Themes <plugins@yithemes.com>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.13
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Acties Aangepast veld toevoegen aan rapporttabel (scheid ze met komma's voor verschillende kolommen). Tag kolom toevoegen aan rapport. Voeg aangepaste kolommen toe aan rapport Aangepast veld toevoegen Alle voorraad Pas kosten toe op voorgaande bestellingen Categorieën Kosten Cost of Goods Aangepast: Aanpassen Er is een fout opgetreden tijdens het bijwerken van het product Cost of Goods. Exporteer CSV Algemeen Algemene instellingen Importeer Cost of Goods van WooCommerce Op voorraad Totale verzendkosten inbegrepen Belastingkosten voor ieder product inbegrepen Totale kosten inbegrepen Afgelopen 7 Dagen Afgelopen Maand Er zijn geen kosten ingesteld voor dit product Geen producten gevonden. Aantal items per pagina in de rapporttabel Aantal items per pagina in de voorraad tabel Niet op voorraad Potentiële winst Product Product Kosten Productprijs Product Prijzen Productwinst Product zoeken Totale productkosten Totale productprijs Raporten Herstellen Verkoop per categorie Verkoop op datum Verkoop per product Zoek op: Selecteer categorieën&hellip; Stel de kosten van de goederen in Stel de kosten in Stel de paginering in voor de rapporten Verzendkosten zijn inbegrepen in de totale productkosten Toon een tag kolom voor producten in de rapporttabel Rapporten weergeven voor: Voorraad rapporten Voorraad per categorie Voorraad per product Voorraad status Tags Belastingkosten zullen worden opgenomen in de totale productkosten De kosten gerelateerd aan de betaalmethode die wordt gebruikt, zullen worden inbegrepen in de totale productkosten Deze Maand Dit zal kosten van eerdere bestellingen toepassen als de kosten niet zijn ingesteld. Hiermee worden de kosten van eerdere bestellingen toegepast, waarbij de vorige kosten worden overschreven als deze zijn ingesteld. Dit zal de productkosten van WooCommerce importeren. Totale Kosten Totale Prijs Totale Winst Totale Verkoop Weergeven YITH Cost of Goods Jaar 