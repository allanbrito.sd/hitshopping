��    P      �  k         �     �     �     �        Y     )   b     �      �     �  	   �     �  
                  %  *   -     X  0   ]  
   �     �     �  X   �  %   	     1	     :	  #   V	     z	     �	  
   �	     �	     �	     �	     �	  ,   �	  +   
     @
     V
     c
     t
     |
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
               )  K   C     �     �  !   �  9   �  @     2   G     z     �     �     �     �     �  4   �  W     
   _  A   j  U   �  <     
   ?     J     W     f     r     �     �  �  �     -     C     Z     r  i   {  1   �  +     *   C     n     �  #   �     �     �     �     �  -   �     "  2   )     \     i     q  d   �  (   �            -   =     k     �     �     �     �  -   �     �  8   �  ;   3  !   o     �     �     �     �     �     �     �          ,     5     >     J     S     i     z     �     �  M   �          !  +   2  D   ^  >   �  N   �     1     J     a     {     �  	   �  F   �  `   �     [  M   d  i   �  >        [     g     t     �     �     �     �     <   
   I       *   L       P                    !   $                      )   @   2   7      ;          B   >   %         F             "   	         H   5                 3           C       0          J   9   1                     .      M          ?   #      N                 6         ,       A   &   G       4             D       +   -   :   O   /   E   '   =       8          (   K       AVG Product Cost AVG Product Margin AVG Product Prices Actions Add a custom field to the report table (Separate them with commas for different columns). Add a percentage margin column to report. Add a tag column to report. Add custom columns to the report Add custom field All stock Apply costs to previous orders Categories Cost Currency Settings Custom: Display the currency symbol in the reports Edit Error during updating the product Cost of Goods. Export CSV General General settings If this option is disable, the report will not display the currency symbol of the values Import Cost of Goods from WooCommerce In stock Include shipping total cost Include taxes cost for each product Include total fees Last 7 Days Last Month Margin Margin % No cost is set for this product No products found. Number of items per page in the Report table Number of items per page in the Stock table Order Status Settings Out of stock Potential Profit Product Product Cost Product Price Product Search Product Total Cost Product Total Price Quantity Reports Reset Revenue Sales by category Sales by date Sales by product Search for: Select categories&hellip; Select the minimum order status required to display the data in the reports Set Cost of Goods Set cost Set the pagination to the reports Shipping costs will be included in the total product cost Show a percentage margin column for products in the report table Show a tag column for products in the report table Showing reports for: Stock Reports Stock by category Stock by product Stock status Tags Tax costs will be included in the total product cost The cost related to the payment gateway used will be included in the total product cost This Month This will apply costs to previous orders if the cost was not set. This will apply costs to previous orders, overriding the previous cost if it was set. This will import the costs of the products from WooCommerce. Total Cost Total Margin Total Margin % Total Price Total Quantity View Year Project-Id-Version: YITH Cost of Goods for WooCommerce
POT-Creation-Date: 2018-08-20 15:14+0100
PO-Revision-Date: 2018-08-20 15:27+0100
Last-Translator: 
Language-Team: Your Inspiration Themes <plugins@yithemes.com>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 Coste de producto AVG Margen de producto AVG Precios de producto AVG Acciones Añadir un campo personalizado a la tabla de informes (Separalos por con comas para columnas diferentes). Añadir una columna para el margen en el informe. Añadir una etiqueta de columna al informe. Añadir columnas personalizadas al informe Añadir campo personalizado Todo el inventario Aplicar costes a pedidos anteriores Categorías Coste Ajustes de moneda Personalizado: Mostrar el símbolo de moneda en los informes Editar Error al actualizar el Cost of Goods del producto. Exportar CSV General Ajustes generales Si esta opción está deshabilitada, el informe no mostrará el símbolo de la moneda en los valores Importar Cost of Goods desde WooCommerce En inventario Incluir coste total de envío Incluir coste de impuestos para cada producto Incluir cuotas totales Últimos 7 días Último mes Margen Margen % No se ha establecido coste para este producto No se encontraron productos. Número de artículos por página en la tabla de informe Número de artículos por página en la tabla de inventario Opciones de estado de los pedidos Sin existencias Beneficio potencial Producto Coste de producto Precio de producto Búsqueda de producto Coste total del producto Precio total del producto Cantidad Informes Restablecer Ingresos Ventas por categoría Ventas por fecha Ventas por producto Buscar por: Seleccionar categorías&hellip; Selecciona el estado mínimo requerido para mostrar los pedidos en el informe Establecer Cost of Goods Establecer coste Establecer la paginación para los informes Los gastos de envío serás incluidos en el coste total del producto Añadir una columna para el porcentaje de margen en el informe Mostrar una columna para las etiquetas de los productos en la tabla de informe Mostrando informes para: Informes de inventario Inventario por categoría Inventario por producto Estado del inventario Etiquetas Los costes de impuesto serán incluidos en el coste total del producto El coste relacionado con la pasarela de pago usada será incluido en el coste total del producto Este mes Esto aplicará costes a pedidos anteriores si el coste no estaba establecido. Esto aplicará costes a pedidos anteriores, sobrescribiendo el coste anterior si este estaba establecido. Esto importará los costes de los productos desde WooCommerce. Coste total Margen total Margen total % Precio total Cantidad total Ver Año 