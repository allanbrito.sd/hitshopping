<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'YITH_FAV_Quick_View_Module' ) ) {

	class YITH_FAV_Quick_View_Module {

		public function __construct() {


			add_action( 'wp_enqueue_scripts', array( $this, 'quick_view_scripts' ), 99 );
		}

		public function quick_view_scripts() {

			if ( ! wp_script_is( 'youtube-api' ) ) {
				wp_enqueue_script( 'youtube-api', '//www.youtube.com/player_api' );
			}
			if ( ! wp_script_is( 'vimeo-api' ) ) {
				wp_enqueue_script( 'vimeo-api', '//f.vimeocdn.com/js/froogaloop2.min.js' );
			}

			if ( ! wp_script_is( 'yith-wcqv-frontend' ) ) {
				wp_enqueue_script( 'yith-wcqv-frontend', YITH_WCQV_ASSETS_URL . '/js/' . yit_load_js_file( 'frontend.js' ), array( 'jquery' ), YITH_WCQV_VERSION, true );
			}

			if ( ! wp_style_is( 'ywcfav_style' ) ) {
				wp_enqueue_style( 'ywcfav_style', YWCFAV_ASSETS_URL . 'css/ywcfav_frontend.css', array(), YWCFAV_VERSION );
			}

			if ( ! wp_script_is( 'videojs' ) ) {
				wp_enqueue_script( 'videojs', YWCFAV_ASSETS_URL . 'js/external_libraries/video.min.js', array( 'jquery' ), false, true );
			}

			if ( ! wp_style_is( 'videojs_style' ) ) {
				wp_enqueue_style( 'videojs_style', YWCFAV_ASSETS_URL . '/css/videojs/video-js.min.css' );
			}

			if ( ! wp_script_is( 'soundcloud_api' ) ) {
				wp_enqueue_script( 'soundcloud_api', YWCFAV_ASSETS_URL . 'js/external_libraries/soundcloud_api.js', array( 'jquery' ), false, true );
			}

			$ywcfav_params = array(
				'ajax_url'                     => admin_url( 'admin-ajax.php', is_ssl() ? 'https' : 'http' ),
				'gallery_image_container'      => '.images',
				'image_container'              => '.woocommerce-main-image',
				'no_image_container'           => '.woocommerce-main-image',
				'gallery_thumbnails_container' => '.yith-quick-view-thumbs',
				'thubmanil_container'          => '.yith-quick-view-single-thumb',
				'actions'                      => array(
					'get_featured_content'        => 'get_featured_content',
					'get_video_image'             => 'get_video_image',
					'change_video_in_featured'    => 'change_video_in_featured',
					'change_audio_in_featured'    => 'change_audio_in_featured',
					'get_product_variation_video' => 'get_product_variation_video'
				)

			);

			wp_register_script( 'ywcfav_frontend_quick_view', YWCFAV_ASSETS_URL . 'js/' . yit_load_js_file( 'ywcfav_quick_view.js' ), array(
				'jquery',
				'yith-wcqv-frontend'
			), YWCFAV_VERSION, true );

			wp_localize_script( 'ywcfav_frontend_quick_view', 'ywcfav_quick_view_param', $ywcfav_params );
			wp_enqueue_script( 'ywcfav_frontend_quick_view' );
		}

	}
}

function YITH_FAV_Quick_View_Module() {
	return new YITH_FAV_Quick_View_Module();
}