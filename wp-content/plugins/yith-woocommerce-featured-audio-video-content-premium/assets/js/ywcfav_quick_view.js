jQuery(document).ready(function ($) {


    var lock = false,
        block_params = {
            message: null,
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            },
            ignoreIfBlocked: true
        },
        params = ywcfav_quick_view_param,
        featured_img_src = $( params.thubmanil_container+':first-child').find('img').attr('src');
        featured_img_src_set = $( params.thubmanil_container+':first-child').find('img').attr('srcset');




    var hide_featured_image = function () {

            if ($(params.image_container).length) {
                $(params.image_container).addClass('ywcfav_has_featured');
            }
        },
        show_featured_image = function () {
            if ($(params.image_container).length) {
                $(params.image_container).removeClass('ywcfav_has_featured');
            }
        },
        show_gallery_image = function( thumbnail ){
            if( thumbnail.hasClass('ywcfav_featured_thumbnail' ) ){
                hide_featured_image();
                show_video_wrapper();

            }else{
                show_featured_image();
                hide_video_wrapper();
            }
        },
        show_video_wrapper = function(){
            var video_content = $('.ywcfav_video_content');
            video_content.show();

        },
        hide_video_wrapper = function(){
            var video_content = $('.ywcfav_video_content'),
                video_iframe =  video_content.find('iframe');
                    video_content.hide();

            if( video_content.hasClass('youtube') ){

                video_iframe[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*' );

            }else if( video_content.hasClass('vimeo')){

                var player = $f(video_iframe[0]);
                player.api("pause");
            }
        },
        set_video_in_quick_view = function ( data, image_id ) {

            $(params.gallery_image_container).block(block_params);
            $.ajax({
                type: 'POST',
                url: params.ajax_url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    lock = false;
                    if (response.result) {

                        hide_featured_image();
                        $(params.image_container).before(response.template);

                        var video_content = $(params.gallery_image_container).find('div[class^="ywcfav_video"]'),
                            audio_content = $(params.gallery_image_container).find('div[class^="ywcfav_audio"]'),
                            featured_info = false ;

                        if( video_content.length ) {
                            featured_info = video_content.data('video_info');
                        }else if( audio_content.length ){
                            featured_info = audio_content.data('video_info');
                        }


                        set_thumbnail_video_in_gallery( featured_info, image_id );

                    } else {
                        show_featured_image();

                        reset_original_thumbnail_in_gallery( featured_img_src, featured_img_src_set );
                    }

                    $(params.gallery_image_container).unblock();
                }
            });


        },
        set_thumbnail_video_in_gallery = function( featured_info, image_id ){
            var data = {
                'video_info': featured_info,
                'context' : 'yith_quick_view',
                'action': params.actions.get_video_image
            };

            $.ajax({
                type: 'POST',
                url: params.ajax_url,
                data: data,
                dataType: 'json',
                success: function (response) {

                    var first_thumbnail = '';
                    if( image_id == -1 ){
                        first_thumbnail = $( params.thubmanil_container).filter(':first-child');
                    }else {

                         first_thumbnail = $(params.thubmanil_container + "[data-attachment_id ='" + image_id + "']");
                    }

                   first_thumbnail.find('img').attr('src', response.image_url);
                   first_thumbnail.find('img').attr('srcset', response.image_url);


                }
            });

        },
        reset_original_thumbnail_in_gallery = function( src, srcset, image_id ) {
            var first_thumbnail = $( params.thubmanil_container+"[data-attachment_id ='"+image_id+"']");

            first_thumbnail.find('img').attr('src', src );
            first_thumbnail.find('img').attr('srcset', srcset );
        },
        remove_video_in_slider  = function () {

        var video_content = $(params.gallery_image_container).find('div[class^="ywcfav_video"]'),
            audio_content = $(params.gallery_image_container).find('div[class^="ywcfav_audio"]');

        if (video_content.length) {

            if (video_content.hasClass('host')) {
                var video_id = video_content.data('video_id');
                if (typeof video_id !== 'undefined')
                    videojs(video_id).dispose();
            }


            video_content.remove();

        }

        if (audio_content.length) {

            audio_content.remove();
        }


         show_featured_image();

    };
    $(document).on('qv_loader_stop', function () {

        if( $('.variations_form.cart').length ){

            var variation_id = $('.variation_id').val();

            if( variation_id!=''){
                var product_id = variation_id,
                    data = {
                        'product_id': product_id,
                        'video_id': '',
                        'context': 'yith_quick_view',
                        'action': params.actions.get_product_variation_video
                    };
                set_video_in_quick_view( data, -1 );
            }

            $(document).on('show_variation', '.variations_form.cart',function (e, variation_data, purchasable ){

                remove_video_in_slider();

                if( !( typeof variation_data.video_variation == 'undefined' ) ) {

                    if (!lock) {

                        lock = true;
                        var data = {
                            'product_id': variation_data.variation_id,
                            'video_id': variation_data.video_variation,
                            'context': 'yith_quick_view',
                            'action': params.actions.get_product_variation_video
                        };


                        set_video_in_quick_view(data, variation_data.image_id );
                    }
                }else{
                    reset_original_thumbnail_in_gallery( variation_data.image.thumb_src, variation_data.image.srcset, variation_data.image_id );
                }

                lock = false;


            } ).on('reset_data',function(e){
                remove_video_in_slider();

                if( !lock ){
                    lock = true;
                    var product_id = $('#yith_wcqv_product_id').val(),
                        data = {
                            'product_id': product_id,
                            'context': 'yith_quick_view',
                            'action': params.actions.get_featured_content
                        };
                    set_video_in_quick_view( data , -1 );
                }
            });
        }else {
            var product_id = $('#yith_wcqv_product_id').val(),
                data = {
                    'product_id': product_id,
                    'context': 'yith_quick_view',
                    'action': params.actions.get_featured_content
                };
            set_video_in_quick_view( data , -1 );
        }
        $(params.gallery_thumbnails_container).on( 'click', params.thubmanil_container, function(){

            if( $(this).is(':first-child' ) ){
                $(this).addClass('ywcfav_featured_thumbnail');
            }

            show_gallery_image( $(this) );
        } );
    });

});