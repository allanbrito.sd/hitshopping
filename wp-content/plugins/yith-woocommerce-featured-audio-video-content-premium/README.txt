=== YITH WooCommerce Featured Audio Video Content ===

Contributors: yithemes
Tags: woocommerce, featured video, featured image, featured, product, e-commerce, shop, vimeo, youtube, html5, player, video player, html5 video, responsive, videojs, yit, yith, yithemes
Requires at least: 3.5.1
Tested up to: 4.9.8
Stable tag: 1.1.19
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

YITH WooCommerce Featured Video allows you to set a video in place of the featured image in the product detail page.

== Description ==

YITH WooCommerce Featured Video plugin is an extension of Woocommerce plugin that allow your users to see a Youtube or Vimeo video in place of featured image in the product detail page.
You can simply set a video in place of featured image in the product detail page, by setting the URL of youtube or vimeo video in the specific field of product configuration page.

= Installation =

Once you have installed the plugin, you just need to activate the plugin in order to enable it.

= Configuration =

1. Open the configuration page of product where you want to set the featured video;
2. Set the URL of video in the "Featured Video URL" field, in the "Product Data" box.

== Installation ==

1. Unzip the downloaded zip file.
2. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `YITH WooCommerce Featured Video` from Plugins page

== Screenshots ==

1. The product detail page with featured video.
2. The admin field in product configuration.

== Changelog ==

= 1.1.19 =
* New: Support to WooCommerce 3.5.0 RC1
* New: Support to WordPress 4.9.8
* Update: Plugin framework
* Dev: Add filter ywcfav_extra_video_modal_classes
* Dev: Add filter ywcfav_extra_video_classes
* Remove: Support to WooCommerce 2.6.x

= 1.1.18 =
* New: Support to WooCommerce 3.4.2
* New: Support to WordPress 4.9.6
* Update: Italian language 
* Update: Spanish language
* Fix: Zoom Magnifier integration

= 1.1.17 =
* New: Support to WooCommerce 3.3.0-beta2
* Update: Plugin Framework
* Tweak: Integration with The Polygon theme

= 1.1.16 =

* New: Support to YITH Frontend Manager For WooCommerce Premium
* Update: Plugin Framework

= 1.1.15 =
* New: Support to WooCommerce 3.2.0
* Dev: added ywfavc_mute_autoplay filter
* Update: Plugin Framework
* Fix: Video doesn't show on the product page

= 1.1.14 =
* New: Support to WooCommerce 3.2.0-RC2
* New: Integration with YITH WooCommerce Quick View Premium
* New: Dutch language file
* Update: Plugin Framework

= 1.1.13 =
* New: Support to WooCommerce 3.1.2
* Update: Plugin Framework
* Fix: Video disappears when changing product tab or clicking on image gallery

= 1.1.12 =
* New: Option to show the audio and video sliders in a sidebar instead of under the product image gallery
* Update: Language File
* Update: Plugin Framework

= 1.1.11 =
* New: Support to WooCommerce 3.1
* Update: Plugin Framework
* Tweak: Integration with YITH Zoom Magnifier Premium


= 1.1.10 =
* New: Support to WooCommerce 3.0.5
* Update: Plugin Framework
* Fix: Miss custom style for uploaded video
* Fix: Miss thumbnail modal video
* Fix: Fatal error due to huge amount of post meta

= 1.1.9 =

* New: Support to WooCommerce 3.0.1
* Tweak: Responsive modal video and audio
* Update: Plugin Framework
* Update: Language file

= 1.1.8 =
* New: Support to WordPress 4.7
* New: Support to WooCommerce 2.6.11
* Update: Plugin Framework


= 1.1.7 =

* Fix: YITH Zoom Magnifier Option always disabled
* Update : Plugin Framework

= 1.1.6 =

* Fix: Product page not show on mobile
* Update: Plugin Framework

= 1.1.5 =

* New: Spanish language file
* New: Italian language file
* Update: Plugin Framework

= 1.1.4 =

* Update: Plugin Framework

= 1.1.3 =

* Tweak: improved integration with YITH WooCommerce Zoom Magnifier
* Fix: Upload image in admin

= 1.1.2 =

* New: filter "yith_featured_video_enabled"
* Update: Plugin Framework

= 1.1.1 =

* New: show or hide video related option for YouTube

= 1.1.0 =

* Tweak: improved uploading of video and audio files in admin side
* Tweak: improved uploading of video and audio files for variable products both for admin and in frontend
* New: aspect ratio option for videos
* New: additional options for YouTube and Vimeo
* New: compatibility fixes with YITH WooCommerce Zoom Magnifier (free and premium versions)
* New: slider for non-featured videos and audio files
* Remove: videojs for YouTube and Vimeo

= 1.0.3 =

* Fix : Change Video in Variable Product
* Fix : Black screen on video uploaded


= 1.0.2 =

* Update: Plugin Framework 2.0

= 1.0.1 =

* Fix : javascript error in admin
* Fix : aspect-ratio for video and audio
* Fix : function Hex2RGB already exists

= 1.0.0 =

* Initial release
