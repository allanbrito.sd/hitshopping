== Changelog ==

= Version 1.1.4 - Released: 04 Jun 2018 =

* New - added '2 Days' and '3 Days' in bestseller update time option
* New - support to WooCommerce 3.4.x
* New - support to WordPress 4.9.6
* Update - Italian language
* Update - Spanish language
* Update - Dutch language
* Update - plugin framework
* Tweak - improved style

= Version 1.1.3 - Released: 31 Jan 2018 =

* New - support to WooCommerce 3.3
* New - Dutch language
* Update - Plugin Framework

= Version 1.1.2 - Released: 09 Jan 2018 =

* Fix - WPML issues
* Update - Plugin Framework 3
* Dev - added yith_wcbs_remove_best_seller filter
* Dev - added yith_wcbs_get_best_sellers filter

= Version 1.1.1 - Released: 11 Oct 2017 =

* New - support to Support to WooCommerce 3.2.0 RC2
* Fix - bestseller image issue

= Version 1.1.0 - Released: 28 Mar 2017 =

* New - support to WooCommerce 3.0-RC2
* New - Italian language
* New - Spanish language
* Update - language files
* Fix - show bestseller limit when saving settings in admin
* Fix - issue with PHP 5.4

= Version 1.0.8 - Released: 21 Nov 2016 =

* New - choose the number of best sellers shown
* New - display the "Best Seller" badge only for products in general Top list

= Version  1.0.7 - Released: 03 Aug 2016 =

* New - Spanish language

= Version  1.0.6 - Released: 16 Feb 2016 =

* Tweak: fixed minor bugs

= Version  1.0.5 - Released: 31 Dec 2015 =

* New - support to WooCommerce 2.5 BETA 3

= Version  1.0.4 - Released: 17 Nov 2015 =

* New - compatibility with YITH Themes

= Version  1.0.3 - Released: 29 Oct 2015 =

* New - Italian Language
* Fix - minor bug

= Version  1.0.2 - Released: 30 Sep 2015 =

* New - Possibility to choose the number of bestsellers visible in widget
* Tweak - improved transient use

= Version  1.0.1 - Released: 15 Sep 2015 =

* Fix - minor bug

= Version  1.0.0 - Released: 09 Sep 2016=

* Initial release