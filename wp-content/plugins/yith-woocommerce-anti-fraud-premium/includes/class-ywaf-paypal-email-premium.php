<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YWAF_PayPal_Verify' ) ) {

	/**
	 * Implements Coupon Mail for YWCES plugin
	 *
	 * @class   YWAF_PayPal_Verify
	 * @package Yithemes
	 * @since   1.0.0
	 * @author  Your Inspiration Themes
	 * @extends WC_Email
	 *
	 */
	class YWAF_PayPal_Verify extends WC_Email {

		/**
		 * @var string key for PayPal verification
		 */
		var $verify_key;

		/**
		 * Constructor
		 *
		 * Initialize email type and set templates paths
		 *
		 * @since   1.0.0
		 * @author  Alberto Ruggiero
		 */
		public function __construct() {

			$this->id             = 'yith-anti-fraud';
			$this->customer_email = true;
			$this->description    = __( 'YITH WooCommerce Anti-Fraud is the best way to understand and recognize all suspicious purchases made in your e-commerce site.', 'yith-woocommerce-anti-fraud' );
			$this->title          = __( 'Anti Fraud PayPal Verification', 'yith-woocommerce-coupon-email-system' );
			$this->enabled        = 'yes';
			$this->template_html  = '/emails/paypal-email.php';
			$this->template_plain = '/emails/plain/paypal-email.php';

			parent::__construct();

		}

		/**
		 * Trigger email send
		 *
		 * @since   1.0.0
		 *
		 * @param   $mail_address
		 * @param   $order
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function trigger( WC_Order $order, $mail_address ) {

			$subject          = str_replace( '{site_title}', get_option( 'blogname' ), get_option( 'ywaf_paypal_mail_subject' ) );
			$this->object     = $order;
			$this->verify_key = base64_encode( base64_encode( '#' . yit_get_order_id( $order ) ) . ',' . base64_encode( $mail_address ) . ',' . base64_encode( yit_get_prop( $order, 'date_created' ) ) );
			$this->email_type = get_option( 'ywaf_paypal_mail_type' );
			$this->heading    = $subject;
			$this->subject    = $subject;
			$this->recipient  = $mail_address;

			if ( ! $this->get_recipient() ) {
				return;
			}

			$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), "" );

		}

		/**
		 * Get HTML content
		 *
		 * @since   1.0.0
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_content_html() {

			ob_start();

			wc_get_template( $this->template_html, array(
				'email_heading' => $this->get_heading(),
				'order'         => $this->object,
				'verify_key'    => $this->verify_key,
				'sent_to_admin' => false,
				'plain_text'    => false,
				'email'         => $this,

			), '', YWAF_TEMPLATE_PATH );

			return ob_get_clean();

		}

		/**
		 * Get Plain content
		 *
		 * @since   1.0.0
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_content_plain() {

			ob_start();

			wc_get_template( $this->template_plain, array(
				'email_heading' => $this->get_heading(),
				'order'         => $this->object,
				'verify_key'    => $this->verify_key,
				'sent_to_admin' => false,
				'plain_text'    => true,
				'email'         => $this,
			), '', YWAF_TEMPLATE_PATH );

			return ob_get_clean();

		}

		/**
		 * Get email content type.
		 *
		 * @since   1.0.4
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_content_type() {
			switch ( get_option( 'ywaf_paypal_mail_type' ) ) {
				case 'html' :
					return 'text/html';
				default :
					return 'text/plain';
			}
		}

		/**
		 * Checks if this email is enabled and will be sent.
		 * @since   1.0.4
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function is_enabled() {
			return ( get_option( 'ywaf_paypal_enable' ) === 'yes' );
		}

		/**
		 * Admin Panel Options Processing - Saves the options to the DB
		 *
		 * @since   1.0.4
		 * @return  boolean|null
		 * @author  Alberto Ruggiero
		 */
		public function process_admin_options() {

			woocommerce_update_options( $this->form_fields['paypal'] );

		}

		/**
		 * Setup email settings screen.
		 *
		 * @since   1.0.4
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function admin_options() {

			?>
			<table class="form-table">
				<?php woocommerce_admin_fields( $this->form_fields['paypal'] ); ?>
			</table>
			<?php

		}

		/**
		 * Initialise Settings Form Fields
		 *
		 * @since   1.0.4
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function init_form_fields() {

			$this->form_fields = include( YWAF_DIR . '/plugin-options/paypal-options.php' );

		}

	}

}

return new YWAF_PayPal_Verify();