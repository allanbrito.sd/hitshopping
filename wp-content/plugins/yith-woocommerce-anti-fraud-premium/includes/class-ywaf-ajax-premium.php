<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'YWAF_Ajax_Premium' ) ) {

    /**
     * Implements AJAX for YWAF plugin
     *
     * @class   YWAF_Ajax_Premium
     * @package Yithemes
     * @since   1.0.0
     * @author  Your Inspiration Themes
     *
     */
    class YWAF_Ajax_Premium {

        /**
         * Single instance of the class
         *
         * @var \YWAF_Ajax_Premium
         * @since 1.0.0
         */
        protected static $instance;

        /**
         * Returns single instance of the class
         *
         * @return \YWAF_Ajax_Premium
         * @since 1.0.0
         */
        public static function get_instance() {

            if ( is_null( self::$instance ) ) {

                self::$instance = new self( $_REQUEST );

            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * @since   1.0.0
         * @return  mixed
         * @author  Alberto Ruggiero
         */
        public function __construct() {

            add_action( 'wp_ajax_ywaf_resend_email', array( $this, 'ywaf_resend_email' ) );
            add_action( 'wp_ajax_nopriv_ywaf_resend_email', array( $this, 'ywaf_resend_email' ) );

        }

        /**
         * Re-send verify email to customer
         *
         * @since   1.0.0
         * @return  void
         * @author  Alberto Ruggiero
         */
        public function ywaf_resend_email() {

            if ( check_admin_referer( 'ywaf-resend-email' ) ) {

                $response = array();

                try {

                    $order = wc_get_order( $_GET['order_id'] );

                    YITH_WAF()->ywaf_paypal_mail_send( $order );

                    if ( is_ajax() ) {

                        $response['status'] = 'success';
                        wc_add_notice( 'L\'email è stata inviata con successo, controlla la tua casella di posta', 'success' );

                    }
                    else {
                        $redirect = wp_get_referer() ? wp_get_referer() : $order->get_view_order_url();
                        wp_safe_redirect( $redirect );
                        exit;
                    }


                } catch ( Exception $e ) {

                    if ( !empty( $e ) ) {
                        $response['status'] = 'failure';
                        wc_add_notice( $e->getMessage(), 'error' );
                    }

                }

                if ( is_ajax() ) {

                    ob_start();
                    wc_print_notices();
                    $messages = ob_get_clean();

                    $response['messages'] = isset( $messages ) ? $messages : '';

                }

                wp_send_json( $response );

            }

            exit;

        }

    }

    /**
     * Unique access to instance of YWAF_Ajax_Premium class
     *
     * @return \YWAF_Ajax_Premium
     */
    function YWAF_Ajax_Premium() {

        return YWAF_Ajax_Premium::get_instance();

    }

    new YWAF_Ajax_Premium();

}