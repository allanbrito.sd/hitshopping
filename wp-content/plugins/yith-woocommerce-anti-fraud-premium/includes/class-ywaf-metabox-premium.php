<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'YWAF_Metabox_Premium' ) ) {

	/**
	 * Shows Meta Box in order's details page
	 *
	 * @class   YWAF_Metabox_Premium
	 * @package Yithemes
	 * @since   1.0.0
	 * @author  Your Inspiration Themes
	 *
	 */
	class YWAF_Metabox_Premium extends YWAF_Metabox {

		/**
		 * Returns single instance of the class
		 *
		 * @return \YWAF_Metabox_Premium
		 * @since 1.0.0
		 */
		public static function get_instance() {

			if ( is_null( self::$instance ) ) {

				self::$instance = new self( $_REQUEST );

			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since   1.0.0
		 * @return  mixed
		 * @author  Alberto Ruggiero
		 */
		public function __construct() {

			parent::__construct();

			add_filter( 'ywaf_metabox_score', array( $this, 'add_score_knob' ), 10, 3 );
			add_filter( 'ywaf_metabox_repeat', array( $this, 'add_repeat_button' ), 10, 2 );
			add_filter( 'ywaf_metabox_class', array( $this, 'add_class' ) );
			add_filter( 'ywaf_metabox_paypal', array( $this, 'add_paypal_resend' ) );

		}

		/**
		 * Add score knob to metabox
		 *
		 * @since   1.0.0
		 *
		 * @param   $value
		 * @param   $data
		 * @param   $risk_factor
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function add_score_knob( $value, $data, $risk_factor ) {

			ob_start();

			?>
			<div class="ywaf-risk-container-knob">
				<div>
					<label style="background: <?php echo $data['color'] ?>" for="ywaf_risk"><?php echo $risk_factor['score'] ?>%
						<span><?php echo $data['tip'] ?></span></label>
					<input
						id="ywaf_risk"
						data-fgColor="<?php echo $data['color'] ?>"
						data-bgColor="#b5b5b6"
						data-thickness=".25"
						data-displayInput="false"
						data-readOnly="true"
						data-width="200"
						value="<?php echo $risk_factor['score'] ?>"
					/>
				</div>

			</div>

			<?php

			return ob_get_clean();

		}

		/**
		 * Add repeat check button to metabox
		 *
		 * @since   1.0.0
		 *
		 * @param   $value
		 * @param   $order
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function add_repeat_button( $value, WC_Order $order ) {

			ob_start();

			?>
			<div class="ywaf-nocheck">
				<p>
					<button type="button" class="button button-primary ywaf-repeat-check"><?php _e( 'Repeat Check!', 'yith-woocommerce-anti-fraud' ) ?></button>
				</p>
			</div>

			<?php

			$value = ob_get_clean();


			return $value;

		}

		/**
		 * Add premium class to metabox
		 *
		 * @since   1.0.0
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function add_class() {

			return 'ywaf-premium';

		}

		/**
		 * If there is a pending paypal verification
		 *
		 * @since   1.0.0
		 *
		 * @param   $value
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function add_paypal_resend( $value ) {

			ob_start();

			?>
			<p>
				<?php _e( 'This order is currently waiting for a PayPal verification.', 'yith-woocommerce-anti-fraud' ); ?>
			</p>

			<p>
				<button type="button" class="button button-primary ywaf-repeat-check"><?php _e( 'Re-send verification request', 'yith-woocommerce-anti-fraud' ) ?></button>
			</p>

			<?php

			return ob_get_clean();

		}

	}

	/**
	 * Unique access to instance of YWAF_Metabox_Premium class
	 *
	 * @return \YWAF_Metabox_Premium
	 */
	function YWAF_Metabox_Premium() {

		return YWAF_Metabox_Premium::get_instance();

	}

	new YWAF_Metabox_Premium();

}