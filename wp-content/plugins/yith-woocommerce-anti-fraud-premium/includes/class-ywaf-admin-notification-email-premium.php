<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YWAF_Admin_Notification' ) ) {

	/**
	 * Implements Admin Notification email for YWAF plugin
	 *
	 * @class   YWAF_Admin_Notification
	 * @package Yithemes
	 * @since   1.0.5
	 * @author  Your Inspiration Themes
	 * @extends WC_Email
	 *
	 */
	class YWAF_Admin_Notification extends WC_Email {

		/**
		 * Constructor
		 *
		 * Initialize email type and set templates paths
		 *
		 * @since   1.0.5
		 * @author  Alberto Ruggiero
		 */
		public function __construct() {

			$this->id             = 'yith-anti-fraud-admin';
			$this->title          = __( 'Anti-Fraud Admin Notification', 'yith-woocommerce-anti-fraud' );
			$this->description    = __( 'YITH WooCommerce Anti-Fraud is the best way to understand and recognize all suspicious purchases made in your e-commerce site.', 'yith-woocommerce-anti-fraud' );
			$this->heading        = __( 'Anti-Fraud check', 'yith-woocommerce-anti-fraud' );
			$this->subject        = __( '[{site_title}] Anti-fraud checks on order #{order_number}', 'yith-woocommerce-anti-fraud' );
			$this->recipient      = $this->get_option( 'recipient', get_option( 'admin_email' ) );
			$this->template_html  = '/emails/admin-email.php';
			$this->template_plain = '/emails/plain/admin-email.php';

			parent::__construct();

		}

		/**
		 * Trigger email send
		 *
		 * @since   1.0.5
		 *
		 * @param   $order
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function trigger( WC_Order $order ) {

			$this->object     = $order;
			$this->email_type = get_option( 'ywaf_admin_mail_type' );

			if ( version_compare( WC()->version, '3.2', '<' ) ) {
				$this->find['order-number']    = '{order_number}';
				$this->replace['order-number'] = $order->get_order_number();
			} else {
				$this->placeholders['{order_number}'] = $order->get_order_number();
			}

			if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
				return;
			}

			$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), "" );

		}

		/**
		 * Get HTML content
		 *
		 * @since   1.0.5
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_content_html() {

			ob_start();

			wc_get_template( $this->template_html, array(
				'email_heading' => $this->get_heading(),
				'order'         => $this->object,
				'sent_to_admin' => true,
				'plain_text'    => false,
				'email'         => $this
			), '', YWAF_TEMPLATE_PATH );

			return ob_get_clean();

		}

		/**
		 * Get Plain content
		 *
		 * @since   1.0.5
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_content_plain() {

			ob_start();

			wc_get_template( $this->template_plain, array(
				'email_heading' => $this->get_heading(),
				'order'         => $this->object,
				'sent_to_admin' => true,
				'plain_text'    => true,
				'email'         => $this
			), '', YWAF_TEMPLATE_PATH );

			return ob_get_clean();

		}

		/**
		 * Get email content type.
		 *
		 * @since   1.0.5
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function get_content_type() {

			switch ( get_option( 'ywaf_admin_mail_type' ) ) {
				case 'html' :
					return 'text/html';
				default :
					return 'text/plain';
			}

		}

		/**
		 * Checks if this email is enabled and will be sent.
		 * @since   1.0.5
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function is_enabled() {
			return ( get_option( 'ywaf_admin_mail_enable' ) === 'yes' );
		}

		/**
		 * Admin Panel Options Processing - Saves the options to the DB
		 *
		 * @since   1.0.5
		 * @return  boolean|null
		 * @author  Alberto Ruggiero
		 */
		public function process_admin_options() {

			woocommerce_update_options( $this->form_fields['premium-general'] );

		}

		/**
		 * Setup email settings screen.
		 *
		 * @since   1.0.5
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function admin_options() {

			?>
			<table class="form-table">
				<?php woocommerce_admin_fields( $this->form_fields['premium-general'] ); ?>
			</table>
			<?php

		}

		/**
		 * Initialise Settings Form Fields
		 *
		 * @since   1.0.5
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function init_form_fields() {

			$this->form_fields = include( YWAF_DIR . '/plugin-options/premium-general-options.php' );

		}

	}

}

return new YWAF_Admin_Notification();