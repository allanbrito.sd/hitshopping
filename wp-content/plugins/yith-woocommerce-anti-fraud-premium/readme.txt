﻿=== YITH WooCommerce Anti-Fraud ===

Contributors: yithemes
Tags: woocommerce, products, themes, yit, yith, yithemes, e-commerce, shop, anti-fraud, fraud risk,
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.1.6 =

* New: Support to WordPress 4.9.6
* New: Support to WooCommerce 3.4.0
* New: Privacy Policy Guide
* Update: plugin framework

= 1.1.5 =

* New: Support to WordPress 4.9.6 RC2
* New: Support to WooCommerce 3.4.0 RC1
* New: GDPR compliance
* Update: plugin framework

= 1.1.4 =

* New: Support to WooCommerce 3.3.0
* Update: plugin framework
* Fix: fraud check process

= 1.1.3 =

* New: Support to WooCommerce 3.2.0 RC2
* Update: plugin framework
* Tweak: improved proxy check rule

= 1.1.2 =

* Update: plugin framework

= 1.1.1 =

* Fix: Paypal check not working
* Update: plugin framework

= 1.1.0 =

* Tweak: Support for WooCommerce 3.0.0-RC 1

= 1.0.8 =

* Fix: wrong query prefix

= 1.0.7 =

* New: Italian Translation
* New: Spanish Translation

= 1.0.6 =

* Tested with WooCommerce 2.6.0 beta 3

= 1.0.5 =

* New: minified versions of CSS files
* New feature: check if the order amount exceeds a certain amount

= 1.0.4 =

* New: compatibility with WooCommerce 2.5

= 1.0.3 =

* New: filter "ywaf_proxy_dnsbls" to manage dnsbls for the rule control proxy
* New: default woocommerce geolocation service set as backup in case of failure of IP-api.com

= 1.0.1 =

* Update: Plugin core framework

= 1.0.0 =

* Initial release