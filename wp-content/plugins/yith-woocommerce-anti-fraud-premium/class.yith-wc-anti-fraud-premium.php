<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Main class
 *
 * @class   YITH_WC_Anti_Fraud_Premium
 * @package Yithemes
 * @since   1.0.0
 * @author  Your Inspiration Themes
 */

if ( ! class_exists( 'YITH_WC_Anti_Fraud_Premium' ) ) {

	class YITH_WC_Anti_Fraud_Premium extends YITH_WC_Anti_Fraud {

		/**
		 * @var array
		 */
		protected $_email_types = array();

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WC_Anti_Fraud_Premium
		 * @since 1.0.0
		 */
		public static function get_instance() {

			if ( is_null( self::$instance ) ) {

				self::$instance = new self;

			}

			return self::$instance;

		}

		/**
		 * Constructor
		 *
		 * @since   1.0.0
		 * @return  mixed
		 * @author  Alberto Ruggiero
		 */
		public function __construct() {

			parent::__construct();

			// register plugin to licence/update system
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
			add_action( 'plugins_loaded', array( $this, 'include_privacy_text' ), 20 );


			if ( get_option( 'ywaf_enable_plugin' ) == 'yes' ) {

				$this->includes_premium();

				$this->rules = $this->get_ywaf_rules_premium( $this->rules );

				$this->_email_types = array(
					'paypal_check'       => array(
						'class' => 'YWAF_PayPal_Verify',
						'file'  => 'class-ywaf-paypal-email-premium.php',
						'hide'  => true,
					),
					'admin_notification' => array(
						'class' => 'YWAF_Admin_Notification',
						'file'  => 'class-ywaf-admin-notification-email-premium.php',
						'hide'  => true,
					),
				);

				add_action( 'ywaf_after_fraud_check', array( $this, 'ywaf_add_to_blacklist' ), 10, 3 );
				add_action( 'ywaf_after_fraud_check', array( $this, 'ywaf_send_admin_mail' ), 10, 1 );
				add_action( 'ywaf_paypal_cron', array( $this, 'ywaf_paypal_cron' ) );

				add_filter( 'ywaf_after_check_status', array( $this, 'ywaf_set_order_status' ), 10, 3 );
				add_filter( 'ywaf_paypal_check', array( $this, 'ywaf_check_paypal' ), 10, 2 );
				add_filter( 'ywaf_check_blacklist', array( $this, 'ywaf_check_blacklist' ), 10, 2 );
				add_filter( 'yith_wcet_email_template_types', array( $this, 'add_yith_wcet_template' ) );
				add_filter( 'woocommerce_email_classes', array( $this, 'ywaf_paypal_mail' ) );

				if ( is_admin() ) {

					add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts_admin_premium' ) );
					add_filter( 'woocommerce_admin_order_actions', array( $this, 'ywaf_table_action' ), 10, 2 );
					add_filter( 'ywaf_paypal_status', array( $this, 'ywaf_paypal_status' ), 10, 1 );
					add_action( 'woocommerce_admin_settings_sanitize_option_ywaf_rules_risk_country_list', array( $this, 'sanitize_empty_array' ) );


				} else {

					add_action( 'woocommerce_before_my_account', array( $this, 'ywaf_verify_paypal_code' ) );
					add_action( 'woocommerce_before_customer_login_form', array( $this, 'ywaf_verify_paypal_code' ) );
					add_action( 'woocommerce_view_order', array( $this, 'ywaf_view_order' ), 5, 1 );
					add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts_frontend' ) );

				}

			}

		}

		/**
		 * Files inclusion
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		private function includes_premium() {

			include_once( 'includes/rules/class-ywaf-proxy-premium.php' );
			include_once( 'includes/rules/class-ywaf-suspicious-email-premium.php' );
			include_once( 'includes/rules/class-ywaf-risk-country-premium.php' );
			include_once( 'includes/rules/class-ywaf-high-amount-premium.php' );
			include_once( 'includes/rules/class-ywaf-high-amount-fixed-premium.php' );
			include_once( 'includes/rules/class-ywaf-many-attempts-premium.php' );
			include_once( 'includes/rules/class-ywaf-ip-multiple-details-premium.php' );
			include_once( 'includes/rules/class-ywaf-blacklist-premium.php' );
			include_once( 'includes/rules/class-ywaf-paypal-premium.php' );
			include_once( 'includes/class-ywaf-ajax-premium.php' );
			include_once( 'includes/functions.ywaf-gdpr.php' );

			if ( is_admin() ) {

				include_once( 'templates/admin/class-yith-wc-custom-textarea.php' );
				include_once( 'templates/admin/class-yith-wc-custom-checklist.php' );
				include_once( 'templates/admin/class-yith-wc-custom-country-select.php' );
				include_once( 'includes/class-ywaf-metabox-premium.php' );

			}

			do_action( 'ywaf_custom_includes' );

		}

		/**
		 * Get premium Anti-Fraud rules
		 *
		 * @since   1.0.0
		 *
		 * @param   $rules
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function get_ywaf_rules_premium( $rules ) {

			$rules['YWAF_Proxy']               = array(
				'rule'   => new YWAF_Proxy(),
				'active' => get_option( 'ywaf_rules_proxy_enable' ),
			);
			$rules['YWAF_Risk_Country']        = array(
				'rule'   => new YWAF_Risk_Country(),
				'active' => get_option( 'ywaf_rules_risk_country_enable' ),
			);
			$rules['YWAF_High_Amount']         = array(
				'rule'   => new YWAF_High_Amount(),
				'active' => get_option( 'ywaf_rules_high_amount_enable' ),
			);
			$rules['YWAF_High_Amount_Fixed']   = array(
				'rule'   => new YWAF_High_Amount_Fixed(),
				'active' => get_option( 'ywaf_rules_high_amount_fixed_enable' ),
			);
			$rules['YWAF_Many_Attempts']       = array(
				'rule'   => new YWAF_Many_Attempts(),
				'active' => get_option( 'ywaf_rules_many_attempts_enable' ),
			);
			$rules['YWAF_IP_Multiple_Details'] = array(
				'rule'   => new YWAF_IP_Multiple_Details(),
				'active' => get_option( 'ywaf_rules_ip_multiple_details_enable' ),
			);
			$rules['YWAF_Suspicious_Email']    = array(
				'rule'   => new YWAF_Suspicious_Email(),
				'active' => get_option( 'ywaf_rules_suspicious_email_enable' ),
			);

			return apply_filters( 'ywaf_rules', $rules );

		}

		/**
		 * Set custom order status
		 *
		 * @since   1.0.0
		 *
		 * @param   $order_status
		 * @param   $risk_score
		 * @param   $order
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 *
		 */
		public function ywaf_set_order_status( $order_status, $risk_score, $order ) {

			switch ( true ) {

				case  $risk_score >= $this->risk_thresholds['high']:

					$order_status['status'] = 'wc-cancelled';
					$order_status['note']   = __( 'Fraud risk check not passed. Fraud risk is high!', 'yith-woocommerce-anti-fraud' );
					break;

				case $risk_score >= $this->risk_thresholds['medium'] && $risk_score < $this->risk_thresholds['high']:

					$order_status['status'] = 'wc-on-hold';
					$order_status['note']   = __( 'Fraud risk check passed with medium fraud risk.', 'yith-woocommerce-anti-fraud' );
					break;

				default:

					$order_status['note'] = __( 'Fraud risk check passed with low fraud risk', 'yith-woocommerce-anti-fraud' );
					break;

			}

			return $order_status;

		}

		/**
		 * Check if billing email is in blacklist
		 *
		 * @since   1.0.0
		 *
		 * @param   $result
		 * @param   $order
		 *
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_check_blacklist( $result, WC_Order $order ) {

			if ( get_option( 'ywaf_email_blacklist_enable' ) == 'yes' ) {

				$blacklist = new YWAF_Blacklist();

				if ( $blacklist->get_fraud_risk( $order ) === true ) {

					$result = true;

				}

			}

			return $result;

		}

		/**
		 * Add billing email in blacklist
		 *
		 * @since   1.0.0
		 *
		 * @param   $blacklist
		 * @param   $order
		 * @param   $risk_score
		 *
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_add_to_blacklist( WC_Order $order, $blacklist, $risk_score ) {

			if ( ! $blacklist && get_option( 'ywaf_email_blacklist_enable' ) == 'yes' && get_option( 'ywaf_email_blacklist_auto_add' ) == 'yes' ) {

				if ( $risk_score >= $this->risk_thresholds['high'] ) {

					$blacklist = new YWAF_Blacklist();
					$blacklist->add_to_blacklist( yit_get_prop( $order, 'billing_email' ) );

				}

			}

		}

		/**
		 * Send email to administrator
		 *
		 * @since   1.0.5
		 *
		 * @param   $order
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_send_admin_mail( WC_Order $order ) {

			$wc_email = WC_Emails::instance();
			$email    = $wc_email->emails['YWAF_Admin_Notification'];

			$email->trigger( $order );

		}

		/**
		 * Check if paypal email is verified
		 *
		 * @since   1.0.0
		 *
		 * @param   $order
		 * @param   $result
		 *
		 * @return  bool
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_check_paypal( $result, WC_Order $order ) {

			if ( ( get_option( 'ywaf_paypal_enable' ) == 'yes' ) && ( yit_get_prop( $order, '_payment_method' ) == 'paypal' ) ) {

				$paypal = new YWAF_PayPal();

				if ( $paypal->get_fraud_risk( $order ) === true || yit_get_prop( $order, 'ywaf_paypal_check' ) == 'process' || yit_get_prop( $order, 'ywaf_paypal_check' ) == 'failed' ) {

					$result = false;

				}

			}

			return $result;

		}

		/**
		 * Custom status for PayPal verification pending
		 *
		 * @since   1.0.0
		 *
		 * @param   $value
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_paypal_status( $value ) {

			global $post;

			$order        = wc_get_order( $post->ID );
			$paypal_check = yit_get_prop( $order, 'ywaf_paypal_check' );

			switch ( $paypal_check ) {

				case 'process':
					$tip   = __( 'Waiting for PayPal verification.', 'yith-woocommerce-anti-fraud' );
					$value = sprintf( '<mark class="paypal tips" data-tip="%s">%s</mark>', $tip, $tip );
					break;
				case 'failed':

					$tip   = __( 'PayPal verification failed.', 'yith-woocommerce-anti-fraud' );
					$value = sprintf( '<mark class="paypal-failed tips" data-tip="%s">%s</mark>', $tip, $tip );
					break;

			}

			return $value;

		}

		/**
		 * Add the YWAF_PayPal_Verify class to WooCommerce mail classes
		 *
		 * @since   1.0.0
		 *
		 * @param   $email_classes
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_paypal_mail( $email_classes ) {

			foreach ( $this->_email_types as $type => $email_type ) {
				$email_classes[ $email_type['class'] ] = include( "includes/{$email_type['file']}" );
			}

			return $email_classes;
		}

		/**
		 * Send PayPal verification email
		 *
		 * @since   1.0.0
		 *
		 * @param   $order
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_paypal_mail_send( WC_Order $order ) {

			$paypal_email = yit_get_prop( $order, 'Payer PayPal address' );
			$wc_email     = WC_Emails::instance();
			$email        = $wc_email->emails['YWAF_PayPal_Verify'];

			$email->trigger( $order, $paypal_email );

		}

		/**
		 * Daily check of orders waiting paypal verification
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_paypal_cron() {

			$args = array(
				'post_type'      => 'shop_order',
				'posts_per_page' => - 1,
				'post_status'    => 'any',
				'meta_query'     => array(
					array(
						'key'     => 'ywaf_paypal_check',
						'value'   => 'process',
						'compare' => '='
					)
				)

			);

			add_filter( 'posts_where', array( $this, 'ywaf_paypal_waiting_where' ) );
			$resend_query = new WP_Query( $args );
			remove_filter( 'posts_where', array( $this, 'ywaf_paypal_waiting_where' ) );

			if ( $resend_query->have_posts() ) {

				while ( $resend_query->have_posts() ) {

					$resend_query->the_post();

					$order = wc_get_order( $resend_query->post->ID );

					$this->ywaf_paypal_mail_send( $order );

				}
			}

			wp_reset_query();
			wp_reset_postdata();

			add_filter( 'posts_where', array( $this, 'ywaf_paypal_cancel_where' ) );
			$cancel_query = new WP_Query( $args );
			remove_filter( 'posts_where', array( $this, 'ywaf_paypal_cancel_where' ) );

			if ( $cancel_query->have_posts() ) {

				while ( $cancel_query->have_posts() ) {

					$cancel_query->the_post();

					$order = wc_get_order( $cancel_query->post->ID );
					$order->update_status( 'wc-cancelled', 'PayPal verification failed. The email address is not verified.' );

					yit_save_prop( $order, 'ywaf_risk_factor', array(
						'score'        => 100,
						'failed_rules' => array( 'YWAF_PayPal' )
					) );

					yit_save_prop( $order, array( 'ywaf_check_status' => 'high_risk', 'ywaf_paypal_check' => 'failed' ), false, true );

					$this->ywaf_add_to_blacklist( $order, false, 100 );
					$this->ywaf_send_admin_mail( $order );

				}

			}

			wp_reset_query();
			wp_reset_postdata();

		}

		/**
		 * Set custom where condition
		 *
		 * @since   1.0.0
		 *
		 * @param   $where
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_paypal_waiting_where( $where = '' ) {

			$resend_days = get_option( 'ywaf_paypal_resend_days' );
			$cancel_days = get_option( 'ywaf_paypal_cancel_days' );

			$where .= " AND post_date > '" . date( 'Y-m-d', strtotime( '-' . $cancel_days . ' days' ) ) . "'" . " AND post_date <= '" . date( 'Y-m-d', strtotime( '-' . $resend_days . ' days' ) ) . "'";

			return $where;

		}

		/**
		 * Set custom where condition
		 *
		 * @since   1.0.0
		 *
		 * @param   $where
		 *
		 * @return  string
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_paypal_cancel_where( $where = '' ) {

			$cancel_days = get_option( 'ywaf_paypal_cancel_days' );

			$where .= " AND post_date <= '" . date( 'Y-m-d', strtotime( '-' . $cancel_days . ' days' ) ) . "'";

			return $where;

		}

		/**
		 * If is active YITH WooCommerce Email Templates, add YWAF to list
		 *
		 * @since   1.0.0
		 *
		 * @param   $templates
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function add_yith_wcet_template( $templates ) {

			$templates[] = array(
				'id'   => 'yith-anti-fraud',
				'name' => 'YITH WooCommerce Anti-Fraud',
			);
			$templates[] = array(
				'id'   => 'yith-anti-fraud-admin',
				'name' => 'YITH WooCommerce Anti-Fraud Admin',
			);

			return $templates;

		}

		/**
		 * ADMIN FUNCTIONS
		 */

		/**
		 * Enqueue script file
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function enqueue_scripts_admin_premium() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			wp_enqueue_script( 'ywaf-admin-knob', YWAF_ASSETS_URL . '/js/jquery.knob' . $suffix . '.js', array( 'jquery' ) );
			wp_enqueue_script( 'ywaf-admin-premium', YWAF_ASSETS_URL . '/js/ywaf-admin-premium' . $suffix . '.js', array( 'jquery', 'ywaf-admin-knob' ) );

			wp_enqueue_style( 'ywaf-admin-premium', YWAF_ASSETS_URL . '/css/ywaf-admin-premium' . $suffix . '.css' );

		}

		/**
		 * Add fraud check button to order table
		 *
		 * @since   1.0.0
		 *
		 * @param   $actions
		 * @param   $order
		 *
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_table_action( $actions, $order ) {

			$risk_factor  = yit_get_prop( $order, 'ywaf_risk_factor' );
			$button_label = ( $risk_factor ) ? __( 'Repeat fraud risk check', 'yith-woocommerce-anti-fraud' ) : __( 'Start fraud risk check', 'yith-woocommerce-anti-fraud' );
			$query_args   = array(
				'action'   => 'ywaf_fraud_risk_check',
				'order_id' => yit_get_order_id( $order ),
				'_wpnonce' => wp_create_nonce( 'ywaf-check-fraud-risk' ),
				'repeat'   => ( $risk_factor ) ? 'true' : 'false'
			);
			$ajax_url     = add_query_arg( $query_args, str_replace( array( 'https:', 'http:' ), '', admin_url( 'admin-ajax.php' ) ) );

			$actions['ywaf-check'] = array(
				'url'    => $ajax_url,
				'name'   => $button_label,
				'action' => 'ywaf-check'

			);

			return $actions;

		}

		/**
		 * Sanitize empty array
		 *
		 * @since   1.1.1
		 * @return  array
		 * @author  Alberto Ruggiero
		 */
		public function sanitize_empty_array( $option ) {
			return ( empty( $option ) ? array() : $option );
		}

		/**
		 * FRONTEND FUNCTIONS
		 */

		/**
		 * Enqueue script file
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function enqueue_scripts_frontend() {

			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

			wp_enqueue_script( 'ywaf-frontend-premium', YWAF_ASSETS_URL . '/js/ywaf-frontend-premium' . $suffix . '.js', array( 'jquery' ) );

			$query_args = array(
				'action'   => 'ywaf_resend_email',
				'_wpnonce' => wp_create_nonce( 'ywaf-resend-email' )
			);
			$ajax_url   = add_query_arg( $query_args, str_replace( array( 'https:', 'http:' ), '', admin_url( 'admin-ajax.php' ) ) );

			wp_localize_script( 'ywaf-frontend-premium', 'ywaf_ajax_url', $ajax_url );


		}

		/**
		 * Verify paypal email address and proceed to anti-fraud check
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_verify_paypal_code() {

			if ( isset( $_GET['ywaf_pvk'] ) && get_option( 'ywaf_paypal_enable' ) == 'yes' ) {

				$params     = explode( ',', base64_decode( $_GET['ywaf_pvk'] ) );
				$order_id   = str_replace( '#', '', base64_decode( $params[0] ) );
				$email      = base64_decode( $params[1] );
				$order_date = base64_decode( $params[2] );
				$order      = wc_get_order( $order_id );

				if ( $order && yit_get_prop( $order, 'date_created' ) == $order_date ) {

					$paypal_check = yit_get_prop( $order, 'ywaf_paypal_check' );

					if ( $paypal_check == 'process' ) {

						$stored_email = yit_get_prop( $order, 'Payer PayPal address' );

						if ( $email == $stored_email ) {

							yit_save_prop( $order, 'ywaf_paypal_check', 'success' );

							$paypal = new YWAF_PayPal();
							$paypal->add_to_verified( $email );

							$this->set_fraud_check( yit_get_order_id( $order ) );

							wc_add_notice( __( 'PayPal email address has been successfully checked!', 'yith-woocommerce-anti-fraud' ), 'success' );
							wc_print_notices();

						}

					}

				}

			}

		}

		/**
		 * Show info for order status
		 *
		 * @since   1.0.0
		 *
		 * @param   $order_id
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function ywaf_view_order( $order_id ) {

			$order = wc_get_order( $order_id );

			$paypal_check = yit_get_prop( $order, 'ywaf_paypal_check' );
			$check_status = yit_get_prop( $order, 'ywaf_check_status' );

			switch ( true ) {

				case $paypal_check == 'process':

					?>

					<h2>
						<?php _e( 'Order Status', 'yith-woocommerce-anti-fraud' ); ?>
					</h2>
					<p>
						<?php _e( 'In order to complete the order, you have to complete your verification process by clicking on the link we sent to your PayPal email address.', 'yith-woocommerce-anti-fraud' ) ?>
					</p>
					<p>
						<?php _e( 'If you have not received the verification email, you can ask for a new by clicking the following button', 'yith-woocommerce-anti-fraud' ) ?>
					</p>
					<p>
						<input type="hidden" id="ywcc_order_id" value="<?php echo yit_get_order_id( $order ); ?>" />
						<button type="button" class="button button-primary ywaf-resend-email"><?php _e( 'Re-send email', 'yith-woocommerce-anti-fraud' ) ?></button>
					</p>
					<?php

					break;

				case $paypal_check == 'failed':

					?>

					<h2>
						<?php _e( 'Order Status', 'yith-woocommerce-anti-fraud' ); ?>
					</h2>
					<p>
						<?php _e( 'We could not verify your PayPal email address and therefore your order has been cancelled. If you think an error has occurred, contact our customer service.', 'yith-woocommerce-anti-fraud' ) ?>
					</p>

					<?php

					break;

				case $check_status == 'high_risk':

					?>

					<h2>
						<?php _e( 'Order Status', 'yith-woocommerce-anti-fraud' ); ?>
					</h2>
					<p>
						<?php _e( 'The order has not passed anti-fraud tests, therefore it has been cancelled. If you think an error has occurred, contact our customer service.', 'yith-woocommerce-anti-fraud' ) ?>
					</p>

					<?php

					break;

				default:

			}

		}

		/**
		 * YITH FRAMEWORK
		 */

		/**
		 * Register plugins for activation tab
		 *
		 * @since   2.0.0
		 * @return  void
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 */
		public function register_plugin_for_activation() {
			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once 'plugin-fw/licence/lib/yit-licence.php';
				require_once 'plugin-fw/licence/lib/yit-plugin-licence.php';
			}
			YIT_Plugin_Licence()->register( YWAF_INIT, YWAF_SECRET_KEY, YWAF_SLUG );
		}

		/**
		 * Register privacy text
		 *
		 * @since   1.0.0
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function include_privacy_text() {
				include_once('includes/class-ywaf-privacy.php');
		}

		/**
		 * Register plugins for update tab
		 *
		 * @since   2.0.0
		 * @return  void
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 */
		public function register_plugin_for_updates() {
			if ( ! class_exists( 'YIT_Upgrade' ) ) {
				require_once( 'plugin-fw/lib/yit-upgrade.php' );
			}
			YIT_Upgrade()->register( YWAF_SLUG, YWAF_INIT );
		}

	}

}

