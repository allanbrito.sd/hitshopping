<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( !defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

return array(

    'paypal' => array(

        'ywaf_paypal_title'        => array(
            'name' => __( 'PayPal settings', 'yith-woocommerce-anti-fraud' ),
            'type' => 'title',
        ),
        'ywaf_paypal_enable'       => array(
            'name'    => __( 'Enable PayPal verification', 'yith-woocommerce-anti-fraud' ),
            'type'    => 'checkbox',
            'id'      => 'ywaf_paypal_enable',
            'default' => 'yes',
            'class'   => 'ywaf-checkbox'
        ),
        'ywaf_paypal_resend_days'  => array(
            'name'              => __( 'Time span before further attempts', 'yith-woocommerce-anti-fraud' ),
            'type'              => 'number',
            'id'                => 'ywaf_paypal_resend_days',
            'desc'              => __( 'Number of days that have to pass before sending another email if the order is still waiting for verification', 'yith-woocommerce-anti-fraud' ),
            'default'           => '2',
            'class'             => 'ywaf-thresholds ywaf-medium',
            'custom_attributes' => array(
                'min'      => 1,
                'max'      => 30,
                'required' => 'required'
            )
        ),
        'ywaf_paypal_cancel_days'  => array(
            'name'              => __( 'Time span before the orders are cancelled', 'yith-woocommerce-anti-fraud' ),
            'type'              => 'number',
            'id'                => 'ywaf_paypal_cancel_days',
            'desc'              => __( 'Number of days that have to pass before deleting the order if it is not verified', 'yith-woocommerce-anti-fraud' ),
            'default'           => '5',
            'class'             => 'ywaf-thresholds ywaf-high',
            'custom_attributes' => array(
                'min'      => 1,
                'max'      => 30,
                'required' => 'required'
            )
        ),
        'ywaf_paypal_mail_type'    => array(
            'name'    => __( 'Email type', 'yith-woocommerce-anti-fraud' ),
            'type'    => 'select',
            'desc'    => __( 'Choose a format for the email.', 'yith-woocommerce-anti-fraud' ),
            'options' => array(
                'html'  => __( 'HTML', 'yith-woocommerce-anti-fraud' ),
                'plain' => __( 'Plain text', 'yith-woocommerce-anti-fraud' )
            ),
            'default' => 'html',
            'id'      => 'ywaf_paypal_mail_type'
        ),
        'ywaf_paypal_mail_subject' => array(
            'name'              => __( 'Email subject', 'yith-woocommerce-anti-fraud' ),
            'type'              => 'text',
            'desc'              => '<b>{site_title}</b> ' . __( 'Replaced with the site title', 'yith-woocommerce-anti-fraud' ),
            'id'                => 'ywaf_paypal_mail_subject',
            'default'           => __( '[{site_title}] Confirm your PayPal email address', 'yith-woocommerce-anti-fraud' ),
            'css'               => 'width: 400px;',
            'custom_attributes' => array(
                'required' => 'required'
            )
        ),
        'ywaf_paypal_mail_body'    => array(
            'name'              => __( 'Email body', 'yith-woocommerce-anti-fraud' ),
            'type'              => 'yith-wc-textarea',
            'desc'              => '<b>{site_title}</b> ' . __( 'Replaced with the site title', 'yith-woocommerce-anti-fraud' ) . '<br /><br /><b>{site_email}</b> ' . __( 'Replaced with the site title', 'yith-woocommerce-anti-fraud' ),
            'id'                => 'ywaf_paypal_mail_body',
            'default'           => __( 'Hi!
We have received your order on {site_title}, but to complete we have to verify your PayPal email address.

If you haven\'t made or authorized any purchase, please, contact PayPal support service immediately,
and email us to {site_email} for having your money back.', 'yith-woocommerce-anti-fraud' ),
            'css'               => 'resize: vertical; width: 100%; min-height: 40px; height:300px',
            'custom_attributes' => array(
                'required' => 'required'
            )
        ),
        'ywaf_paypal_verified'     => array(
            'name'        => __( 'PayPal verified addresses', 'yith-woocommerce-anti-fraud' ),
            'type'        => 'yith-wc-custom-checklist',
            'id'          => 'ywaf_paypal_verified',
            'default'     => '',
            'desc'        => __( 'Verified email addresses', 'yith-woocommerce-anti-fraud' ),
            'placeholder' => __( 'Enter an email address&hellip;', 'yith-woocommerce-anti-fraud' ),
        ),
        'ywaf_paypal_end'          => array(
            'type' => 'sectionend',
        ),

    )

);