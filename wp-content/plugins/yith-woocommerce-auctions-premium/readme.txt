=== YITH WooCommerce Auctions ===

== Changelog ==


= Version 1.2.3 - Released: Oct 02, 2018 =

* New : Send notification to customer who lost auction
* New : Daily cronjob to resend failed emails to winners
* Tweak : Improve slow queries
* Update : Dutch language
* Fix : Time format on related product section
* Dev : Filter yith_wcact_check_email_is_send
* Dev : Filter yith_wcact_congratulation_message
* Dev : Filter yith_wcact_my_account_congratulation_message
* Dev : Filter yith_wcact_product_exceeded_reserve_price_message
* Dev : Filter yith_wcact_product_has_reserve_price_message
* Dev : Filter yith_wcact_product_does_not_have_a_reserve_price_message
* Dev : Action yith_wcact_auction_status_my_account_closed
* Dev : Action yith_wcact_auction_status_my_account_started

= Version 1.2.2 - Released: Jun 27, 2018 =

* New: Admin option to resend failed emails to winners
* New: Daily cronjob to resend failed emails to winners
* Update: Italian language
* Update: Spanish language
* Tweak: Possibility to change recipient email
* Dev : Filter yith_wcact_check_email_is_send


= Version 1.2.1 - Released: May 21, 2018 =

* New: Support to WordPress 4.9.6 RC2
* New: Support to WooCommerce 3.4.0 RC1
* New: Metabox auction status
* New: Possibility to resend auction email
* Update: Plugin Framework
* Dev: Filter yith_wcact_show_time_in_customer_time
* Dev: Filter yith_wcact_tab_auction_show_name
* Dev: Filter yith_wcact_display_user_anonymous_name

= Version 1.2.0 - Released: Apr 03, 2018 =

* New: Shortcode [yith_auction_non_stated] to show no started auctions
* New: Support WPML Currency Switcher
* Tweak: WPML Currency Language
* Update: Plugin Framework
* Fix: Problem WPML products in cart and checkout page
* Fix: Problem with buy now button

= Version 1.1.14 - Released: Feb 09, 2018 =

* New: support to WordPress 4.9.4
* New: support to WooCommerce 3.3.1
* New: shortcode [yith_auction_current] to show current live auctions
* Tweak: pagination in auction shortcodes
* Tweak: select number of columns and product per page in auction shortcodes

= Version 1.1.13 - Released: Jan 30, 2018 =

* New: support to WordPress 4.9.2
* New: support to WooCommerce 3.3.0-RC2
* Update: plugin framework 3.0.11

= Version 1.1.12 - Released: Jan 10, 2018 =

* New: Product parameter in end-auction email
* Fix: notice in wp-query
* Fix: problem check stock in auction.php template
* Update: Plugin core
* Update: Spanish translation
* Dev: filter yith_wcact_max_bid_manual
* Dev: filter yith_wcact_auction_product_id
* Dev: filter yith_wcact_show_buy_now_button
* Dev: action yith_wcact_auction_auction_reserve_price
* Dev: action yith_wcact_after_auction_end

= Version 1.1.11 - Released: Oct 24, 2017 =

* New: added new successfully bid email
* Update: Plugin core

= Version 1.1.10 - Released: Oct 20, 2017 =

* Fix: error get image and bids on auction product page
* Update: Plugin core

= Version 1.1.9 - Released: Oct 17, 2017 =

* Fix: error load bid table
* Dev: added filter yith_wcact_show_list_bids

* New: Support to WooCommerce 3.2.0 RC2
* Update: Plugin core

= Version 1.1.8 - Released: Oct 10, 2017 =

* New: Support to WooCommerce 3.2.0 RC2
* Update: Plugin core

= Version 1.1.7 - Released: Oct 02, 2017 =

* Fix:  Issue with timeleft
* Fix : Issue send admin winner email
* Fix : Get right url using WPML
* Dev : Added action yith_wcact_before_add_to_cart_form

= Version 1.1.6 - Released: Aug 28, 2017 =

* Fix: Show products on shop page when out of stock general option is enabled
* Fix : Style issue on my auctions chart

= Version 1.1.5 - Released: Aug 16, 2017 =
* New: Dutch translation
* Fix: Send multiple emails when the auction is in overtime.
* Dev: Added filter yith_wcact_display_watchlist

= Version 1.1.4 - Released: Aug 14, 2017 =

* New: add more than one recipient to the winner email sent to the admin
* New: added tax class and tax status
* New: added new label when the auction is closed and no customer won the auction
* New: shortcode [yith_auction_out_of_date] to show out of date auctions
* Fix: URL encode to prevent redirect error.
* Fix: check if the auction has ended when a customer bids
* Fix: count auction product in shop loop.
* Fix: show pay-now button when an auction is rescheduled.
* Dev: added filter yith_wcact_datetime_table
* Dev: added filter yith_wcact_bid_tab_title
* Dev: added filter yith_wcact_priority_bid_tab
* Dev: added filter yith_wcact_bid_tab

= Version 1.1.3 - Released: Jul 07, 2017 =

* New: Compatibility with  YITH Infinite Scrooling Premium
* Fix: remove auction product on shop loop when option is disabled
* Fix: remove Pay now button when the bid doesn't exceed the reserve price on my account page
* Dev: added action yith_wcact_render_product_columns
* Dev: added filter yith_wcact_product_columns

= Version 1.1.2 - Released: May 04, 2017 =

* New: Admin can delete customer's bid
* New: Customers register to a watchlist for each auction product and be notified by email when auction is about to end
* New: Minimum amount to increase manual bids
* New: added wc_notice in product page
* Fix: Auction product price not changing when clicking on buy now
* Fix: show a NaN number in timeleft when auction has not started
* Dev: added action yith_wcact_before_form_auction_product
* Dev: added filter yith_wcact_load_script_widget_everywhere

= Version 1.1.1 - Released: Apr 04, 2017 =

* New: support to WooCommerce 3.0.0-RC2
* New: possibility to add auction product and other products to cart in the same order
* New: reschedule auction without bids automatically

= Version 1.1.0 - Released: Mar 10, 2017 =
* New: support to WooCommerce 2.7.0-RC1
* New: live auctions on My account page
* New: live auctions on product page
* New: compatibility with WPML
* New: bid list on admin product page
* Update: YITH Plugin Framework

= Version 1.0.14 - Released: Feb 07, 2017 =

* New: show upbid and overtime in product page
* New: tooltip info in product page
* New: message info when auction is in overtime
* New: shortcode named [yith_auction_products] that allows you to show the auctions on any page.
* Dev: added action yith_wcact_before_add_button_bid

= Version 1.0.13 - Released: Dec 23, 2016 =

* Fixed: Issue with date time in bid tab

= Version 1.0.12 - Released: Dec 16, 2016 =

* Added: Overtime option in general settings
* Fixed: Issue with bid button
* Fixed: Product issues

= Version 1.0.11 - Released: Dec 13, 2016 =

* Added: Admin option to regenerate auction prices.
* Added: Pay now option from My account.
* Added: Possibility to add overtime to an auction.
* Updated: name and text domain.
* Updated: language file.
* Fixed: Issues with admin emails.
* Fixed: Reschedule auction when product has buy now status.
* Dev: added yith_wcact_auction_price_html filter.

= Version 1.0.10 - Released: Oct 17, 2016 =

* Fixed: "Buy Now" issue

= Version 1.0.9 - Released: Oct 04, 2016 =

* Fixed: Sending email issue.

= Version 1.0.8 - Released: Sep 28, 2016 =

* Fixed: Datetime format in product page.
* Fixed: Missing arguments in order page.
* Fixed: Username in product page problem.

= Version 1.0.7 - Released: Sep 20, 2016 =

* Added: Notification email to admin when an auction ends and has a winner.
* Added: Possibility to filter by auction status.
* Fixed: Enable/Disable email notifications.
* Fixed: Show Datetime in local time

= Version 1.0.6 - Released: Sep 13, 2016 =

* Added: Option in product settings to show buttons in product page to increase or decrease the bid.
* Fixed: Problems with the translation in emails.
* Fixed: Problems with tab bid.
* Fixed: Prevent issues with manage stock.
* Fixed: Problems with order by price in shop loop.
* Added: Admin setting that show or not the pay now button in product page when the auction is ends.

= Version 1.0.5 - Released: Sep 01, 2016 =

* Fixed: username in winner email
* Fixed: timeleft in shop

= Version 1.0.4 - Released: Aug 30, 2016 =

* Fixed: enqueue script issues
* Fixed: Pay now button in winner email
* Fixed: translation issues

= Version 1.0.2 - Released: Aug 22, 2016 =

* Fixed: updated textdomain for untranslatable strings
* Fixed: Problems with pay-now button in winner email when users are not logged in.
* Fixed: Problems with product text link in winner email.
* Updated: yith-auctions-for-woocommerce.pot


= Version 1.0.1 - Released: Aug 18, 2016 =

* Added: Margin button in auction widget
* Fixed: Problems when not exist reserve price in auctions with automatic bids.
* Fixed: Problems with the translation.

= Version 1.0.0 - Released: Aug 10, 2016 =

* First release

== Suggestions ==

If you have suggestions about how to improve YITH WooCommerce Auctions, you can [write us](mailto:plugins@hitoutlets.com "Your Inspiration Themes") so we can bundle them into the next release of the plugin.

== Translators ==

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://hitoutlets.com/contact/ "Your Inspiration Themes") so we can bundle it into YITH WooCommerce Auctions languages.

 = Available Languages =
 * English
 * Spanish
