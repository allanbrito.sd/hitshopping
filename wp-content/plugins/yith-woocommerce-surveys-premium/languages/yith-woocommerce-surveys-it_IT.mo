��    w      �  �   �      
     
     
     -
     :
     C
  
   P
     [
     _
     n
  &   �
     �
     �
     �
     �
               0     M     Y     j     v     }     �     �     �     �     �               +     3     H     ]     b     g     l     ~     �     �     �     �     �  ,   �  �     *   �  �   �  �   a     	          &  	   ,     6  
   ;  	   F     P     a     {     �     �     �     �     �  C   �               ,     @     P     b     j     q     x     �     �     �     �     �  f   �     4     @     I     g     n     }     �     �     �     �     �     �     �               -     ?     E     K     P     ^     k  
   w     �     �  
   �     �     �     �  C   �       h   2  }   �  /        I  "   h     �      �     �      �  P       ^     a     u  
   �  
   �     �     �     �     �  .   �       %   "  )   H  '   r     �     �  &   �     �          !     2  '   ;  ,   c  )   �  $   �     �     �               3     <     \     |     �     �     �     �     �     �     �     �     �  ?     �   V  5     �   N  �   �  	   �     �     �  	                  !     /  $   H     m  
   t          �     �     �  >   �               .     E     V     h     q  
   y     �  	   �     �     �     �     �  n   �     d      s   ,   �   	   �      �      �      �      �      !  '   "!     J!     S!     i!     !     �!     �!     �!  	   �!     �!     �!     �!     
"     ""     6"     N"     e"     q"     v"     �"  I   �"     �"  g   #     k#     �#  	   $     $     $     $$  	   -$     7$     
          -   '   !      U          9       @   :   D   /   Y   $         o   8              4       m                     \   Q      >   i           p       G   +   6   O              .       S                 H      c   	       j   [       W   C   h       w   r   E   B   A   3       u   b   )       %          k   "      V   d   2             0   M       =   T   ,   N       t       a   F             g                 #   q   v           *       n   _   (   1          ^   K   7   f   J       R   ;   L      s       &   ]   l   Z       <          `              e       ?       X   5           P   I                 %s %s draft updated %s published %s saved %s submitted %s updated ASC Add New Survey Add Survey Answer Add YITH WooCommerce Surveys shortcode Add shortcode After Add to Cart button After Checkout Billing Form After Checkout Shipping Form After Customer Details After Order Notes After Single Product Summary All Surveys All survey types All surveys Answer Before Add to Cart button Before Checkout Billing Form Before Checkout Shipping Form Before Customer Details Before Order Notes Checkout Choose Product Choose data to export Confirm Custom field deleted Custom field updated DATE DESC Date Display Survey in Edit Survey Enable plugin Enter a title Export Data Filter by survey Filter by survey type Hide survey only after one answer is given.  If "Checkout" is selected, surveys will show in checkout page, while if "Other Pages" is selected,
            surveys can be added using shortcode or a widget If checked, survey is required in checkout If checked, users will no longer be able to see surveys they have answered. Works only in Survey, visible in product or via shortcode or widget If checkout, surveys will show in checkout page.%1$sIf product, surveys will
show in product page.%1$sIf other page, surveys can be placed using shortcode or a widget. Insert Insert a Survey Title Items Live demo NAME No Details No Orders No surveys found No surveys found in Trash Order Order By Order Details Order totals Other Pages Parent Item: Please, give an answer to the questions to contribute to the survey Plugin Documentation Position in Checkout Position in Product Premium Version Premium live demo Product Remove Report Reports Required Return to rule list Search Surveys Select a Survey Select an option Select products in which you want to show surveys. Leave it blank to show the survey on every product. Send Answer Settings Show your Surveys in sidebar! Survey Survey Answers Survey Option Survey Options Survey Settings Survey Title Survey restored to version %s Surveys Surveys in Checkout Surveys in Product Surveys in other pages Thank-you message Thanks for voting Title Tools Type Update Survey View Details View Survey View order View result View results Visible in Votes WooCommerce Checkout WooCommerce Product Works only in Survey, visible in product or via shortcode or widget YITH WooCommerce Surveys YITH WooCommerce Surveys Premium is enabled but not effective. It requires WooCommerce in order to work. You have reached the maximum number of elements for this survey, remove o edit existing
                ones to add new ones. post type general nameYITH WooCommerce Surveys post type singular nameSurvey yith-woocommerce-surveysSurvey ID yith-woocommerce-surveysanswer yith-woocommerce-surveysanswers yith-woocommerce-surveyssurvey yith-woocommerce-surveyssurveys Project-Id-Version: YITH WooCommerce Surveys
POT-Creation-Date: 2016-07-25 17:31+0200
PO-Revision-Date: 2016-08-01 16:08+0200
Language-Team: Yithemes <plugins@yithemes.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c
Last-Translator: 
Language: it_IT
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 %s %s bozza aggiornata %s pubblicato %s salvato %s inviato %s aggiornato ASC Aggiungi Nuovo Sondaggio Aggiungi Risposta Sondaggio Aggiungi shortcode di YITH WooCommerce Surveys Aggiungi shortcode Dopo il pulsante Aggiungi al Carrello Dopo il form di fatturazione del checkout Dopo il form di spedizione del checkout Dopo i dettagli del cliente Dopo le note dell'ordine Dopo il riepilogo del prodotto singolo Tutti i Sondaggi Tutti i tipi di sondaggio Tutti i sondaggi Risposta Prima del pulsante Aggiungi al Carrello Prima del form di fatturazione del checkout Prima del form di spedizione del checkout Prima delle informazioni del cliente Prima delle note dell'ordine Checkout Scegli il prodotto Scegli i dati da esportare Conferma Eliminato campo personalizzato  Aggiornato campo personalizzato DATA DECR Data Mostra Sondaggio in Modifica Sondaggio Attiva il plugin Inserisci un titolo Esporta dati Filtra per sondaggio Filtra per tipo di sondaggio Nascondi il sondaggio solo dopo che è stata data una risposta. Se è selezionato "Checkout", i sondaggi verranno mostrati nella pagina checkout, mentre se è selezionato "Altre pagine"
i sondaggi possono essere aggiunti utilizzando lo shortcode o un widget Se selezionato, il sondaggio è richiesto al checkout Se selezionato, gli utenti non potranno più vedere i sondaggi a cui hanno risposto. Funziona solo in Sondaggio, visibile nel prodotto via shortcode o widget Se checkout, i sondaggi verranno mostrati nella pagina checkout.%1$sSe prodotto, i sondaggi verranno
mostrati nella pagina prodotto.%1$sSe altra pagina, i sondaggi possono essere posizionati utilizzando lo shortcode o un widget. Inserisci Inserisci un Titolo Sondaggio Oggetti Live demo NOME Nessun dettaglio Nessun ordine Nessun sondaggio trovato Nessun sondaggio trovato nel cestino Ordine Ordina per Dettagli dell'ordine Totale ordini Altre pagine Elemento genitore: Per favore, rispondi alle domande per contribuire al sondaggio Documentazione plugin Posizione nel Checkout Posizione nel prodotto Versione Premium Live demo premium Prodotto Rimuovi Statistica Statistiche Richiesto Ritorna alla lista delle regole Cerca Sondaggi Seleziona un Sondaggio Seleziona un'opzione Seleziona i prodotti in cui vuoi mostrare i sondaggi. Lascia vuoto per mostrare il sondaggio su ogni prodotto. Invia risposta Impostazioni Mostra i tuoi Sondaggi nella barra laterale! Sondaggio Risposte del sondaggio Opzione sondaggio Opzioni sondaggio Impostazioni del sondaggio Titolo del Sondaggio Sondaggio ripristinato alla versione %s Sondaggi Sondaggi nel Checkout Sondaggi nel Prodotto Sondaggi in altre pagine Messaggio di ringraziamento Grazie per aver votato Titolo Strumenti Tipo Aggiorna Sondaggio Visualizza i Dettagli Visualizza il Sondaggio Visualizza l'ordine Visualizza il risultato Visualizza i risultati Visibile in Voti WooCommerce Checkout Prodotto WooCommerce Funziona solo nel Sondaggio, visibile nel prodotto via shortcode o widget YITH WooCommerce Surveys YITH WooCommerce Surveys Premium è attivo ma non operativo. Il suo funzionamento richiede WooCommerce. Hai raggiunto il numero massimo di elementi per questo sondaggio, elimina o modifica quelli esistenti
per aggiungerne di nuovi. YITH WooCommerce Surveys Sondaggio ID sondaggio risposta risposte sondaggio sondaggi 