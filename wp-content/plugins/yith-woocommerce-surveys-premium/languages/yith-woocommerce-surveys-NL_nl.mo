��    w      �  �   �      
     
     
     -
     :
     C
  
   P
     [
     _
     n
  &   �
     �
     �
     �
     �
               0     M     Y     j     v     }     �     �     �     �     �               +     3     H     ]     b     g     l     ~     �     �     �     �     �  ,   �  �     *   �  �   �  �   a     	          &  	   ,     6  
   ;  	   F     P     a     {     �     �     �     �     �  C   �               ,     @     P     b     j     q     x     �     �     �     �     �  f   �     4     @     I     g     n     }     �     �     �     �     �     �     �               -     ?     E     K     P     ^     k  
   w     �     �  
   �     �     �     �  C   �       h   2  }   �  /        I  "   h     �      �     �      �  N       \     _     {  
   �     �     �     �     �     �  ,   �       "   -     P  +   o     �     �  '   �     �     �            &     #   F  -   j     �     �     �     �     �  
   �                :     @     E     K     a     t     �     �     �     �  7   �  �     8   �  �     �   �     �     �  	   �  	   �     �     �             (   8  
   a     l     {     �     �     �  K   �               9     P     _     q     y     �  	   �  	   �  '   �     �     �     �  x   
      �      �       �      �      �      �      �      !     #!      2!  	   S!     ]!     |!     �!     �!     �!     �!     �!     �!     �!     
"     "     ."     C"     W"     k"     x"     �"     �"  L   �"     �"  t   #  �   �#     $     2$     ;$     G$  
   P$     [$  	   d$     
          -   '   !      U          9       @   :   D   /   Y   $         o   8              4       m                     \   Q      >   i           p       G   +   6   O              .       S                 H      c   	       j   [       W   C   h       w   r   E   B   A   3       u   b   )       %          k   "      V   d   2             0   M       =   T   ,   N       t       a   F             g                 #   q   v           *       n   _   (   1          ^   K   7   f   J       R   ;   L      s       &   ]   l   Z       <          `              e       ?       X   5           P   I                 %s %s draft updated %s published %s saved %s submitted %s updated ASC Add New Survey Add Survey Answer Add YITH WooCommerce Surveys shortcode Add shortcode After Add to Cart button After Checkout Billing Form After Checkout Shipping Form After Customer Details After Order Notes After Single Product Summary All Surveys All survey types All surveys Answer Before Add to Cart button Before Checkout Billing Form Before Checkout Shipping Form Before Customer Details Before Order Notes Checkout Choose Product Choose data to export Confirm Custom field deleted Custom field updated DATE DESC Date Display Survey in Edit Survey Enable plugin Enter a title Export Data Filter by survey Filter by survey type Hide survey only after one answer is given.  If "Checkout" is selected, surveys will show in checkout page, while if "Other Pages" is selected,
            surveys can be added using shortcode or a widget If checked, survey is required in checkout If checked, users will no longer be able to see surveys they have answered. Works only in Survey, visible in product or via shortcode or widget If checkout, surveys will show in checkout page.%1$sIf product, surveys will
show in product page.%1$sIf other page, surveys can be placed using shortcode or a widget. Insert Insert a Survey Title Items Live demo NAME No Details No Orders No surveys found No surveys found in Trash Order Order By Order Details Order totals Other Pages Parent Item: Please, give an answer to the questions to contribute to the survey Plugin Documentation Position in Checkout Position in Product Premium Version Premium live demo Product Remove Report Reports Required Return to rule list Search Surveys Select a Survey Select an option Select products in which you want to show surveys. Leave it blank to show the survey on every product. Send Answer Settings Show your Surveys in sidebar! Survey Survey Answers Survey Option Survey Options Survey Settings Survey Title Survey restored to version %s Surveys Surveys in Checkout Surveys in Product Surveys in other pages Thank-you message Thanks for voting Title Tools Type Update Survey View Details View Survey View order View result View results Visible in Votes WooCommerce Checkout WooCommerce Product Works only in Survey, visible in product or via shortcode or widget YITH WooCommerce Surveys YITH WooCommerce Surveys Premium is enabled but not effective. It requires WooCommerce in order to work. You have reached the maximum number of elements for this survey, remove o edit existing
                ones to add new ones. post type general nameYITH WooCommerce Surveys post type singular nameSurvey yith-woocommerce-surveysSurvey ID yith-woocommerce-surveysanswer yith-woocommerce-surveysanswers yith-woocommerce-surveyssurvey yith-woocommerce-surveyssurveys Project-Id-Version: YITH WooCommerce Surveys
POT-Creation-Date: 2018-01-29 15:23+0000
PO-Revision-Date: 2018-01-29 16:10+0000
Language-Team: Yithemes <plugins@yithemes.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c
Last-Translator: 
Language: nl
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 %s %s eerste versie bijgewerkt %s gepubliceerd %s bewaard %s ingevoerd %s bijgewerkt ASC Nieuwe Enquête toevoegen Enquête antwoorden toevoegen YITH WooCommerce Surveys shortcode toevoegen Voeg shortcode toe Na Toevoegen aan winkelmandje knop Na het afrekenfactuurformulier Na het afrekeningsformulier voor verzending Na Klantdetails Na ordernotities Na Samenvatting afzonderlijke producten Alle enquêtes Alle Enquête types Alle enquêtes Antwoord Vóór Toevoegen aan winkelmandje knop Vóór Checkout-facturatieformulier Voor het afrekeningsformulier voor verzending Voor klantdetails Vóór ordernotities Afrekenproces Kies Product Kies data om te exporteren Bevestigen Aangepast veld is verwijderd Aangepast veld is bijgewerkt DATUM DESC Datum Enquête weergeven in Enquête Bijwerken Schakel plugin in Voer een Titel in Exporteer Data Filteren op Enquête  Filteren op Enquête type Verberg enquête nadat er maar een antwoord is gegeven. Als 'Afrekenen' is geselecteerd, worden enquêtes weergegeven op de afrekenpagina, terwijl 'Andere pagina's' is geselecteerd,
            enquêtes kunnen worden toegevoegd met behulp van shortcode of een widget Indien aangevinkt, is enquête vereist bij het afrekenen Als ingeschakeld, kunnen gebruikers enquêtes die ze hebben beantwoord niet meer zien. Werkt alleen binnen de Enquête, zichtbaar in product of via shortcode of widget Als u uitcheckt, worden enquêtes weergegeven op de afrekenpagina.%1$sInstellingen, enquêtes
worden weergegeven op de productpagina.%1$sIndien andere pagina's beschikbaar zijn, kunnen enquêtes worden geplaatst met behulp van een shortcode of een widget. Invoeren Enquête Titel invoeren Artikelen Live demo NAAM Geen Informatie Geen Bestellingen Geen Enquêtes gevonden Geen Enquêtes gevonden in de Prullenbak Bestelling Bestellen door Bestelinformatie Bestellingstotaal Andere Paginas Ouder product: Geef alsjeblieft een antwoord op de vragen om bij te dragen aan de enquête Plugin Documentatie Positie in het afrekenproces Positie binnen Product Premium Versie Premium live demo Product Verwijderen Rapport Rapporten Verplicht Terug keren naar de lijst met de regels Zoek binnen Enquêtes Selecteer een Enquête Selecteer een optie Selecteer producten waarin u enquêtes wilt weergeven. Laat dit veld leeg om de enquête voor elk product weer te geven. Verstuur Antwoord Instellingen Toon uw enquêtes in de zijbalk! Enquête Antwoorden Enquête Enquête Instellingen Enquête Opties Instellingen van de Enquête Enquête Titel Enquête hersteld naar versie %s Enquêtes Enquêtes in het afrekenproces Enquêtes in het Product Enquêtes binnen andere paginas Bedankt bericht Bedankt voor het stemmen titel Gereedschap Model Enquête Bijstellen Details Weergeven Weergeef Enquête Bestelling Weergeven Weergeef resultaten Weergeef resultaten Zichtbaar in Stemmen WooCommerce Afrekenproces WooCommerce Product Werkt alleen in de Enquête, zichtbaar in product of via shortcode of widget YITH WooCommerce Surveys YITH WooCommerce  Surveys Premium is ingeschakeld, maar niet effectief. Het vereist WooCommerce om te kunnen werken. U heeft het maximale aantal elementen voor deze enquête bereikt, verwijder o bewerk bestaande
                degenen om nieuwe toe te voegen. YITH WooCommerce Surveys Enquête Enquête ID Antwoord Antwoorden Enquête Enquêtes 