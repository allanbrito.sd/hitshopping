== Changelog ==

== 1.5.4 == Released on Sep 26, 2018

* New: Support to WooCommerce 3.4.5.
* Update: Plugin Core.
* Update: Dutch translation.

== 1.5.3 == Released on Aug 22, 2018

* New: Export waiting list users in CSV.
* Update: Plugin Core.
* Fix: Wrong success message showed when double op-tin is enabled.

== 1.5.2 == Released on Jul 27, 2018

* New: Show alternative success message when double opt-in is enabled.
* Updated: Dutch language file.
* Updated: Spanish language file.
* Fix: Unable to save stock.

== 1.5.1 == Released on May 30, 2018

* New: Support to WooCommerce 3.4.1.
* New: Privacy Policy DPA.

== 1.5.0 == Released on May 16, 2018

* New: Support to WooCommerce 3.4 RC1.
* New: General Data Protection Regulation (GDPR) compliance.
* New: Option to add a Privacy Policy checkbox to the waiting list form.
* New: Option to enable Double Opt-In subscription method.
* New: Filter by product stock status for Waiting Lists table.
* New: Search by product name for Waiting Lists table.
* New: Search by email address for Waiting List Users table.
* Update: Language files.
* Update: Plugin Core.
* Fix: Bulk action for Waiting Lists table.

== 1.4.1 == Released on Mar 30, 2018

* New: Support to WooCommerce 3.3.4.
* New: Support to WordPress 4.9.4.
* Update: Plugin Core.
* Fix: Add endpoint method.
* Fix: Add product param to email templates filter.
* Fix: Wrong name for custom attributes in waiting list table.
* Dev: New filter 'yith_wcwtl_recipient_mail_subscribe'.

== 1.4.0 == Released on Feb 02, 2018

* New: Support to WooCommerce 3.3.0.
* New: Support to WordPress 4.9.2.
* New: Dutch translation.
* New: Submit frontend form using AJAX.
* Update: Plugin Core.
* Update: Language files.
* Fix: Product name including variations on plugin's emails.

== 1.3.1 == Released on Nov 28, 2017

* New: Support to WooCommerce 3.2.5.
* New: Support to WordPress 4.9.0.
* New: Norwegian translate (thanks to Bernhard Brynildsen).
* New: Add option to invert exclusions list logic.
* Fix: Add form on get variations AJAX action.
* Fix: Flush rewrite rules to prevent 404 error on my account plugin section.

== 1.3.0 == Released on Nov 09, 2017

* New: Support to WooCommerce 3.2.3.
* New: Support to WordPress 4.8.3.
* New: New methods to send an email to all the users in the waiting list when a product is set back as 'In-stock'.
* Update: Plugin Core.
* Fix: Remove product from exclusions list.

== 1.2.3 == Released on Oct 26, 2017

* New: Support to WooCommerce 3.2.1.
* Update: Languages files.
* Fix: Wrong argument for shortcode [ywcwtl_form].

== 1.2.2 == Released on Oct 12, 2017

* New: Support to WooCommerce 3.2.0.
* New: Support to WordPress 4.8.2.
* Update: Plugin Core.

== 1.2.1 == Released on Sep 05, 2017

* New: Support to WooCommerce 3.1.2.
* New: Support to WordPress 4.8.1.
* Update: Plugin Core.
* Update: Language files.
* Fix: Correct image size for product thumb on email.
* Fix: Uncaught error, [] operator not supported for strings.
* Fix: Notice for string to array conversion for older php version, less then 5.6.
* Fix: Compatibility issue with YITH WooCommerce Multi Vendor Premium.
* Tweak: Auto delete plugin meta for products that don't exists anymore.

== 1.2.0 == Released on Mar 28, 2017

* New: Support to WooCommerce 3.0.0 RC2.
* New: Support to WordPress 4.7.3.
* Update: Plugin Core.
* Update: Language files.

== 1.1.5 == Released on Jan 26, 2017

* New: Compatibility with Iconic WooCommerce Quick View.
* New: Compatibility with YITH WooCommerce Quick View.
* New: Support to WooCommerce 2.6.13.
* New: Support to WordPress 4.7.2.
* Fix: Double email subscription for guest customer.
* Dev: Add product object $product as second parameter to filter yith_wcwtl_can_product_have_waitlist.

== 1.1.4 == Released on Oct 28, 2016

* Fix: Waiting list for variable products.

== 1.1.3 == Released on Oct 27, 2016

* New: Support to WooCommerce 2.6.7
* New: Support to WordPress 4.6.1
* New: Italian translation.
* New: Compatibility with YITH WooCommerce Email Templates.
* New: Shortcode for print waiting list form [ywcwtl_form product_id=""].
* Update: Plugin Core.
* Update: Language files.
* Fix: YITH WooCommerce Multi Vendor compatbility issue with variable products.

== 1.1.2 == Released on Jun 10, 2016

* New: Support to WooCommerce 2.6 RC1
* New: Spanish translation.
* Update: Plugin Core.
* Update: Language file .pot.

== 1.1.1 == Released on Apr 15, 2016

* New: Compatibility with Wordpress 4.5.
* New: Product thumbnail in "My Waiting List" table.
* New: Shortcode [ywcwtl_waitlist_table] using to print the waiting list table for customer.
* New: Added minimized js files. Plugin loads full files version if the constant "SCRIPT_DEBUG" is defined and is true.
* Update: Plugin Core.
* Update: Language file .pot.

== 1.1.0 == Released on Mar 11, 2016

* Update: WPML config XML.
* Update: Plugin Core.
* Update: Language file .pot.
* Fix: Error on my-account page when a product on waitlist doesn't exists anymore.
* Fix: Backorder products compatibility.

== 1.0.9 == Released on Jan 04, 2016

* New: Compatibility with WooCommerce 2.5 BETA.
* Update: Plugin Core.
* Update: Language file .pot.

== 1.0.8 == Released on Dec 14, 2015

* Update: Changed text domain from yith-wcwtl to yith-woocommerce-waiting-list.
* Update: Language file .pot.
* Fix: Compatibility issue with YITH WooCommerce Multi Vendor.

== 1.0.7 == Released on Dec 11, 2015

* New: Compatibility with Wordpress 4.4.
* Update: Plugin Core.
* Fix: Double menu entry for YITH Plugins.

== 1.0.6 == Released on Nov 24, 2015

* New: Compatibility with YITH WooCommerce Multi Vendor.
* Update: Plugin Core.
* Update: Language file.

== 1.0.5 == Released on Oct 12, 2015

* New: Mandrill Integration.
* New: Advanced text editor for content email options.
* Update: Plugin Core.

== 1.0.4 == Released on Sep 09, 2015

* Fix: Automatic email for variable product.
* Fix: Redirect to correct page after sending mail.

== 1.0.3 == Released on Aug 21, 2015

* New: Compatibility with Wordpress 4.3.
* Fix: Exclusion List and Waiting List Tables error.
* Fix: WPML config xml.

== 1.0.2 == Released on Aug 13, 2015

* New: Compatibility with WooCommerce 2.4.
* Update: Plugin Core.
* Update: Language file.

== 1.0.1 == Released on Apr 21, 2015

* Initial release