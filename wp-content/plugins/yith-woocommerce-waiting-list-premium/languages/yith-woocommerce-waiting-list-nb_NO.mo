��    .      �  =   �      �     �     �             C   *  $   n     �     �  $   �     �     �     �               #     3     6     U     t     |     �     �     �  !   �     �  
   �  
   �          $  '   1  m   Y  	   �     �  	   �  
   �                     1     Q  F   U  (   �  O   �            �        �
  
   �
     �
     �
  L   �
  '   9     a     r     �     �     �     �     �     �     �     �  
   �  
                  0     6     B  "   R     u     �     �     �     �  5   �  d   �     ]     c     x  	   �  
   �  
   �     �     �     �  2   �  3     '   <     d     k               
                                  +             $                     !       -             "             ,      	       (             .            '          %      *   #          )   &                Action Actions Add new user Add user to the Waiting list An error occurred sending the email to the users. Please try again. An error occurred, please try again. Delete Waiting list Delete waiting list Do you really want to send the mail? Edit product Email Address Email sent correctly. Is Customer Leave My Waiting List No Plugin Menu TitleWaiting List Plugin page titleWaiting List Product Products with a Waiting List Remove Remove User Remove from list Return to waiting lists checklist Search for an user Send Email Send email Send the email to the users Stock Status There are no users in this waiting list There is %s user in the waiting list for this product There are %s users in the waiting list for this product Thumbnail Users in the Waiting list Variation View Users Waiting List Waiting list Waiting list Checklist Waiting list for variation: #%s Yes You have successfully sent the email to the users of the waiting list! You haven't subscribed any waiting list. You must provide a valid email address to join the waiting list of this product email here Project-Id-Version: YITH WooCommerce Waiting List
POT-Creation-Date: 2018-05-16 12:45+0200
PO-Revision-Date: 2018-05-16 12:46+0200
Last-Translator: 
Language-Team: Yithemes <plugins@yithemes.com>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 Handling Handlinger Legg til ny kunde Legg kunde til ventelisten Det oppsto en feil i sending e-posten til kundene. Vennligst prøv på nytt. En feil oppstod. Vennligst prøv igjen. Slett venteliste Slett venteliste Vil du virkelig sende e-post? Rediger produkt E-postadresse E-post sent. Er registrert kunde Fjern Min venteliste Nei Venteliste Venteliste Produkt Produkter med venteliste Fjern Fjern kunde Fjern fra liste Tilbake til venteliste sjekklisten Søk etter kunde Send e-post Send e-post Send e-post til kundene Lagerstatus Det er ingen kunder i ventelisten på dette produktet Det er %s kunde i ventelisten på dette produktet Det er %s kunder i ventelisten på dette produktet Bilde Kunder i ventelisten Variant Se kunder Venteliste Venteliste Venteliste sjekkliste Venteliste for variant: #%s Ja Du har sendt e-posten til kundene på ventelisten! Du er ikke lagt til på noen produkters venteliste. Vennligst oppgi en gyldig e-postadresse e-post her 