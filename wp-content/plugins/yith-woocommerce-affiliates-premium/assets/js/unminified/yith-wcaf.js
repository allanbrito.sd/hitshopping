jQuery(document).ready( function( $ ){

    /*
     * Get enhanced select labels
     *
     * @use yith_wcaf
     * @return mixed
     */
    function getEnhancedSelectFormatString() {
        return {
            formatMatches: function( matches ) {
                if ( 1 === matches ) {
                    return yith_wcaf.labels.select2_i18n_matches_1;
                }

                return yith_wcaf.labels.select2_i18n_matches_n.replace( '%qty%', matches );
            },
            formatNoMatches: function() {
                return yith_wcaf.labels.select2_i18n_no_matches;
            },
            formatAjaxError: function( jqXHR, textStatus, errorThrown ) {
                return yith_wcaf.labels.select2_i18n_ajax_error;
            },
            formatInputTooShort: function( input, min ) {
                var number = min - input.length;

                if ( 1 === number ) {
                    return yith_wcaf.labels.select2_i18n_input_too_short_1;
                }

                return yith_wcaf.labels.select2_i18n_input_too_short_n.replace( '%qty%', number );
            },
            formatInputTooLong: function( input, max ) {
                var number = input.length - max;

                if ( 1 === number ) {
                    return yith_wcaf.labels.select2_i18n_input_too_long_1;
                }

                return yith_wcaf.labels.select2_i18n_input_too_long_n.replace( '%qty%', number );
            },
            formatSelectionTooBig: function( limit ) {
                if ( 1 === limit ) {
                    return yith_wcaf.labels.select2_i18n_selection_too_long_1;
                }

                return yith_wcaf.labels.select2_i18n_selection_too_long_n.replace( '%qty%', limit );
            },
            formatLoadMore: function( pageNumber ) {
                return yith_wcaf.labels.select2_i18n_load_more;
            },
            formatSearching: function() {
                return yith_wcaf.labels.select2_i18n_searching;
            }
        };
    }

    /*
     * Extends jQuery object to implement commissions dashboard view js functions
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_dashboard_commissions = function(){
        var t = $(this);

        // Ajax product search box
        t.find( ':input.wc-product-search' ).filter( ':not(.enhanced)' ).each( function() {
            var select2_args = {
                allowClear:  !! $( this ).data( 'allow_clear' ),
                placeholder: $( this ).data( 'placeholder' ),
                minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '3',
                escapeMarkup: function( m ) {
                    return m;
                },
                ajax: {
                    url:         yith_wcaf.ajax_url,
                    dataType:    'json',
                    quietMillis: 250,
                    data: function( term, page ) {
                        return {
                            term:     term,
                            action:   'yith_wcaf_json_search_products_and_variations',
                            security: yith_wcaf.search_products_nonce
                        };
                    },
                    results: function( data, page ) {
                        var terms = [];
                        if ( data ) {
                            $.each( data, function( id, text ) {
                                terms.push( { id: id, text: text } );
                            });
                        }
                        return { results: terms };
                    },
                    cache: true
                }
            };

            select2_args.multiple = false;
            select2_args.initSelection = function( element, callback ) {
                var data = {id: element.val(), text: element.attr( 'data-selected' )};
                return callback( data );
            };

            select2_args = $.extend( select2_args, getEnhancedSelectFormatString() );

            $( this ).select2( select2_args ).addClass( 'enhanced' );
        });

        // datepicker
        t.find( '.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            numberOfMonths: 1,
            showButtonPanel: true,
            beforeShow: function(input, inst) {
                $('#ui-datepicker-div')
                    .removeClass(function() {
                        return $('input').get(0).id;
                    })
                    .addClass( 'yith-wcaf-datepicker' );
            }
        });
    };

    /*
     * Extends jQuery object to implement clicks dashboard view js functions
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_dashboard_clicks = function(){
        var t = $(this);

        // datepicker
        t.find( '.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            numberOfMonths: 1,
            showButtonPanel: true,
            beforeShow: function(input, inst) {
                $('#ui-datepicker-div')
                    .removeClass(function() {
                        return $('input').get(0).id;
                    })
                    .addClass( 'yith-wcaf-datepicker' );
            }
        });
    };

    /*
     * Extends jQuery object to implement payments dashboard view js functions
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_dashboard_payments = function(){
        var t = $(this);

        // datepicker
        t.find( '.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            numberOfMonths: 1,
            showButtonPanel: true,
            beforeShow: function(input, inst) {
                $('#ui-datepicker-div')
                    .removeClass(function() {
                        return $('input').get(0).id;
                    })
                    .addClass( 'yith-wcaf-datepicker' );
            }
        });
    };

    /*
     * Extends jQuery object to implement withdraw dashboard view js functions
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_dashboard_withdraw = function(){
        var t = $(this),
            from = $( '#withdraw_from' ),
            to = $( '#withdraw_to' );

        from.add( to ).on( 'change', function(){

            var from_val = from.val(),
                to_val = to.val(),
                data = {
                    action: 'get_withdraw_amount',
                    withdraw_from: from_val,
                    withdraw_to: to_val,
                    security: yith_wcaf.get_withdraw_amount
                };

            if( ! from_val || ! to_val ){
                return;
            }

            $.ajax( {
                type:		'POST',
                url:		yith_wcaf.ajax_url,
                data:		data,
                beforeSend: function(){
                    t.find( '.information-panel' ).block({
                        message: null,
                        overlayCSS: {
                            background: '#fff',
                            opacity: 0.6
                        }
                    });
                },
                complete: function(){
                    t.find( '.information-panel' ).unblock();
                },
                success:	function( amount ) {
                    t.find('.withdraw-current-total').html( amount );
                },
                dataType: 'html'
            } );
        } );

        // datepicker
        t.find( '.datepicker')
            .datepicker({
                dateFormat: "yy-mm-dd",
                numberOfMonths: 1,
                showButtonPanel: true,
                beforeShow: function(input, inst) {
                    $('#ui-datepicker-div')
                        .removeClass(function() {
                            return $('input').get(0).id;
                        })
                        .addClass( 'yith-wcaf-datepicker' );
                }
            });
    };

    /*
     * Extends jQuery object to implement generate link view js functions
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_generate_link = function(){
        var t = $(this);

        t.find('.copy-trigger').on( 'click', function ( event ) {
            var obj_to_copy = $(this).parents('p').find( '.copy-target' );

            if ( obj_to_copy.length > 0 ) {

                if( obj_to_copy.is('input') ) {
                    obj_to_copy.select();
                    document.execCommand("copy");
                }
                else{
                    var hidden = $('<input/>', {
                        val : obj_to_copy.text(),
                        type: 'text'
                    });

                    $('body').append( hidden );
                    hidden.select();
                    document.execCommand("copy");

                    hidden.remove();

                }

                if ( ! $(document).triggerHandler('yith_wcaf_hide_link_copied_alert') ) {
                    alert(yith_wcaf.labels.link_copied_message);
                }
            }
        } );
    };

    /*
     * Extends jQuery object to implement registration form js functions
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_registration_form = function(){
        var t = $(this),
            how_promote = t.find('#how_promote'),
            custom_promote = t.find('#custom_promote').closest('.form-row');

        if( how_promote.length ){
            how_promote.on( 'change', function(){
                if( 'others' === $(this).val() ){
                    custom_promote.show();
                }
                else{
                    custom_promote.hide();
                }
            } ).change();
        }
    };

    /*
     * Extends jQuery object to implement set referrer form behaviour
     *
     * @use yith_wcaf
     */
    $.fn.yith_wcaf_set_referrer = function(){
        var t = $(this);

        t.on( 'click', 'a.show-referrer-form', function(ev){
            ev.preventDefault();

            t.find('form').slideToggle();
        } );

        t.on( 'submit', 'form.referrer-form', function(ev){
            var form = $(this),
                data = {
                    action:		    'yith_wcaf_set_referrer',
                    referrer_token:	form.find( 'input[name="referrer_code"]' ).val(),
                    security:       yith_wcaf.set_referrer_nonce
                };

            ev.preventDefault();

            form.addClass( 'processing' ).block({
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            });

            $.ajax({
                type:		'POST',
                url:		yith_wcaf.ajax_url,
                data:		data,
                success:	function( code ) {
                    $( '.woocommerce-error, .woocommerce-message' ).remove();
                    form.removeClass( 'processing' ).unblock();

                    if ( code ) {
                        form
                            .before( code )
                            .find( 'input[name="referrer_code"]' ).prop( 'disabled' );
                        form.slideUp();
                    }
                },
                dataType: 'html'
            });

            return false;
        } );
    };

    $( '.yith-wcaf-commissions').yith_wcaf_dashboard_commissions();
    $( '.yith-wcaf-clicks').yith_wcaf_dashboard_clicks();
    $( '.yith-wcaf-payments').yith_wcaf_dashboard_payments();
    $( '.yith-wcaf-withdraw').yith_wcaf_dashboard_withdraw();
    $( '.yith-wcaf-set-referrer').yith_wcaf_set_referrer();
    $( '.yith-wcaf-link-generator').yith_wcaf_generate_link();
    $( '.yith-wcaf-registration-form' ).add( '.woocommerce-form-register' ).add( '.yith-wcaf-settings' ).yith_wcaf_registration_form();
} );