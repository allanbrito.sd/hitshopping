<?php
/**
 * Affiliate Details Admin Panel
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Affiliates
 * @version 1.0.0
 */

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_WCAF' ) ) {
	exit;
} // Exit if accessed directly
?>

<form method="post" id="post" autocomplete="off">
	<input type="hidden" name="affiliate_id" id="affiliate_id" value="<?php echo $affiliate[ 'ID' ] ?>" />
	<input type="hidden" name="affiliates[]" value="<?php echo $affiliate[ 'ID' ] ?>" />

	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="postbox-container-1" class="postbox-container">
				<div id="side-sortables" class="meta-box-sortables ui-sortable">

					<!-- Affiliates Actions -->
					<div id="yith_wcaf_affiliate_actions" class="postbox">
						<h3 class="hndle ui-sortable-handle">
							<span><?php _e( 'Affiliate actions', 'yith-woocommerce-affiliates' ) ?></span></h3>

						<div class="inside">
							<select name="action">
								<option value=""><?php _e( 'Action', 'yith-woocommerce-affiliates' ) ?></option>
								<?php if ( ! empty( $available_affiliate_actions ) ): ?>
									<?php foreach ( $available_affiliate_actions as $id => $label ): ?>
										<option value="<?php echo esc_attr( $id ) ?>"><?php echo esc_html( $label ) ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>

							<button class="button" title="<?php _e( 'Apply', 'yith-woocommerce-affiliates' ) ?>">
								<span><?php _e( 'Apply', 'yith-woocommerce-affiliates' ) ?></span></button>

                            <hr>

                            <button class="button save_affiliate button-primary" name="save" value="<?php _e( 'Update', 'yith-woocommerce-affiliates' )?>"><?php _e( 'Update', 'yith-woocommerce-affiliates' ) ?></button>
						</div>
					</div>

				</div>
			</div>

			<div id="postbox-container-2" class="postbox-container">
				<div id="normal-sortables" class="meta-box-sortables ui-sortable">
					<div id="woocommerce-order-data" class="postbox">
						<div class="inside">

							<div class="panel-wrap woocommerce">
								<div id="order_data" class="yith-affiliate panel">
									<h2>
										<?php printf( __( '<b>%s</b> Details', 'yith-woocommerce-affiliates' ), $username ) ?>
										<a href="<?php echo esc_url( remove_query_arg( 'affiliate_id' ) ) ?>" class="add-new-h2" id="back_to_affiliate"><?php _e( 'Back to affiliates table', 'yith-woocommerce-affiliates' ) ?></a>
									</h2>

                                    <div class="order_data_column_container">
                                        <p>
                                            <strong><?php _e( 'Referral link:' ) ?></strong>
                                            <?php echo YITH_WCAF()->get_referral_url( $affiliate['token'] )?>
                                        </p>
                                    </div>

                                    <div class="order_data_column_container affiliate-stats">
                                        <div class="order_data_column">
                                            <div class="address">
                                                <p>
                                                    <strong><?php _e( 'Earnings:', 'yith-woocommerce-affiliates' ) ?></strong>
                                                    <?php echo wc_price( $affiliate['earnings'] ) ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="order_data_column">
                                            <div class="address">
                                                <p>
                                                    <strong><?php _e( 'Refunds:', 'yith-woocommerce-affiliates' ) ?></strong>
                                                    <?php echo wc_price( -1 * $affiliate['refunds'] ) ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="order_data_column">
                                            <div class="address">
                                                <p>
                                                    <strong><?php _e( 'Paid:', 'yith-woocommerce-affiliates' ) ?></strong>
                                                    <?php echo wc_price( $affiliate['paid'] ) ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="order_data_column">
                                            <div class="address">
                                                <p>
                                                    <strong><?php _e( 'Active balance:', 'yith-woocommerce-affiliates' ) ?></strong>
                                                    <?php echo wc_price( $affiliate['earnings'] - $affiliate['refunds'] - $affiliate['paid'] ) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clear"></div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

            <div id="postbox-container-4" class="postbox-container">
                <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                    <div id="woocommerce-order-data" class="postbox">
                        <div class="inside">

                            <div class="panel-wrap woocommerce">
                                <div id="affiliate_commissions" class="yith-affiliate-commissions panel">

									<?php
									$commissions_url = add_query_arg( array(
										'page' => 'yith_wcaf_panel',
										'tab' => 'commissions',
										'_user_id' => $affiliate['user_id']
									), admin_url( 'admin.php' ) );
									?>

                                    <h3>
										<?php _e( 'Last affiliate commissions', 'yith-wcaf-affiliates' ) ?>
                                        <a href="<?php echo esc_url( $commissions_url ) ?>" class="add-new-h2" id="back_to_commission"><?php _e( 'See all commissions', 'yith-woocommerce-affiliates' ) ?></a>
                                    </h3>
									<?php if( ! empty( $commissions ) ): ?>
                                        <div id="woocommerce-order-items">
                                            <div class="woocommerce_order_items_wrapper">
                                                <table cellpadding="0" cellspacing="0" class="woocommerce_order_items">
                                                    <thead>
                                                    <tr>
                                                        <th><?php _e( 'ID', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Date', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Status', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Product', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Rate', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Amount', 'yith-woocommerce-affiliates' )?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
													<?php foreach( $commissions as $commission ): ?>

														<?php $commission_url = add_query_arg( 'commission_id', $commission['ID'], $commissions_url ) ?>

                                                        <tr>
                                                            <td><?php printf( '<a href="%s">#%s</a>', $commission_url, $commission['ID'] ) ?></td>
                                                            <td><?php echo date_i18n( wc_date_format(), strtotime( $commission['created_at'] ) ) ?></td>
                                                            <td><?php echo YITH_WCAF_Commission_Handler()->get_readable_status( $commission['status'] ) ?></td>
                                                            <td><?php echo $commission['product_name'] ?></td>
                                                            <td><?php echo number_format( $commission['rate'], 2 ) ?> %</td>
                                                            <td><?php echo wc_price( $commission['amount'] ) ?></td>
                                                        </tr>
													<?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <p class="no-items-found"><?php _e( 'No commission registered yet', 'yith-woocommerce-affiliates' )?></p>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="postbox-container-5" class="postbox-container">
                <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                    <div id="woocommerce-order-data" class="postbox">
                        <div class="inside">

                            <div class="panel-wrap woocommerce">
                                <div id="affiliate_payments" class="yith-affiliate-payments panel">
	                                <?php
	                                $payments_url = add_query_arg( array(
		                                'page' => 'yith_wcaf_panel',
		                                'tab' => 'payments',
		                                '_affiliate_id' => $affiliate['ID']
	                                ), admin_url( 'admin.php' ) );
	                                ?>

                                    <h3>
		                                <?php _e( 'Last affiliate payments', 'yith-wcaf-affiliates' ) ?>
                                        <a href="<?php echo esc_url( $payments_url ) ?>" class="add-new-h2" id="back_to_payment"><?php _e( 'See all payments', 'yith-woocommerce-affiliates' ) ?></a>
                                    </h3>
	                                <?php if( ! empty( $payments ) ): ?>
                                        <div id="woocommerce-order-items">
                                            <div class="woocommerce_order_items_wrapper">
                                                <table cellpadding="0" cellspacing="0" class="woocommerce_order_items">
                                                    <thead>
                                                    <tr>
                                                        <th><?php _e( 'ID', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Status', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Amount', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Created at', 'yith-woocommerce-affiliates' )?></th>
                                                        <th><?php _e( 'Completed at', 'yith-woocommerce-affiliates' )?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
					                                <?php foreach( $payments as $payment ): ?>

						                                <?php $payment_url = add_query_arg( 'payment_id', $payment['ID'], $payments_url ) ?>

                                                        <tr>
                                                            <td><?php printf( '<a href="%s">#%s</a>', $payment_url, $payment['ID'] ) ?></td>
                                                            <td><?php echo YITH_WCAF_Payment_Handler()->get_readable_status( $payment['status'] ) ?></td>
                                                            <td><?php echo wc_price( $payment['amount'] ) ?></td>
                                                            <td><?php echo date_i18n( wc_date_format(), strtotime( $payment['created_at'] ) ) ?></td>
                                                            <td><?php echo date_i18n( wc_date_format(), strtotime( $payment['completed_at'] ) ) ?></td>
                                                        </tr>
					                                <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
	                                <?php else: ?>
                                        <p class="no-items-found"><?php _e( 'No payment registered yet', 'yith-woocommerce-affiliates' )?></p>
	                                <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="postbox-container-3" class="postbox-container">
                <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                    <div id="woocommerce-order-data" class="postbox">
                        <div class="inside">

                            <div class="panel-wrap woocommerce">
                                <div id="affiliate_details" class="yith-affiliate-detail panel">
                                    <?php YITH_WCAF_Affiliate_Handler()->render_affiliate_extra_fields( $user ) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
</form>