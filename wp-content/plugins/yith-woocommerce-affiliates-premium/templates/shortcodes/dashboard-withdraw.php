<?php
/**
 * Affiliate Dashboard Withdraw
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Affiliates
 * @version 1.0.5
 */

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_WCAF' ) ) {
	exit;
} // Exit if accessed directly
?>

<div class="yith-wcaf yith-wcaf-withdraw woocommerce">

	<?php
	if( function_exists( 'wc_print_notices' ) ){
		wc_print_notices();
	}
	?>

	<?php do_action( 'yith_wcaf_before_dashboard_section', 'withdraw' ) ?>

    <div class="left-column <?php echo ( ! $show_right_column ) ? 'full-width' : '' ?>">
        <?php if( ! $can_withdraw ): ?>
            <?php echo apply_filters( 'yith_wcaf_affiliate_cannot_withdraw_message', sprintf( __( 'You already have an active payment request; please check payment status in <a href="%s">Payments\' page</a>' ), $payments_endpoint ) ) ?>
        <?php else: ?>
            <form method="POST" enctype="multipart/form-data">
                <div class="first-step">
                    <label for="withdraw_from"><?php _e( 'From:', 'yith-woocommerce-affiliates' ) ?></label>
                    <input type="text" id="withdraw_from" name="withdraw_from" class="datepicker" value="<?php echo esc_attr( $withdraw_from )?>" />

                    <label for="withdraw_to"><?php _e( 'To:', 'yith-woocommerce-affiliates' ) ?></label>
                    <input type="text" id="withdraw_to" name="withdraw_to" class="datepicker" value="<?php echo esc_attr( $withdraw_to )?>" />

                    <div class="information-panel">

                        <p class="total">
                            <b><?php _e( 'Withdraw Total:', 'yith-woocommerce-affiliates' ) ?></b>
                            <span class="withdraw-current-total"><?php echo wc_price( $current_amount ); ?></span>
                        </p>

                        <?php if( $min_withdraw ): ?>
                            <p class="min-withdraw">
                                <b><?php _e( 'Minimum amount to withdraw:', 'yith-woocommerce-affiliates' ) ?></b>
                                <?php echo wc_price( $min_withdraw ) ?>
                            </p>
                        <?php endif; ?>

                        <p class="max-withdraw">
                            <b><?php _e( 'Current balance:', 'yith-woocommerce-affiliates' ) ?></b>
                            <?php echo wc_price( $max_withdraw ) ?>
                        </p>

                    </div>
                </div>

                <div class="second-step">
                    <label for="payment_email"><?php _e( 'Payment email', 'yith-woocommerce-affiliates' ) ?></label>
                    <input type="email" id="payment_email" name="payment_email" value="<?php echo esc_attr( $payment_email ) ?>" />
                </div>

                <?php if( $require_invoice ): ?>
                    <div class="third-step woocommerce-billing-fields">
                        <?php if( 'upload' == $invoice_mode || 'both' == $invoice_mode ): ?>
                            <h4><?php _e( 'Upload your invoice', 'yith-woocommerce-affiliates' ) ?></h4>
                            <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo 1048576 * apply_filters( 'yith_wcaf_max_invoice_size', 3 ) ?>" />
                            <input type="file" id="invoice_file" name="invoice_file" accept="<?php echo apply_filters( 'yith_wcaf_invoice_upload_mime', 'application/pdf' )?>" />

                            <?php if( $invoice_example ): ?>
                                <p class="description">
                                    <?php apply_filters( 'yith_wcaf_example_invoice_text', printf( __( 'Please, refer to the following <a href="%s">example</a> for invoice creation', 'yith-woocommerce-affiliates' ), $invoice_example ), $invoice_example ) ?>
                                </p>
                            <?php endif; ?>
                        <?php endif;?>

                        <?php if( 'both' == $invoice_mode && ! empty( $invoice_fields ) ): ?>
                            <span class="invoice-mode-separator"><?php _e( 'or', 'yith-woocommerce-affiliates' ) ?></span>
                        <?php endif; ?>

                        <?php if( ( 'generate' == $invoice_mode || 'both' == $invoice_mode ) && ! empty( $invoice_fields ) ): ?>
                            <h4><?php _e( 'Generate a new one', 'yith-woocommerce-affiliates' ) ?></h4>
                            <?php
                                foreach( $invoice_fields as $field ):
                                ?>
                                    <label for="invoice-<?php echo $field ?>"><?php ?></label>

                                    <?php
                                    $field_data = YITH_WCAF_Shortcode_Premium::get_field( $field );
                                    woocommerce_form_field( $field, $field_data );
                                    ?>

                                <?php
                               endforeach;

                               wp_enqueue_script( 'wc-country-select' );
                            ?>

                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php wp_nonce_field( 'yith_wcaf_withdraw', '_withdraw_nonce' ) ?>

                <input class="button submit" type="submit" value="<?php echo esc_attr( apply_filters( 'yith_wcaf_withdraw_submit_button', __( 'Request Withdraw', 'yith-woocommerce-affiliates' ) ) ) ?>" />

            </form>
        <?php endif; ?>
    </div>

    <!--NAVIGATION MENU-->
	<?php
	$atts = array(
		'show_right_column' => $show_right_column,
		'show_left_column' => true,
		'show_dashboard_links' => $show_dashboard_links,
		'dashboard_links' => $dashboard_links
	);
	yith_wcaf_get_template( 'navigation-menu.php', $atts, 'shortcodes' )
	?>

	<?php do_action( 'yith_wcaf_after_dashboard_section', 'withdraw' ) ?>

</div>