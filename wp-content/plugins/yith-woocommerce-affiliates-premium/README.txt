=== YITH WooCommerce Affiliates ===

Contributors: yithemes
Tags:  affiliate, affiliate marketing, affiliate plugin, affiliate tool, affiliates, woocommerce affiliates, woocommerce referral, lead, link, marketing, money, partner, referral, referral links, referrer, sales, woocommerce, wp e-commerce, affiliate campaign, affiliate marketing, affiliate plugin, affiliate program, affiliate software, affiliate tool, track affiliates, tracking, affiliates manager, yit, yith, yithemes, yit affiliates, yith affiliates, yithemes affiliates
Requires at least: 4.0.0
Tested up to: 4.9.8
Stable tag: 1.4.0
License: GPLv2 or later
Documentation: https://hitoutlets.com/docs-plugins/yith-woocommerce-affiliates

== Changelog ==

= 1.4.0 = Released: Oct, 03 - 2018 =

* New: support to WooCommerce 3.5-RC1
* New: support to WordPress 4.9.8
* New: updated plugin framework
* New: added new Reject status for affiliates
* New: affiliates receives an email on account status change
* New: added commissions Trash
* New: affiliates can now request commissions withdraws
* New: affiliates can now upload invoices for their withdraw requests
* New: affiliates can now generate invoices for their withdraw requests
* New: added affiliate details page
* Fix: affiliate backend creation
* Fix: fixed some queries on various admin views
* Tweak: improved balance calculation
* Dev: added filter get_referral_url filter

= 1.3.1 - Released: Jul, 19 - 2018 =

* New: support to YITH PayPal Payouts for WooCommerce
* New: added new fields during affiliate registration
* New: admin can now exclude products/users from affiliate program
* Tweak: improved filters, counters, views and redirection on affiliates admin panel
* Tweak: manual payment are now registered by default as on-hold
* Fixed: warning occurring when WooCommerce does not send all params to woocommerce_email_order_meta action
* Dev: added filter yith_wcaf_dashboard_affiliate_message

= 1.3.0 - Released: May, 28 - 2018 =

* New: WooCommerce 3.4 compatibility
* New: WordPress 4.9.6 compatibility
* New: updated plugin-fw
* New: GDPR compliance
* New: admin can now ban Affiliates
* Update: Italian Language
* Update: Spanish language
* Tweak: improved pagination of dashboard sections
* Fix: preventing notice when filtering by date payments

= 1.2.4 - Released: Apr, 05 - 2018 =

* New: added "process orphan commissions" procedure
* New: added shortcodes for Affiliate Dashboard sections ( [yith_wcaf_show_clicks], [yith_wcaf_show_commissions], [yith_wcaf_show_payments], [yith_wcaf_show_settings] )
* New: added handling for subscription renews (YITH WooCommerce Subscription 1.3.2 required)
* Dev: added yith_wcaf_requester_link filter to let third party code change requester link

= 1.2.3 - Released: Mar, 02 - 2018 =

* New: "yith_wcaf_show_if_affiliate" shortcode
* Tweak: remove user_trailingslashit from get_referral_url to improve compatibility
* Tweak: improved user capability handling, now all admin operations require at least manage_woocommerce capability (edited)
* Dev: new filter "yith_wcaf_panel_capability" to let third party code change minimum required capability for admin operations
* Dev: added "order_id" param for "yith_wcaf_affiliate_rate" filter
* Update: italian translation

= 1.2.2 - Released: Feb, 01 - 2018 =

* New: added WooCommerce 3.3.x support
* New: added WordPress 4.9.2 support
* New: added Dutch translation
* New: pay commissions every day
* New: pay only commissions older than a certain number of days
* Tweak: added SAMEORIGIN header to Affiliate Dashboard page
* Tweak: fixed error with wrong Affiliate ID when adding new affiliate to database
* Fix: preventing fatal error on commission details view when order meta are retrieved as objects (WC 3.0+)
* Dev: added yith_wcaf_commissions_csv_heading and yith_wcaf_commissions_csv_row filters to let third party developers change output of csv export operation

= 1.2.1 - Released: Nov, 14 - 2017 =

* Fix: added check over user before adding role

= 1.2.0 - Released: Nov, 10 - 2017 =

* New: WooCommerce 3.2.x support
* New: new affiliate role
* New: added login form in "Registration form" template
* New: added copy button for generated referral url
* New: added export csv procedure for commissions
* Tweak: added "Commissions table" to new order admin email
* Fix: removed profile panel when customer have permissions lower then shop manager
* Fix: problem with manual order affiliate assignment, when there are no previous commissions to delete
* Dev: added yith_wcaf_settings_form_start action
* Dev: added yith_wcaf_settings_form action
* Dev: added yith_wcaf_save_affiliate_settings action
* Dev: added yith_wcaf_show_dashboard_links filter to let dev show navigation menu on all affiliates dashboard pages

= 1.1.0 - Released: Apr, 03 - 2017 =

* New: WordPress 4.7.3 compatibility
* New: WooCommerce 3.0-RC2 compatibility
* New: field to user profile, to let admin set current permanent affiliate token for the user
* New: option to let admin choose that referral cookie won't change once set, till its expiration
* New: capability for the admin to set an affiliate for an unassigned order
* New: capability for the admin to remove an affiliate and relative commissions from an order
* New: Delete bulk action for payments
* New: option to force commissions deletion
* New: added Hungarian - HUNGARY translation (thanks to Szabolcs)
* Tweak: text domain to yith-woocommerce-affiliates. IMPORTANT: this will delete all previous translations
* Tweak: send paid email at yith_wcaf_commission_status_paid
* Tweak: complete revision for paid commissions emails triggers
* Tweak: delete notes while deleting commission
* Fix: email replacements
* Fix: delete method for payments
* Fix: commission paid email trigger
* Fix: commission delete process
* Fix: commission notes delete process
* Dev: added yith_wcaf_notify_user_pending_commission filter to let third party plugin prevent or enable pending commission notification
* Dev: added yith_wcaf_notify_user_paid_commissions filter to let third party plugin prevent or enable paid commission notification
* Dev: added yith_wcaf_affiliate_rate filter to let third party plugin customize affiliate commission rate
* Dev: added yith_wcaf_use_percentage_rates filter to let switch from percentage rate to fixed amount (use it at your own risk, as no control over item total is performed)
* Dev: added yith_wcaf_become_an_affiliate_redirection filter to let third party plugin customize redirection after "Become an Affiliate" butotn is clicked
* Dev: added yith_wcaf_become_affiliate_button_text filter to let third party plugin change Become Affiliate button label
* Dev: added yith_wcaf_persistent_rate filter to let third party plugin enable/disable persistent rate
* Dev: added yith_wcaf_payment_email_required filter to let third party plugin to remove payment email from affiliate registration form
* Dev: added yith_wcaf_create_order_commissions filter, to let dev skip commission handling
* Dev: added filters yith_wcaf_before_dashboard_section and yith_wcaf_after_dashboard_section
* Dev: added hooks after payment status change
* Dev: added yith_wcaf_get_current_affiliate_token function to get current affiliate token
* Dev: added yith_wcaf_get_current_affiliate function to get current affiliate object
* Dev: added yith_wcaf_get_current_affiliate_user function to get current affiliate user object

= 1.0.8 - Released: Jun, 08 - 2016 =

* Added: support WC 2.6 RC1
* Added: italian translation
* Added: spanish translation
* Added: attributes to affiliate_dashboard shortcode (will be passed to single section shortcode callbacks)
* Added: current_page attribute to all shortcodes that implements pagination
* Added: per page input in affiliate dashboard
* Added: style to #yith_wcaf_order_referral_commissions, #yith_wcaf_payment_affiliate, #yith_wcaf_commission_payments
* Tweak: added controls to show variation everywhere a variable product may be print
* Tweak: let rate set for Variable Product to apply to all variations
* Tweak: added filter yith_wcaf_is_hosted to filter check over submitted host / server name match in link_generator callback
* Fixed: Order links class/query vars

= 1.0.7 - Released: May, 05 - 2016 =

* Added: WordPress 4.5.x support
* Added: option to avoid referral cookie to be deleted after first customer checkout
* Added: new stat in Stas panel (sum of all affiliation earnings since program start)
* Fixed: removed useless library invocation
* Fixed: generate link shortcode (removed protocol before check for local url)

= 1.0.6 - Released: Apr, 05 - 2016 =

* Added: check over product existence on product table rates print method
* Added: capability for the admin to set commissions completed, without using any gateway
* Added: WooCommerce 2.5.x compatibility
* Added: WordPress 4.4.x compatibility
* Added: Users can now enter affiliate code from checkout page
* Added: Permanent token can now be locked, so to not be changed when a new affiliation link is visited
* Tweak: Performance improved with new plugin core 2.0
* Fixed: order awaiting payment handling
* Fixed: problems with views, due to new YITH menu name
* Fixed: generate link shortcode (url parsing improvements)
* Fixed: affiliate search method
* Fixed: default WC emails templates not found

= 1.0.5 - Released: Oct, 16 - 2015 =

* Added: Option to prevent referral cookie to expire
* Added: Option to prevent referral history cookie to expire
* Tweak: Increased expire seconds limit
* Tweak: Changed disabled attribute in readonly attribute for link-generator template
* Fixed: Corrected email templates
* Fixed: Option for auto-enable affiliates not showing on settings page
* Fixed: Commissions/Payment status now translatable from .po files
* Fixed: Fatal error occurring sometimes when using YOAST on backend

= 1.0.4 - Released: Aug, 13 - 2015 =

* Added: Compatibility with WC 2.4.2
* Tweak: Added missing text domain on link-generator template (thanks to dabodude)
* Tweak: Updated internal plugin-fw

= 1.0.3 - Released: Aug, 05 - 2015 =

* Initial release