<?php
/**
 * New best price notification - email for admin PLAIN
 *
 * @author        Yithemes
 * @package       YITH Best Price Guaranteed for WooCommerce Premium
 * @version       1.0.0
 */

if ( !defined ( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

echo "= " . $email_heading . " =\n\n";
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo $best_price->get_display_name_by_email() . " has found theproduct '" . $best_price->get_product_title() . "' at a lower price. Read more about the information he/she has provided \r'";

echo "User name : " . $best_price->user_name . "\n\n";
echo "User email : " . $best_price->user_email . "\n\n";
echo "Best price : " . $best_price->suggested_price . "\n\n";
echo "Site Url : " . $best_price->site_url . "\n\n";
echo "Note : " . $best_price->note . "\n\n";

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );

?>