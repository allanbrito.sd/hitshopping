<?php
/**
 * Coupon email for customer - Plain text
 *
 * @author        Yithemes
 * @package       YITH Best Price Guaranteed for WooCommerce Premium
 * @version       1.0.0
 */

if ( !defined ( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

echo "= " . $email_heading . " =\n\n";
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo $custom_message. " =\n\n";

echo "Product : " . $best_price->get_product_title() . "\n\n";
echo "Base price : " . get_woocommerce_currency_symbol() . ' ' . yit_get_display_price($best_price->product) . "\n\n";
echo "Special price for you : " . get_woocommerce_currency_symbol() . ' ' .  $best_price->suggested_price . "\n\n";
echo "Copy and paste the following link in the browser : " . $link . "\n\n";

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );

?>