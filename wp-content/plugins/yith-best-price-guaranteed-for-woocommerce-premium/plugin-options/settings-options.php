<?php
// exit if accessed directly
!defined('YITH_WCBPG') && exit();
$tab = array(
    'settings'  =>  array(
        'general-options'   =>  array(
            'title'     =>  __( 'General Options', 'yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'title',
            'desc'      =>  '',
        ),
        'activate-plugin'   =>  array(
            'name'      =>  __('Activate plugin','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'checkbox',
            'desc'      =>  __( 'Enable the plugin','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  'yes',
            'id'        =>  'yith-wcbpg-activate-plugin'
        ),
        'show-button-in'   =>  array(
            'name'      =>  __('Show button','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'select',
            'desc'      =>  __( 'Select the position of the button in product page','yith-best-price-guaranteed-for-woocommerce' ),
            'options'   =>  array(
                'before_single_price'   =>  __( 'Before price','yith-best-price-guaranteed-for-woocommerce' ),
                'after_single_price'    =>  __( 'After price','yith-best-price-guaranteed-for-woocommerce' ),
                'before_add_to_cart'    =>  __( 'Before Add to Cart button','yith-best-price-guaranteed-for-woocommerce' ),
                'after_add_to_cart'     =>  __( 'After Add to Cart button','yith-best-price-guaranteed-for-woocommerce' ),
                'before_excerpt'        =>  __( 'Before product excerpt','yith-best-price-guaranteed-for-woocommerce' ),
                'after_exceprt'         =>  __( 'After product excerpt','yith-best-price-guaranteed-for-woocommerce' )
            ),
            'default'   =>  'after_add_to_cart',
            'id'        =>  'yith-wcbpg-show-button-in'
        ),
        'hide-product-info'     =>  array(
            'name'              =>  __('Hide product info','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Hide product info in the Best Price modal window','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'no',
            'id'                =>  'yith-wcbpg-hide-product-info'
        ),
        'show-site-url'     =>  array(
            'name'              =>  __('Show site url','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Show/hide site url field in best price popup','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'yes',
            'id'                =>  'yith-wcbpg-show-site-url'
        ),
        'is-site-url-required'       =>  array(
            'name'              =>  __('Required site url','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Set as required the site url field in best price popup','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'yes',
            'id'                =>  'yith-wcbpg-is-site-url-required',
        ),
        'show-note'     =>  array(
            'name'              =>  __('Show note','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Show/hide note field in best price popup','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'yes',
            'id'                =>  'yith-wcbpg-show-note'
        ),
        'is-note-required'       =>  array(
            'name'              =>  __('Required note','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Set as required the note field in best price popup','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'no',
            'id'                =>  'yith-wcbpg-is-note-required',
        ),
        'show-name'     =>  array(
            'name'              =>  __('Show name','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Show/hide name field in best price popup (only for unlogged users)','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'no',
            'id'                =>  'yith-wcbpg-show-name'
        ),
        'is-name-required'       =>  array(
            'name'              =>  __('Required name','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Set as required the name field in best price popup','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'no',
            'id'                =>  'yith-wcbpg-is-name-required',
        ),
        'show-in-loop'          =>  array(
            'name'              =>  __('Show in loop','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Show button in archive pages','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'no',
            'id'                =>  'yith-wcbpg-show-in-loop'
        ),
        'modal-width'           =>  array(
            'name'              =>  __('Modal With','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'number',
            'desc'              =>  __( 'Modal width (set 0 for auto)','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  '1000',
            'id'                =>  'yith-wcbpg-modal-width'
        ),
        'modal-height'          =>  array(
            'name'              =>  __('Modal Height','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'number',
            'desc'              =>  __( 'Modal height (set 0 for auto)','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  '0',
            'id'                =>  'yith-wcbpg-modal-height'
        ),
        'additional-field-title'=>  array(
            'name'              =>  __('Additional field title','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'textarea',
            'default'           =>  '',
            'id'                =>  'yith-wcbpg-additional-field-title'
        ),
        'additional-field-message' =>  array(
            'name'              =>  __('Additional field message','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'textarea',
            'default'           =>  '',
            'id'                =>  'yith-wcbpg-additional-field-message'
        ),
        'show-gdpr-checkbox'          =>  array(
            'name'              =>  __('Show checkbox for GDPR Compliance','yith-best-price-guaranteed-for-woocommerce'),
            'type'              =>  'checkbox',
            'desc'              =>  __( 'Show checkbox for GDPR Compliance in best price popup','yith-best-price-guaranteed-for-woocommerce' ),
            'default'           =>  'no',
            'id'                =>  'yith-wcbpg-show-gdpr-checkbox'
        ),
        'general-options-end'   => array(
            'type'              => 'sectionend',
        )
    )
);

return $tab;