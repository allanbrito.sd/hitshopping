<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly


return array(
    'colors' => array(

        'colors'                       => array(
            'name' => __( 'Colors', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type' => 'title',
            'id'   => 'yith-wcbpg-colors'
        ),

        'popup_background_color'          => array(
            'name'    => __( 'Popup background color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_background_color',
            'default' => '#ffffff'
        ),
        'popup_title_color'          => array(
            'name'    => __( 'Popup title color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_title_color',
            'default' => '#1a1a1a'
        ),
        'popup_title_background_color'          => array(
            'name'    => __( 'Popup title background color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_title_background_color',
            'default' => '#f9f9f9'
        ),
        'popup_title_border_color'          => array(
            'name'    => __( 'Popup title border color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_title_border_color',
            'default' => '#cccccc'
        ),
        'popup_product_info_border_color'          => array(
            'name'    => __( 'Popup product info border color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_product_info_border_color',
            'default' => '#cccccc'
        ),
        'popup_product_info_product_name_color'          => array(
            'name'    => __( 'Popup product name color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_product_name_color',
            'default' => '#1a1a1a'
        ),
        'popup_form_background_color'          => array(
            'name'    => __( 'Popup form background color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_form_background_color',
            'default' => '#f9f9f9'
        ),
        'popup_form_border_color'          => array(
            'name'    => __( 'Popup form border color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_popup_form_border_color',
            'default' => '#cccccc'
        ),        
        'input_border_color'          => array(
            'name'    => __( 'Popup input border color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_input_border_color',
            'default' => '#d1d1d1'
        ),
        'input_background_color'          => array(
            'name'    => __( 'Popup input background color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_input_background_color',
            'default' => '#f7f7f7'
        ),
        'label_color'          => array(
            'name'    => __( 'Popup input label', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_label_color',
            'default' => '#1a1a1a'
        ),
        'button_color'          => array(
            'name'    => __( 'Popup button color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_button_color',
            'default' => '#ffffff'
        ),
        'button_background_color'          => array(
            'name'    => __( 'Popup button background color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_button_background_color',
            'default' => '#1a1a1a'
        ),
        'close_button_color'          => array(
            'name'    => __( 'Popup close button color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_close_button_color',
            'default' => '#ffffff'
        ),
        'close_button_background_color'          => array(
            'name'    => __( 'Popup close button background color', 'yith-best-price-guaranteed-for-woocommerce' ),
            'type'    => 'color',
            'desc'    => '',
            'id'      => 'yith_wcbpg_close_button_background_color',
            'default' => '#000000'
        ),
        'colors_end'                   => array(
            'type' => 'sectionend',
            'id'   => 'yith_wcbpg_colors_end'
        )

    )
);