<?php
// exit if accessed directly
!defined('YITH_WCBPG') && exit();
$tab = array(
    'labels'  =>  array(
        'labels'   =>  array(
            'title'     =>  __( 'Labels & Messages', 'yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'title',
            'desc'      =>  '',
        ),
        'product-label'   =>  array(
            'name'      =>  __('Text to open the modal window','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'This text is shown in product detail page.','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Found this product at a lower price?','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-product-label'
        ),
        'popup-title'   =>  array(
            'name'      =>  __('Popup title','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'This text is shown as title in the modal window','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Best price on the product','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-popup-title'
        ),
        'site-popup'   =>  array(
            'name'      =>  __('Website field','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Label for the field in which the user can suggest the qualifying competitor website where the same product is sold at a lower price','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Type in the website where you found this product at a lower price','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-site-popup'
        ),
        'price-popup'   =>  array(
            'name'      =>  __('Price field','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Label for the field in which the user can suggest the lower price','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Type in the lower price','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-price-popup'
        ),
        'note-popup'   =>  array(
            'name'      =>  __('Note field','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Label for the field where the user can add notes about the product','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Write here any additional note about this product','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-note-popup'
        ),
        'user-name-popup'   =>  array(
            'name'      =>  __('User name','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Label for the field where the user can add his/her name','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Type your name','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-user-name-popup'
        ),
        'gdpr-checkbox-label-popup'   =>  array(
            'name'      =>  __('GDPR Checkbox label','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Label for GDPR checkbox','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Your email will be used to notify you about your proposal. You can read more in our [privacy_policy].','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-label-gdpr-checkbox-popup'
        ),
        'email-popup'   =>  array(
            'name'      =>  __('Email','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Label for the field where the user can add his/her own email address','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Type your email address','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-email-popup'
        ),
        'success-popup'   =>  array(
            'name'      =>  __('Success message','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Message displayed when the user\'s request has been successfully submitted','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('Thanks for suggesting a lower price. We will check the information you provided and will get back to you if we agree on the suggested price','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-success-popup'
        ),
        'error-popup'   =>  array(
            'name'      =>  __('Error message','yith-best-price-guaranteed-for-woocommerce'),
            'type'      =>  'textarea',
            'desc'      =>  __( 'Message displayed if an error occurs when the user is trying to submit a price match request','yith-best-price-guaranteed-for-woocommerce' ),
            'default'   =>  __('An error has occurred. Please, try again','yith-best-price-guaranteed-for-woocommerce'),
            'id'        =>  'yith-wcbpg-error-popup'
        ),
        'labels-end' => array(
            'type'      => 'sectionend',
        )
    )
);

return $tab;