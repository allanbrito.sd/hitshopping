<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists ( "WC_Email" ) ) {
    require_once ( WC ()->plugin_path () . '/includes/emails/class-wc-email.php' );
}

if ( !class_exists( 'YITH_WCBPG_Admin_Notification' ) ) :

    /**
     * User Notification
     *
     * An email sent to the admin when a user sent a message bu form of product page
     *
     * @class       YITH_WCBPG_Admin_Notification
     * @version     1.0.0
     */
    class YITH_WCBPG_Admin_Notification extends WC_Email {

        /**
         * @var YITH_WCBPG
         */
        public $object;

        public $product;
        
        public $custom_message;
        /**
         * Constructor.
         */
        public function __construct() {
            $this->id          = 'yith-wcbpg-new-best-price-admin';
            $this->title       = __( 'Best Price Guaranteed - New best price', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->description = __( 'The admin receives this email if the user asks to buy the product at a better price than the suggested one', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->heading     = __( 'Best priced product', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->subject     = __( 'Best priced product', 'yith-best-price-guaranteed-for-woocommerce' );
            $this->reply_to    = '';
            $this->template_html  = 'emails/new-best-price-admin-notification.php';
            $this->template_plain = 'emails/plain/new-best-price-admin-notification.php';
            $this->template_base = YITH_WCBPG_TEMPLATE_PATH;
            

            // Triggers for this email
            add_action( 'yith-wcbpg-new-best-price-admin_notification',array( $this, 'trigger') );
            add_action( 'yith-wcbpg-product-details', array( $this,'yit_wcbpg_product_details' ), 10, 2 );

            // Call parent constructor
            parent::__construct();

            // Other settings
            $this->recipient = $this->get_option( 'recipient', get_option( 'admin_email' ) );

            parent::__construct ();
        }


        public function trigger( $post_id ) {
            if( isset( $post_id ) && ( $post_id != '' ) ){
                $this->object = new YITH_WCBPG_Best_Price( $post_id );                
            }

            $result = $this->send( 
                $this->get_recipient(), 
                $this->get_subject(), 
                $this->get_content(), 
                $this->get_headers(), 
                $this->get_attachments() 
            );
            
            return $result;
            
        }

        /**
         * get_content_html function.
         *
         * @since 0.1
         * @return string
         */
        public function get_content_html () {
            $url = admin_url('post.php');
            $params = array('post'=>$this->object->post_id,'action'=>'edit');
            $url = esc_url( add_query_arg( $params, $url ) );
            ob_start ();
            wc_get_template ( 'emails/new-best-price-admin-notification.php', array (
                'best_price'        =>  $this->object,
                'email_heading'     =>  $this->get_heading (),
                'sent_to_admin'     =>  true,
                'plain_text'        =>  false,
                'email'             =>  $this,
                'link_to_post'      =>  $url
            ),
                '',
                YITH_WCBPG_TEMPLATE_PATH );

            return ob_get_clean ();
        }

        /**
         * get_content_plain function.
         *
         * @access public
         * @return string
         */
        function get_content_plain() {
            ob_start();
            wc_get_template( $this->template_plain, array(
                'best_price'        =>  $this->object,
                'email_heading'  => $this->get_heading(),                
                'email'          => $this,
                'sent_to_admin'     =>  true,
                'plain_text'        =>  true,

            ), '', $this->template_base );

            return ob_get_clean();
        }

        /**
         * Initialise settings form fields.
         */
        public function init_form_fields() {
            $this->form_fields = array(
                'enabled'    => array(
                    'title'   => __( 'Enable/Disable', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Enable this email notification', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'default' => 'yes'
                ),
                'recipient'  => array(
                    'title'       => __( 'Recipient(s)', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'text',
                    'description' => sprintf( __( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', 'yith-best-price-guaranteed-for-woocommerce' ), esc_attr( get_option( 'admin_email' ) ) ),
                    'placeholder' => '',
                    'default'     => '',
                    'desc_tip'    => true
                ),
                'subject'    => array(
                    'title'       => __( 'Subject', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'text',
                    'description' => sprintf( __( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', 'woocommerce' ), $this->subject ),
                    'placeholder' => '',
                    'default'     => '',
                    'desc_tip'    => true
                ),
                'heading'    => array(
                    'title'       => __( 'Email Heading', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'text',
                    'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.', 'woocommerce' ), $this->heading ),
                    'placeholder' => '',
                    'default'     => '',
                    'desc_tip'    => true
                ),
                'email_type' => array(
                    'title'       => __( 'Email type', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'type'        => 'select',
                    'description' => __( 'Choose the format for the email sent.', 'yith-best-price-guaranteed-for-woocommerce' ),
                    'default'     => 'html',
                    'class'       => 'email_type wc-enhanced-select',
                    'options'     => $this->get_email_type_options(),
                    'desc_tip'    => true
                )
            );

        }

        public function yit_wcbpg_product_details( $best_price, $link_to_post ){
            wc_get_template ( 'emails/product-details.php', array (
                'best_price'        =>  $best_price,
                'link_to_post'      =>  $link_to_post
            ),
                '',
                YITH_WCBPG_TEMPLATE_PATH );
        }        
        
    }

endif;

return new YITH_WCBPG_Admin_Notification();