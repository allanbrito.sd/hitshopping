<?php
/**
 * Main class
 *
 * @author  YIThemes
 * @package YITH Best Price Guaranteed for WooCommerce
 * @version 1.0.0
 */
if ( !defined( 'YITH_WCBPG' ) ) {
    exit;
} // Exit if accessed directly

if( !class_exists( 'YITH_WCBPG' ) ) {
    class YITH_WCBPG
    {

        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG
         * @since 1.0.0
         */
        protected static $instance;


        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG
         * @since 1.0.0
         * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public static function get_instance()
        {

            if (is_null(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * @since   1.0.0
         * @return  mixed
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __construct()
        {
            add_action( 'plugins_loaded',array( $this, 'plugin_fw_loader' ),15 );
            add_action( 'plugins_loaded', array( $this, 'load_privacy_dpa' ), 20 );
            YITH_WCBPG_Notifier();
            $register_enqueue = YITH_WCBPG_Register_Enqueue_Scripts::get_instance();
            // Class admin
            if ( is_admin() && !defined( 'DOING_AJAX' ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX && ( !isset( $_REQUEST[ 'context' ] ) || ( isset( $_REQUEST[ 'context' ] ) && $_REQUEST[ 'context' ] !== 'frontend' ) ) ) )  {
                add_action( 'admin_enqueue_scripts', array( $register_enqueue, 'enqueue_admin_scripts') );
                YITH_WCBPG_Admin();
                if( function_exists('YITH_WCBPG_Privacy') ){
                    YITH_WCBPG_Privacy();
                }
            } else {
                add_action( 'wp_enqueue_scripts', array( $register_enqueue,'enqueue_frontend_scripts' ) );
                YITH_WCBPG_Frontend();

            }
            YITH_WCBPG_AJAX::get_instance();
            YITH_WCBPG_Cart();

        }



        /**
         * Load Plugin Framework
         *
         * @since  1.0
         * @access public
         * @return void
         * @author Andrea Grillo <andrea.grillo@hitoutlets.com>
         */
        public function plugin_fw_loader() {
            if ( !defined( 'YIT_CORE_PLUGIN' ) ) {
                global $plugin_fw_data;
                if ( !empty( $plugin_fw_data ) ) {
                    $plugin_fw_file = array_shift( $plugin_fw_data );
                    require_once( $plugin_fw_file );
                }
            }
        }

        /**
         * Load privacy DPA
         */
        public function load_privacy_dpa(  ) {
            require_once( YITH_WCBPG_INCLUDES_PATH . '/class.yith-wcbpg-privacy-dpa.php' );
        }
        
    }
}

/**
 * Unique access to instance of YITH_WCBPG class
 *
 * @return \YITH_WCBPG
 * @since 1.0.0
 * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
 */
function YITH_WCBPG() {
    return YITH_WCBPG::get_instance();
}