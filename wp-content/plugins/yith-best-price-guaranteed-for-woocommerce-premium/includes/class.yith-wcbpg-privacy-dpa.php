<?php
if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_WCBPG_VERSION' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'YITH_WCBPG_Privacy_DPA' ) ) {
    /**
     * Class YITH_WCBPG_Privacy_DPA
     * Privacy Class
     */
    class YITH_WCBPG_Privacy_DPA extends YITH_Privacy_Plugin_Abstract {

        /**
         * YITH_YWRAQ_Privacy constructor.
         */
        public function __construct() {
            parent::__construct( _x( 'YITH Woocommerce Best Price Guaranteed Premium', 'Privacy Policy Content', 'yith-best-price-guaranteed-for-woocommerce' ) );
        }

        public function get_privacy_message( $section ) {
            $message = '';

            switch ( $section ){
                case 'collect_and_store':
                    $message = '<p>' . __( 'We keep the customer email address and use it to send a promotional email to purchase the product', 'yith-best-price-guaranteed-for-woocommerce' ) . '</p>' .
                               '<p>' . __( 'When the customer is erased, the email address associated to the request will be anonymized', 'yith-best-price-guaranteed-for-woocommerce' ) . '</p>';
                    break;
                case 'has_access':
                    $message = '<p>' . __( 'Members of our team have access to the information you provide us. For example, both Administrators and Shop Managers can access:', 'yith-best-price-guaranteed-for-woocommerce' ) . '</p>';
                default;

            }


            return $message;
        }
    }
}

new YITH_WCBPG_Privacy_DPA();