<?php
/**
 * AJAX class
 *
 * @author  YIThemes
 * @package YITH WooCommerce Booking
 * @version 1.0.0
 */


if ( !defined( 'YITH_WCBPG' ) ) {
    exit;
} // Exit if accessed directly

if ( !class_exists( 'YITH_WCBPG_AJAX' ) ) {
    /**
     * YITH Best Price Guaranteed for WooCommerce AJAX
     *
     * @since 1.0.0
     */
    class YITH_WCBPG_AJAX {

        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG_AJAX
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        protected static $instance;

        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG_AJAX
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public static function get_instance() {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Constructor
         *
         * @return YITH_WCBPG_AJAX
         * @since 1.0.0
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __construct() {
            $ajax_actions = array(
                'insert_post',
                'generate_coupon',
                'set_content_modal'
            );

            foreach ( $ajax_actions as $ajax_action ) {
                add_action( 'wp_ajax_yith_wcbpg_' . $ajax_action, array( $this, $ajax_action ) );
                add_action( 'wp_ajax_nopriv_yith_wcbpg_' . $ajax_action, array( $this, $ajax_action ) );
            }

        }

        /**
         * Insert new post in post type "Best Price"
         *
         * @since   1.0.0
         * @return  void
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */

        public function insert_post(){
            $my_post = array(
                'post_status' => 'publish',
                'post_type' => YITH_WCBPG_Admin()->get_post_type_name(),
            );

            $post_id = wp_insert_post( $my_post );
            $form_data = array();
            parse_str($_POST['formData'], $form_data);
            if( isset( $form_data['wcbg_product_id'] ) ) update_post_meta($post_id, '_wcbg_product_id', $form_data['wcbg_product_id']);
            if( isset( $form_data['wcbg_variation_id'] ) ) update_post_meta($post_id, '_wcbg_variation_id', $form_data['wcbg_variation_id']);
            if( isset( $form_data['wcbpg_site_url'] ) ) update_post_meta($post_id, '_wcbpg_site_url', $form_data['wcbpg_site_url']);
            if( isset( $form_data['wcbpg_product_price'] ) ) update_post_meta($post_id, '_wcbpg_suggested_price', $form_data['wcbpg_product_price']);
            if( isset( $form_data['wcbpg_note'] ) ) update_post_meta($post_id, '_wcbpg_note', $form_data['wcbpg_note']);
            if( isset( $form_data['wcbpg_user_name'] ) ) update_post_meta($post_id, '_wcbpg_user_name', $form_data['wcbpg_user_name']);
            if( isset( $form_data['wcbpg_user_email'] ) ) update_post_meta($post_id, '_wcbpg_user_email', $form_data['wcbpg_user_email']);
            if( isset( $form_data['wcbpg_gdpr_checkbox'] ) ) update_post_meta($post_id, '_wcbpg_gdpr_checkbox', $form_data['wcbpg_gdpr_checkbox']);
            WC()->mailer();
            do_action( 'yith-wcbpg-new-best-price-admin_notification', $post_id );
            die();
        }

        /**
         * Generate coupon code for new post
         *
         * @since   1.0.0
         * @return  void
         * @author  Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */

        public function generate_coupon(){
            $best_price = new YITH_WCBPG_Best_Price( $_POST['best_price_id'] );
            $product_display_price = yit_get_display_price( $best_price->product );
            $product_price = $best_price->product->get_price();
            // Filter to use to aply a change to the suggest proce amount (for example if you want offer a product forever less 5% to suggested price)
            $suggested_price = apply_filters('ywbpg_suggested_price',$best_price->suggested_price);
            $coupon_details = $this->get_coupon_details( $product_display_price, $product_price, $suggested_price );
            $product_id = yit_get_prop($best_price->product,'id') ;
            $coupon_code = 'BESTPRICE_'.rand(); // Code
            $coupon = array(
                'post_title'    => $coupon_code,
                'post_content'  => '',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'		=> 'shop_coupon'
            );

            $new_coupon_id = wp_insert_post( $coupon );
            

            // Add meta
            update_post_meta( $new_coupon_id, 'discount_type', $coupon_details['type'] );
            update_post_meta( $new_coupon_id, 'coupon_amount', $coupon_details['amount'] );
            update_post_meta( $new_coupon_id, 'individual_use', apply_filters( 'ywbpg_individual_use_coupon', 'no', $best_price, $new_coupon_id ) );
            update_post_meta( $new_coupon_id, 'product_ids', $product_id );
            update_post_meta( $new_coupon_id, 'exclude_product_ids', '' );
            update_post_meta( $new_coupon_id, 'customer_email', array($best_price->user_email) );
            update_post_meta( $new_coupon_id, 'usage_limit', 1 );
            update_post_meta( $new_coupon_id, 'expiry_date', '' );
            update_post_meta( $new_coupon_id, 'apply_before_tax', 'yes' );
            update_post_meta( $new_coupon_id, 'free_shipping', 'no' );
            update_post_meta( $best_price->post_id, '_wcbpg_coupon_code', $coupon_code );
            

            WC()->mailer();
            do_action( 'yith-wcbpg-new-coupon-user_notification', $best_price->post_id );
            die();
        }


        public function set_content_modal(){
            $product_id = $_POST['product_id'];
            $variation_id = $_POST['variation_id'];
            wc_get_template( 'ywbg-product-popup.php', array('product_id' => $product_id, 'variation_id' => $variation_id), '', YITH_WCBPG_TEMPLATE_PATH );
            die();
        }
        
        public function get_coupon_details( $product_display_price, $product_price, $suggested_price ){
            $suggested_price = floatval(str_replace(',', '.', str_replace('.', '', $suggested_price)));
            $coupon = array(
                'type'      =>  '',
                'amount'    =>  ''
            );
           if(get_option('woocommerce_calc_taxes') == 'yes'){
               if( get_option('woocommerce_prices_include_tax') == 'no') {
                   $coupon['type'] = 'percent_product';
                   $coupon['amount'] = (float) ($product_display_price - $suggested_price) / $product_display_price * 100;
               }else{
                   $coupon['type'] = 'percent_product';
                   $coupon['amount'] = (float) ($product_price - $suggested_price) / $product_price * 100;
               }
           }else{
               $coupon['type'] = 'fixed_product';
               $coupon['amount'] = (float) $product_price - $suggested_price;
           }
            return $coupon;
        }



    }
}