<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if( !class_exists( 'YITH_WCBPG_Cart' ) ){
    class YITH_WCBPG_Cart{

        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG_Cart
         * @since 1.0.0
         */
        protected static $instance;

        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG_Cart
         * @since 1.0.0
         */
        public static function get_instance()
        {
            if (is_null(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * Constructor
         * @return mixed
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __construct(){
            add_action( 'template_redirect', array( $this,'redirect_to_cart' ));
        }

        /**
         * Redirect user to cart page from email link
         * @return void
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function redirect_to_cart(){
            if( isset( $_REQUEST ) && array_key_exists( 'wcbpg_id',$_REQUEST ) ){
                $this->set_content_cart();
                wp_redirect( wc_get_cart_url() );exit();
            }
        }

        /**
         * Set content cart for user purchase
         * @return void
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        private function set_content_cart(){
            $best_price = new YITH_WCBPG_Best_Price( $_REQUEST['wcbpg_id'] );
            $product_id = yit_get_prop($best_price->product,'id') ;
            WC()->cart->empty_cart();
            WC()->cart->remove_coupons();
            WC()->cart->add_to_cart( $product_id );
            WC()->cart->add_discount( $best_price->coupon_code );
        }



        

    }
}

/**
 * Unique access to instance of YITH_WCBPG_Admin class
 *
 * @return \YITH_WCBPG_Admin
 * @since 1.0.0
 */
function YITH_WCBPG_Cart() {
    return YITH_WCBPG_Cart::get_instance();
}