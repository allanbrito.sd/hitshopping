<?php
/**
 * Privacy/GDPR related functionality which ties into WordPress functionality.
 *
 * @since 1.2.0
 * @package YITH Best Price Guaranteed for WooCommerce
 */

defined('ABSPATH') || exit;

/**
 * WC_Privacy Class.
 */
class YITH_WCBPG_Privacy extends WC_Abstract_Privacy
{

    /**
     * Single instance of the class
     *
     * @var \YITH_WCBPG_Privacy
     * @since 1.2.0
     */
    protected static $instance;


    /**
     * Returns single instance of the class
     *
     * @return \YITH_WCBPG_Privacy
     * @since    1.2.0
     * @author   Alessio Torrisi <alessio.torrisi@yourinspiration.it>
     */
    public static function get_instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    /**
     * YITH_WCBPG_Privacy constructor.
     */
    public function __construct()
    {
        parent::__construct( __( 'YITH Best Price Guarenteed', 'yith-woocommerce-best-price-guaranteed' ) );

        $this->add_exporter('best-price-guaranteed', __('Best Price Guaranteed', 'yith-best-price-guaranteed-for-woocommerce'), array($this, 'best_price_exporter_section'));
        $this->add_eraser( 'best-price-guaranteed', __( 'Best Price Guaranteed', 'woocommerce' ), array( $this, 'best_price_data_eraser' ) );
    }


    /**
     * Export all best price requests of selected customer
     * @param $email_address
     * @param $page
     * @return array
     */
    public function best_price_exporter_section($email_address, $page)
    {
        global $wpdb;
        $user = get_user_by('email', $email_address); // Check if user has an ID in the DB to load stored personal data.
        $data_to_export = array();
        $best_prices_query = "
        SELECT post.ID 
        FROM {$wpdb->prefix}posts as post INNER JOIN {$wpdb->prefix}postmeta as postmeta 
        ON post.ID = postmeta.post_id 
        WHERE postmeta.meta_key = '_wcbpg_user_email' AND postmeta.meta_value = '{$email_address}'
        ";
        $best_prices = $wpdb->get_results($best_prices_query);
        if (!empty($best_prices)) {
            foreach ($best_prices as $best_price) {
                $data_to_export[] = array(
                    'group_id' => 'best_price',
                    'group_label' => __('Best Price Guaranteed', 'yith-best-price-guaranteed-for-woocommerce'),
                    'item_id' => 'user-best-price-' . $best_price->ID,
                    'data' => self::get_best_prices_customer_personal_data($best_price->ID),
                );
            }

        }

        return array(
            'data' => $data_to_export,
            'done' => true,
        );
    }


    /**
     * Retrieve all best price informations associated to customer
     * @param $best_price_id
     * @return array|mixed|void
     */
    protected static function get_best_prices_customer_personal_data($best_price_id)
    {
        $personal_data = array();

        if (!$best_price_id) {
            return array();
        }


        $best_price = new YITH_WCBPG_Best_Price($best_price_id);

        $personal_data = array(
            array(
                'name' => __('Request ID', 'yith-best-price-guaranteed-for-woocommerce'),
                'value' => $best_price_id,
            ),
            array(
                'name' => __('User email', 'yith-best-price-guaranteed-for-woocommerce'),
                'value' => $best_price->user_email,
            ),
            array(
                'name' => __('product', 'yith-best-price-guaranteed-for-woocommerce'),
                'value' => yit_get_prop($best_price->product, 'name')
            ),
            array(
                'name' => __('Suggested price', 'yith-best-price-guaranteed-for-woocommerce'),
                'value' => $best_price->suggested_price
            ),
            array(
                'name' => __('Site url', 'yith-best-price-guaranteed-for-woocommerce'),
                'value' => $best_price->site_url
            ),
            array(
                'name' => __('Note', 'yith-best-price-guaranteed-for-woocommerce'),
                'value' => $best_price->note
            ),

        );


        $personal_data = apply_filters('yith_wcbpg_customer_personal_data', $personal_data, $best_price_id);

        return $personal_data;
    }


    /**
     * Retain email address in best price request
     * @param $email_address
     * @param $page
     * @return array
     */
    public function best_price_data_eraser( $email_address, $page ){
        global $wpdb;
        $response = array(
            'items_removed'  => false,
            'items_retained' => false,
            'messages'       => array(),
            'done'           => true,
        );
        $query = "
        SELECT post.ID 
        FROM {$wpdb->prefix}posts as post INNER JOIN {$wpdb->prefix}postmeta as postmeta 
        ON post.ID = postmeta.post_id 
        WHERE postmeta.meta_key = '_wcbpg_user_email' AND postmeta.meta_value = '{$email_address}'
        ";
        $best_prices = $wpdb->get_results($query);
        $n_best_prices = count($best_prices);
        if( $n_best_prices > 0 ){
            $best_prices_ids = '';
            $i=1;
            foreach ( $best_prices as $best_price ){
                $best_prices_ids .= $best_price->ID;
                if( $i < $n_best_prices ){
                    $best_prices_ids .= ',';
                    $i++;
                }
            }
            $query = "
        UPDATE {$wpdb->prefix}postmeta as postmeta 
        SET postmeta.meta_value = 'anonymous'
        WHERE postmeta.post_id IN ({$best_prices_ids})
        AND postmeta.meta_key = '_wcbpg_user_email' 
        OR postmeta.meta_key = '_wcbpg_user_name' 
        ";
            $results = $wpdb->get_results( $query );
            $response['messages'][]    = sprintf( __( 'Customer Best Price Requests have been retained %s Ids: %s', 'woocommerce' ), '<br>',$best_prices_ids );
            $response['items_retained'] = true;
        }

        return $response;
    }

}


/**
 * Unique access to instance of YITH_WCBPG_Privacy class
 *
 * @return \YITH_WCBPG_Privacy
 * @since 1.0.0
 */
function YITH_WCBPG_Privacy()
{
    return YITH_WCBPG_Privacy::get_instance();
}