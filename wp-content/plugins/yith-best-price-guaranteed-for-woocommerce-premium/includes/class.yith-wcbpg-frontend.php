<?php
if ( !defined( 'YITH_WCBPG' ) ) {
    exit;
} // Exit if accessed directly

if( !class_exists( 'YITH_WCBPG_Frontend' ) ) {
    class YITH_WCBPG_Frontend
    {

        /**
         * Single instance of the class
         *
         * @var \YITH_WCBPG
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        protected static $instance;


        private $single_product_hooks;


        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCBPG
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public static function get_instance()
        {
            if (is_null(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * YITH_WCBPG_Frontend constructor.
         * @return mixed
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function __construct()        {
            
            $this->set_single_product_hooks();
            $show_button_in = get_option('yith-wcbpg-show-button-in');
            $show_button_in_loop = get_option('yith-wcbpg-show-in-loop');
            if( $show_button_in_loop == 'yes'){
                add_action('woocommerce_after_shop_loop_item',array($this,'add_button'),15);
            }
            add_action( 'woocommerce_single_product_summary',array( $this,'add_button' ),$this->single_product_hooks[$show_button_in] );
            add_action( 'wp_footer',array( $this,'add_modal_popup') );
            add_action( 'wp_enqueue_scripts', array($this,'localize_scripts') );            
        }

        
        /**
         * Insert link in single product page to allow user to report best price
         * @return void
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function add_button(){
            global $product;
            if( $product->is_type('gift-card') ) return;
            if( is_product() || $product->is_type('simple') ){
                echo '<a class="btn" data-popup-open="wcbpg-popup" data-id="'. yit_get_prop($product,'id') .'" href="#" id="wcbpg_open_popup">'. get_option('yith-wcbpg-product-label')  .'</a>';
            }else{
                echo '<a class="btn" id="wcbpg_open_popup" href="'. $product->get_permalink() .'">'. get_option('yith-wcbpg-product-label')  .'</a>';
            }

        }


        /**
         * Set hooks for link in the single product page
         * @return void
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function set_single_product_hooks(){
            $this->single_product_hooks = array(
                'before_single_price'   =>  7,
                'after_single_price'    =>  12,
                'before_add_to_cart'    =>  27,
                'after_add_to_cart'     =>  32,
                'before_excerpt'        =>  18,
                'after_exceprt'         =>  22
            );
        }

        /**
         * Add modal popup in single product page
         * @return void
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function add_modal_popup(){
            global $product;
            echo '<div class="popup" data-popup="wcbpg-popup" id="wcbpg-popup"></div>';
        }

        /**
         * Localize frontend scripts
         * @return void
         * @since 1.0.0
         * @author Alessio Torrisi <alessio.torrisi@yourinspiration.it>
         */
        public function localize_scripts(){ 
            wp_register_script( 'yith-wcbpg-frontend', YITH_WCBPG_ASSETS_URL . '/js/frontend.js', array( 'jquery' ), '1.0.0', true );
            wp_localize_script( 'yith-wcbpg-frontend', 'yith_wcbpg_frontend', array(
                'ajaxurl'   => admin_url( 'admin-ajax.php' ),
                'success'   =>  get_option('yith-wcbpg-success-popup'),
                'error'     =>  get_option('yith-wcbpg-error-popup'),
                'error_fields'  =>  __('Please, fill out the form as required.','yith-best-price-guaranteed-for-woocommerce'),
                'woocommerce_price_decimal_separator'   =>  get_option('woocommerce_price_decimal_sep'),
                'price_tooltip_error_message'           =>  sprintf( __('Please enter in monetary decimal (%s) format without thousand separators and currency symbols.','yith-best-price-guaranteed-for-woocommerce'),get_option('woocommerce_price_decimal_sep')),
                'required_field'   =>  __('Required field','yith-best-price-guaranteed-for-woocommerce'),
                'select_variation'   =>  __('Please before select a variation','yith-best-price-guaranteed-for-woocommerce')
            ) );
        }
            }
}


/**
 * Unique access to instance of YITH_WCBPG_Frontend class
 *
 * @return \YITH_WCBPG_Frontend
 * @since 1.0.0
 */
function YITH_WCBPG_Frontend() {
    return YITH_WCBPG_Frontend::get_instance();
}