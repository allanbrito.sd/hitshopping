== YITH Best Price Guaranteed for WooCommerce ===


== Changelog ==

= 1.2.5 - Released: Sept 27, 2018 =

* New: support to WooCommerce 3.4.5
* Update: Core Framework 3.0.23
* Update: Dutch language file
* Dev: new filter 'ywbpg_individual_use_coupon'


= 1.2.4 - Released: Jun 28, 2018 =

* New: support to WooCommerce 3.4.2
* Update: Italian language
* Tweak: close popup clicking outside the popup
* Fix: prevent plugin from use on gift card products


= 1.2.3 - Released: May 31, 2018 =

* Fix: fatal error 'YITH_WCBPG_Privacy function does not exists'


= 1.2.2 - Released: May 30, 2018 =

* New: support to variations
* New: edit price before approve request
* New: custom message to show in user proposal email


= 1.2.1 - Released: May 25, 2018 =

* Update: Update Core Framework 3.0.16
* New: Privacy Policy Guide

= 1.2.0 - Released: May 24, 2018 =

* New: support to WordPress 4.9.6
* New: support to WooCommerce 3.4.0
* New: Support to GDPR compliance - Export personal data
* New: Support to GDPR compliance - Erase personal data
* New: GDPR privacy message
* Update: Update Core Framework 3.0.15


= 1.1.8- Released: Feb 07, 2018 =

* New: support to WooCommerce 3.3.1
* New: support to WordPress 4.9.4

= 1.1.7- Released: Feb 06, 2018 =

* New: support to WordPress 4.9.2
* New: dutch translation
* New: spanish translation
* New: option "show site url"
* Update: plugin framwerok to version 3.0.11
* Tweak: checking coupon usage in best price list
* Fix: unlocalized string


= 1.1.6- Released: Sep 15, 2017 =

* New: New: placeholder {first_name} to show first name in best price customer email


= 1.1.5- Released: Aug 23, 2017 =

* Tweak: prevent incorrect price insertion

= 1.1.4- Released: Aug 04, 2017 =

* Dev: added 'ywbpg_suggested_price' filter
* New: possibility to show an additional field into the popup
* Update: plugin framework

= 1.1.3- Released: July 06, 2017 =

* New: support to WooCommerce 3.1
* Update: Plugin core framework

= 1.1.2- Released: Apr 11, 2017 =

* Fix: coupon amount not calculated correctly when product price doesn't include tax

= 1.1.1- Released: Mar 13, 2017 =

* New: possibility to set the popup size
* New: possibility to set  the popup fields as required
* New: field Name in popup for unlogged users


= 1.1.0- Released: Mar 03, 2017 =

* New: WC 2.7-RC1 support
* New: add best price button to archive page
* Tweak: best price modal popup generated via ajax
* Tweak: minify js scripts
* Tweak: modal popup style

= 1.0.3- Released: Feb 20, 2017 =

* Fixed: wpml string translation issue

= 1.0.2- Released: Oct 18, 2016 =

* Fixed: prevent fatal error in product popup
* Fixed: css issue in best price modal window
* Added: option to hide product info in the Best Price modal window


= 1.0.1 - Released: Sept 26, 2016 =

* Fixed: Custom post type registration

= 1.0.0 - Released: Sept 22, 2016 =

* Initial release
