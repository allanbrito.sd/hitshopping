<?php
/**
 * Subscription checkbox template
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Active Campaign
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCAC' ) ) {
	exit;
} // Exit if accessed directly
?>
<fieldset>
    <legend><?php echo $show_tags_label ?></legend>
	<?php

	foreach ( $selected_show_tags as $tag ) {
		?>
        <input type="checkbox" name="yith_wcac_subscribe_tags[<?php echo $tag; ?>]" value="yes" /> <?php echo $tag; ?>
        <br />
		<?php
	}
	?>
</fieldset>
