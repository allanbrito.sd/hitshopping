=== YITH Active Campaign for WooCommerce ===

Contributors: yithemes
Tags: Active Campaign, newsletter, branding, email, advert, commercial, mail, commercial
Requires at least: 4.0
Tested up to: 4.9.6
Stable tag: 1.0.13
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html/

Create custom forms in just a few instants, assign specific tags, send emails as soon as a purchase threshold is reached and so on…

== Description ==

Emails are one of the most rewarding channels in online business, that’s no secret.

Almost the entirety of worldwide online market owners state that a big part of their income comes from their mailing list, since any email sent is a good opportunity to obtain new sales.

= Premium features: =


= Premium Live Demo =


Do you want to discover all plugin features? Would you like to try it?

Visit our **[test sandbox](http://plugins.hitoutlets.com/yith-active-campaign-for-woocommerce/)**
By accessing our testing platform, you will be able to discover all plugin features and test it as your prefer, both in back end and in front end.

What are you waiting for, visit the official "**[live demo](http://plugins.hitoutlets.com/yith-active-campaign-for-woocommerce/)**" of the plugin and click on "LAUNCH ADMIN DEMO" link that you find in the topbar to make test our plugin right now.

= Coming Soon =

- Add Option to make events expire after end date (no more purchasable/no more visible)

= Languages =

Also abalable in:

* English (default).

== Screenshots ==

== Installation ==

**Important**: First of all, you have to download and activate [WooCommerce](https://wordpress.org/plugins/woocommerce) plugin, because without it YITH Event Ticket for WooCommerce cannot work.

1. Unzip the downloaded zip file.
2. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `YITH Event Ticket for WooCommerce` from Plugins page.


== Changelog ==

= 1.0.13 – Released: May, 28 - 2018 =

* New: WooCommerce 3.4 compatibility
* New: WordPress 4.9.6 compatibility
* New: updated plugin framework
* New: GDPR compliance
* Update: Italian language
* Update: Spanish translation

= 1.0.12 - Released: Feb, 14 - 2018 =

* New: Updated plugin core.
* Fix: Issue with license activation.

= 1.0.11 - Released: Feb, 01 - 2018 =

* Fix: Minor changes.

= 1.0.1 - Released: Jan, 31 - 2018 =

* New: Italian translation.
* New: Support to WooCommerce 3.3.x version.
* New: Support to YITH Plugin Framework 3.0.11
* Fix: Localize strings.
* Fix: Conflict with AJAX Call.
* Fix: Issue with load fields for export waiting lists.
* Fix: Register plugin activation and updates.

= 1.0.0 - Released: Oct, 2 - 2017 =

* Initial release

