#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Active Campaign\n"
"POT-Creation-Date: 2018-05-07 12:42+0200\n"
"PO-Revision-Date: 2015-10-16 14:09+0100\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <plugins@hitoutlets.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.13\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,"
"_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin-fw\n"

#: includes/class.yith-wcac-admin.php:77
msgctxt "Panel tab"
msgid "Integration"
msgstr ""

#: includes/class.yith-wcac-admin.php:78
msgctxt "Panel tab"
msgid "Checkout"
msgstr ""

#: includes/class.yith-wcac-admin.php:79
msgctxt "Panel tab"
msgid "Shortcode"
msgstr ""

#: includes/class.yith-wcac-admin.php:80
msgctxt "Panel tab"
msgid "Widget"
msgstr ""

#: includes/class.yith-wcac-admin.php:81
msgctxt "Panel tab"
msgid "Register"
msgstr ""

#: includes/class.yith-wcac-admin.php:82
msgctxt "Panel tab"
msgid "Export"
msgstr ""

#: includes/class.yith-wcac-admin.php:185
#: includes/class.yith-wcac-admin.php:186
msgid "Active Campaign"
msgstr ""

#: includes/class.yith-wcac-admin.php:353
msgid "Shipping method name"
msgstr ""

#: includes/class.yith-wcac-admin.php:354
msgid "Payment method name"
msgstr ""

#: includes/class.yith-wcac-admin.php:355
msgid "User ID"
msgstr ""

#: includes/class.yith-wcac-admin.php:364
#: templates/admin/types/advanced-integration-condition.php:9
msgid "Custom"
msgstr ""

#: includes/class.yith-wcac-admin.php:510
msgctxt "Backend update list button"
msgid "Update Lists"
msgstr ""

#: includes/class.yith-wcac-admin.php:511
msgctxt "Backend update tag button"
msgid "Update Tags"
msgstr ""

#: includes/class.yith-wcac-admin.php:512
msgid "Update Fields"
msgstr ""

#: includes/class.yith-wcac-admin.php:521
msgctxt "Email label defined by Active Campaign"
msgid "Email Address"
msgstr ""

#: includes/class.yith-wcac-admin.php:522
msgctxt "First name label defined by Active Campaign"
msgid "First name"
msgstr ""

#: includes/class.yith-wcac-admin.php:523
msgctxt "Last name label defined by Active Campaign"
msgid "Last name"
msgstr ""

#: includes/class.yith-wcac-admin.php:581
msgid "Export Users"
msgstr ""

#: includes/class.yith-wcac-admin.php:582
msgid "Download CSV"
msgstr ""

#: includes/class.yith-wcac-admin.php:615
msgid "Plugin Documentation"
msgstr ""

#: includes/class.yith-wcac-widget.php:31
msgid "Display an Active Campaign subscription form in sidebars"
msgstr ""

#: includes/class.yith-wcac-widget.php:33
msgid "YITH Active Campaign Subscription Form"
msgstr ""

#: includes/class.yith-wcac-widget.php:47
#, php-format
msgid ""
"You can customize the options of the <b>Active Campaign Subscription Form</"
"b> widget from YITH WooCommerce Active Campaign <a href=\"%s\">admin\n"
"page</a>"
msgstr ""

#: includes/class.yith-wcac.php:289
msgid "Email Address"
msgstr ""

#: includes/class.yith-wcac.php:293
msgid "First name"
msgstr ""

#: includes/class.yith-wcac.php:297
msgid "Last name"
msgstr ""

#: includes/class.yith-wcac.php:1329 plugin-options/shortcode-options.php:52
#: plugin-options/widget-options.php:53
msgid "Great! You're now subscribed to our newsletter"
msgstr ""

#: includes/class.yith-wcac.php:1337
msgid "Email is required"
msgstr ""

#: includes/class.yith-wcac.php:1378
msgctxt "validate message fields"
msgid "Some field was wrong"
msgstr ""

#: init.php:129
msgid ""
"YITH WooCommerce Active Campaign is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""

#: init.php:139
msgid ""
"You can't activate the free version of YITH WooCommerce Active Campaign "
"while you are using the premium one."
msgstr ""

#: plugin-options/checkout-options.php:21
#: plugin-options/integration-options.php:17
msgid "Active Campaign Options"
msgstr ""

#: plugin-options/checkout-options.php:28
msgid "Register after"
msgstr ""

#: plugin-options/checkout-options.php:30
msgid "Select the moment in which the user will be added to the list"
msgstr ""

#: plugin-options/checkout-options.php:33
msgid "Never"
msgstr ""

#: plugin-options/checkout-options.php:34
msgid "Order completed"
msgstr ""

#: plugin-options/checkout-options.php:35
msgid "Order placed"
msgstr ""

#: plugin-options/checkout-options.php:41
#: plugin-options/register-options.php:35
msgid "Show \"Newsletter subscription\" checkbox"
msgstr ""

#: plugin-options/checkout-options.php:44
msgid ""
"If you select this option, a checkbox will be added to the checkout form, "
"inviting users to subscribe to the\n"
"\t\t\tnewsletter; otherwise, users will be subscribed automatically"
msgstr ""

#: plugin-options/checkout-options.php:50
#: plugin-options/register-options.php:44
msgid "\"Newsletter subscription\" label"
msgstr ""

#: plugin-options/checkout-options.php:52
#: plugin-options/register-options.php:46
msgid ""
"Enter here the label you want to use for the \"Newsletter subscription\" "
"checkbox"
msgstr ""

#: plugin-options/checkout-options.php:54
#: plugin-options/register-options.php:48
msgid "Subscribe to our cool newsletter"
msgstr ""

#: plugin-options/checkout-options.php:59
msgid "Position of \"Newsletter subscription\""
msgstr ""

#: plugin-options/checkout-options.php:61
msgid ""
"Select the position of the \"Newsletter subscription\" checkbox on the page"
msgstr ""

#: plugin-options/checkout-options.php:64
msgid "Above customer details"
msgstr ""

#: plugin-options/checkout-options.php:65
msgid "Below customer details"
msgstr ""

#: plugin-options/checkout-options.php:66
msgid "Above \"Place order\" button"
msgstr ""

#: plugin-options/checkout-options.php:67
msgid "Below \"Place order\" button"
msgstr ""

#: plugin-options/checkout-options.php:68
msgid "Above \"Review order\" total"
msgstr ""

#: plugin-options/checkout-options.php:69
msgid "Above billing details"
msgstr ""

#: plugin-options/checkout-options.php:70
msgid "Below billing details"
msgstr ""

#: plugin-options/checkout-options.php:71
msgid "Above shipping details"
msgstr ""

#: plugin-options/checkout-options.php:78
#: plugin-options/register-options.php:53
msgid "Show \"Newsletter subscription\" as checked"
msgstr ""

#: plugin-options/checkout-options.php:81
msgid ""
"When you check this option, \"Newsletter subscription\" checkbox will be "
"displayed as already checked"
msgstr ""

#: plugin-options/checkout-options.php:87
msgid "Contact status"
msgstr ""

#: plugin-options/checkout-options.php:90
msgid "Contact status in Active Campaign list"
msgstr ""

#: plugin-options/checkout-options.php:92 plugin-options/export-options.php:214
#: plugin-options/register-options.php:67
#: plugin-options/shortcode-options.php:71 plugin-options/widget-options.php:71
msgid "Active"
msgstr ""

#: plugin-options/checkout-options.php:93
msgid "Unsubscribed"
msgstr ""

#: plugin-options/checkout-options.php:99 plugin-options/export-options.php:221
#: plugin-options/register-options.php:74
#: plugin-options/shortcode-options.php:78 plugin-options/widget-options.php:78
msgid "Instant responders"
msgstr ""

#: plugin-options/checkout-options.php:102
msgid "Whether or not to send instant autoresponders"
msgstr ""

#: plugin-options/checkout-options.php:107
msgid "Future responders"
msgstr ""

#: plugin-options/checkout-options.php:110
msgid "Whether or not to send any future autoresponder"
msgstr ""

#: plugin-options/checkout-options.php:115
#: plugin-options/register-options.php:95
#: plugin-options/shortcode-options.php:99 plugin-options/widget-options.php:99
msgid "Send last message"
msgstr ""

#: plugin-options/checkout-options.php:118
#: plugin-options/register-options.php:98
#: plugin-options/shortcode-options.php:102
#: plugin-options/widget-options.php:102
msgid ""
"Whether or not to send the last broadcast campaign message to newly-"
"subscribed users"
msgstr ""

#: plugin-options/checkout-options.php:129
#: plugin-options/register-options.php:108
#: plugin-options/shortcode-options.php:112
#: plugin-options/widget-options.php:112
msgid "List Options"
msgstr ""

#: plugin-options/checkout-options.php:136
msgid "Integration mode"
msgstr ""

#: plugin-options/checkout-options.php:138
msgid ""
"Select whether to use a basic set of options or add integration settings"
msgstr ""

#: plugin-options/checkout-options.php:141
msgid "Simple"
msgstr ""

#: plugin-options/checkout-options.php:142
msgid "Advanced"
msgstr ""

#: plugin-options/checkout-options.php:147
#: plugin-options/export-options.php:196
#: plugin-options/register-options.php:115
#: plugin-options/shortcode-options.php:119
#: plugin-options/widget-options.php:119
#: templates/admin/types/advanced-integration-item.php:14
msgid "Active Campaign list"
msgstr ""

#: plugin-options/checkout-options.php:149
#: plugin-options/export-options.php:198
#: plugin-options/register-options.php:117
#: plugin-options/shortcode-options.php:121
#: plugin-options/widget-options.php:121
#: templates/admin/types/advanced-integration-item.php:27
msgid "Select a list for new users"
msgstr ""

#: plugin-options/checkout-options.php:160
#: plugin-options/register-options.php:128
#: plugin-options/shortcode-options.php:132
#: plugin-options/widget-options.php:132
#: templates/admin/types/advanced-integration-item.php:32
msgid "Auto-subscribe tags"
msgstr ""

#: plugin-options/checkout-options.php:162
#: plugin-options/register-options.php:130
#: plugin-options/shortcode-options.php:134
#: plugin-options/widget-options.php:134
msgid "Select tags which will be automatically added to new users"
msgstr ""

#: plugin-options/checkout-options.php:174
msgid "Advanced options"
msgstr ""

#: plugin-options/checkout-options.php:188
msgid "Tags"
msgstr ""

#: plugin-options/checkout-options.php:190
#: templates/admin/types/advanced-integration-item.php:44
msgid "Select tags for the new user"
msgstr ""

#: plugin-options/export-options.php:37
msgid "Select customers"
msgstr ""

#: plugin-options/export-options.php:38
msgid "Select products"
msgstr ""

#: plugin-options/export-options.php:39
msgid "Select categories"
msgstr ""

#: plugin-options/export-options.php:40
msgid "Select tags"
msgstr ""

#: plugin-options/export-options.php:43
msgid "Customers to export"
msgstr ""

#: plugin-options/export-options.php:44
msgid "Select customers to export"
msgstr ""

#: plugin-options/export-options.php:52 plugin-options/export-options.php:91
msgid "Filter by product"
msgstr ""

#: plugin-options/export-options.php:53
msgid "Export users that bought at least one of the selected products"
msgstr ""

#: plugin-options/export-options.php:61 plugin-options/export-options.php:101
msgid "Filter by category"
msgstr ""

#: plugin-options/export-options.php:62
msgid ""
"Export users that bought a product belonging at least to one of the selected "
"categories"
msgstr ""

#: plugin-options/export-options.php:71 plugin-options/export-options.php:111
msgid "Filter by tag"
msgstr ""

#: plugin-options/export-options.php:72
msgid ""
"Export users that bought a product with at least one of the selected tags"
msgstr ""

#: plugin-options/export-options.php:81
msgid "Users to export"
msgstr ""

#: plugin-options/export-options.php:82
msgid "Select users to export"
msgstr ""

#: plugin-options/export-options.php:92
msgid "Export users that bought a specific product"
msgstr ""

#: plugin-options/export-options.php:102
msgid "Export users that bought a product belonging to a specific category"
msgstr ""

#: plugin-options/export-options.php:112
msgid "Export users that bought a product with a specific tag"
msgstr ""

#: plugin-options/export-options.php:189
msgid "Export"
msgstr ""

#: plugin-options/export-options.php:191
msgid ""
"Export a group of users from your store to one of your Active Campaign lists"
msgstr ""

#: plugin-options/export-options.php:209 plugin-options/register-options.php:62
#: plugin-options/shortcode-options.php:66 plugin-options/widget-options.php:66
msgid "Status"
msgstr ""

#: plugin-options/export-options.php:212
msgid "Define the default contact status"
msgstr ""

#: plugin-options/export-options.php:215 plugin-options/register-options.php:68
#: plugin-options/shortcode-options.php:72 plugin-options/widget-options.php:72
msgid "Unsubscribe"
msgstr ""

#: plugin-options/export-options.php:224
msgid "Send instant responders (only with active status)"
msgstr ""

#: plugin-options/export-options.php:234 plugin-options/register-options.php:87
#: plugin-options/shortcode-options.php:91 plugin-options/widget-options.php:91
msgid "No future responders"
msgstr ""

#: plugin-options/export-options.php:237 plugin-options/register-options.php:90
#: plugin-options/shortcode-options.php:94 plugin-options/widget-options.php:94
msgid "Do not send any future responders"
msgstr ""

#: plugin-options/export-options.php:242 plugin-options/export-options.php:301
msgid "Groups of users"
msgstr ""

#: plugin-options/export-options.php:244 plugin-options/export-options.php:303
msgid "Select a group of users to export"
msgstr ""

#: plugin-options/export-options.php:248 plugin-options/export-options.php:307
msgid "All Users"
msgstr ""

#: plugin-options/export-options.php:249 plugin-options/export-options.php:308
msgid "All Customers"
msgstr ""

#: plugin-options/export-options.php:250 plugin-options/export-options.php:309
msgid "Select a group of users manually"
msgstr ""

#: plugin-options/export-options.php:251 plugin-options/export-options.php:310
msgid "Filter users using custom conditions"
msgstr ""

#: plugin-options/export-options.php:254 plugin-options/export-options.php:313
msgid "Waiting Lists"
msgstr ""

#: plugin-options/export-options.php:269 plugin-options/export-options.php:328
msgid "Filter by date"
msgstr ""

#: plugin-options/export-options.php:271 plugin-options/export-options.php:330
msgid "Export users that purchased within this date range"
msgstr ""

#: plugin-options/export-options.php:276
msgid "Waiting products"
msgstr ""

#: plugin-options/export-options.php:278
msgid ""
"Specify an Active Campaign field where saving the waiting-list products "
"subscribed by users"
msgstr ""

#: plugin-options/export-options.php:294
msgid "CSV Download"
msgstr ""

#: plugin-options/export-options.php:296
msgid "Export a group of users from your store to a CSV file"
msgstr ""

#: plugin-options/integration-options.php:24
msgid "Active Campaign API URL"
msgstr ""

#: plugin-options/integration-options.php:27
msgid ""
"Active Campaign API URL; you can get one at <b>//&lt;your_company&gt;."
"activehosted.com/admin/main.php?action=settings#tab_api</b>"
msgstr ""

#: plugin-options/integration-options.php:33
msgid "Active Campaign API Key"
msgstr ""

#: plugin-options/integration-options.php:36
msgid ""
"Active Campaign API key; you can get one at <b>//&lt;your_company&gt;."
"activehosted.com/admin/main.php?action=settings#tab_api</b>"
msgstr ""

#: plugin-options/integration-options.php:42
msgid "Integration status"
msgstr ""

#: plugin-options/register-options.php:23
msgid "Register Options"
msgstr ""

#: plugin-options/register-options.php:25
msgid ""
"Set the options for <b>YITH Active Campaign Subscription Form</b> that "
"appears below the registration form."
msgstr ""

#: plugin-options/register-options.php:29
msgid "Enable \"Newsletter subscription\" on register page"
msgstr ""

#: plugin-options/register-options.php:38
msgid ""
"When you select this option, a checkbox will be added to the register form, "
"inviting users to subscribe to the\n"
"\t\t\tnewsletter; otherwise, users will automatically subscribe"
msgstr ""

#: plugin-options/register-options.php:56
msgid ""
"When you check this option, the \"Newsletter subscription\" checkbox will be "
"printed as already checked"
msgstr ""

#: plugin-options/register-options.php:65
#: plugin-options/shortcode-options.php:69 plugin-options/widget-options.php:69
msgid "Define the default contact status when users submit the form"
msgstr ""

#: plugin-options/register-options.php:77
#: plugin-options/shortcode-options.php:81 plugin-options/widget-options.php:81
msgid "Send instant responders (only if the status is active)"
msgstr ""

#: plugin-options/register-options.php:141
#: plugin-options/shortcode-options.php:145
#: plugin-options/widget-options.php:145
#: templates/admin/types/advanced-integration.php:13
msgid "Show tags"
msgstr ""

#: plugin-options/register-options.php:143
#: plugin-options/shortcode-options.php:147
#: plugin-options/widget-options.php:147
#: templates/admin/types/advanced-integration.php:26
msgid "Select tags among which users can choose"
msgstr ""

#: plugin-options/register-options.php:154
#: plugin-options/shortcode-options.php:158
#: plugin-options/widget-options.php:158
#: templates/admin/types/advanced-integration.php:3
msgid "Tags label"
msgstr ""

#: plugin-options/register-options.php:156
#: plugin-options/shortcode-options.php:160
#: plugin-options/widget-options.php:160
#: templates/admin/types/advanced-integration.php:8
msgid ""
"Type here a text that will be used as title for the Tags section on the "
"checkout page"
msgstr ""

#: plugin-options/register-options.php:167
#: plugin-options/shortcode-options.php:171
#: plugin-options/widget-options.php:171
msgid "Field Options"
msgstr ""

#: plugin-options/register-options.php:174
#: plugin-options/shortcode-options.php:178
#: plugin-options/widget-options.php:178
#: templates/admin/types/advanced-integration-item.php:50
msgid "Fields"
msgstr ""

#: plugin-options/register-options.php:185
#: plugin-options/shortcode-options.php:189
#: plugin-options/widget-options.php:189
msgid "Style Options"
msgstr ""

#: plugin-options/register-options.php:192
#: plugin-options/shortcode-options.php:196
#: plugin-options/widget-options.php:196
msgid "Enable custom CSS"
msgstr ""

#: plugin-options/register-options.php:194
#: plugin-options/shortcode-options.php:198
#: plugin-options/widget-options.php:198
msgid "Check this option to enable custom CSS"
msgstr ""

#: plugin-options/register-options.php:200
#: plugin-options/shortcode-options.php:260
#: plugin-options/widget-options.php:260
msgid "Custom CSS"
msgstr ""

#: plugin-options/register-options.php:202
msgid "Enter your custom CSS for the register area here"
msgstr ""

#: plugin-options/shortcode-options.php:22
msgid "Shortcode Options"
msgstr ""

#: plugin-options/shortcode-options.php:24
msgid ""
"Insert the <b>[yith_wcac_subscription_form]</b> shortcode into your pages to "
"print a subscription form; set the options for your form here"
msgstr ""

#: plugin-options/shortcode-options.php:29 plugin-options/widget-options.php:30
msgid "Form title"
msgstr ""

#: plugin-options/shortcode-options.php:32 plugin-options/widget-options.php:33
msgid "Select a title for the newsletter subscription form"
msgstr ""

#: plugin-options/shortcode-options.php:33 plugin-options/widget-options.php:34
msgid "Newsletter"
msgstr ""

#: plugin-options/shortcode-options.php:38 plugin-options/widget-options.php:39
msgid "\"Submit\" button label"
msgstr ""

#: plugin-options/shortcode-options.php:41 plugin-options/widget-options.php:42
msgid "Select a label for the \"Submit\" button in the form"
msgstr ""

#: plugin-options/shortcode-options.php:42 plugin-options/widget-options.php:43
msgid "SUBMIT"
msgstr ""

#: plugin-options/shortcode-options.php:47 plugin-options/widget-options.php:48
msgid "\"Successfully Registered\" message"
msgstr ""

#: plugin-options/shortcode-options.php:50 plugin-options/widget-options.php:51
msgid ""
"Select a message to display when users complete their registration "
"successfully"
msgstr ""

#: plugin-options/shortcode-options.php:57 plugin-options/widget-options.php:58
msgid "Hide form after registration"
msgstr ""

#: plugin-options/shortcode-options.php:60
msgid ""
"If you select this option, the registration form will be hidden after a "
"successful registration"
msgstr ""

#: plugin-options/shortcode-options.php:204
#: plugin-options/widget-options.php:204
msgid "Round Corners for \"Subscribe\" Button"
msgstr ""

#: plugin-options/shortcode-options.php:206
msgid "Check this option  to make the button corners round"
msgstr ""

#: plugin-options/shortcode-options.php:212
#: plugin-options/widget-options.php:212
msgid "\"Subscribe\" Button Background Color"
msgstr ""

#: plugin-options/shortcode-options.php:220
#: plugin-options/widget-options.php:220
msgid "\"Subscribe\" Button Text Color"
msgstr ""

#: plugin-options/shortcode-options.php:228
#: plugin-options/widget-options.php:228
msgid "\"Subscribe\" Button Border Color"
msgstr ""

#: plugin-options/shortcode-options.php:236
#: plugin-options/widget-options.php:236
msgid "\"Subscribe\" Button Hover Background Color"
msgstr ""

#: plugin-options/shortcode-options.php:244
#: plugin-options/widget-options.php:244
msgid "\"Subscribe\" Button Hover Text Color"
msgstr ""

#: plugin-options/shortcode-options.php:252
#: plugin-options/widget-options.php:252
msgid "\"Subscribe\" Button Hover Border Color"
msgstr ""

#: plugin-options/shortcode-options.php:262
msgid "Enter here the custom CSS that will applied to the shortcode"
msgstr ""

#: plugin-options/widget-options.php:23
msgid "Widget Options"
msgstr ""

#: plugin-options/widget-options.php:25
#, php-format
msgid ""
"Set here the options for <b>YITH Active Campaign Subscription Form</b> "
"widget; use the widget in you sidebars, by selecting it from <a href=\"%s"
"\">Appearance > Widgets</a>"
msgstr ""

#: plugin-options/widget-options.php:61
msgid ""
"When you select this option, the registration form will be hidden after a "
"successful registration"
msgstr ""

#: plugin-options/widget-options.php:206
msgid "Check this option to make the button corners round"
msgstr ""

#: plugin-options/widget-options.php:262
msgid "Enter your custom CSS for the widget here"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:13
#: templates/admin/types/integration-status.php:17
msgid "Active Campaign user"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:13
msgid "No user can be found with this API key"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:14
#: templates/admin/active-campaign-dashboard-widget.php:31
#: templates/admin/active-campaign-dashboard-widget.php:37
#: templates/admin/types/integration-status.php:19
msgid "&lt; Not Found &gt;"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:19
#: templates/admin/types/integration-status.php:24
msgid "Status:"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:22
#: templates/admin/types/integration-status.php:27
msgid "Correctly synchronized"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:22
msgid "OK"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:24
#: templates/admin/types/integration-status.php:29
msgid "Wrong API key"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:24
msgid "KO"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:29
msgid "Name:"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:35
msgid "Email:"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:47
msgid "Prev"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:48
msgid "Next"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:56
msgid "Member count"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:59
msgid "total"
msgstr ""

#: templates/admin/active-campaign-dashboard-widget.php:68
msgid "Refresh stats"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:4
msgid "Condition"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:6
msgid "Product in cart"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:7
msgid "Product category in cart"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:8
msgid "Order total"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:13
msgid "Details"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:17
msgid "Contains at least one of"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:18
msgid "Contains all of"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:19
#: templates/admin/types/advanced-integration-condition.php:72
msgid "Does not contain"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:24
#: templates/admin/types/advanced-integration-condition.php:75
msgid "Less than"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:25
#: templates/admin/types/advanced-integration-condition.php:76
msgid "Less than or equal to"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:26
#: templates/admin/types/advanced-integration-condition.php:77
msgid "Equal to"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:27
#: templates/admin/types/advanced-integration-condition.php:78
msgid "Greater than or equal to"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:28
#: templates/admin/types/advanced-integration-condition.php:79
msgid "Greater than"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:39
msgid "Search for a product&hellip;"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:45
msgid "Select a category"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:60
msgid "Threshold"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:64
msgid "Field name on checkout page"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:68
msgid "String operator"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:69
msgid "Is"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:70
msgid "Is not"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:71
msgid "Contains"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:74
msgid "Number"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:85
msgid "Field value on checkout page"
msgstr ""

#: templates/admin/types/advanced-integration-condition.php:87
#: templates/admin/types/advanced-integration-field.php:36
#: templates/admin/types/custom-fields-item.php:31
msgid "Remove"
msgstr ""

#: templates/admin/types/advanced-integration-field.php:4
msgid "Checkout field"
msgstr ""

#: templates/admin/types/advanced-integration-field.php:22
msgid "Active Campaign field"
msgstr ""

#: templates/admin/types/advanced-integration-field.php:34
#: templates/admin/types/custom-fields-item.php:26
msgid "Update fields"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:3
msgid "toggle"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:4
msgid "remove"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:5
msgid "Set options #"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:10
msgid "Lists & Tags"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:26
msgid "Update Lists"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:43
#: templates/admin/types/advanced-integration.php:25
msgid "Update Tags"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:52
#: templates/admin/types/custom-fields.php:4
msgid "+ Add New Field"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:55
msgid ""
"Select the checkout field to connect with the Active Campaign list fields"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:77
msgid "Conditions"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:79
msgid "+ Add New Condition"
msgstr ""

#: templates/admin/types/advanced-integration-item.php:81
msgid ""
"Select order matching conditions for user subscription; all conditions "
"selected must match in order\n"
"            to complete the subscription"
msgstr ""

#: templates/admin/types/advanced-integration.php:31
msgid "Position for tags"
msgstr ""

#: templates/admin/types/advanced-integration.php:49
msgid "+ Add set option"
msgstr ""

#: templates/admin/types/advanced-integration.php:50
msgid ""
"Click \"Add set option\" button to add a bunch of options; don't forget to "
"save, when you're done"
msgstr ""

#: templates/admin/types/custom-fields-item.php:4
msgid "Field name"
msgstr ""

#: templates/admin/types/custom-fields-item.php:8
msgid "Merge var"
msgstr ""

#: templates/admin/types/custom-fields.php:5
msgid ""
"Add new fields from Active Campaign and customise the field name (label)"
msgstr ""

#: templates/admin/types/date-range.php:6
#: templates/admin/types/date-range.php:7
msgid "From"
msgstr ""

#: templates/admin/types/date-range.php:8
#: templates/admin/types/date-range.php:9
msgid "To"
msgstr ""

#: templates/admin/types/integration-status.php:18
msgid "No user found with this API key"
msgstr ""

#: templates/admin/types/integration-status.php:27
msgctxt "Correct API provided"
msgid "OK"
msgstr ""

#: templates/admin/types/integration-status.php:29
msgctxt "wrong API provided"
msgid "KO"
msgstr ""

#: templates/admin/types/integration-status.php:34
msgctxt "Active Campaign account details"
msgid "Name:"
msgstr ""

#: templates/admin/types/integration-status.php:36
#: templates/admin/types/integration-status.php:42
msgctxt "Active Campaign account details"
msgid "&lt; Not Found &gt;"
msgstr ""

#: templates/admin/types/integration-status.php:40
msgctxt "Active Campaign account details"
msgid "Email:"
msgstr ""
