/**
 * General admin panel handling
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Active Campaign
 * @version 1.0.0
 */

jQuery(document).ready(function ($) {
    var list_select = $('#yith_wcac_active_campaign_list, #yith_wcac_shortcode_active-campaign_list, #yith_wcac_widget_active-campaign_list, #yith_wcac_register_active-campaign_list, #yith_wcac_export_list'),
        tag_select = $('#yith_wcac_active_campaign_tags, #yith_wcac_shortcode_active-campaign_tags, #yith_wcac_shortcode_active-campaign_show_tags, #yith_wcac_widget_active-campaign_tags, #yith_wcac_widget_active-campaign_show_tags, #yith_wcac_register_active-campaign_tags, #yith_wcac_register_active-campaign_show_tags'),
        field_select = $('#yith_wcac_export_field_waiting_products'),
        shortcode_status = $('#yith_wcac_shortcode_status'),
        register_status = $('#yith_wcac_register_status'),
        widget_status = $('#yith_wcac_widget_status');

    // add updater list button
    list_select.after($('<a>').addClass('button button-secondary ajax-active-campaign-updater ajax-active-campaign-updater-list').attr('id', 'yith_wcac_active_campaign_list_updater').attr('href', '#').text(yith_wcac.labels.update_list_button));

    // add updater tags button
    tag_select.after($('<a>').addClass('button button-secondary ajax-active-campaign-updater ajax-active-campaign-updater-tags').attr('id', 'yith_wcac_active_campaign_tags_updater').attr('href', '#').text(yith_wcac.labels.update_group_button));

    // add updater fields button
    field_select.after($('<a>').addClass('button button-secondary ajax-active-ampaign-updater ajax-active-campaign-updater-field update-fields').attr('id', 'yith_wcac_active_campaign_field_updater').attr('href', '#').text(yith_wcac.labels.update_field_button));

    var handle_lists = function (ev) {
            var t = $(this),
                list = $(this).closest('td').find('select'),
                selected_option = list.find('option:selected').val();

            ev.preventDefault();

            $.ajax({
                beforeSend: function () {
                    t.block({
                        message   : null,
                        overlayCSS: {
                            background: '#fff',
                            opacity   : 0.6
                        }
                    });


                },
                complete  : function () {
                    t.unblock();
                },
                data      : {
                    request                     : 'list/list',
                    force_update                : true,
                    args                        : {
                        ids : 'all',
                        full: 0
                    },
                    action                      : yith_wcac.actions.yith_wcac_do_request_via_ajax_action,
                    yith_wcac_ajax_request_nonce: yith_wcac.ajax_request_nonce
                },
                dataType  : 'json',
                method    : 'POST',
                success   : function (lists) {
                    var new_options = '',
                        i = 0;
                    while (typeof lists[i] != 'undefined') {
                        new_options += '<option value="' + lists[i].id + '" ' + ( ( selected_option == lists[i].id ) ? 'selected="selected"' : '' ) + ' >' + lists[i].name + '</option>';
                        i++;
                    }

                    list.html(new_options);

                    if (new_options.length == 0) {
                        list.prop('disabled');
                    }
                    else {
                        list.removeProp('disabled');
                    }

                },
                url       : ajaxurl
            });
        },
        handle_tags = function (ev) {
            var t = $(this),
                tag = $(this).closest('tr').find('select'),
                selected_options = [];

            tag.find('option:selected').each(function (e) {
                selected_options.push($(this).val());
            });

            ev.preventDefault();

            $.ajax({
                beforeSend: function () {
                    t.block({
                        message   : null,
                        overlayCSS: {
                            background: '#fff',
                            opacity   : 0.6
                        }
                    });
                },
                complete  : function () {
                    t.unblock();
                },
                data      : {
                    request                     : 'tags/list',
                    force_update                : true,
                    args                        : {},
                    action                      : yith_wcac.actions.yith_wcac_do_request_via_ajax_action,
                    yith_wcac_ajax_request_nonce: yith_wcac.ajax_request_nonce
                },
                dataType  : 'json',
                method    : 'POST',
                success   : function (tags) {
                    tags = jQuery.parseJSON(tags);
                    var new_options = '',
                        i = 0;
                    if (tags) {
                        while (typeof tags[i] != 'undefined') {
                            new_options += '<option value="' + tags[i].name + '" ' + ($.inArray(tags[i].name, selected_options) >= 0 ? 'selected="selected"' : '' ) + ' >' + tags[i].name + '</option>';
                            i++;
                        }
                    }
                    tag.html(new_options);

                    if (new_options.length == 0) {
                        tag.prop('disabled');
                    }
                    else {
                        tag.removeProp('disabled');
                    }

                },
                url       : ajaxurl
            });
        },
        handle_fields = function (ev) {
            var t = $(this).hasClass('ajax-active-campaign-updater-field') ? $(this).parent().find('select') : $(this).parents('tr').next().find('select'),
                button = t.closest('td').find('.update-fields'),
                list_id = t.closest('tr').siblings().find('.list-select').find('option:selected').val(),
                select = t.parent().find('select'),
                selected_option = select.val();

            ev.preventDefault();

            if (list_id.length == 0) {
                t.prop('disabled');
            }
            else {
                t.removeProp('disabled');
            }
            $.ajax({
                beforeSend: function () {
                    button.block({
                        message   : null,
                        overlayCSS: {
                            background: '#fff',
                            opacity   : 0.6
                        }
                    });


                },
                complete  : function () {
                    button.unblock();
                },
                data      : {
                    force_update                : true,
                    action                      : yith_wcac.actions.yith_wcac_get_fields_via_ajax_action,
                    yith_wcac_ajax_request_nonce: yith_wcac.ajax_request_nonce
                },
                dataType  : 'json',
                method    : 'POST',
                success   : function (fields) {
                    var new_options = '',
                        i = 0;

                    jQuery.each(fields, function (i, item) {
                        new_options += '<option value="' + i + '" ' + ( ( selected_option == i ) ? 'selected="selected"' : '' ) + ' >' + item.title + '</option>';
                    });

                    select.html(new_options);

                    if (new_options.length == 0) {
                        select.prop('disabled');
                    }
                    else {
                        select.removeProp('disabled');
                    }

                },
                url       : ajaxurl
            });
        },
        add_updater_functions = function () {
            $(document).off('click', '.ajax-active-campaign-updater-list');
            $(document).off('click', '.ajax-active-campaign-updater-tags');
            $(document).off('click', '.ajax-active-campaign-updater-field');
            $(document).off('change', '.list-select');

            // add updater button handler
            $(document).on('click', '.ajax-active-campaign-updater-list', handle_lists);
            $(document).on('click', '.ajax-active-campaign-updater-tags', handle_tags);
            $(document).on('click', '.ajax-active-campaign-updater-field', handle_fields);
            $(document).on('change', '.list-select', function () {
                var t = $(this).parents().find('.ajax-active-campaign-updater-tags').click();
                var t = $(this).parents().find('.ajax-active-campaign-updater-field').click();
            });
        };

    $('body').on('add_updater_handler', add_updater_functions);
    add_updater_functions();

    // add dependencies handler
    $('#yith_wcac_checkout_trigger').on('change', function () {
        var t = $(this),
            subscription_checkbox = $('#yith_wcac_checkout_subscription_checkbox'),
            contact_status = $('#yith_wcac_contact_status');

        if (t.val() != 'never') {
            subscription_checkbox.parents('tr').show();
            contact_status.parents('tr').show();

            $('#yith_wcac_checkout_subscription_checkbox_label').parents('tr').show();
            $('#yith_wcac_checkout_subscription_checkbox_position').parents('tr').show();
            $('#yith_wcac_checkout_subscription_checkbox_default').parents('tr').show();
            $('#yith_wcac_future_responders').parents('tr').show();
            $('#yith_wcac_send_last_message').parents('tr').show();
            $('#yith_wcac_update_existing').parents('tr').show();
            $('#yith_wcac_instant_responders').parents('tr').show();

            subscription_checkbox.change();
            contact_status.change();
        }
        else {
            subscription_checkbox.parents('tr').hide();
            contact_status.parents('tr').hide();

            $('#yith_wcac_checkout_subscription_checkbox_label').parents('tr').hide();
            $('#yith_wcac_checkout_subscription_checkbox_position').parents('tr').hide();
            $('#yith_wcac_checkout_subscription_checkbox_default').parents('tr').hide();
            $('#yith_wcac_future_responders').parents('tr').hide();
            $('#yith_wcac_send_last_message').parents('tr').hide();
            $('#yith_wcac_update_existing').parents('tr').hide();
            $('#yith_wcac_instant_responders').parents('tr').hide();
        }
    }).change();

    $('#yith_wcac_checkout_subscription_checkbox, #yith_wcac_register_subscription_checkbox').on('change', function () {
        var t = $(this);

        if (!t.is(':visible')) {
            return;
        }

        if (t.is(':checked')) {
            $('#yith_wcac_checkout_subscription_checkbox_label, #yith_wcac_register_subscription_checkbox_label').parents('tr').show();
            $('#yith_wcac_checkout_subscription_checkbox_position').parents('tr').show();
            $('#yith_wcac_checkout_subscription_checkbox_default, #yith_wcac_register_subscription_checkbox_default').parents('tr').show();
        }
        else {
            $('#yith_wcac_checkout_subscription_checkbox_label, #yith_wcac_register_subscription_checkbox_label').parents('tr').hide();
            $('#yith_wcac_checkout_subscription_checkbox_position').parents('tr').hide();
            $('#yith_wcac_checkout_subscription_checkbox_default, #yith_wcac_register_subscription_checkbox_default').parents('tr').hide();
        }
    }).change();

    $('#yith_wcac_contact_status').on('change', function () {
        hide_show_instant_responders($(this), '');
    }).change();

    shortcode_status.on('change', function () {
        hide_show_instant_responders($(this), 'shortcode_');
    }).change();

    register_status.on('change', function () {
        hide_show_instant_responders($(this), 'register_');
    }).change();
    widget_status.on('change', function () {
        hide_show_instant_responders($(this), 'widget_');
    }).change();

    // add tagged select2 to tag input
    tag_select.select2({
        allowClear             : true,
        minimumResultsForSearch: Infinity,
        tokenSeparators        : [',', ' ']
    });

    function hide_show_instant_responders(t, context){
            var val = t.val();
        if (!t.is(':visible')) {
            return;
        }
        if (val == 1) {
            $('#yith_wcac_' + context + 'instant_responders').closest('tr').show();
        }
        else {
            $('#yith_wcac_' + context + 'instant_responders').closest('tr').hide();
        }
    }
});