<?php
/**
 * Admin class
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Active Campaign
 * @version 1.0.0
 */

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_WCAC' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCAC_Admin' ) ) {
	/**
	 * WooCommerce Active Campaign Admin
	 *
	 * @since 1.0.0
	 */
	class YITH_WCAC_Admin {
		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WCAC_Admin
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Docs url
		 *
		 * @var string Official documentation url
		 * @since 1.0.0
		 */
		public $doc_url = 'https://docs.hitoutlets.com/yith-active-campaign-for-woocommerce/';

		/**
		 * Premium landing url
		 *
		 * @var string Premium landing url
		 * @since 1.0.0
		 */
		public $premium_landing_url = 'http://hitoutlets.com/themes/plugins/yith-woocommerce-active-campaign/';

		/**
		 * List of available tab for active campaign panel
		 *
		 * @var array
		 * @access public
		 * @since  1.0.0
		 */
		public $available_tabs = array();

		/**
		 * Constructor method
		 *
		 * @return \YITH_WCAC_Admin
		 * @since 1.0.0
		 */
		public function __construct() {

			/* === Register plugin to licence/update system === */
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			// sets available tab
			$this->available_tabs = apply_filters( 'yith_wcac_available_admin_tabs', array(
				'integration' => _x( 'Integration', 'Panel tab', 'yith-woocommerce-active-campaign' ),
				'checkout'    => _x( 'Checkout', 'Panel tab', 'yith-woocommerce-active-campaign' ),
				'shortcode'   => _x( 'Shortcode', 'Panel tab', 'yith-woocommerce-active-campaign' ),
				'widget'      => _x( 'Widget', 'Panel tab', 'yith-woocommerce-active-campaign' ),
				'register'    => _x( 'Register', 'Panel tab', 'yith-woocommerce-active-campaign' ),
				'export'      => _x( 'Export', 'Panel tab', 'yith-woocommerce-active-campaign' ),
			) );

			// register plugin panel
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'woocommerce_admin_field_yith_wcac_integration_status', array(
				$this,
				'print_custom_yith_wcac_integration_status'
			) );

			// register plugin links & meta row
			add_filter( 'plugin_row_meta', array( $this, 'add_plugin_meta' ), 10, 2 );

			// enqueue style
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );

			// register premium options
			add_action( 'woocommerce_admin_field_yith_wcac_advanced_integration', array(
				$this,
				'print_custom_yith_wcac_advanced_integration'
			) );
			add_action( 'woocommerce_admin_field_yith_wcac_custom_fields', array(
				$this,
				'print_custom_yith_wcac_custom_fields'
			) );
			add_action( 'woocommerce_admin_field_date_range', array( $this, 'print_custom_date_range' ) );

			// adds dashboard widget
			add_action( 'admin_init', array( $this, 'refresh_lists_for_widget' ) );
			add_action( 'wp_dashboard_setup', array( $this, 'register_dashboard_widget' ) );

			// AJAX Handler
			add_action( 'wp_ajax_wcac_add_advanced_panel_item', array( $this, 'print_advanced_integration_item' ) );
			add_action( 'wp_ajax_wcac_add_advanced_panel_field', array( $this, 'print_advanced_integration_field' ) );
			add_action( 'wp_ajax_wcac_add_advanced_panel_condition', array(
				$this,
				'print_advanced_integration_condition'
			) );
			add_action( 'wp_ajax_wcac_add_custom_field', array( $this, 'print_custom_fields_item' ) );

			// handle exports
			add_action( 'yit_panel_wc_after_update', array( $this, 'manage_users_export' ) );
			add_action( 'yit_panel_wc_after_update', array( $this, 'manage_csv_download' ) );
			add_filter( 'pre_update_option_yith_wcac_export_list', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_email_type', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_double_optin', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_update_existing', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_user_set', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_users', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_filter_product', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_filter_category', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_export_filter_tag', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_csv_user_set', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_csv_users', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_csv_filter_product', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_csv_filter_category', '__return_empty_string' );
			add_filter( 'pre_update_option_yith_wcac_csv_filter_tag', '__return_empty_string' );

			// register metabox to show user preferences within the order
			add_action( 'add_meta_boxes', array( $this, 'add_order_metabox' ) );
		}

		/**
		 * Register plugins for activation tab
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function register_plugin_for_activation() {
			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once 'plugin-fw/licence/lib/yit-licence.php';
				require_once 'plugin-fw/licence/lib/yit-plugin-licence.php';
			}

			YIT_Plugin_Licence()->register( YITH_WCAC_INIT, YITH_WCAC_SECRET_KEY, YITH_WCAC_SLUG );
		}

		/**
		 * Register plugins for update tab
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function register_plugin_for_updates() {
			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once( YITH_WCAC_DIR . 'plugin-fw/lib/yit-upgrade.php' );
			}

			YIT_Upgrade()->register( YITH_WCAC_SLUG, YITH_WCAC_INIT );
		}

		/* === PLUGIN PANEL METHODS === */

		/**
		 * Register panel
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function register_panel() {
			$args = array(
				'create_menu_page' => true,
				'parent_slug'      => '',
				'page_title'       => __( 'Active Campaign', 'yith-woocommerce-active-campaign' ),
				'menu_title'       => __( 'Active Campaign', 'yith-woocommerce-active-campaign' ),
				'capability'       => 'manage_options',
				'parent'           => '',
				'parent_page'      => 'yit_plugin_panel',
				'page'             => 'yith_wcac_panel',
				'admin-tabs'       => $this->available_tabs,
				'options-path'     => YITH_WCAC_DIR . 'plugin-options'
			);

			/* === Fixed: not updated theme  === */
			if ( ! class_exists( 'YIT_Plugin_Panel_WooCommerce' ) ) {
				require_once( YITH_WCAC_DIR . 'plugin-fw/lib/yit-plugin-panel-wc.php' );
			}

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Output integration status filed
		 *
		 * @param $value array Array representing the field to print
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_custom_yith_wcac_integration_status( $value ) {

			$result = YITH_WCAC()->do_request( 'account/view' );
			$fname  = isset( $result->fname ) ? $result->fname : false;
			$lname  = isset( $result->lname ) ? $result->lname : false;
			$email  = isset( $result->email ) ? $result->email : false;

			$username = $fname;
			$name     = $fname . ' ' . $lname;
			$avatar   = ( isset( $result->account ) && isset( $result->email ) ) ? '//' . $result->account . '/gravatar.php?h=' . $result->email : false;

			$attributes = array(
				'result'   => $result,
				'fname'    => $fname,
				'lname'    => $lname,
				'email'    => $email,
				'username' => $username,
				'name'     => $name,
				'avatar'   => $avatar,
				'value'    => $value
			);

			yith_wcac_get_template( 'integration-status', $attributes, 'admin/types' );

		}

		/* === PRINT CUSTOM TYPE FIELDS FOR PANEL === */

		/**
		 * Prints the template for advanced checkout options
		 *
		 * @param $value array Array of field settings
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_custom_yith_wcac_advanced_integration( $value ) {
			// init customer object, to handle checkout fields

			// Session class, handles session data for users - can be overwritten if custom handler is needed
			$session_class = apply_filters( 'woocommerce_session_handler', 'WC_Session_Handler' );

			include_once( WC()->plugin_path() . '/includes/abstracts/abstract-wc-session.php' );
			include_once( WC()->plugin_path() . '/includes/class-wc-session-handler.php' );

			// Class instances
			WC()->session  = new $session_class();
			WC()->cart     = new WC_Cart();
			WC()->customer = new WC_Customer();

			$advanced_options            = get_option( 'yith_wcac_advanced_integration', array() );
			$advanced_items              = isset( $advanced_options['items'] ) ? $advanced_options['items'] : array();
			$selected_show_tags          = isset( $advanced_options['show_tags'] ) ? $advanced_options['show_tags'] : array();
			$show_tags_label             = isset( $advanced_options['show_tags_label'] ) ? $advanced_options['show_tags_label'] : '';
			$selected_show_tags_position = isset( $advanced_options['show_tags_position'] ) ? $advanced_options['show_tags_position'] : '';

			$tags = YITH_WCAC()->retrieve_tags();

			$attributes = array(
				'advanced_options'            => $advanced_options,
				'advanced_items'              => $advanced_items,
				'selected_show_tags'          => $selected_show_tags,
				'show_tags_label'             => $show_tags_label,
				'selected_show_tags_position' => $selected_show_tags_position,
				'tags'                        => $tags,
				'value'                       => $value
			);

			yith_wcac_get_template( 'advanced-integration', $attributes, 'admin/types' );
		}

		/**
		 * Print advanced integration item
		 *
		 * @param $args array An array with options of the item to output (optional)
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_advanced_integration_item( $args = array() ) {
			$attributes = wp_parse_args( $args, array(
				'item_id'       => isset( $_POST['item_id'] ) ? $_POST['item_id'] : 0,
				'selected_list' => 0,
				'selected_tags' => array(),
				'fields'        => array(),
				'conditions'    => array(),
				'lists'         => YITH_WCAC()->retrieve_lists(),
				'tags'          => YITH_WCAC()->retrieve_tags()
			) );

			yith_wcac_get_template( 'advanced-integration-item', $attributes, 'admin/types' );

			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
				die();
			}
		}

		/**
		 * Print advanced integration item
		 *
		 * @param $args array An array with options of the item to output (optional)
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_advanced_integration_field( $args = array() ) {
			$attributes = wp_parse_args( $args, array(
				'item_id'            => isset( $_POST['item_id'] ) ? $_POST['item_id'] : 0,
				'field_id'           => isset( $_POST['field_id'] ) ? $_POST['field_id'] : 0,
				'selected_list'      => isset( $_POST['list_id'] ) ? $_POST['list_id'] : 0,
				'selected_checkout'  => '',
				'selected_merge_var' => ''
			) );


			$attributes['fields']          = YITH_WCAC()->retrieve_fields();
			$attributes['checkout_fields'] = array();

			// retrieve dynamic checkout fields
			$checkout_fields_raw = WC()->checkout()->checkout_fields;
			if ( ! empty( $checkout_fields_raw ) ) {
				foreach ( $checkout_fields_raw as $category_id => $category ) {
					if ( ! empty( $category ) ) {
						$attributes['checkout_fields'][ $category_id ]['name']   = ucwords( str_replace( '_', ' ', $category_id ) );
						$attributes['checkout_fields'][ $category_id ]['fields'] = array();

						foreach ( $category as $id => $field ) {
							$attributes['checkout_fields'][ $category_id ]['fields'][ $id ] = isset( $field['label'] ) ? $field['label'] : ucwords( str_replace( '_', ' ', $id ) );
						}
					}
				}
			}

			//unset account fields, if needed
			$unset_account_fields = apply_filters( 'yith_wcac_unset_account_fields', true );

			if ( $unset_account_fields ) {
				unset( $attributes['checkout_fields']['account'] );
			}

			// adds custom checkout fields
			$custom_fields = array(
				'shipping_method_title' => __( 'Shipping method name', 'yith-woocommerce-active-campaign' ),
				'payment_method_title'  => __( 'Payment method name', 'yith-woocommerce-active-campaign' ),
				'customer_user'         => __( 'User ID', 'yith-woocommerce-active-campaign' )
			);

			if ( isset( $attributes['checkout_fields']['custom'] ) ) {
				$attributes['checkout_fields']['custom']['fields'] = array_merge(
					$attributes['checkout_fields']['custom']['fields'],
					$custom_fields
				);
			} else {
				$attributes['checkout_fields']['custom']['name']   = __( 'Custom', 'yith-woocommerce-active-campaign' );
				$attributes['checkout_fields']['custom']['fields'] = $custom_fields;
			}


			yith_wcac_get_template( 'advanced-integration-field', $attributes, 'admin/types' );

			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
				die();
			}

		}

		/**
		 * Print advanced integration item
		 *
		 * @param $args array An array with options of the item to output (optional)
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_advanced_integration_condition( $args = array() ) {
			$attributes = wp_parse_args( $args, array(
				'item_id'      => isset( $_POST['item_id'] ) ? $_POST['item_id'] : 0,
				'condition_id' => isset( $_POST['condition_id'] ) ? $_POST['condition_id'] : 0,
				'condition'    => 'product_in_cart',
				'op_set'       => 'contain',
				'op_number'    => 'less_than',
				'products'     => array(),
				'prod_cats'    => array(),
				'order_total'  => 0,
				'custom_key'   => '',
				'op_mixed'     => 'is',
				'custom_value' => ''
			) );

			extract( $attributes );

			$attributes['json_ids'] = array();
			if ( ! empty( $products ) ) {
				$product_ids = ! is_array( $products ) ? array_filter( array_map( 'absint', explode( ',', $products ) ) ) : $products;

				foreach ( $product_ids as $product_id ) {
					$attributes['json_ids'][ $product_id ] = wp_kses_post( get_the_title( $product_id ) );
				}
			}

			yith_wcac_get_template( 'advanced-integration-condition', $attributes, 'admin/types' );

			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
				die();
			}
		}

		/**
		 * Output custom fields type
		 *
		 * @param $value array Array representing the field to print
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_custom_yith_wcac_custom_fields( $value ) {
			extract( $value );

			$id             = $value['id'];
			$fields_options = get_option( $id, array() );
			$list_id        = get_option( str_replace( 'custom_fields', 'active_campaign_list', $id ) );

			$attributes = array_merge( $value, array(
					'id'             => $id,
					'fields_options' => $fields_options,
					'list_id'        => $list_id
				)
			);
			yith_wcac_get_template( 'custom-fields', $attributes, 'admin/types' );
		}

		/**
		 * Prints an item for custom fields type
		 *
		 * @param $args array Array with the argument required for the template
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_custom_fields_item( $args ) {
			$attributes = wp_parse_args( $args, array(
				'id'                 => isset( $_POST['id'] ) ? $_POST['id'] : '',
				'item_id'            => isset( $_POST['item_id'] ) ? $_POST['item_id'] : '',
				'selected_list'      => isset( $_POST['list_id'] ) ? $_POST['list_id'] : 0,
				'selected_name'      => '',
				'selected_merge_var' => '',
				'removable'          => true
			) );

			extract( $args );

			$attributes['fields'] = YITH_WCAC()->retrieve_fields();
			$tab                  = isset( $_REQUEST['tab'] ) ? $_REQUEST['tab'] : '';
			if ( $tab == 'register' ) {
				//On register tab AC email field will be automatic matched with email field. So is no needed here.
				$attributes['fields'] = array_slice( $attributes['fields'], 1, null, true );
			}

			yith_wcac_get_template( 'custom-fields-item', $attributes, 'admin/types' );

			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
				die();
			}
		}

		/**
		 * Print date range type
		 *
		 * @param $value array Array of options used to print field
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_custom_date_range( $value ) {
			yith_wcac_get_template( 'date-range', array( 'value' => $value ), 'admin/types' );
		}

		/**
		 * Enqueue scripts and stuffs
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function enqueue() {
			$path   = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '/unminified' : '';
			$prefix = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '' : '.min';
			$screen = get_current_screen();

			if( 'shop_order' == $screen->id ){
				wp_enqueue_style( 'yith-wcac-admin', YITH_WCAC_URL . '/assets/css/admin/yith-wcac.css', array(), YITH_WCAC_VERSION );
			}

			if ( 'yith-plugins_page_yith_wcac_panel' == $screen->id ) {
				wp_enqueue_style( 'yith-wcac-admin', YITH_WCAC_URL . '/assets/css/admin/yith-wcac.css', array(), YITH_WCAC_VERSION );
				wp_enqueue_style( 'owl-carousel', YITH_WCAC_URL . '/assets/css/owl.carousel.css', array(), '1.3.3' );
				wp_enqueue_script( 'owl-carousel', YITH_WCAC_URL . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '1.3.3', true );
				wp_enqueue_script( 'yith-wcac-admin', YITH_WCAC_URL . '/assets/js/admin' . $path . '/yith-wcac' . $prefix . '.js', array(
					'jquery',
					'jquery-blockui',
					'select2'
				), YITH_WCAC::YITH_WCAC_VERSION, true );

				wp_localize_script( 'yith-wcac-admin', 'yith_wcac', array(
					'labels'             => array(
						'update_list_button'  => _x( 'Update Lists', 'Backend update list button', 'yith-woocommerce-active-campaign' ),
						'update_group_button' => _x( 'Update Tags', 'Backend update tag button', 'yith-woocommerce-active-campaign' ),
						'update_field_button' => __( 'Update Fields', 'yith-woocommerce-active-campaign' )

					),
					'actions'            => array(
						'yith_wcac_do_request_via_ajax_action' => 'yith_wcac_do_request_via_ajax',
						'yith_wcac_get_fields_via_ajax_action' => 'yith_wcac_get_fields_via_ajax'
					),
					'tab'                => isset( $_REQUEST['tab'] ) ? $_REQUEST['tab'] : '',
					'texts'              => array(
						'email'      => _x( 'Email Address', 'Email label defined by Active Campaign', 'yith-woocommerce-active-campaign' ),
						'first_name' => _x( 'First name', 'First name label defined by Active Campaign', 'yith-woocommerce-active-campaign' ),
						'last_name'  => _x( 'Last name', 'Last name label defined by Active Campaign', 'yith-woocommerce-active-campaign' ),
					),
					'ajax_request_nonce' => wp_create_nonce( 'yith_wcac_ajax_request' )
				) );

				if ( ( isset( $_REQUEST['tab'] ) && $_REQUEST['tab'] == 'checkout' ) || ! isset( $_REQUEST['tab'] ) ) {
					$items        = get_option( 'yith_wcac_advanced_integration', array() );
					$items_number = empty( $items['items'] ) ? 0 : count( $items['items'] );

					wp_enqueue_script( 'yith-wcac-advanced-panel', YITH_WCAC_URL . 'assets/js/admin' . $path . '/yith-wcac-advanced-panel' . $prefix . '.js', array(
						'jquery',
						'jquery-blockui'
					), YITH_WCAC_VERSION, true );
					wp_enqueue_style( 'yith-wcac-advanced-panel', YITH_WCAC_URL . 'assets/css/admin/yith-wcac-advanced-panel.css', array(), YITH_WCAC_VERSION );

					wp_localize_script(
						'yith-wcac-advanced-panel',
						'yith_wcac_advanced_panel',
						array(
							'actions' => array(
								'wcac_add_advanced_panel_item_action'      => 'wcac_add_advanced_panel_item',
								'wcac_add_advanced_panel_field_action'     => 'wcac_add_advanced_panel_field',
								'wcac_add_advanced_panel_condition_action' => 'wcac_add_advanced_panel_condition'
							),
							'item_id' => ++ $items_number
						)
					);

				}

				if ( isset( $_REQUEST['tab'] ) && ( $_REQUEST['tab'] == 'shortcode' | $_REQUEST['tab'] == 'widget' | $_REQUEST['tab'] == 'register' ) ) {
					$items        = get_option( 'yith_wcac_shortcode_custom_fields', array() );
					$items_number = count( $items );

					wp_enqueue_script( 'yith-wcac-custom-fields', YITH_WCAC_URL . 'assets/js/admin' . $path . '/yith-wcac-custom-fields' . $prefix . '.js', array(
						'jquery',
						'jquery-blockui',
						'jquery-ui-sortable'
					), YITH_WCAC_VERSION, true );
					wp_enqueue_style( 'yith-wcac-custom-fields', YITH_WCAC_URL . 'assets/css/admin/yith-wcac-custom-fields.css', array(), YITH_WCAC_VERSION );

					wp_localize_script(
						'yith-wcac-custom-fields',
						'yith_wcac_custom_fields',
						array(
							'actions' => array(
								'wcac_add_custom_field_action' => 'wcac_add_custom_field'
							),
							'item_id' => ++ $items_number
						)
					);
				}

				if ( isset( $_REQUEST['tab'] ) && $_REQUEST['tab'] == 'export' ) {
					wp_enqueue_script( 'yith-wcac-export-panel', YITH_WCAC_URL . 'assets/js/admin' . $path . '/yith-wcac-export-panel' . $prefix . '.js', array( 'jquery' ), YITH_WCAC_VERSION, true );

					wp_localize_script( 'yith-wcac-export-panel', 'yith_wcac_export_panel', array(
						'labels' => array(
							'export_users' => __( 'Export Users', 'yith-woocommerce-active-campaign' ),
							'download_csv' => __( 'Download CSV', 'yith-woocommerce-active-campaign' )
						)
					) );
				}

			}
		}

		/* === PLUGIN LINK METHODS === */

		/**
		 * Get the premium landing uri
		 *
		 * @since   1.0.0
		 * @author  Andrea Grillo <andrea.grillo@hitoutlets.com>
		 * @return  string The premium landing link
		 */
		public function get_premium_landing_uri() {
			return defined( 'YITH_REFER_ID' ) ? $this->premium_landing_url . '?refer_id=' . YITH_REFER_ID : $this->premium_landing_url . '?refer_id=1030585';
		}

		/**
		 * Adds plugin row meta
		 *
		 * @param $plugin_meta array Array of unfiltered plugin meta
		 * @param $plugin_file string Plugin base file path
		 *
		 * @return array Filtered array of plugin meta
		 * @since 1.0.0
		 */
		public function add_plugin_meta( $plugin_meta, $plugin_file ) {
			if ( $plugin_file == plugin_basename( YITH_WCAC_DIR . 'init.php' ) ) {
				// documentation link
				$plugin_meta['documentation'] = '<a target="_blank" href="' . $this->doc_url . '">' . __( 'Plugin Documentation', 'yith-woocommerce-active-campaign' ) . '</a>';
			}

			return $plugin_meta;
		}

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WCAC_Admin
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( YITH_WCAC_Admin::$instance ) ) {
				YITH_WCAC_Admin::$instance = new YITH_WCAC_Admin;
			}

			return YITH_WCAC_Admin::$instance;
		}

		/* === HANDLE DASHBOARD WIDGET === */

		/**
		 * Performs a request to active campaign to update lists stats
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function refresh_lists_for_widget() {
			if ( isset( $_GET['refresh_lists_nonce'] ) && wp_verify_nonce( $_GET['refresh_lists_nonce'], 'refresh_lists_action' ) ) {
				YITH_WCAC()->retrieve_lists();

				wp_redirect( esc_url_raw( remove_query_arg( 'refresh_lists_nonce' ) ) );
			}
		}

		/**
		 * Register dashboard widget
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function register_dashboard_widget() {
			wp_add_dashboard_widget( 'yith_wcac_dashboard_widget', 'Active Campaign Lists Stats', array(
				$this,
				'print_dashboard_widget'
			) );
		}

		/**
		 * Print dashboard widget
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_dashboard_widget() {
			$attributes = array();
			$result     = YITH_WCAC()->do_request( 'account/view' );
			$fname      = isset( $result->fname ) ? $result->fname : false;
			$lname      = isset( $result->lname ) ? $result->lname : false;

			$attributes['email']    = isset( $result->email ) ? $result->email : false;
			$attributes['username'] = $fname;
			$attributes['name']     = $fname . ' ' . $lname;
			$attributes['avatar']   = ( isset( $result->account ) && isset( $result->email ) ) ? '//' . $result->account . '/gravatar.php?h=' . $result->email : false;

			$attributes['lists'] = YITH_WCAC()->retrieve_full_lists();

			yith_wcac_get_template( 'active-campaign-dashboard-widget', $attributes, 'admin' );
		}

		/* === HANDLE EXPORT === */

		/**
		 * Manage user export, requesting a batch subscribe
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function manage_users_export() {
			if ( isset( $_POST['export_users'] ) && isset( $_POST['yith_wcac_export_list'] ) ) {

				$export_list               = isset( $_POST['yith_wcac_export_list'] ) ? $_POST['yith_wcac_export_list'] : '';
				$export_status             = isset( $_POST['yith_wcac_export_status'] ) ? $_POST['yith_wcac_export_status'] : '';
				$export_instant_responders = isset( $_POST['yith_wcac_export_instant_responders'] ) ? $_POST['yith_wcac_export_instant_responders'] : '';
				$export_no_responders      = isset( $_POST['yith_wcac_export_no_responders'] ) ? $_POST['yith_wcac_export_no_responders'] : '';
				$users                     = $this->_retrieve_users_to_export( array(
					'user_set'         => isset( $_POST['yith_wcac_export_user_set'] ) ? $_POST['yith_wcac_export_user_set'] : 'all',
					'users_selected'   => isset( $_POST['yith_wcac_export_users'] ) ? $_POST['yith_wcac_export_users'] : '',
					'product_filter'   => ! empty( $_POST['yith_wcac_export_filter_product'] ) ? explode( ',', $_POST['yith_wcac_export_filter_product'] ) : array(),
					'category_filter'  => ! empty( $_POST['yith_wcac_export_filter_category'] ) ? explode( ',', $_POST['yith_wcac_export_filter_category'] ) : array(),
					'tag_filter'       => ! empty( $_POST['yith_wcac_export_filter_tag'] ) ? explode( ',', $_POST['yith_wcac_export_filter_tag'] ) : array(),
					'from_date_filter' => isset( $_POST['yith_wcac_export_filter_date']['from'] ) ? $_POST['yith_wcac_export_filter_date']['from'] : '',
					'to_date_filter'   => isset( $_POST['yith_wcac_export_filter_date']['to'] ) ? $_POST['yith_wcac_export_filter_date']['to'] : ''
				) );

				$waiting_products_field = ! empty( $_POST['yith_wcac_export_field_waiting_products'] ) ? $_POST['yith_wcac_export_field_waiting_products'] : '';

				if ( ! empty( $users ) ) {
					foreach ( $users as $user ) {
						$args = array();
						if ( ! empty( $user['user_email'] ) ) {
							$args['email'] = $user['user_email'];

							if ( ! empty( $user['first_name'] ) ) {
								$args['first_name'] = $user['first_name'];
							}
							if ( ! empty( $user['last_name'] ) ) {
								$args['last_name'] = $user['last_name'];
							}
							if ( ! empty( $export_list ) ) {
								$args['p'] = array( $export_list => $export_list );
							}

							if ( ! empty( $export_status ) ) {
								$args['status'] = array( $export_list => $export_status );
							}
							if ( ! empty( $export_instant_responders ) ) {
								if ( '1' == $export_status ) {
									$args['instantresponders'] = array( $export_list => $export_instant_responders );
								}
							}
							if ( ! empty( $export_no_responders ) ) {
								$args['noresponders'] = array( $export_list => $export_no_responders );
							}
							if ( $waiting_products_field && ! empty( $user['waiting_products'] ) ) {
								$args[ 'field[' . $waiting_products_field . ', 0]' ] = $user['waiting_products'];
							}
							$res = YITH_WCAC()->synchronize_contact( $args );
						}
					}
				}
			}
		}

		/**
		 * Manage CSV download
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function manage_csv_download() {
			if ( isset( $_POST['export_csv'] ) ) {
				$users = $this->_retrieve_users_to_export( array(
					'user_set'         => isset( $_POST['yith_wcac_csv_user_set'] ) ? $_POST['yith_wcac_csv_user_set'] : 'all',
					'users_selected'   => isset( $_POST['yith_wcac_csv_users'] ) ? $_POST['yith_wcac_csv_users'] : '',
					'product_filter'   => ! empty( $_POST['yith_wcac_csv_filter_product'] ) ? explode( ',', $_POST['yith_wcac_csv_filter_product'] ) : array(),
					'category_filter'  => ! empty( $_POST['yith_wcac_csv_filter_category'] ) ? explode( ',', $_POST['yith_wcac_csv_filter_category'] ) : array(),
					'tag_filter'       => ! empty( $_POST['yith_wcac_csv_filter_tag'] ) ? explode( ',', $_POST['yith_wcac_csv_filter_tag'] ) : array(),
					'from_date_filter' => isset( $_POST['yith_wcac_csv_filter_date']['from'] ) ? $_POST['yith_wcac_csv_filter_date']['from'] : '',
					'to_date_filter'   => isset( $_POST['yith_wcac_csv_filter_date']['to'] ) ? $_POST['yith_wcac_csv_filter_date']['to'] : ''
				) );
				if ( ! empty( $users ) ) {
					$csv = '';

					// add csv heading
					$csv .= '"Email Address", first_name, last_name';

					if ( isset( $_POST['yith_wcac_csv_user_set'] ) && $_POST['yith_wcac_csv_user_set'] == 'waiting_lists' ) {
						$csv .= ',"Waiting Products"';
					}

					$csv .= "\n";

					foreach ( $users as $user ) {
						$csv .= '"' . $user['user_email'] . '",';
						$csv .= '"' . $user['user_first_name'] . '",';
						$csv .= '"' . $user['user_last_name'] . '"';

						if ( isset( $_POST['yith_wcac_csv_user_set'] ) && $_POST['yith_wcac_csv_user_set'] == 'waiting_lists' ) {
							$csv .= ',"' . $user['waiting_products'] . '"';
						}

						$csv .= "\n";
					}

					header( 'Content-Type: text/csv; charset=utf-8' );
					header( 'Content-Disposition: attachment; filename=data.csv' );

					echo $csv;
					die();
				}
			}
		}

		/**
		 * Retrieve users to export
		 *
		 * @param $args array Array of parameters to use to filter users
		 *
		 * @return array Array of users to export
		 * @since 1.0.0
		 */
		protected function _retrieve_users_to_export( $args = array() ) {
			$args = wp_parse_args(
				$args,
				array(
					'user_set'         => 'all',
					'users_selected'   => '',
					'product_filter'   => '',
					'category_filter'  => '',
					'tag_filter'       => '',
					'from_date_filter' => '',
					'to_date_filter'   => ''
				)
			);

			extract( $args );

			$users_id = array();
			$users    = array();

			switch ( $user_set ) {
				case 'set':
					$users_id = is_serialized( $users_selected ) ? unserialize( $users_selected ) : explode( ',', $users_selected );
					break;
				case 'filter':
					$get_posts_args = array(
						'posts_per_page' => - 1,
						'post_type'      => wc_get_order_types( 'view-orders' ),
						'post_status'    => array( 'wc-processing', 'wc-completed' )
					);

					if ( ! empty( $from_date_filter ) ) {
						$get_posts_args['meta_query'][] = array(
							'key'     => '_completed_date',
							'value'   => $from_date_filter . ' 00:00:00',
							'compare' => '>='
						);
					}

					if ( ! empty( $to_date_filter ) ) {
						$get_posts_args['meta_query'][] = array(
							'key'     => '_completed_date',
							'value'   => $to_date_filter . ' 23:59:59',
							'compare' => '<='
						);
					}

					$query  = new WP_Query( $get_posts_args );
					$orders = $query->get_posts();

					if ( ! empty( $orders ) ) {
						foreach ( $orders as $order ) {
							$order_obj   = wc_get_order( $order->ID );
							$customer    = yit_get_prop( $order_obj, 'customer_user' );
							$order_items = $order_obj->get_items( 'line_item' );

							if ( ( ! empty( $product_filter ) || ! empty( $category_filter ) || ! empty( $tag_filter ) ) && ! empty( $order_items ) ) {
								foreach ( $order_items as $item_id => $item ) {
									$valid        = true;
									$product_id   = $item['product_id'];
									$variation_id = $item['variation_id'];

									// filter for products, if any selected
									if ( ! empty( $product_filter ) && ! ( in_array( $product_id, $product_filter ) || in_array( $variation_id, $product_filter ) ) ) {
										$valid = false;
										continue;
									}

									// filter for category, if any selected
									if ( ! empty( $category_filter ) ) {
										$product_cats    = get_the_terms( $product_id, 'product_cat' );
										$product_cat_ids = array();

										if ( ! empty( $product_cats ) ) {
											foreach ( $product_cats as $term ) {
												$product_cat_ids[] = $term->term_id;
											}
										}

										$cat_intersect = array_intersect( $product_cat_ids, $category_filter );

										if ( empty( $cat_intersect ) ) {
											$valid = false;
											continue;
										}
									}

									// filter for tag, if any selected
									if ( ! empty( $tag_filter ) ) {
										$product_tags    = get_the_terms( $product_id, 'product_tag' );
										$product_tag_ids = array();

										if ( ! empty( $product_tags ) ) {
											foreach ( $product_tags as $tag ) {
												$product_tag_ids[] = $tag->term_id;
											}
										}

										$tag_intersect = array_intersect( $product_tag_ids, $tag_filter );

										if ( empty( $tag_intersect ) ) {
											$valid = false;
											continue;
										}
									}

									if ( $valid && ! in_array( $customer, $users_id ) && ! empty( $customer ) ) {
										$users_id[] = $customer;
									}
								}
							} else {
								if ( ! in_array( $customer, $users_id ) ) {
									$users_id[] = $customer;
								}
							}
						}
					}

					break;
				case 'customers':
					$users_selected = get_users( array(
						'meta_key'   => 'paying_customer',
						'meta_value' => 1,
						'fields'     => array( 'ID' )
					) );

					if ( ! empty( $users_selected ) ) {
						foreach ( $users_selected as $user_obj ) {
							$users_id[] = $user_obj->ID;
						}
					}
					break;
				case 'waiting_lists':
					global $wpdb;

					$formatted_set = array();
					$waiting_lists = $wpdb->get_results( $wpdb->prepare( "SELECT pm.post_id AS product_id, p.post_name AS product_slug, pm.meta_value AS list FROM {$wpdb->postmeta} AS pm LEFT JOIN {$wpdb->posts} AS p ON pm.post_id = p.ID WHERE pm.meta_key = %s", '_yith_wcwtl_users_list' ), ARRAY_A );

					if ( ! empty( $waiting_lists ) ) {
						foreach ( $waiting_lists as $record ) {
							$waiting_list = maybe_unserialize( $record['list'] );
							$product_slug = $record['product_slug'];

							if ( ! empty( $waiting_list ) ) {
								foreach ( $waiting_list as $registered_email ) {
									if ( ! in_array( $registered_email, array_keys( $formatted_set ) ) ) {
										$formatted_set[ $registered_email ] = array();
									}

									$formatted_set[ $registered_email ][] = $product_slug;
								}
							}
						}
					}

					if ( ! empty( $formatted_set ) ) {
						foreach ( $formatted_set as $email => $products ) {
							$users[] = array(
								'user_email'       => $email,
								'waiting_products' => implode( ', ', $products ),
								'user_first_name'  => false,
								'user_last_name'   => false,
							);
						}
					}
					break;
				case 'all':
				default:
					$users_selected = get_users( array(
						'fields' => array( 'ID' )
					) );

					if ( ! empty( $users_selected ) ) {
						foreach ( $users_selected as $user_obj ) {
							$users_id[] = $user_obj->ID;
						}
					}
					break;
			}

			if ( ! empty( $users_id ) ) {
				foreach ( $users_id as $id ) {
					$user = get_user_by( 'id', $id );

					if ( ! $user ) {
						continue;
					}

					$users[] = array(
						'id'              => $id,
						'user_email'      => $user->user_email,
						'user_first_name' => ! empty( $user->billing_first_name ) ? $user->billing_first_name : $user->first_name,
						'user_last_name'  => ! empty( $user->billing_last_name ) ? $user->billing_last_name : $user->last_name,
					);
				}
			}

			return $users;
		}

		/* === HANDLE METABOX === */
		
		/**
		 * Add metabox to order edit page
		 *
		 * @return void
		 * @since 1.1.3
		 */
		public function add_order_metabox() {
			add_meta_box( 'yith_wcac_user_preferences', __( 'Active Campaign status', 'yith-woocommerce-active-campaign' ), array( $this, 'print_user_preferences_metabox' ), 'shop_order', 'side' );
		}

		/**
		 * Print metabox for order edit page
		 *
		 * @return void
		 */
		public function print_user_preferences_metabox( $post ) {
			$order = wc_get_order( $post );

			if( ! $order ){
				return;
			}

			$show_checkbox = yit_get_prop( $order, '_yith_wcac_show_checkbox', true );
			$submitted_value = yit_get_prop( $order, '_yith_wcac_submitted_value', true );
			$customer_subscribed = yit_get_prop( $order, '_yith_wcac_customer_subscribed', true );
			$personal_data = yit_get_prop( $order, '_yith_wcac_personal_data', true );

			include( YITH_WCAC_DIR . 'templates/admin/metaboxes/user-preferences-metabox.php' );
		}
	}
}

/**
 * Unique access to instance of YITH_WCAC_Admin class
 *
 * @return \YITH_WCAC_Admin
 * @since 1.0.0
 */
function YITH_WCAC_Admin() {
	$instance = apply_filters( 'yith_wcac_admin_single_instance', null );

	if ( ! $instance ) {
		$instance = YITH_WCAC_Admin::get_instance();
	}

	return $instance;
}
