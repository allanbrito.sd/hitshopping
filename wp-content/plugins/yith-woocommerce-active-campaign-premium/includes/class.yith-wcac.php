<?php
/**
 * Main class
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Active Campaign
 * @version 1.0.0
 */

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_WCAC' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCAC' ) ) {
	/**
	 * WooCommerce Active Campaign
	 *
	 * @since 1.0.0
	 */
	class YITH_WCAC {
		/**
		 * Plugin version
		 *
		 * @const string
		 * @since 1.0.0
		 */
		const YITH_WCAC_VERSION = '1.0.0';

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WCAC
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Active Campaign API wrapper class
		 *
		 * @var \ActiveCampaign
		 * @since 1.0.0
		 */
		protected $active_campaign = null;

		/**
		 * Cachable requests
		 *
		 * @var array
		 * @since 1.0.0
		 */
		public $cachable_requests = array();

		/**
		 * Constructor.
		 *
		 * @return \YITH_WCAC
		 * @since 1.0.0
		 */
		public function __construct() {
			do_action( 'yith_wcac_startup' );

			// init cachable requests
			$this->cachable_requests = apply_filters( 'yith_wcac_cachable_requests', array(
				'list/list',
				'list/field/view?ids=all',
				'tags/list',
			) );

			// load plugin-fw
			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'plugins_loaded', array( $this, 'privacy_loader' ), 20 );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );

			// init api key
			add_action( 'update_option_yith_wcac_active_campaign_api_url', array( $this, 'init_api' ) );
			add_action( 'update_option_yith_wcac_active_campaign_api_key', array( $this, 'init_api' ) );
			$this->init_api();

			// handle ajax requests
			add_action( 'wp_ajax_yith_wcac_do_request_via_ajax', array( $this, 'yith_wcac_do_request_via_ajax' ) );
			add_action( 'wp_ajax_yith_wcac_get_fields_via_ajax', array( $this, 'yith_wcac_get_fields_via_ajax' ) );

			// update checkout page
			add_action( 'init', array( $this, 'add_subscription_checkout' ) );

			// update register page
			add_action( 'woocommerce_register_form', array( $this, 'add_subscription_register' ) );

			// register subscription functions
			add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'adds_order_meta' ), 10, 1 );
			add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'subscribe_on_checkout' ), 10, 1 );
			add_action( 'woocommerce_order_status_completed', array( $this, 'subscribe_on_completed' ), 15, 1 );

			// Shortcode
			add_action( 'init', array( $this, 'register_shortcode' ) );
			add_action( 'init', array( $this, 'post_form_subscribe' ) );
			add_action( 'wp_ajax_yith_wcac_subscribe', array( $this, 'ajax_form_subscribe' ) );
			add_action( 'wp_ajax_nopriv_yith_wcac_subscribe', array( $this, 'ajax_form_subscribe' ) );

			// Inits widget
			add_action( 'widgets_init', array( $this, 'register_widget' ) );

			// Register page
			add_action( 'user_register', array( $this, 'register_subscribe' ) );

		}

		/* === PLUGIN FW LOADER === */

		/**
		 * Loads plugin fw, if not yet created
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once( $plugin_fw_file );
				}
			}
		}

		/* === PRIVACY LOADER === */

		/**
		 * Loads privacy class
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function privacy_loader() {
			if( class_exists( 'YITH_Privacy_Plugin_Abstract' ) ) {
				require_once( YITH_WCAC_INC . 'class.yith-wcac-privacy.php' );
				new YITH_WCAC_Privacy();
			}
		}

		/* === ENQUEUE SCRIPTS === */

		/**
		 * Enqueue scripts
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function enqueue() {
			$path   = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '/unminified' : '';
			$prefix = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '' : '.min';

			wp_enqueue_script( 'yith-wcac', YITH_WCAC_URL . 'assets/js' . $path . '/yith-wcac' . $prefix . '.js', array(
				'jquery',
				'jquery-blockui'
			), YITH_WCAC_VERSION, true );

			wp_localize_script( 'yith-wcac', 'yith_wcac', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'actions'  => array(
					'yith_wcac_subscribe_action' => 'yith_wcac_subscribe'
				)
			) );
		}

		function enqueue_form() {
			global $wp_scripts, $woocommerce;

			// Enqueue scripts to form.
			$path   = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '/unminified' : '';
			$prefix = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '' : '.min';

			$jquery_version = isset( $wp_scripts->registered['jquery-ui-core']->ver ) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.11.4';

			wp_register_style( 'jquery-ui-style', '//code.jquery.com/ui/' . $jquery_version . '/themes/smoothness/jquery-ui.min.css', array(), $jquery_version );
			wp_register_style( 'wc_select2', WC()->plugin_url() . '/assets/css/select2.css', array(), WC()->version );
			wp_register_style( 'yith-wcac-subscription-form-style', YITH_WCAC_URL . '/assets/css/frontend/yith-wcac-subscription-form.css', array( 'jquery-ui-style' ), YITH_WCAC::YITH_WCAC_VERSION );
			wp_enqueue_style( 'wc_select2' );
			wp_enqueue_style( 'yith-wcac-subscription-form-style' );

			wp_register_script( 'yith-wcac-subscription-form-script', YITH_WCAC_URL . '/assets/js/frontend' . $path . '/yith-wcac-subscription-form' . $prefix . '.js', array(
				'jquery',
				'jquery-ui-datepicker',
				'jquery-blockui',
				'select2'
			), YITH_WCAC::YITH_WCAC_VERSION, true );
			wp_enqueue_script( 'yith-wcac-subscription-form-script' );

			do_action( 'before_newsletter_subscription_form' );
		}

		/* === HANDLE REQUEST TO ACTIVE CAMPAIGN === */

		/**
		 * Init Api class
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function init_api() {
			$api_url = get_option( 'yith_wcac_active_campaign_api_url' );
			$api_key = get_option( 'yith_wcac_active_campaign_api_key' );
			if ( ! empty( $api_url ) && ! empty( $api_key ) ) {
				set_time_limit( 0 );
				$this->active_campaign = new ActiveCampaign( $api_url, $api_key );
			} else {
				$this->active_campaign = null;
			}
		}

		/**
		 * Retrieve lists registered for current API Key
		 *
		 * @return array Array of available list, in id -> name format
		 * @since 1.0.0
		 */
		public function retrieve_lists() {
			$lists        = $this->do_request( 'list/list', array( 'ids' => 'all', 'full' => '0' ) );
			$counter      = 0;
			$list_options = array();

			while ( isset( $lists->$counter ) ) {
				$list_options[ $lists->$counter->id ] = $lists->$counter->name;
				$counter ++;
			}

			return $list_options;
		}

		/**
		 * Retrieve lists registered for current API Key with all data
		 *
		 * @return array Array of available list with all data
		 * @since 1.0.0
		 */
		public function retrieve_full_lists() {
			$lists        = $this->do_request( 'list/list', array( 'ids' => 'all', 'full' => '' ) );
			$counter      = 0;
			$list_options = array();

			while ( isset( $lists->$counter ) ) {
				$list_options[ $lists->$counter->id ] = array(
					'name'             => $lists->$counter->name,
					'subscriber_count' => $lists->$counter->subscriber_count
				);
				$counter ++;
			}

			return $list_options;
		}

		/**
		 * Retrieve tags registered for current API Key
		 *
		 * @return array Array of available tags
		 * @since 1.0.0
		 */
		public function retrieve_tags() {
			$tags_options = array();
			$tags         = json_decode( $this->do_request( 'tags/list' ) );
			if ( ! empty( $tags ) ) {
				foreach ( $tags as $tag ) {
					$tags_options[ $tag->name ] = $tag->name;
				}
			}

			return $tags_options;
		}

		/**
		 * Synchronize contact defined by args
		 *
		 * @param $args array the arguments that needed on Active Campaign to make synchronization.
		 *
		 * @return mixed API response array with the result request.
		 * @since 1.0.0
		 */
		public function synchronize_contact( $args ) {
			$res = $this->do_request( 'contact/sync', apply_filters( 'yith_wcac_subscribe_args', $args ) );

			return $res;
		}

		/**
		 * Unsubscribe user by email
		 *
		 * @param $args array the arguments that needed on Active Campaign to make synchronization.
		 *
		 * @return mixed API response array with the result request.
		 * @since 1.0.0
		 */
		public function unsubscribe( $list_id, $email ) {
			$args = array(
				'email' => $email,
				'status' => array( $list_id => "2" )
			);

			$res = $this->synchronize_contact( $args );

			return $res;
		}

		/**
		 * Retrieve fields registered for current API Key
		 *
		 * @return array Array of available fields
		 * @since 1.0.0
		 */
		//https://fmateo78660.api-us1.com/admin/api.php?api_key=a8adc408577b3fb1c1a341da996ee574df230d4ab5c82684c56c4186c349b4808978b22a&api_action=list_field_view&api_output=json&ids=all
		public function retrieve_fields( $force_update = false ) {
			$fields_options = array();
			$fields         = $this->do_request( 'list/field/view?ids=all', array(), $force_update );
			if ( ! empty( $fields ) ) {
				$fields_options['email']      = array(
					'title' => __( 'Email Address', 'yith-woocommerce-active-campaign' ),
					'type'  => 'email'
				);
				$fields_options['first_name'] = array(
					'title' => __( 'First name', 'yith-woocommerce-active-campaign' ),
					'type'  => 'text'
				);
				$fields_options['last_name']  = array(
					'title' => __( 'Last name', 'yith-woocommerce-active-campaign' ),
					'type'  => 'text'
				);
				foreach ( $fields as $field ) {
					if ( is_object( $field ) ) {
						$fields_options[ $field->id ] = array(
							'title' => $field->title,
							'type'  => $field->type
						);

						if ( 'dropdown' == $field->type | 'listbox' == $field->type | 'radio' == $field->type | 'checkbox' == $field->type ) {
							$fields_options[ $field->id ]['options'] = $field->options;
						}
					}
				}
			}

			ksort( $fields_options );

			$fields_defaults = array();
			$fields_items    = array();
			foreach ( $fields_options as $key => $item ) {

				if ( is_string( $key ) ) {
					$fields_defaults[ $key ] = $item;
				} else {
					$fields_items[ $key ] = $item;
				}
			}
			$result = array_merge( $fields_items, $fields_defaults );

			return $result;
		}

		/**
		 * Send a request to active campaign servers
		 *
		 * @param $request string API handle to call (e.g. 'lists/list')
		 * @param $args    array Associative array of params to use in the request (default to empty array)
		 *
		 * @return mixed API response (as an associative array)
		 * @since 1.0.0
		 */
		public function do_request( $request, $args = array(), $force_update = false ) {
			if ( is_null( $this->active_campaign ) ) {
				return false;
			}

			$api_url        = get_option( 'yith_wcac_active_campaign_api_url' );
			$api_key        = get_option( 'yith_wcac_active_campaign_api_key' );
			$transient_name = 'yith_wcac_' . md5( $api_url . $api_key );
			$data           = get_transient( $transient_name );
			$args_index     = md5( http_build_query( $args ) );

			if ( in_array( $request, $this->cachable_requests ) && ! $force_update && ! empty( $data ) && isset( $data[ $request ] ) && isset( $data[ $request ][ $args_index ] ) ) {
				return $data[ $request ][ $args_index ];
			}

			try {
				$result = $this->active_campaign->api( $request, $args );

				if ( in_array( $request, $this->cachable_requests ) ) {
					$data[ $request ][ $args_index ] = $result;

					set_transient( $transient_name, $data, apply_filters( 'yith_wcac_transient_expiration', DAY_IN_SECONDS ) );
				}

				return $result;
			} catch ( Exception $e ) {
				return array(
					'status'  => false,
					'code'    => $e->getCode(),
					'message' => $e->getMessage()
				);
			}
		}

		/**
		 * Handles AJAX request, used to call API handles
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function yith_wcac_do_request_via_ajax() {
			// return if not ajax request
			if ( ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				wp_send_json( false );
			}

			// retrieve params for the request
			$request      = isset( $_REQUEST['request'] ) ? trim( $_REQUEST['request'] ) : false;
			$args         = isset( $_REQUEST['args'] ) ? $_REQUEST['args'] : array();
			$force_update = isset( $_REQUEST['force_update'] ) ? boolval( $_REQUEST['force_update'] ) : false;

			// return if required params are missing
			if ( empty( $request ) || empty( $_REQUEST['yith_wcac_ajax_request_nonce'] ) ) {
				wp_send_json( false );
			}

			// return if nonce check fails
			if ( ! wp_verify_nonce( $_REQUEST['yith_wcac_ajax_request_nonce'], 'yith_wcac_ajax_request' ) ) {
				wp_send_json( false );
			}

			// do request
			$result = $this->do_request( $request, $args, $force_update );

			// return json encoded result
			wp_send_json( $result );
		}

		/**
		 * Handles AJAX request, used to call API handles
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function yith_wcac_get_fields_via_ajax() {
			// return if not ajax request
			if ( ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				wp_send_json( false );
			}

			// return if required params are missing
			if ( empty( $_REQUEST['yith_wcac_ajax_request_nonce'] ) ) {
				wp_send_json( false );
			}

			// return if nonce check fails
			if ( ! wp_verify_nonce( $_REQUEST['yith_wcac_ajax_request_nonce'], 'yith_wcac_ajax_request' ) ) {
				wp_send_json( false );
			}

			// do request
			$result = $this->retrieve_fields( true );
			// return json encoded result
			wp_send_json( $result );
		}

		/* === ADDS FRONTEND CHECKBOX === */

		/**
		 * Register action to print subscription items on Checkbox page
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function add_subscription_checkout() {
			$positions_hook_relation = apply_filters( 'yith_wcac_checkbox_position_hook', array(
				'above_customer'    => 'woocommerce_checkout_before_customer_details',
				'below_customer'    => 'woocommerce_checkout_after_customer_details',
				'above_place_order' => 'woocommerce_review_order_before_submit',
				'below_place_order' => 'woocommerce_review_order_after_submit',
				'above_total'       => 'woocommerce_review_order_before_order_total',
				'above_billing'     => 'woocommerce_checkout_billing',
				'below_billing'     => 'woocommerce_after_checkout_billing_form',
				'above_shipping'    => 'woocommerce_checkout_shipping'
			) );

			$trigger           = get_option( 'yith_wcac_checkout_trigger', 'never' );
			$show_checkbox     = 'yes' == get_option( 'yith_wcac_checkout_subscription_checkbox' );
			$checkbox_position = get_option( 'yith_wcac_checkout_subscription_checkbox_position' );

			// Print Checkbox Subscription on Checkout page
			if ( $trigger != 'never' && $show_checkbox ) {
				if ( ! in_array( $checkbox_position, array_keys( $positions_hook_relation ) ) ) {
					$checkbox_position = 'below_customer';
				}

				$hook = $positions_hook_relation[ $checkbox_position ];
				add_action( $hook, array( $this, 'print_subscription_checkbox' ) );
			}

			$advanced_options            = get_option( 'yith_wcac_advanced_integration', array() );
			$selected_show_tags_position = isset( $advanced_options['show_tags_position'] ) ? $advanced_options['show_tags_position'] : 'below_customer';
			$integration_mode            = get_option( 'yith_wcac_active_campaign_integration_mode', false );

			//Print Checks Tags Subscriptions on Checkout page
			if ( $trigger != 'never' && ! empty( $advanced_options['show_tags'] ) && $integration_mode == 'advanced' ) {

				$hook = $positions_hook_relation[ $selected_show_tags_position ];

				add_action( $hook, array( $this, 'print_subscription_tags' ) );
			}
		}

		/**
		 * Prints subscription checkbox
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_subscription_checkbox( $context = 'checkout' ) {
			$label_option   = 'yith_wcac_checkout_subscription_checkbox_label';
			$checked_option = 'yith_wcac_checkout_subscription_checkbox_default';

			if ( 'register' == $context ) {
				$label_option   = 'yith_wcac_register_subscription_checkbox_label';
				$checked_option = 'yith_wcac_register_subscription_checkbox_default';
			}

			$checkbox_label   = get_option( $label_option );
			$checkbox_checked = 'yes' == get_option( $checked_option );

			if ( function_exists( 'wc_privacy_policy_page_id' ) ) {
				$privacy_link   = sprintf( '<a href="%s">%s</a>', get_the_permalink( wc_privacy_policy_page_id() ), apply_filters( 'yith_wcac_privacy_policy_page_label', __( 'Privacy Policy', 'yith-woocommerce-active-campaign' ) ) );
				$checkbox_label = str_replace( '%privacy_policy%', $privacy_link, $checkbox_label );
			}

			$attributes = array(
				'checkbox_label'   => $checkbox_label,
				'checkbox_checked' => $checkbox_checked,
				'context'          => $context
			);

			yith_wcac_get_template( 'active-campaign-subscription-checkbox', $attributes );
		}

		/**
		 * Prints subscription tags
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_subscription_tags() {
			$advanced_options            = get_option( 'yith_wcac_advanced_integration', array() );
			$selected_show_tags          = isset( $advanced_options['show_tags'] ) ? $advanced_options['show_tags'] : array();
			$show_tags_label             = isset( $advanced_options['show_tags_label'] ) ? $advanced_options['show_tags_label'] : '';
			$selected_show_tags_position = isset( $advanced_options['show_tags_position'] ) ? $advanced_options['show_tags_position'] : '';


			$attributes = array(
				'advanced_options'            => $advanced_options,
				'selected_show_tags'          => $selected_show_tags,
				'show_tags_label'             => $show_tags_label,
				'selected_show_tags_position' => $selected_show_tags_position
			);

			yith_wcac_get_template( 'active-campaign-subscription-tags', $attributes );
		}

		/* === HANDLES ORDER SUBSCRIPTION === */

		/**
		 * Adds metas to order post, saving active campaign informations
		 *
		 * @param $order_id int Order id
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function adds_order_meta( $order_id ) {
			$show_checkbox   = 'yes' == get_option( 'yith_wcac_checkout_subscription_checkbox' );
			$submitted_value = isset( $_POST['yith_wcac_subscribe_me'] ) ? 'yes' : 'no';
			$subscribe_tags  = isset( $_POST['yith_wcac_subscribe_tags'] ) ? $_POST['yith_wcac_subscribe_tags'] : 'no';

			update_post_meta( $order_id, '_yith_wcac_show_checkbox', $show_checkbox );
			update_post_meta( $order_id, '_yith_wcac_submitted_value', $submitted_value );
			update_post_meta( $order_id, '_yith_wcac_subscribe_tags', $subscribe_tags );
		}

		/**
		 * Subscribe user to newsletter (called on order placed)
		 *
		 * @param $order_id int Order id
		 *
		 * @return bool Status of the operation
		 * @since 1.0.0
		 */
		public function subscribe_on_checkout( $order_id ) {
			$trigger         = get_option( 'yith_wcac_checkout_trigger' );
			$show_checkbox   = get_post_meta( $order_id, '_yith_wcac_show_checkbox', true );
			$submitted_value = get_post_meta( $order_id, '_yith_wcac_submitted_value', true );

			// return if admin don't want to subscribe users at this point
			if ( $trigger != 'created' ) {
				return false;
			}

			// return if subscription checkbox is printed, but not submitted
			if ( $show_checkbox && $submitted_value == 'no' ) {
				return false;
			}

			return $this->order_subscribe( $order_id );
		}

		/**
		 * Subscribe user to newsletter (called on order completed)
		 *
		 * @param $order_id int Order id
		 *
		 * @return bool Status of the operation
		 * @since 1.0.0
		 */
		public function subscribe_on_completed( $order_id ) {
			$trigger         = get_option( 'yith_wcac_checkout_trigger' );
			$show_checkbox   = get_post_meta( $order_id, '_yith_wcac_show_checkbox', true );
			$submitted_value = get_post_meta( $order_id, '_yith_wcac_submitted_value', true );
			// return if admin don't want to subscribe users at this point
			if ( $trigger != 'completed' ) {
				return false;
			}

			// return if subscription checkbox is printed, but not submitted
			if ( $show_checkbox && $submitted_value == 'no' ) {
				return false;
			}

			return $this->order_subscribe( $order_id );
		}

		/**
		 * Call subscribe API handle, to register user to a specific list
		 *
		 * @param $order_id int Order id
		 *
		 * @return bool status of the operation
		 */
		public function order_subscribe( $order_id, $args = array() ) {
			$order = wc_get_order( $order_id );
			$res   = false;

			$list_id            = get_option( 'yith_wcac_active_campaign_list' );
			$contact_status     = get_option( 'yith_wcac_contact_status' );
			$instant_responders = get_option( 'yith_wcac_instant_responders' );
			$future_responders  = get_option( 'yith_wcac_future_responders' );
			$send_last_message  = get_option( 'yith_wcac_send_last_message' );
			$integration_mode   = get_option( 'yith_wcac_active_campaign_integration_mode', 'simple' );
			$email              = yit_get_prop( $order, 'billing_email', true );
			$first_name         = yit_get_prop( $order, 'billing_first_name', true );
			$last_name          = yit_get_prop( $order, 'billing_last_name', true );
			$user_id            = yit_get_prop( $order, 'customer_user', true );

			if ( empty( $list_id ) ) {
				return false;
			}
			if ( 'simple' == $integration_mode ) {
				$selected_tags = get_option( 'yith_wcac_active_campaign_tags', array() );

				$args = array_merge(
					array(
						'email'        => yit_get_prop( $order, 'billing_email' ),
						'first_name'   => yit_get_prop( $order, 'billing_first_name' ),
						'last_name'    => yit_get_prop( $order, 'billing_last_name' ),
						'tags'         => $selected_tags,
						'p'            => array(
							$list_id => $list_id
						),
						'status'       => array(
							$list_id => $contact_status
						),
						'noresponders' => array(
							$list_id => ! $future_responders
						),
						'lastmessage'  => array(
							$list_id => $send_last_message
						),
					),
					( 1 == $contact_status ) ? array(
						'instantresponders' => array(
							$list_id => $instant_responders
						)
					) : array()
					, $args );

				$res = $this->synchronize_order_contact( $order_id, $args );

				if( $res ) {
					$this->_register_customer_subscribed_lists( isset( $args['p'] ) ? array_pop( array_values( $args['p'] ) ) : $list_id, $email, $user_id, $order_id );

					// register personal data
					$personal_data = apply_filters( 'yith_wcac_customer_personal_data', array(
						'billing_first_name' => array( 'label' => __( 'First name', 'yith-woocommerce-active-campaign' ), 'value' => $first_name ),
						'billing_last_name' => array( 'label' => __( 'Last name', 'yith-woocommerce-active-campaign' ), 'value' => $last_name ),
						'billing_email' => array( 'label' => __( 'Email', 'yith-woocommerce-active-campaign' ), 'value' => $email )
					), $args, $order_id );

					if( ! empty( $personal_data ) ) {
						$this->_register_customer_personal_data( $order_id, $personal_data );
					}

					yit_save_prop( $order, '_yith_wcac_customer_subscribed', true );
				}
			} else {
				$res              = true;
				$advanced_options = get_option( 'yith_wcac_advanced_integration', array() );
				$advanced_items   = isset( $advanced_options['items'] ) ? $advanced_options['items'] : array();
				$subscribe_tags   = get_post_meta( $order_id, '_yith_wcac_subscribe_tags', true );
				$subscribe_tags   = is_array( $subscribe_tags ) ? array_keys( $subscribe_tags ) : array();

				$args['tags']         = ! empty( $subscribe_tags ) ? $subscribe_tags : array();
				$args['email']        = $email;
				$args['first_name']   = $first_name;
				$args['last_name']    = $last_name;
				$args['status']       = array(
					$list_id => $contact_status
				);
				$args['noresponders'] = array(
					$list_id => ! $future_responders
				);
				$args['lastmessage']  = array(
					$list_id => $send_last_message
				);
				$args['p']            = array(
					$list_id => $list_id
				);

				if ( 1 == $contact_status ) {
					$args['instantresponders'] = array(
						$list_id => $instant_responders
					);
				}

				if ( ! empty( $advanced_items ) ) {
					foreach ( $advanced_items as $option ) {

						// Checks conditions
						$selected_conditions = isset( $option['conditions'] ) ? $option['conditions'] : array();
						if ( ! empty( $selected_conditions ) ) {
							if ( ! $this->_check_conditions( $selected_conditions, $order_id ) ) {
								continue;
							}
						}

						$args['tags']         = isset( $option['tags'] ) ? array_merge( $args['tags'], $option['tags'] ) : $args['tags'];
						$args['status']       = array(
							$option['list'] => $contact_status
						);
						$args['noresponders'] = array(
							$option['list'] => ! $future_responders
						);
						$args['lastmessage']  = array(
							$option['list'] => $send_last_message
						);
						// set list id to current section
						$args['p'] = array(
							$option['list'] => $option['list']
						);

						// manage fields
						$selected_fields = isset( $option['fields'] ) ? $option['fields'] : array();

						$field_structure = $this->_create_field_structure( $selected_fields, $order_id );
						if ( ! empty( $field_structure ) ) {
							foreach ( $field_structure as $key => $field_item ) {
								if ( 'first_name' == $key ) {
									$args['first_name'] = $field_item;
								} elseif ( 'last_name' == $key ) {
									$args['last_name'] = $field_item;
								} elseif ( 'email' == $key ) {
									$args['email'] = $field_item;
								} else {
									$args[ 'field[' . $key . ',0]' ] = $field_item;
								}
							}
						}

						$res = $this->synchronize_order_contact( $order_id, $args );

						if( $res ) {
							$this->_register_customer_subscribed_lists( isset( $args['p'] ) ? array_pop( array_values( $args['p'] ) ) : $list_id, $email, $user_id, $order_id );
						}
					}
				} else {
					$res = $this->synchronize_order_contact( $order_id, $args );

					if( $res ) {
						$this->_register_customer_subscribed_lists( isset( $args['p'] ) ? array_pop( array_values( $args['p'] ) ) : $list_id, $email, $user_id, $order_id );
					}
				}
			}
			error_log( print_r( $res, true ) );

			return $res;
		}

		/**
		 * Synchronize contact with Active Campaign access, add or edit the contact
		 *
		 * @param $order_id int Order id
		 * @param $args     array the array arguments.
		 *
		 * @return boolean
		 * @since 1.0.0
		 */
		public function synchronize_order_contact( $order_id, $args ) {

			$order = wc_get_order( $order_id );
			do_action( 'yith_wcac_user_subscribing', $order_id );
			$res = $this->synchronize_contact( $args );

			if ( isset( $res->message ) && isset( $res->result_message ) ) {
				$order->add_order_note( sprintf( '%s: %s', $res->message, $res->result_message ) );
			}

			do_action( 'yith_wcac_user_subscribed', $order_id );

			return $res;

		}

		/**
		 * Create structure to register fields to a specific user
		 *
		 * @param $selected_fields array Array of selected fields to register
		 * @param $order_id        int Order id
		 *
		 * @return array A valid array to use in subscription request
		 * @since 1.0.0
		 */
		protected function _create_field_structure( $selected_fields, $order_id ) {
			if ( empty( $selected_fields ) ) {
				return array();
			}

			$order = wc_get_order( $order_id );

			if ( empty( $order ) ) {
				return array();
			}

			$field_structure = array();

			foreach ( $selected_fields as $field ) {
				$field_value = yit_get_prop( $order, $field['checkout'], true );
				$field_structure[ $field['merge_var'] ] = $field_value;

				$checkout_fields = WC()->checkout()->get_checkout_fields();

				foreach( $checkout_fields as $group => $fields ){
					if( isset( $fields[ $field['checkout'] ] ) ){
						$label = $fields[ $field['checkout'] ]['label'];
						break;
					}
				}

				if( empty( $label ) ){
					$label = $field['checkout'];
				}

				$this->_register_customer_personal_data( $order_id, $field['checkout'], $label, $field_value );
			}

			return $field_structure;
		}

		/**
		 * Check if selected conditions are matched
		 *
		 * @param $selected_conditions array Array of selected conditions to match
		 * @param $order_id            int Order id
		 *
		 * @return boolean True, if all conditions are matched; false otherwise
		 * @since 1.0.0
		 */
		protected function _check_conditions( $selected_conditions, $order_id ) {
			$order            = wc_get_order( $order_id );
			$condition_result = true;

			if ( empty( $selected_conditions ) ) {
				return true;
			}

			foreach ( $selected_conditions as $condition ) {
				$condition_type = $condition['condition'];
				switch ( $condition_type ) {
					case 'product_in_cart':
						$set_operator      = $condition['op_set'];
						$selected_products = ! is_array( $condition['products'] ) ? explode( ',', $condition['products'] ) : $condition['products'];
						$items             = $order->get_items( 'line_item' );
						$products_in_cart  = array();

						if ( ! empty( $items ) ) {
							foreach ( $items as $item ) {
								if ( is_object( $item ) ) {
									/**
									 * @var $item \WC_Order_Item_Product
									 */
									$products_in_cart[] = $item->get_product_id();
								} else {
									if ( ! empty( $item['product_id'] ) && ! in_array( $item['product_id'], $products_in_cart ) ) {
										$products_in_cart[] = $item['product_id'];
									}

									if ( ! empty( $item['variation_id'] ) && ! in_array( $item['variation_id'], $products_in_cart ) ) {
										$products_in_cart[] = $item['variation_id'];
									}
								}
							}
							$products_in_cart = array_unique( $products_in_cart );
						}

						switch ( $set_operator ) {
							case 'contains_one':

								if ( ! empty( $selected_products ) && ! empty( $products_in_cart ) ) {
									$found = false;
									foreach ( (array) $selected_products as $product ) {
										if ( in_array( $product, $products_in_cart ) ) {
											$found = true;
											break;
										}
									}

									if ( ! $found ) {
										$condition_result = false;
									}
								} elseif ( ! empty( $selected_products ) ) {
									$condition_result = false;
								}

								break;
							case 'contains_all':

								if ( ! empty( $selected_products ) && ! empty( $products_in_cart ) ) {
									foreach ( (array) $selected_products as $product ) {
										if ( ! in_array( $product, $products_in_cart ) ) {
											$condition_result = false;
											break;
										}
									}
								} elseif ( ! empty( $selected_products ) ) {
									$condition_result = false;
								}

								break;
							case 'not_contain':

								if ( ! empty( $selected_products ) && ! empty( $products_in_cart ) ) {
									foreach ( (array) $selected_products as $product ) {
										if ( in_array( $product, $products_in_cart ) ) {
											$condition_result = false;
											break;
										}
									}
								} elseif ( ! empty( $selected_products ) ) {
									$condition_result = false;
								}

								break;
						}

						break;
					case 'product_cat_in_cart':

						$set_operator  = $condition['op_set'];
						$selected_cats = $condition['prod_cats'];
						$items         = $order->get_items( 'line_item' );
						$cats_in_cart  = array();

						if ( ! empty( $items ) ) {
							foreach ( $items as $item ) {
								/**
								 * @var $item array|\WC_Order_Item_Product
								 */
								$product_id = is_object( $item ) ? $item->get_product_id() : $item['product_id'];
								$item_terms = get_the_terms( $product_id, 'product_cat' );

								if ( ! empty( $item_terms ) ) {
									foreach ( $item_terms as $term ) {
										if ( ! in_array( $term->term_id, $cats_in_cart ) ) {
											$cats_in_cart[] = $term->term_id;
										}
									}
								}
							}
						}

						switch ( $set_operator ) {
							case 'contains_one':

								if ( ! empty( $selected_cats ) && ! empty( $cats_in_cart ) ) {
									$found = false;
									foreach ( (array) $selected_cats as $cat ) {
										if ( in_array( $cat, $cats_in_cart ) ) {
											$found = true;
											break;
										}
									}

									if ( ! $found ) {
										$condition_result = false;
									}
								} elseif ( ! empty( $selected_cats ) ) {
									$condition_result = false;
								}

								break;
							case 'contains_all':

								if ( ! empty( $selected_cats ) && ! empty( $cats_in_cart ) ) {
									foreach ( (array) $selected_cats as $cat ) {
										if ( ! in_array( $cat, $cats_in_cart ) ) {
											$condition_result = false;
											break;
										}
									}
								} elseif ( ! empty( $selected_cats ) ) {
									$condition_result = false;
								}

								break;
							case 'not_contain':

								if ( ! empty( $selected_cats ) && ! empty( $cats_in_cart ) ) {
									foreach ( (array) $selected_cats as $cat ) {
										if ( in_array( $cat, $cats_in_cart ) ) {
											$condition_result = false;
											break;
										}
									}
								} elseif ( ! empty( $selected_cats ) ) {
									$condition_result = false;
								}

								break;
						}

						break;
					case 'order_total':

						$number_operator = $condition['op_number'];
						$threshold       = $condition['order_total'];
						$order_total     = $order->get_total();

						switch ( $number_operator ) {
							case 'less_than':
								if ( ! ( $order_total < $threshold ) ) {
									$condition_result = false;
								}
								break;
							case 'less_or_equal':
								if ( ! ( $order_total <= $threshold ) ) {
									$condition_result = false;
								}
								break;
							case 'equal':
								if ( ! ( $order_total == $threshold ) ) {
									$condition_result = false;
								}
								break;
							case 'greater_or_equal':
								if ( ! ( $order_total >= $threshold ) ) {
									$condition_result = false;
								}
								break;
							case 'greater_than':
								if ( ! ( $order_total > $threshold ) ) {
									$condition_result = false;
								}
								break;
						}

						break;
					case 'custom':

						$operator       = $condition['op_mixed'];
						$field_key      = $condition['custom_key'];
						$expected_value = $condition['custom_value'];

						// retrieve field value (first check in post meta)
						$field = yit_get_prop( $order, $field_key, true );

						// retrieve field value (then check in $_REQUEST superglobal)
						if ( empty( $field ) ) {
							$field = isset( $_REQUEST[ $field_key ] ) ? $_REQUEST[ $field_key ] : '';
						}

						// nothing found? condition failed
						if ( empty( $field ) ) {
							$condition_result = false;
							break;
						}

						switch ( $operator ) {
							case 'is':
								if ( ! ( strcmp( $field, $expected_value ) == 0 ) ) {
									$condition_result = false;
								}
								break;
							case 'not_is':
								if ( ! ( strcmp( $field, $expected_value ) != 0 ) ) {
									$condition_result = false;
								}
								break;
							case 'contains':
								if ( ! ( strpos( $field, $expected_value ) !== false ) ) {
									$condition_result = false;
								}
								break;
							case 'not_contains':
								if ( ! ( strpos( $field, $expected_value ) === false ) ) {
									$condition_result = false;
								}
								break;
							case 'less_than':
								if ( ! ( $field < $expected_value ) ) {
									$condition_result = false;
								}
								break;
							case 'less_or_equal':
								if ( ! ( $field <= $expected_value ) ) {
									$condition_result = false;
								}
								break;
							case 'equal':
								if ( ! ( $field == $expected_value ) ) {
									$condition_result = false;
								}
								break;
							case 'greater_or_equal':
								if ( ! ( $field >= $expected_value ) ) {
									$condition_result = false;
								}
								break;
							case 'greater_than':
								if ( ! ( $field > $expected_value ) ) {
									$condition_result = false;
								}
								break;
						}

						break;
				}

				if ( ! $condition_result ) {
					break;
				}
			}

			return $condition_result;
		}

		/* === HANDLE SHORTCODE === */

		/**
		 * Register newsletter subscription form shortcode
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function register_shortcode() {
			add_shortcode( 'yith_wcac_subscription_form', array( $this, 'print_subscription_form' ) );
		}

		/**
		 * Retrieve default attributes from options, in order to print subscription form
		 *
		 * @param $context string Current context (shortcode/widget)
		 *
		 * @eturn array Array of default args
		 */
		public function get_default_attributes( $context ) {

			/*** GENERATE DEFAULT ATTRIBUTES ***/
			// Array defaults with general attributes
			$defaults = array(
				'list'                         => get_option( 'yith_wcac_' . $context . '_active-campaign_list' ),
				'title'                        => get_option( 'yith_wcac_' . $context . '_title' ),
				'submit_label'                 => get_option( 'yith_wcac_' . $context . '_submit_button_label' ),
				'success_message'              => get_option( 'yith_wcac_' . $context . '_success_message' ),
				'show_privacy_field'           => get_option( 'yith_wcac_' . $context . '_show_privacy_field' ),
				'privacy_label'                => get_option( 'yith_wcac_' . $context . '_privacy_label' ),
				'hide_form_after_registration' => get_option( 'yith_wcac_' . $context . '_hide_after_registration' ),
				'status'                       => get_option( 'yith_wcac_' . $context . '_status' ),
				'instant_responders'           => get_option( 'yith_wcac_' . $context . '_instant_responders' ),
				'no_responders'                => get_option( 'yith_wcac_' . $context . '_no_responders' ),
				'send_last_message'            => get_option( 'yith_wcac_' . $context . '_send_last_message' ),
				'show_tags'                    => get_option( 'yith_wcac_' . $context . '_active-campaign_show_tags' ),
				'tags_label'                   => get_option( 'yith_wcac_' . $context . '_active-campaign_tags_label' ),
				'widget'                       => ( $context != 'widget' ) ? 'no' : 'yes'
			);

			// Set privacy options

			$defaults['show_privacy_field'] = $defaults['show_privacy_field'] == 'yes';

			if ( function_exists( 'wc_privacy_policy_page_id' ) ) {
				$privacy_link              = sprintf( '<a href="%s">%s</a>', get_the_permalink( wc_privacy_policy_page_id() ), apply_filters( 'yith_wcac_privacy_policy_page_label', __( 'Privacy Policy', 'yith-woocommerce-active-campaign' ) ) );
				$defaults['privacy_label'] = str_replace( '%privacy_policy%', $privacy_link, $defaults['privacy_label'] );
			}

			// Set default fields attributes

			$selected_fields = get_option( 'yith_wcac_' . $context . '_custom_fields' );
			$def_fields      = array();
			// Loop selected fields defined from options and implement format on $def_fields
			if ( ! empty( $selected_fields ) ) {
				foreach ( $selected_fields as $field ) {
					$def_fields[ $field['merge_var'] ] = array(
						'name'      => $field['name'],
						'merge_var' => $field['merge_var']
					);
				}
			}

			// Set default tags attributes

			$selected_tags = get_option( 'yith_wcac_' . $context . '_active-campaign_tags' );
			$def_tags      = ! empty( $selected_tags ) ? implode( ',', $selected_tags ) : '';

			// Set default show_tags attributes

			$selected_show_tags = get_option( 'yith_wcac_' . $context . '_active-campaign_show_tags' );
			$def_show_tags      = ! empty( $selected_show_tags ) ? implode( ',', $selected_show_tags ) : '';

			// Set default styles attributes

			// Array with general defaults attributes...
			$style_defaults = array(
				'enable_style'           => get_option( 'yith_wcac_' . $context . '_style_enable' ),
				'round_corners'          => get_option( 'yith_wcac_' . $context . '_subscribe_button_round_corners', 'no' ),
				'background_color'       => get_option( 'yith_wcac_' . $context . '_subscribe_button_background_color' ),
				'text_color'             => get_option( 'yith_wcac_' . $context . '_subscribe_button_color' ),
				'border_color'           => get_option( 'yith_wcac_' . $context . '_subscribe_button_border_color' ),
				'background_hover_color' => get_option( 'yith_wcac_' . $context . '_subscribe_button_background_hover_color' ),
				'text_hover_color'       => get_option( 'yith_wcac_' . $context . '_subscribe_button_hover_color' ),
				'border_hover_color'     => get_option( 'yith_wcac_' . $context . '_subscribe_button_border_hover_color' ),
				'custom_css'             => get_option( 'yith_wcac_' . $context . '_custom_css' ),
			);


			// Now merge all defaults attributes on the same array.
			$defaults = array_merge( $defaults, array(
				'fields'    => $def_fields,
				'tags'      => $def_tags,
				'show_tags' => $def_show_tags,
			), $style_defaults );

			return $defaults;
		}

		/**
		 * Print newsletter subscription form shortcode
		 *
		 * @param $atts    array Array of attributes passed to shortcode
		 * @param $content string Shortcode content
		 *
		 * @return string Shortcode template
		 * @since 1.0.0
		 */
		public function print_subscription_form( $attributes, $content = "" ) {
			// generate unique shortcode id
			$unique_id = mt_rand();

			$defaults = $this->get_default_attributes( 'shortcode' );

			// Merge the attributes defined by options with $attributes.
			$attributes              = shortcode_atts( $defaults, $attributes );
			$attributes['unique_id'] = $unique_id;

			if ( ! is_array( $attributes['fields'] ) ) {
				// generate structure for fields
				$fields_chunk    = array();
				$fields_subchunk = array_filter( explode( '|', $attributes['fields'] ) );
				if ( ! empty( $fields_subchunk ) ) {
					foreach ( $fields_subchunk as $subchunk ) {
						if ( strpos( $subchunk, ',' ) === false ) {
							continue;
						}

						list( $name, $merge_var ) = explode( ',', $subchunk );
						$fields_chunk[ $merge_var ] = array( 'name' => $name, 'merge_var' => $merge_var );
					}
				}
				$attributes['fields'] = $fields_chunk;
			}

			// define context
			$attributes['context'] = ( isset( $attributes['widget'] ) && $attributes['widget'] == 'yes' ) ? 'widget' : 'shortcode';

			// replace "yes"/"no" values with true/false
			$attributes['enable_style']  = ( 'yes' == $attributes['enable_style'] );
			$attributes['round_corners'] = ( 'yes' == $attributes['round_corners'] );

			if ( empty( $attributes['list'] ) ) {
				return '';
			}

			// retrieve fields informations from active-campaign
			$attributes['fields_data'] = $this->retrieve_fields();

			if ( empty( $attributes['fields_data'] ) ) {
				return '';
			}

			// retrieve style information for template
			$attributes['style'] = '';
			if ( $attributes['enable_style'] ) {
				$attributes['style'] = sprintf(
					'#subscription_form_%d input[type="submit"]{
					    color: %s;
					    border: 1px solid %s;
					    border-radius: %dpx;
					    background: %s;
					}
					#subscription_form_%d input[type="submit"]:hover{
					    color: %s;
					    border: 1px solid %s;
					    background: %s;
					}
					%s',
					$unique_id,
					$attributes['text_color'],
					$attributes['border_color'],
					( $attributes['round_corners'] ) ? 5 : 0,
					$attributes['background_color'],
					$unique_id,
					$attributes['text_hover_color'],
					$attributes['border_hover_color'],
					$attributes['background_hover_color'],
					$attributes['custom_css']
				);
			}

			$attributes['use_placeholders'] = apply_filters( 'yith_wcac_use_placeholders_instead_of_labels', false );

			// retrieve template for the subscription form

			$this->enqueue_form();

			yith_wcac_get_template( 'active-campaign-subscription-form', $attributes );
		}

		/**
		 * Print single subscription form field
		 *
		 * @param $id                   int Unique id of the shortcode
		 * @param $panel_options        array Array of options setted in settings panel
		 * @param $active_campaign_data array Array of data retreieved from active campaign server
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function print_field( $id, $panel_options, $active_campaign_data, $context = 'shortcode' ) {
			if ( empty( $active_campaign_data ) ) {
				return;
			}

			$use_placeholders = apply_filters( 'yith_wcac_use_placeholders_instead_of_labels', false );
			$placeholder      = ! empty( $panel_options['name'] ) && $use_placeholders ? $panel_options['name'] : '';

			$attributes = array(
				'id'                   => $id,
				'panel_options'        => $panel_options,
				'active_campaign_data' => $active_campaign_data,
				'context'              => $context,
				'use_placeholders'     => $use_placeholders,
				'placeholder'          => $placeholder
			);

			yith_wcac_get_template( $active_campaign_data['type'], $attributes, 'types' );

		}

		/* === HANDLE WIDGET === */

		/**
		 * Registers widget used to show subscription form
		 *
		 * @return void
		 * @since1.0.0
		 */
		public function register_widget() {
			register_widget( 'YITH_WCAC_Widget' );
		}

		/* === HANDLE FORM SUBSCRIPTION === */

		/**
		 * Register a user using form fields
		 *
		 * @return array Array with status code and messages
		 * @since 1.0.0
		 */
		public function form_subscribe( $context = 'shortcode' ) {
			$args                      = array();
			$yith_wcac_shortcode_items = isset( $_POST['yith_wcac_shortcode_items'] ) ? $_POST['yith_wcac_shortcode_items'] : array();
			//Set default args that has nor been setted like fields from AC

			if ( $context == 'shortcode' ) {
				$email = isset( $yith_wcac_shortcode_items['default']['email'] ) ? $yith_wcac_shortcode_items['default']['email'] : '';
			} elseif ( $context == 'register' ) {
				$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
			}

			$first_name         = isset( $yith_wcac_shortcode_items['default']['first_name'] ) ? $yith_wcac_shortcode_items['default']['first_name'] : '';
			$last_name          = isset( $yith_wcac_shortcode_items['default']['last_name'] ) ? $yith_wcac_shortcode_items['default']['last_name'] : '';
			$list               = isset( $yith_wcac_shortcode_items['hidden']['list'] ) ? $yith_wcac_shortcode_items['hidden']['list'] : '';
			$status             = ! empty( $list ) ? isset( $yith_wcac_shortcode_items['hidden']['status'] ) ? $yith_wcac_shortcode_items['hidden']['status'] : '' : '';
			$instantresponders  = ! empty( $list ) ? isset( $yith_wcac_shortcode_items['hidden']['instant_responders'] ) ? $yith_wcac_shortcode_items['hidden']['instant_responders'] : '' : '';
			$noresponders       = ! empty( $list ) ? isset( $yith_wcac_shortcode_items['hidden']['no_responders'] ) ? $yith_wcac_shortcode_items['hidden']['no_responders'] : '' : '';
			$sendlastmessage    = ! empty( $list ) ? isset( $yith_wcac_shortcode_items['hidden']['send_last_message'] ) ? $yith_wcac_shortcode_items['hidden']['send_last_message'] : '' : '';
			$show_tags          = isset( $yith_wcac_shortcode_items['default']['show_tags'] ) ? implode( ',', $yith_wcac_shortcode_items['default']['show_tags'] ) : '';
			$tags               = implode( ',', get_option( 'yith_wcac_shortcode_active-campaign_tags' ) );
			$success_message    = ! empty( $_POST['success_message'] ) ? wp_kses_post( $_POST['success_message'] ) : __( 'Great! You\'re now subscribed to our newsletter', 'yith-woocommerce-active-campaign' );
			$show_privacy_field = isset( $_POST['show_privacy_field'] ) ? $_POST['show_privacy_field'] == 'yes' : false;
			$privacy_agreement  = isset( $_POST['privacy_agreement'] );

			if ( ! empty( $email ) ) {
				$args['email'] = $email;
			} else {
				return array(
					'status'  => false,
					'code'    => false,
					'message' => apply_filters( 'yith_wcac_missing_required_arguments_error_message', __( 'Email is required', 'yith-woocommerce-active-campaign' ) )
				);
			}
			if ( ! empty( $first_name ) ) {
				$args['first_name'] = $first_name;
			}
			if ( ! empty( $last_name ) ) {
				$args['last_name'] = $last_name;
			}
			if ( ! empty( $list ) ) {
				$args['p'] = array( $list => $list );
			}
			if ( $show_privacy_field && ! $privacy_agreement ) {
				return array(
					'status'  => false,
					'code'    => false,
					'message' => apply_filters( 'yith_wcac_privacy_error_message', __( 'You must agree privacy agreement', 'yith-woocommerce-active-campaign' ) )
				);
			}

			if ( ! empty( $status ) ) {
				$args['status'] = array( $list => $status );
			}
			if ( ! empty( $instantresponders ) ) {
				if ( '1' == $status ) {
					$args['instantresponders'] = array( $list => $instantresponders );
				}
			}
			if ( ! empty( $noresponders ) ) {
				$args['noresponders'] = array( $list => $noresponders );
			}
			if ( ! empty( $sendlastmessage ) ) {
				$args['lastmessage'] = array( $list => $sendlastmessage );
			}
			if ( ! empty( $show_tags ) | ! empty( $tags ) ) {
				$args['tags'] = $show_tags . ',' . $tags;
			}

			// Set fields args defined by AC
			$fields = isset( $yith_wcac_shortcode_items['fields'] ) ? $yith_wcac_shortcode_items['fields'] : array();
			foreach ( $fields as $key => $field_item ) {
				$args[ 'field[' . $key . ',0]' ] = is_array( $field_item ) ? '||' . implode( '||', $field_item ) . '||' : $field_item;
			}

			$syncronized = $this->synchronize_contact( $args );

			$res = array();
			if ( isset( $syncronized->result_code ) ) {
				$message = $syncronized->result_code ? $success_message : _x( 'Some field was wrong', 'validate message fields', 'yith-woocommerce-active-campaign' );
				$res     = array(
					'status'  => $syncronized->result_code ? true : false,
					'message' => apply_filters( 'yith_wcac_subscribed_message', stripslashes( $message ), $list, $email )
				);
			}

			if ( $res['status'] && is_user_logged_in() ) {
				// register subscribed list
				$this->_register_customer_subscribed_lists( $list, $email, get_current_user_id() );
			}

			return $res;
		}

		/**
		 * Calls form_subscribe(), when posting form data, and adds woocommerce notice with result of the operation
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function post_form_subscribe() {

			if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset( $_POST['yith_wcac_subscribe_nonce'] ) && wp_verify_nonce( $_POST['yith_wcac_subscribe_nonce'], 'yith_wcac_subscribe' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {

				$res = $this->form_subscribe( 'shortcode' );
				wc_add_notice( $res['message'], ( $res['status'] ) ? 'yith-wcac-success' : 'yith-wcac-error' );
			}
		}

		/* === ADDS FRONTEND REGISTER === */

		public function add_subscription_register() {
			$print_sub_register_enable = get_option( 'yith_wcac_register_subscription_checkbox_enable' );
			if ( 'yes' != $print_sub_register_enable ) {
				return;
			}
			// generate basic default array
			$attributes = $this->get_default_attributes( 'register' );
			// define context
			$attributes['context'] = 'register';

			// replace "yes"/"no" values with true/false
			$attributes['enable_style'] = ( 'yes' == $attributes['enable_style'] );

			// retrieve fields informations from active-campaign
			$attributes['fields_data'] = $this->retrieve_fields();

			if ( empty( $attributes['fields_data'] ) ) {
				return '';
			}

			$attributes['style'] = '';

			if ( $attributes['enable_style'] ) {
				$attributes['style'] = sprintf(
					'%s',
					$attributes['custom_css'] );
			}

			$attributes['use_placeholders'] = apply_filters( 'yith_wcac_use_placeholders_instead_of_labels', false );

			$this->enqueue_form();
			yith_wcac_get_template( 'active-campaign-subscription-form-content', $attributes );

		}

		/* === HANDLE FRONTEND REGISTER === */

		public function register_subscribe() {

			$subscribe_user = true;
			if ( isset( $_POST['yith_wcac_subscribe_me_enabled'] ) ) {
				if ( ! isset( $_POST['yith_wcac_subscribe_me'] ) ) {
					$subscribe_user = false;
				}
			}

			if ( $subscribe_user ) {
				$res = $res = $this->form_subscribe( 'register' );
				wc_add_notice( $res['message'], ( $res['status'] ) ? 'yith-wcac-success' : 'yith-wcac-error' );
			}
		}

		/* === HANDLES AJAX REQUESTS === */

		/**
		 * Calls form_subscribe(), from an AJAX request, and print JSON encoded version of its result
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function ajax_form_subscribe() {
			wp_send_json( $this->form_subscribe() );
		}

		/* === UTILITY METHODS === */

		/**
		 * Register personal data sent to Active Campaign servers, within order meta
		 *
		 * @param $order_id int Order id
		 * @param $arg1 string|array ID of the data to save or array of arrays containing label and value to save
		 * @param $arg2 string|bool When $arg1 is an ID, this param will be used as label
		 * @param $arg3 string|bool When $arg1 is an ID, this param will be used as value
		 *
		 * @return void
		 */
		protected function _register_customer_personal_data( $order_id, $arg1, $arg2 = false, $arg3 = false ) {
			$order = wc_get_order( $order_id );

			if( ! $order ){
				return;
			}

			$data = is_array( $arg1 ) ? $arg1 : array( $arg1 => array( 'label' => $arg2, 'value' => $arg3 ) );

			$previous_personal_data = yit_get_prop( $order, '_yith_wcac_personal_data', true );
			$previous_personal_data = ! empty( $previous_personal_data ) ? $previous_personal_data : array();
			$new_personal_data = array_merge( $previous_personal_data, $data );

			yit_save_prop( $order, '_yith_wcac_personal_data', $new_personal_data );
		}

		/**
		 * Register subscribed lists, in order to easily unsubscribe user later
		 *
		 * @param $list_id  string List id
		 * @param $email    string Email being subscribed
		 * @param $user_id  int User id
		 * @param $order_id int|bool Order id
		 *
		 * @return void
		 */
		protected function _register_customer_subscribed_lists( $list_id, $email, $user_id, $order_id = false ) {
			// register list within the customer
			if ( $user_id ) {
				$order_subscribed_lists = get_user_meta( $user_id, '_yith_wcac_subscribed_lists', true );
				$order_subscribed_lists = ! empty( $order_subscribed_lists ) ? $order_subscribed_lists : array();

				if ( ! array_key_exists( $list_id, $order_subscribed_lists ) ) {
					$order_subscribed_lists[ $list_id ] = array();
				}

				if ( ! in_array( $email, $order_subscribed_lists[ $list_id ] ) ) {
					$order_subscribed_lists[ $list_id ][] = $email;

					update_user_meta( $user_id, '_yith_wcac_subscribed_lists', $order_subscribed_lists );
				}
			}

			// eventually register list within the order
			if ( $order_id ) {
				$order = wc_get_order( $order_id );

				if ( $order ) {
					$order_subscribed_lists = yit_get_prop( $order, '_yith_wcac_subscribed_lists', true );
					$order_subscribed_lists = ! empty( $order_subscribed_lists ) ? $order_subscribed_lists : array();

					if ( ! array_key_exists( $list_id, $order_subscribed_lists ) ) {
						$order_subscribed_lists[ $list_id ] = array();
					}

					if ( ! in_array( $email, $order_subscribed_lists[ $list_id ] ) ) {
						$order_subscribed_lists[ $list_id ][] = $email;
						yit_save_prop( $order, '_yith_wcac_subscribed_lists', $order_subscribed_lists );
					}
				}
			}
		}

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WCAC
		 * @since 1.0.2
		 */
		public static function get_instance() {
			if ( is_null( YITH_WCAC::$instance ) ) {
				YITH_WCAC::$instance = new YITH_WCAC;
			}

			return YITH_WCAC::$instance;
		}

	}
}
/**
 * Unique access to instance of YITH_WCAC class
 *
 * @return \YITH_WCAC
 * @since 1.0.0
 */
function YITH_WCAC() {
	return YITH_WCAC::get_instance();
}