<?php
/**
 * Subscription form widget class
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Active Campaign
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCAC' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCAC_Widget' ) ) {
	/**
	 * WooCommerce Active Campaign Widget
	 *
	 * @since 1.0.0
	 */
	class YITH_WCAC_Widget extends WP_Widget {

		/**
		 * Sets up the widgets
		 *
		 * @return \YITH_WCAC_Widget
		 * @since 1.0.0
		 */
		public function __construct() {
			$widget_ops = array(
				'classname'   => 'yith-wcac-subscription-form',
				'description' => __( 'Display an Active Campaign subscription form in sidebars', 'yith-woocommerce-active-campaign' )
			);
			parent::__construct( 'yith-wcac-subscription-form', __( 'YITH Active Campaign Subscription Form', 'yith-woocommerce-active-campaign' ), $widget_ops );
		}

		/**
		 * Outputs the options form on admin
		 *
		 * @param array $instance The widget options
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function form( $instance ) {
			?>
            <p>
				<?php echo sprintf( __( 'You can customize the options of the <b>Active Campaign Subscription Form</b> widget from YITH WooCommerce Active Campaign <a href="%s">admin
page</a>', 'yith-woocommerce-active-campaign' ), esc_url( add_query_arg( array(
					'page' => 'yith_wcac_panel',
					'tab'  => 'widget'
				), admin_url( 'admin.php' ) ) ) ) ?>
            </p>
			<?php
		}

		/**
		 * Output the widget template
		 *
		 * @param $args     mixed Widget arguments
		 * @param $instance mixed Widget saved options
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function widget( $args, $instance ) {
			extract( $args );

			$defaults = array(
				'list'                         => get_option( 'yith_wcac_widget_active-campaign_list' ),
				'title'                        => get_option( 'yith_wcac_widget_title' ),
				'submit_label'                 => get_option( 'yith_wcac_widget_submit_button_label' ),
				'success_message'              => get_option( 'yith_wcac_widget_success_message' ),
				'show_privacy_field'           => get_option( 'yith_wcac_widget_show_privacy_field' ),
				'privacy_label'                => get_option( 'yith_wcac_widget_privacy_label' ),
				'hide_form_after_registration' => get_option( 'yith_wcac_widget_hide_after_registration' ),
				'status'                       => get_option( 'yith_wcac_widget_status' ),
				'instant_responders'           => get_option( 'yith_wcac_widget_instant_responders' ),
				'no_responders'                => get_option( 'yith_wcac_widget_no_responders' ),
				'send_last_message'            => get_option( 'yith_wcac_widget_send_last_message' ),
				'tags'                         => get_option( 'yith_wcac_widget_active-campaign_tags' ),
				'show_tags'                    => get_option( 'yith_wcac_widget_active-campaign_show_tags' ),
				'tags_label'                   => get_option( 'yith_wcac_widget_active-campaign_tags_label' ),
				'widget'                       => 'yes'
			);

			$defaults['tags']      = is_array( $defaults['tags'] ) ? implode( ',', $defaults['tags'] ) : '';
			$defaults['show_tags'] = is_array( $defaults['show_tags'] ) ? implode( ',', $defaults['show_tags'] ) : '';

			$selected_fields = get_option( 'yith_wcac_widget_custom_fields' );
			$textual_fields  = '';

			if ( ! empty( $selected_fields ) ) {
				$first = true;
				foreach ( $selected_fields as $field ) {
					if ( ! $first ) {
						$textual_fields .= '|';
					}

					$textual_fields .= $field['name'] . ',' . $field['merge_var'];

					$first = false;
				}
			}

			$fields_default = array( 'fields' => $textual_fields );
			$defaults       = array_merge( $defaults, $fields_default );

			// add defaults for style
			$style_defaults = array(
				'enable_style'           => get_option( 'yith_wcac_widget_style_enable' ),
				'round_corners'          => get_option( 'yith_wcac_widget_subscribe_button_round_corners', 'no' ),
				'background_color'       => get_option( 'yith_wcac_widget_subscribe_button_background_color' ),
				'text_color'             => get_option( 'yith_wcac_widget_subscribe_button_color' ),
				'border_color'           => get_option( 'yith_wcac_widget_subscribe_button_border_color' ),
				'background_hover_color' => get_option( 'yith_wcac_widget_subscribe_button_background_hover_color' ),
				'text_hover_color'       => get_option( 'yith_wcac_widget_subscribe_button_hover_color' ),
				'border_hover_color'     => get_option( 'yith_wcac_widget_subscribe_button_border_hover_color' ),
				'custom_css'             => get_option( 'yith_wcac_widget_custom_css' ),
			);

			$defaults         = array_merge( $defaults, $style_defaults );
			$textual_defaults = "";


			foreach ( $defaults as $field_id => $field_value ) {
				$textual_defaults .= $field_id . '="' . $field_value . '" ';
			}
			$fields_default = array( 'fields' => $textual_fields );
			$defaults       = array_merge( $defaults, $fields_default );

			echo apply_filters( 'yith_wcac_before_subscription_form_widget', $before_widget );
			echo do_shortcode( "[yith_wcac_subscription_form " . $textual_defaults . "]" );
			echo apply_filters( 'yith_wcac_after_subscription_form_widget', $after_widget );
		}

		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 *
		 * @return array Instance to save
		 * @since 1.0.0
		 */
		public function update( $new_instance, $old_instance ) {
			return $new_instance;
		}
	}
}