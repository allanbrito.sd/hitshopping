/**
 * Frontend
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Colors and Labels Variations Premium
 * @version 1.0.0
 */

;(function( $, window, document ){

    if ( typeof yith_wccl_general === 'undefined' )
        return false;

    /**
     * Matches inline variation objects to chosen attributes and return variation
     * @type {Object}
     */
    var variations_match = function( form, value, current_attribute_name ) {
        var match = false,
            product_variations = form.data( 'product_variations' ),
            all_select = form.find( '.variations select' ),
            settings = [];

        // current selected values
        $.each( all_select, function(){
            var attribute_name = $( this ).data( 'attribute_name' ) || $( this ).attr( 'name' );
            if( current_attribute_name == attribute_name ) {
                settings[attribute_name] = value;
            }
            else {
                settings[attribute_name] = $(this).val();
            }
        });

        for ( var i = 0; i < product_variations.length; i++ ) {
            var variation    = product_variations[i];

            // if found matching variation exit
            if( match ) {
                break;
            }

            match = variation;
            for ( var attr_name in variation.attributes ) {
                if ( variation.attributes.hasOwnProperty( attr_name ) ) {
                    var val1 = variation.attributes[ attr_name ],
                        val2 = settings[ attr_name ];

                    if ( val1 != val2 && val1 != '' ) {
                        match = false;
                    }
                }
            }
        }

        return match;
    };

    /**
     * Add to cart variation loop
     * @param event
     */
    var yith_wccl_add_cart = function( event ){
        event.preventDefault();

        var b          = $( this ),
            product_id = b.data( 'product_id' ),
            quantity   = b.data( 'quantity' ),
            attr = [];

        // get select value
        event.data.select.each( function(index){
            attr[ index ] = this.name + '=' + this.value;
        });

        $.ajax({
            url: yith_wccl_general.ajaxurl.toString().replace( '%%endpoint%%', yith_wccl_general.actionAddCart ),
            type: 'POST',
            data: {
                action: yith_wccl_general.actionAddCart,
                product_id : product_id,
                variation_id : event.data.variation,
                attr: attr.join('&'),
                quantity: quantity,
                context: 'frontend'
            },
            beforeSend: function(){
                b.addClass( 'loading').removeClass( 'added' );
            },
            success: function( res ){

                // redirect to product page if some error occurred
                if ( res.error && res.product_url ) {
                    window.location = res.product_url;
                    return;
                }
                // redirect to cart
                if ( yith_wccl_general.cart_redirect ) {
                    window.location = yith_wccl_general.cart_url;
                    return;
                }

                // change button
                b.removeClass('loading').addClass('added');

                if( ! b.next('.added_to_cart').length ) {
                    b.after(' <a href="' + yith_wccl_general.cart_url + '" class="added_to_cart wc-forward" title="' + yith_wccl_general.view_cart + '">' + yith_wccl_general.view_cart + '</a>');
                }

                $( document.body ).trigger( 'wc_fragment_refresh' );
                // trigger refresh also cart page
                $( document ).trigger( 'wc_update_cart' );

                // added to cart
                $( document.body ).trigger( 'added_to_cart', [ res.fragments, res.cart_hash, b ] );
            }
        });
    }

    /**
     *
     * @param $form
     * @param attr
     * @constructor
     */
    var WCCL = function( $form, attr ) {

        this.$form              = $form;
        this.$attr              = ( typeof yith_wccl != 'undefined' ) ? JSON.parse( yith_wccl.attributes ) : attr;
        this.$select            = this.$form.find( '.variations select' );
        this.$use_ajax          = this.$form.data( 'product_variations' ) === false;
        // variables for loop
        this.$is_loop           = this.$form.hasClass('in_loop');
        this.$wrapper           = this.$form.closest( yith_wccl_general.wrapper_container_shop ).length ? this.$form.closest( yith_wccl_general.wrapper_container_shop ) : this.$form.closest('.product-add-to-cart' );
        this.$image             = this.$wrapper.find( yith_wccl_general.image_selector );
        this.$def_image_src     = ( this.$image.data('lazy-src') ) ? this.$image.data('lazy-src') : this.$image.attr( 'src' );
        this.$def_image_srcset  = ( this.$image.data('lazy-srcset') ) ? this.$image.data('lazy-srcset') : this.$image.attr( 'srcset' );
        this.$price_html        = this.$wrapper.find( 'span.price' ).clone().wrap('<p>').parent().html();
        this.$button            = this.$wrapper.find( 'a.product_type_variable' );
        this.$button_html       = this.$button.html();
        this.$input_qty         = this.$wrapper.find('input.thumbnail-quantity');
        this.$xhr               = null;

        // prevent undefined attr error
        if( typeof this.$attr == 'undefined' ) {
            this.$attr = [];
        }

        $form.on( 'yith_wccl_form_initialized', { obj: this }, this.init );

        // get default value
        this.$select.each( function() {
            this.setAttribute( 'data-default_value', this.value );
        });

        // reset form and select
        this.resetForm( this );

        if( this.$is_loop ) {
            $form.parent().on( 'change', function(e) { e.stopPropagation(); });
        }

        // hide input qty if present
        if( this.$input_qty.length )
            this.$input_qty.hide();

        if( ! this.$form.hasClass( 'inizialized' ) ) {
            this.$form.addClass('initialized').fadeIn().trigger( 'yith_wccl_form_initialized' );
        }
    };

    WCCL.prototype.styleOption = function( obj, option, type, value ) {
        if( type == 'colorpicker' ) {

            value = value.split(',');

            if( value.length == 1 ) {
                option.append($('<span/>', {
                    'class': 'yith_wccl_value',
                    'css': {
                        'background': value
                    }
                }));
            } else {
                option.append($('<span class="yith_wccl_value"><span class="yith-wccl-bicolor"/></span>'));
                option.find('.yith-wccl-bicolor').css({
                    'border-bottom-color': value[0],
                    'border-left-color': value[1]
                });
            }
        } else if( type == 'image' ) {
            option.append($('<img/>', {
                'class': 'yith_wccl_value',
                'src': value
            }));
        } else if( type == 'label' ) {
            option.append($('<span/>', {
                'class': 'yith_wccl_value',
                'text': value
            }));
        }
    };

    WCCL.prototype.addTooltip = function( obj, tooltip, option, type, value ) {

        var tooltip_wrapper = $('<span class="yith_wccl_tooltip"></span>'),
            classes         = yith_wccl_general.tooltip_pos + ' ' + yith_wccl_general.tooltip_ani;

        if( ! yith_wccl_general.tooltip || typeof tooltip == 'undefined' || ! tooltip || option.find( '.yith_wccl_tooltip' ).length ) {
            return;
        }

        if( type == 'image' ) {
            tooltip = tooltip.replace('{show_image}', '<img src="' + value + '" />');
        }

        tooltip_wrapper.addClass( classes );
        option.append( tooltip_wrapper.html( '<span>' + tooltip + '</span>' ) );
    };

    WCCL.prototype.handleSelect = function( event ) {

        var obj = event.data.obj;

        obj.$select.each( function() {
            var t               = $(this),
                current_attr    = obj.$attr[ this.name ],
                select_box      = t.parent().find( '.select_box' ),
                current_option  = [];

            if( typeof current_attr == 'undefined' ) {
                current_attr = obj.$attr[ decodeURIComponent( this.name ) ];
            }

            // Add description
            if ( ! obj.$is_loop && ! obj.$wrapper.length && ! obj.$form.find( '.description_' + this.name ).length
                && yith_wccl_general.description && typeof current_attr != 'undefined' && current_attr.descr ) {
                if( $(this).closest('tr').length ) {
                    $(this).closest('tr').after( '<tr class="description_' + this.name + '"><td colspan="2">' + current_attr.descr + '</td></tr>' );
                } else {
                    $(this).parent().append( '<p class="description_' + this.name + '">' + current_attr.descr + '</p>' );
                }
            }

            var type    = ( typeof current_attr != 'undefined' ) ? current_attr.type : t.data('type'),
                opt     = ( typeof current_attr != 'undefined' ) ? current_attr.terms : false;

            // exit if is not a custom attr
            if ( ( ! obj.$is_loop && ( typeof current_attr == 'undefined' || ! current_attr.terms ) ) || typeof type == 'undefined' || ! type ) {
                return;
            }

            t.addClass('yith_wccl_custom').hide().end().closest('.select-wrapper').addClass('yith_wccl_is_custom');

            if( ! select_box.length || ! yith_wccl_general.grey_out ) {
                select_box.remove();
                select_box = $('<div />', {
                    'class': 'select_box_' + type + ' select_box ' + t.attr('name')
                }).insertAfter(t);
            }

            t.find('option').each(function () {

                var option_val = $(this).val();

                if( ( opt && typeof opt[option_val] != 'undefined') || $(this).data('value') ) {

                    current_option.push( option_val );

                    var o           = $(this),
                        classes     = 'select_option_' + type + ' select_option',
                        value       = opt && typeof opt[option_val] != 'undefined' ? opt[option_val].value : $(this).data('value'),
                        tooltip     = opt && typeof opt[option_val] != 'undefined' ? opt[option_val].tooltip : $(this).data('tooltip'),
                        option      = select_box.find('[data-value="' + option_val + '"]');

                    // add options if missing
                    if( ! option.length ) {

                        // add selected class if is default
                        if( option_val == t.val() || option_val == t.attr( 'data-default_value' ) ) {
                            classes += ' selected';
                        }

                        option = $('<div/>', {
                            'class': classes,
                            'data-value': option_val
                        }).appendTo(select_box);

                        // event
                        option.off('click').on('click', function (e) {

                            var inactive = $(this).hasClass('inactive'),
                                selected = $(this).hasClass('selected');

                            if( inactive ) {

                                var current_attribute_name = t.data('attribute_name') || t.attr('name');

                                if( variations_match( obj.$form, $(this).data('value'), current_attribute_name ) ) {
                                    t.val('').change();
                                } else {
                                    obj.resetForm( obj );
                                }
                            }

                            if( selected ) {
                                t.val('').change();
                            } else {
                                t.val( o.val() ).change();
                            }

                            $(this).toggleClass( 'selected' );
                            $(this).siblings().removeClass( 'selected' );
                        });

                        // style option
                        obj.styleOption( obj, option, type, value );
                        // add tooltip if any
                        obj.addTooltip( obj, tooltip, option, type, value );
                    }
                }
            });

            select_box.children().each(function () {
                var val = $(this).data('value') + '';

                if ( $.inArray( val, current_option ) == '-1' ) {
                    $(this).addClass('inactive');
                }
                else {
                    $(this).removeClass('inactive');
                }
            });

            obj.$form.trigger( 'yith_wccl_select_initialized', [ t, current_attr ] );
        });
    };

    WCCL.prototype.setDefaultValue = function( event ) {
        var obj = event.data.obj;

        obj.$select.each( function () {
            $(this).val( $(this).attr( 'data-default_value' ) );
        });

        obj.$select.first().change();
    }

    WCCL.prototype.changeLoopImage = function( obj, variation ){

        if( ! variation ) {
            obj.$image.attr( 'src', obj.$def_image_src );
            obj.$image.attr( 'srcset', obj.$def_image_srcset );
        } else {
            var var_image           = ( typeof variation.image_src != 'undefined' && variation.image_src ) ? variation.image_src : variation.image.src,
                var_image_srcset    = ( typeof variation.image_srcset != 'undefined' && variation.image_srcset ) ? variation.image_srcset : variation.image.srcset;

            if( var_image && var_image.length ) {
                obj.$image.attr('src', var_image );
                obj.$image.attr('data-lazy-src', var_image );
            }
            if( var_image_srcset && var_image_srcset.length ) {
                obj.$image.attr( 'srcset', var_image_srcset );
                obj.$image.attr( 'data-lazy-srcset', var_image_srcset );
            }
        }
    };

    WCCL.prototype.changeSingleImage = function( obj, variation ) {
        var $product_gallery  = obj.$form.closest( '.product' ).find( '.images' ),
            $product_img_wrap = $product_gallery.find( '.woocommerce-product-gallery__image, .woocommerce-product-gallery__image--placeholder' ).eq( 0 ),
            $product_img      = $product_img_wrap.find( '.wp-post-image' ),
            $product_link     = $product_img_wrap.find( 'a' ).eq( 0 );

        $product_img.wc_set_variation_attr( 'src', variation.image.src );
        $product_img.wc_set_variation_attr( 'height', variation.image.src_h );
        $product_img.wc_set_variation_attr( 'width', variation.image.src_w );
        $product_img.wc_set_variation_attr( 'srcset', variation.image.srcset );
        $product_img.wc_set_variation_attr( 'sizes', variation.image.sizes );
        $product_img.wc_set_variation_attr( 'title', variation.image.title );
        $product_img.wc_set_variation_attr( 'alt', variation.image.alt );
        $product_img.wc_set_variation_attr( 'data-src', variation.image.full_src );
        $product_img.wc_set_variation_attr( 'data-large_image', variation.image.full_src );
        $product_img.wc_set_variation_attr( 'data-large_image_width', variation.image.full_src_w );
        $product_img.wc_set_variation_attr( 'data-large_image_height', variation.image.full_src_h );
        $product_img_wrap.wc_set_variation_attr( 'data-thumb', variation.image.src );
        $product_link.wc_set_variation_attr( 'href', variation.image.full_src );
    };

    WCCL.prototype.changeImageOnHover = function( event ) {

        var obj = event.data.obj;

        if( obj.$select.length != 1 || ! yith_wccl_general.image_hover ) {
            return;
        }

        obj.$form.on('mouseenter', '.select_option', function() {
            var value       = $(this).attr("data-value"),
                attr_name   = obj.$select.attr('name'),
                variation   = variations_match( obj.$form, value, attr_name ); // find variation

            if( $(this).hasClass('selected') || $(this).siblings().hasClass('selected') ){
                return;
            }

            if( variation && variation.image && variation.image.src && variation.image.src.length > 1 ) {
                if( obj.$form.hasClass('in_loop') ) {
                    obj.changeLoopImage( obj, variation );
                } else {
                    obj.changeSingleImage( obj, variation );
                }
            }
        }).on('mouseleave', '.select_option', function() {
            if( $(this).hasClass('selected') || $(this).siblings().hasClass('selected') ){
                return;
            }

            if( obj.$form.hasClass('in_loop') ) {
                obj.changeLoopImage( obj, false );
            } else {
                obj.$form.wc_variations_image_update( false );
            }
        });

    };

    WCCL.prototype.handleCheckVariations = function( event, data, focus ) {
        var obj = event.data.obj;
        if ( ! focus ) {
            if( obj.$found ) {
                event.data.obj.$found = false;
                if( ! obj.$use_ajax ) return;
            }
            if( obj.$changed ) {
                event.data.obj.$changed = false;
                // reset
                obj.resetLoopForm( obj );
            }
        }
    }

    WCCL.prototype.handleFoundVariation = function( event, variation ) {
        var obj = event.data.obj;

        if( obj.$use_ajax ) {
            obj.handleSelect( event );
        } else {
            obj.$select.last().trigger('focusin');
        }

        if ( obj.$is_loop && variation.is_purchasable ) {

            if( obj.$changed ) {
                obj.resetLoopForm( obj );
            }

            // found it!
            event.data.obj.$changed = true;
            event.data.obj.$found = true;

            // change image
            obj.changeLoopImage( obj, variation );

            // change price
            if( variation.price_html ) {
                obj.$wrapper.find('span.price').replaceWith( variation.price_html );
            }

            // show qty input
            if( obj.$input_qty.length ) {
                obj.$input_qty.show();
            }
            // change button and add event add to cart
            if( variation.is_in_stock ) {
                obj.$button.html( yith_wccl_general.add_cart );
                obj.$button.off( 'click' ).on( 'click', { variation: variation.variation_id, select: obj.$select }, yith_wccl_add_cart );
            }
            // add availability
            obj.$wrapper.find('span.price').after( $( variation.availability_html).addClass('ywccl_stock') );

            // set active variation
            obj.$form.data('active_variation', variation.variation_id );

            $(document).trigger('ywccl_found_variation_in_loop', [variation]);
        }
    };

    WCCL.prototype.handleVariationGallery = function( event, variation ) {

        var obj = event.data.obj;

        if( obj.$is_loop || ! $( '.woocommerce-product-gallery' ).length ) {
            return;
        }

        if( obj.$xhr !== null ) {
            obj.$xhr.abort();
        }

        id = typeof variation != 'undefined' ? variation.variation_id : obj.$form.find( 'input[name="product_id"]' ).val();

        obj.$xhr = $.ajax({
            url: yith_wccl_general.ajaxurl.toString().replace( '%%endpoint%%', yith_wccl_general.actionVariationGallery ),
            data: {
                action: yith_wccl_general.actionVariationGallery,
                id : id,
                is_custom : $( '.woocommerce-product-gallery' ).hasClass( 'custom-variation-gallery' ),
                context: 'frontend'
            },
            type: 'POST',
            dataType: 'json',
            beforeSend: function(){
                $( '.woocommerce-product-gallery' ).addClass( 'loading-gallery' );
            },
            success: function( response ){
                if( response.html ) {
                    $( '.woocommerce-product-gallery' ).replaceWith( response.html );
                    $( '.woocommerce-product-gallery' ).wc_product_gallery();
                    if( ! response.default ){
                        $( '.woocommerce-product-gallery' ).addClass( 'custom-variation-gallery' );
                    } else {
                        obj.$form.wc_variations_image_update( variation );
                    }
                }

                $( '.woocommerce-product-gallery' ).removeClass( 'loading-gallery' );

                obj.$xhr = null;
            }
        });
    }

    WCCL.prototype.resetLoopForm = function( obj ){
        // reset image
        obj.changeLoopImage( obj, false );
        obj.$wrapper.find( 'span.price' ).replaceWith( obj.$price_html );
        obj.$wrapper.find('.ywccl_stock').remove();

        if( obj.$input_qty.length ){
            obj.$input_qty.hide();
        }

        obj.$button.html( obj.$button_html )
            .off( 'click', yith_wccl_add_cart )
            .removeClass( 'added' )
            .next('.added_to_cart').remove();

        // set active variation
        obj.$form.data('active_variation', '' );
    }

    WCCL.prototype.resetForm = function( obj ) {
        obj.$form.find( 'div.select_option' ).removeClass( 'selected inactive' );
        obj.$select.val('').change();
        obj.$form.trigger( 'reset_data' );
    };

    WCCL.prototype.onReset = function( event ) {
        event.data.obj.$form.find('.select_option.selected').removeClass('selected inactive');
    }

    WCCL.prototype.init = function( event ) {

        var obj = event.data.obj;

        obj.$form.on( 'click.wc-variation-form', '.reset_variations', { obj: obj }, obj.onReset );
        obj.$form.on( 'woocommerce_update_variation_values', { obj: obj }, obj.handleSelect );
        obj.$form.one( 'yith_wccl_select_initialized', { obj: obj }, obj.changeImageOnHover );
        obj.$form.on( 'check_variations', { obj: obj }, obj.handleCheckVariations );
        obj.$form.on( 'found_variation', { obj: obj }, obj.handleFoundVariation );
        obj.$form.on( 'found_variation', {obj: obj}, obj.handleVariationGallery );
        obj.$form.on( 'reset_image', {obj: obj}, obj.handleVariationGallery );

        // force start select
        obj.handleSelect( event );

        obj.$select.each( function(){
            var val = $(this).attr( 'data-default_value' );
            $(this).removeAttr( 'data-default_value' );
            $(this).val( val );
        });
    }

    // retrocompatibility
    $.yith_wccl = function( attr ) {
        forms = $( '.variations_form.cart:not(.initialized), .owl-item.cloned .variations_form, form.cart.ywcp_form_loaded' );
        // prevent undefined attr error
        if( typeof attr == 'undefined' ) {
            attr = [];
        }

        forms.each(function (){
            new WCCL( $(this), attr );
        });
    };

    // plugin compatibility
    $(document).on( 'yith-wcan-ajax-filtered yith_infs_adding_elem initialized.owl.carousel post-load ajax-tab-loaded', function() {

        if( typeof $.yith_wccl != 'undefined' && typeof $.fn.wc_variation_form != 'undefined' ) {
            // not initialized
            $(document).find( '.variations_form:not(.initialized), .owl-item.cloned .variations_form' ).each( function() {
                $(this).wc_variation_form();
            });

            // prevent undefined attr error
            if( typeof attr == 'undefined' ) {
                attr = [];
            }

            $.yith_wccl(attr);
        }
    });

    // reinit for woocommerce quick view
    $( 'body' ).on( 'quick-view-displayed', function() {
        var attr_qv = $('.pp_woocommerce_quick_view').find('.yith-wccl-data').data('attr');
        if( attr_qv ) {
            $.yith_wccl(attr_qv);
        }
    });

    // Fix for Flatsome Infinite Scrolling
    $('.shop-container > .products').on('append.infiniteScroll',function(){
        $(document).find( '.variations_form:not(.initialized), .owl-item.cloned .variations_form' ).each( function() {
            $(this).wc_variation_form();
        });

        // prevent undefined attr error
        if( typeof attr == 'undefined' ) {
            attr = [];
        }

        $.yith_wccl(attr);

    });

    // START
    $(document).on( 'ready', function(){
        $.yith_wccl();
    });


})( jQuery, window, document );