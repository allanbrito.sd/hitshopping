#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Color and Label Variations\n"
"POT-Creation-Date: 2018-06-08 12:48+0200\n"
"PO-Revision-Date: 2015-06-25 17:07+0100\n"
"Last-Translator: \n"
"Language-Team: YIThemes <plugins@hitoutlets.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.8\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-KeywordsList: _e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;__\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../includes/class.yith-wccl-admin.php:160
#: ../includes/class.yith-wccl-admin.php:181
msgid "Settings"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:187
#: ../includes/class.yith-wccl-admin.php:188
msgid "Color and Label Variations"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:224
msgid "Plugin Documentation"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:449
#: ../includes/class.yith-wccl-admin.php:478
#: ../includes/class.yith-wccl-admin.php:772
msgid "Tooltip"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:450
#: ../includes/class.yith-wccl-admin.php:479
msgid ""
"Use this placeholder {show_image} to show the image on tooltip. Only "
"available for image type"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:593
#: ../includes/class.yith-wccl-admin.php:767
msgid "Value"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:682
msgid "Select terms"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:692
msgid "Select all"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:693
msgid "Select none"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:694
msgid "Add new"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:757
msgid "Create new attribute term"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:761
msgid "Name"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:764
msgid "Slug"
msgstr ""

#: ../includes/class.yith-wccl-admin.php:805
msgid "A value is required for this term"
msgstr ""

#: ../includes/class.yith-wccl-frontend.php:169
msgid "View Cart"
msgstr ""

#: ../includes/function.yith-wccl.php:74
msgid "Colorpicker"
msgstr ""

#: ../includes/function.yith-wccl.php:75
msgid "Image"
msgstr ""

#: ../includes/function.yith-wccl.php:76
msgid "Label"
msgstr ""

#: ../init.php:43
msgid ""
"YITH WooCommerce Color and Label Variations Premium is enabled but not "
"effective. It requires WooCommerce in order to work."
msgstr ""

#: ../plugin-options/general-options.php:11
msgid "General Options"
msgstr ""

#: ../plugin-options/general-options.php:18
msgid "Attribute behavior"
msgstr ""

#: ../plugin-options/general-options.php:19
msgid "Choose attribute style after selection."
msgstr ""

#: ../plugin-options/general-options.php:24
msgid "Hide attributes"
msgstr ""

#: ../plugin-options/general-options.php:25
msgid "Blur attributes"
msgstr ""

#: ../plugin-options/general-options.php:32
msgid "Enable Tooltip"
msgstr ""

#: ../plugin-options/general-options.php:34
msgid "Enable tooltip for attributes"
msgstr ""

#: ../plugin-options/general-options.php:40
msgid "Tooltip position"
msgstr ""

#: ../plugin-options/general-options.php:41
msgid "Select tooltip position"
msgstr ""

#: ../plugin-options/general-options.php:44
msgid "Top"
msgstr ""

#: ../plugin-options/general-options.php:45
msgid "Bottom"
msgstr ""

#: ../plugin-options/general-options.php:52
msgid "Tooltip animation"
msgstr ""

#: ../plugin-options/general-options.php:53
msgid "Select tooltip animation"
msgstr ""

#: ../plugin-options/general-options.php:56
msgid "Fade in"
msgstr ""

#: ../plugin-options/general-options.php:57
msgid "Slide in"
msgstr ""

#: ../plugin-options/general-options.php:64
msgid "Tooltip background"
msgstr ""

#: ../plugin-options/general-options.php:65
msgid "Select tooltip background"
msgstr ""

#: ../plugin-options/general-options.php:72
msgid "Tooltip text color"
msgstr ""

#: ../plugin-options/general-options.php:73
msgid "Select tooltip text color"
msgstr ""

#: ../plugin-options/general-options.php:80
msgid "Show Attribute Description"
msgstr ""

#: ../plugin-options/general-options.php:82
msgid "Choose to show description below each attribute in single product page"
msgstr ""

#: ../plugin-options/general-options.php:88
msgid "Enable plugin in archive pages"
msgstr ""

#: ../plugin-options/general-options.php:90
msgid "Choose to show attribute selection in archive shop pages."
msgstr ""

#: ../plugin-options/general-options.php:96
msgid "Form Position"
msgstr ""

#: ../plugin-options/general-options.php:97
msgid "Choose the form position in archive shop page"
msgstr ""

#: ../plugin-options/general-options.php:100
msgid "Before add to cart button"
msgstr ""

#: ../plugin-options/general-options.php:101
msgid "After add to cart button"
msgstr ""

#: ../plugin-options/general-options.php:108
msgid "Enable AJAX form in archive pages"
msgstr ""

#: ../plugin-options/general-options.php:110
msgid "Enable AJAX handle for variations form in archive shop pages."
msgstr ""

#: ../plugin-options/general-options.php:116
msgid "Label for 'Add to cart' button"
msgstr ""

#: ../plugin-options/general-options.php:118
msgid ""
"Label for 'Add to cart' button when a variation is selected from archive page"
msgstr ""

#: ../plugin-options/general-options.php:119
msgid "Add to cart"
msgstr ""

#: ../plugin-options/general-options.php:124
msgid "Change product image on hover"
msgstr ""

#: ../plugin-options/general-options.php:126
msgid ""
"Change the product image when the mouse hovers the concerned attribute. "
"PLEASE, NOTE: It works only for products that have only one attribute per "
"variation."
msgstr ""

#: ../plugin-options/general-options.php:132
msgid "Show custom attributes on \"Additional Information\" Tab"
msgstr ""

#: ../plugin-options/general-options.php:134
msgid ""
"Show custom attributes style on \"Additional Information\" Tab instead of "
"simple text."
msgstr ""

#: ../templates/admin/yith-wccl-description-field.php:19
#: ../templates/admin/yith-wccl-description-field.php:30
msgid "Description"
msgstr ""

#: ../templates/admin/yith-wccl-description-field.php:23
#: ../templates/admin/yith-wccl-description-field.php:32
msgid "Description for product attributes."
msgstr ""

#: ../templates/yith-wccl-variable-loop.php:31
msgid "Choose an option"
msgstr ""
