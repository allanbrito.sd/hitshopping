msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Coupon Email System\n"
"POT-Creation-Date: 2016-08-17 09:35+0200\n"
"PO-Revision-Date: 2018-05-30 11:27+0200\n"
"Language-Team: Josselyn Jayant <contact@josselynjayant.fr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.7\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Poedit-Basepath: .\n"
"Last-Translator: \n"
"Language: fr_FR\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../class.yith-wc-coupon-email-system-premium.php:255
msgid "You need to set at least a threshold to send a test email"
msgstr "Vous devez au moins renseigner un seuil pour envoyer un mail de test"

#: ../class.yith-wc-coupon-email-system-premium.php:256
msgid "Please select at least a product"
msgstr "Merci de sélectionner au moins un produit"

#: ../class.yith-wc-coupon-email-system-premium.php:257
msgid ""
"You need to select at least the amount/percentage of a coupon to send it in "
"a test email"
msgstr ""
"Vous devez au moins sélectionner le montant/pourcentage du coupon pour "
"envoyer le mail de test"

#: ../class.yith-wc-coupon-email-system-premium.php:258
msgid "Please specify the number of days"
msgstr "Merci de renseigner le nombre de jours"

#: ../class.yith-wc-coupon-email-system-premium.php:287
msgid "Please enter Mandrill API Key for YITH WooCommerce Coupon Email System"
msgstr ""
"Merci de renseigner la clé API Mandrill pour YITH WooCommerce Coupon Email "
"System"

#: ../class.yith-wc-coupon-email-system-premium.php:310
#: ../includes/class-ywces-multivendor.php:201
msgid "You need to select a coupon to send one for a new first purchase."
msgstr ""
"Vous devez sélectionner un coupon à envoyer une fois dès le premier achat."

#: ../class.yith-wc-coupon-email-system-premium.php:320
#: ../includes/class-ywces-multivendor.php:211
msgid ""
"You need to set a threshold to send a coupon once a user reaches a specific "
"number of purchases."
msgstr ""
"Vous devez appliquer un seuil pour envoyer le coupon dès que l’utilisateur "
"atteint un nombre de commande spécifique."

#: ../class.yith-wc-coupon-email-system-premium.php:338
#: ../includes/class-ywces-multivendor.php:231
msgid ""
"You need to set a coupon for each threshold to send one when users reach a "
"specific number of purchases."
msgstr ""
"Vous devez définir un coupon pour chaque seuil en fonction du nombre de "
"commandes du client."

#: ../class.yith-wc-coupon-email-system-premium.php:349
#: ../includes/class-ywces-multivendor.php:243
msgid ""
"You need to set a threshold to send a coupon once a user reaches a specific "
"spent amount."
msgstr ""
"Vous devez définir un seuil pour envoyer le coupon dès que le client atteint "
"un montant dépensé."

#: ../class.yith-wc-coupon-email-system-premium.php:368
#: ../includes/class-ywces-multivendor.php:263
msgid ""
"You need to set a coupon for each threshold to send one when users reach a "
"specific spent amount."
msgstr ""
"Vous devez définir un coupon pour chaque seuil dès que l’utilisateur atteint "
"un montant dépensé."

#: ../class.yith-wc-coupon-email-system-premium.php:379
#: ../includes/class-ywces-multivendor.php:275
msgid ""
"You need to select at least one product to send a coupon once purchased."
msgstr ""
"Vous devez sélectionner au moins un produit pour déclencher l’envoi du "
"coupon une fois le produit acheté."

#: ../class.yith-wc-coupon-email-system-premium.php:387
#: ../includes/class-ywces-multivendor.php:283
msgid ""
"You need to select at least the amount/percentage of a coupon to send it for "
"the purchase of a specific product."
msgstr ""
"Vous devez sélectionner au moins le montant/pourcentage du coupon à envoyer "
"en fonction de l’achat du produit."

#: ../class.yith-wc-coupon-email-system-premium.php:399
#: ../includes/class-ywces-multivendor.php:295
msgid ""
"You need to select at least the amount/percentage of a coupon to send it for "
"the birthday of a user."
msgstr ""
"Vous devez sélectionner au moins le montant/pourcentage du coupon à envoyer "
"à la date d’anniversaire du client."

#: ../class.yith-wc-coupon-email-system-premium.php:411
#: ../includes/class-ywces-multivendor.php:307
msgid ""
"You need to select at least the amount/percentage of a coupon to send it "
"after a specific number of days following the last order."
msgstr ""
"Vous devez sélectionner au moins le montant/pourcentage du coupon à envoyer "
"après un nombre de jours suivant la dernière commande."

#: ../class.yith-wc-coupon-email-system-premium.php:910
#: ../class.yith-wc-coupon-email-system.php:207
#: ../class.yith-wc-coupon-email-system.php:208
#: ../includes/class-ywces-coupon-email.php:52
#: ../includes/class-ywces-multivendor.php:124
#: ../includes/class-ywces-multivendor.php:125
msgid "Coupon Email System"
msgstr "Coupon Email System"

#: ../class.yith-wc-coupon-email-system-premium.php:914
#: ../class.yith-wc-coupon-email-system-premium.php:1039
#: ../class.yith-wc-coupon-email-system-premium.php:1077
msgid "Birth date"
msgstr "Date d’anniversaire"

#: ../class.yith-wc-coupon-email-system-premium.php:974
msgid "YYYY-MM-DD"
msgstr "YYYY-MM-DD"

#: ../class.yith-wc-coupon-email-system-premium.php:975
msgid "YYYY/MM/DD"
msgstr "YYYY/MM/DD"

#: ../class.yith-wc-coupon-email-system-premium.php:976
msgid "MM-DD-YYYY"
msgstr "MM-DD-YYYY"

#: ../class.yith-wc-coupon-email-system-premium.php:977
msgid "MM/DD/YYYY"
msgstr "MM/DD/YYYY"

#: ../class.yith-wc-coupon-email-system-premium.php:978
msgid "DD-MM-YYYY"
msgstr "DD-MM-YYYY"

#: ../class.yith-wc-coupon-email-system-premium.php:979
msgid "DD/MM/YYYY"
msgstr "DD/MM/YYYY"

#: ../class.yith-wc-coupon-email-system.php:187
#: ../class.yith-wc-coupon-email-system.php:197
msgid "General Settings"
msgstr "Réglages généraux"

#: ../class.yith-wc-coupon-email-system.php:191
msgid "Vendors Settings"
msgstr "Réglages des vendeurs"

#: ../class.yith-wc-coupon-email-system.php:195
#: ../plugin-options/mandrill-options.php:18
msgid "Mandrill Settings"
msgstr "Réglages de Mandrill"

#: ../class.yith-wc-coupon-email-system.php:198
#: ../class.yith-wc-coupon-email-system.php:720
msgid "Premium Version"
msgstr "Version premium"

#: ../class.yith-wc-coupon-email-system.php:201
#: ../includes/class-ywces-multivendor.php:118
msgid "How To"
msgstr "Aide"

#: ../class.yith-wc-coupon-email-system.php:239
#: ../templates/admin/class-ywces-custom-send.php:99
msgid "Sending test email..."
msgstr "Envoi du mail de test…"

#: ../class.yith-wc-coupon-email-system.php:240
msgid "Test email has been sent successfully!"
msgstr "Le mail de test est envoyé avec succès !"

#: ../class.yith-wc-coupon-email-system.php:241
msgid "Please insert a valid email address"
msgstr "Merci de renseigner une adresse mail valide"

#: ../class.yith-wc-coupon-email-system.php:262
msgid ""
"In order to use some of the features of YITH WooCommerce Coupon Email System "
"you need to create at least one coupon"
msgstr ""
"Afin d’utiliser certaines des fonctionnalités de YITH WooCommerce Coupon "
"Email System, vous devez créer au moins un coupon"

#: ../class.yith-wc-coupon-email-system.php:362
msgid "Placeholder reference"
msgstr "Référence des balises"

#: ../class.yith-wc-coupon-email-system.php:371
msgid ""
"Replaced with the description of the given coupon. This placeholder must be "
"included."
msgstr ""
"Remplace la description par la description présente dans le coupon. Cette "
"balise doit être insérée."

#: ../class.yith-wc-coupon-email-system.php:379
msgid "Replaced with the site title"
msgstr "Insère le titre du site"

#: ../class.yith-wc-coupon-email-system.php:387
msgid "Replaced with the customer's name"
msgstr "Insère le prénom du client"

#: ../class.yith-wc-coupon-email-system.php:395
msgid "Replaced with the customer's last name"
msgstr "Insère le nom du client"

#: ../class.yith-wc-coupon-email-system.php:403
msgid "Replaced with the customer's email"
msgstr "Insère le mail du client"

#: ../class.yith-wc-coupon-email-system.php:411
msgid "Replaced with the date of the order"
msgstr "Insère la date de la commande"

#: ../class.yith-wc-coupon-email-system.php:422
msgid "Replaced with the number of purchases"
msgstr "Insère le nombre de commandes effectuées"

#: ../class.yith-wc-coupon-email-system.php:430
msgid "Replaced with the amount of money spent by the customer"
msgstr "Insère le montant dépensé par le client"

#: ../class.yith-wc-coupon-email-system.php:438
msgid "Replaced with the spent amount of money"
msgstr "Insère le montant total dépensé"

#: ../class.yith-wc-coupon-email-system.php:446
msgid "Replaced with the number of days since last purchase"
msgstr "Insère le nombre de jours depuis le dernier achat"

#: ../class.yith-wc-coupon-email-system.php:454
msgid "Replaced with the name of a purchased product"
msgstr "Insère le nom du dernier produit acheté"

#: ../class.yith-wc-coupon-email-system.php:464
msgid "Replaced with the name of the vendor"
msgstr "Insère le nom du vendeur"

#: ../class.yith-wc-coupon-email-system.php:521
#: ../includes/class-ywces-multivendor.php:191
msgid "You need to select a coupon to send one for a new user registration."
msgstr ""
"Vous devez sélectionner le coupon à envoyer pour la création du compte."

#: ../class.yith-wc-coupon-email-system.php:717
#: ../includes/class-ywces-multivendor.php:117
msgid "Settings"
msgstr "Réglages"

#: ../class.yith-wc-coupon-email-system.php:747
msgid "Plugin Documentation"
msgstr "Documentation de l’extension"

#: ../includes/class-ywces-ajax-premium.php:84
#, php-format
msgid "Operation completed. %d coupon trashed."
msgid_plural "Operation completed. %d coupons trashed."
msgstr[0] "Opération terminée. %d coupon à la poubelle"
msgstr[1] "Opération terminée. %d coupons à la poubelle"

#: ../includes/class-ywces-ajax-premium.php:89
#, php-format
msgid "An error occurred: %s"
msgstr "Une erreur est arrivée: %s"

#: ../includes/class-ywces-ajax.php:111
msgid "There was an error while sending the email"
msgstr "Il y a eu une erreur lors de l’envoi du mail"

#: ../includes/class-ywces-ajax.php:123
msgid "Coupon not valid"
msgstr "Le coupon n’est pas valide"

#: ../includes/class-ywces-coupon-email.php:51
msgid ""
"YITH WooCommerce Coupon Email System offers an automatic way to send a "
"coupon to your users according to specific events."
msgstr ""
"YITH WooCommerce Coupon Email System offre un moyen automatique d’envoyer un "
"coupon à vos utilisateurs en fonction d’événements spécifiques."

#: ../includes/class-ywces-emails.php:371
msgid "Coupon code: "
msgstr "Code du coupon: "

#: ../includes/class-ywces-emails.php:384
#, php-format
msgid "Coupon amount: %s%s off"
msgstr "Montant du coupon: %s%s"

#: ../includes/class-ywces-emails.php:386
msgid "Free shipping"
msgstr "Livraison gratuite"

#: ../includes/class-ywces-emails.php:392
#, php-format
msgid "Valid for a minimum purchase of %s"
msgstr "Valide pour un minimum d’achat de %s"

#: ../includes/class-ywces-emails.php:395
#, php-format
msgid "Valid for a maximum purchase of %s"
msgstr "Valide pour un maximum d’achat de %s"

#: ../includes/class-ywces-emails.php:398
#, php-format
msgid "Valid for a minimum purchase of %s and a maximum of %s"
msgstr "Valide pour un minimum d’achat de %s et un maximum de %s"

#: ../includes/class-ywces-emails.php:405
msgid "Valid for:"
msgstr "Valide pour:"

#: ../includes/class-ywces-emails.php:408
#: ../includes/class-ywces-emails.php:425
#, php-format
msgid "Following products: %s"
msgstr "Produits suivant: %s"

#: ../includes/class-ywces-emails.php:413
#: ../includes/class-ywces-emails.php:430
#, php-format
msgid "Products of the following categories: %s"
msgstr "Produits présents dans les catégories: %s"

#: ../includes/class-ywces-emails.php:422
msgid "Not valid for:"
msgstr "Non valide pour:"

#: ../includes/class-ywces-emails.php:438
msgid "This coupon cannot be used in conjunction with other coupons"
msgstr "Ce coupon ne peut pas être utilisé additionné avec un autre coupon"

#: ../includes/class-ywces-emails.php:442
msgid "This coupon will not apply to items on sale"
msgstr "Ce coupon ne sera pas appliqué aux produits en promotions"

#: ../includes/class-ywces-emails.php:451
#, php-format
msgid "Expiration date: %s"
msgstr "Date d’expiration: %s"

#: ../init.php:23
msgid ""
"YITH WooCommerce Coupon Email System is enabled but not effective. It "
"requires WooCommerce in order to work."
msgstr ""
"YITH WooCommerce Coupon Email System est activé mais ne fonctionne pas. Il "
"nécessite WooCommerce pour fonctionner."

#: ../plugin-options/admin-vendor-options.php:21
#, php-format
msgid ""
"Coupon management must be enabled to make YITH WooCommerce Coupon Email "
"System work correctly for vendors. %s Enable %s"
msgstr ""
"La gestion des coupon doit être activée pour faire fonctionner YITH "
"WooCommerce Coupon Email System. %s Activer %s"

#: ../plugin-options/admin-vendor-options.php:29
msgid "Allow coupon event management for vendors"
msgstr "Autoriser la gestion des événements de coupon"

#: ../plugin-options/admin-vendor-options.php:35
msgid "Enable coupon on user registration"
msgstr "Activer le coupon à la création du compte utilisateur"

#: ../plugin-options/admin-vendor-options.php:42
msgid "Enable coupon on first purchase"
msgstr "Activer le coupon à la première commande"

#: ../plugin-options/admin-vendor-options.php:49
msgid "Enable coupon on specific order threshold"
msgstr "Activer le coupon à seuil de commande spécifique"

#: ../plugin-options/admin-vendor-options.php:56
msgid "Enable coupon on specific spent threshold"
msgstr "Activer le coupon à un seuil de dépense spécifique"

#: ../plugin-options/admin-vendor-options.php:63
msgid "Enable coupon on specific product purchase"
msgstr "Activer le coupon en fonction de l’achat d’un produit spécifique"

#: ../plugin-options/admin-vendor-options.php:70
msgid "Enable coupon on customer birthday"
msgstr "Activer le coupon à la date d’anniversaire"

#: ../plugin-options/admin-vendor-options.php:77
msgid "Enable coupon on a specific number of days from the last purchase"
msgstr ""
"Activer le coupon en fonction du nombre de jours après le dernier achat"

#: ../plugin-options/general-options.php:20
#: ../plugin-options/premium-general-options.php:20
#: ../plugin-options/vendor/vendor-options.php:20
msgid "Allowed placeholders:"
msgstr "Balises autorisées:"

#: ../plugin-options/general-options.php:21
#: ../plugin-options/premium-general-options.php:21
#: ../plugin-options/vendor/vendor-options.php:21
msgid "More info"
msgstr "Plus d’information"

#: ../plugin-options/general-options.php:29
#: ../plugin-options/premium-general-options.php:35
#: ../plugin-options/vendor/vendor-options.php:35
msgid "Select a coupon"
msgstr "Sélectionner un coupon"

#: ../plugin-options/general-options.php:36
#: ../plugin-options/premium-general-options.php:50
msgid "Coupon Email System settings"
msgstr "Coupon Email System réglages"

#: ../plugin-options/general-options.php:40
#: ../plugin-options/premium-general-options.php:54
msgid "Enable YITH WooCommerce Coupon Email System"
msgstr "Activer YITH WooCommerce Coupon Email System"

#: ../plugin-options/general-options.php:46
#: ../plugin-options/premium-general-options.php:67
msgid "Email type"
msgstr "Type d’email"

#: ../plugin-options/general-options.php:48
#: ../plugin-options/premium-general-options.php:69
msgid "Choose which email format to send."
msgstr "Choisir le format dans lequel le mail sera envoyé."

#: ../plugin-options/general-options.php:50
#: ../plugin-options/premium-general-options.php:71
msgid "HTML"
msgstr "HTML"

#: ../plugin-options/general-options.php:51
#: ../plugin-options/premium-general-options.php:72
msgid "Plain text"
msgstr "Texte brut"

#: ../plugin-options/general-options.php:61
#: ../plugin-options/premium-general-options.php:165
#: ../plugin-options/vendor/vendor-options.php:108
msgid "On first purchase"
msgstr "Au premier achat"

#: ../plugin-options/general-options.php:68
#: ../plugin-options/premium-general-options.php:110
#: ../plugin-options/premium-general-options.php:172
#: ../plugin-options/premium-general-options.php:234
#: ../plugin-options/premium-general-options.php:295
#: ../plugin-options/premium-general-options.php:356
#: ../plugin-options/premium-general-options.php:434
#: ../plugin-options/premium-general-options.php:501
#: ../plugin-options/vendor/vendor-options.php:50
#: ../plugin-options/vendor/vendor-options.php:115
#: ../plugin-options/vendor/vendor-options.php:180
#: ../plugin-options/vendor/vendor-options.php:244
#: ../plugin-options/vendor/vendor-options.php:308
#: ../plugin-options/vendor/vendor-options.php:389
#: ../plugin-options/vendor/vendor-options.php:459
msgid "Enable coupon sending"
msgstr "Activer l’envoi du coupon"

#: ../plugin-options/general-options.php:75
#: ../plugin-options/premium-general-options.php:117
#: ../plugin-options/premium-general-options.php:179
#: ../plugin-options/vendor/vendor-options.php:57
#: ../plugin-options/vendor/vendor-options.php:122
msgid "Selected Coupon"
msgstr "Sélectionner un coupon"

#: ../plugin-options/general-options.php:77
#: ../plugin-options/premium-general-options.php:119
#: ../plugin-options/premium-general-options.php:181
#: ../plugin-options/vendor/vendor-options.php:59
#: ../plugin-options/vendor/vendor-options.php:124
msgid "Choose the coupon to send"
msgstr "Choisir le coupon à envoyer"

#: ../plugin-options/general-options.php:83
#: ../plugin-options/premium-general-options.php:125
#: ../plugin-options/premium-general-options.php:187
#: ../plugin-options/premium-general-options.php:248
#: ../plugin-options/premium-general-options.php:309
#: ../plugin-options/premium-general-options.php:388
#: ../plugin-options/premium-general-options.php:455
#: ../plugin-options/premium-general-options.php:533
#: ../plugin-options/vendor/vendor-options.php:65
#: ../plugin-options/vendor/vendor-options.php:130
#: ../plugin-options/vendor/vendor-options.php:194
#: ../plugin-options/vendor/vendor-options.php:258
#: ../plugin-options/vendor/vendor-options.php:340
#: ../plugin-options/vendor/vendor-options.php:410
#: ../plugin-options/vendor/vendor-options.php:491
msgid "Email subject"
msgstr "Sujet du mail"

#: ../plugin-options/general-options.php:87
#: ../plugin-options/premium-general-options.php:191
#: ../plugin-options/premium-general-options.php:252
#: ../plugin-options/premium-general-options.php:313
#: ../plugin-options/premium-general-options.php:392
#: ../plugin-options/premium-general-options.php:537
msgid "You have received a coupon from {site_title}"
msgstr "Vous avez reçu un coupon de {site_title}"

#: ../plugin-options/general-options.php:94
#: ../plugin-options/premium-general-options.php:136
#: ../plugin-options/premium-general-options.php:198
#: ../plugin-options/premium-general-options.php:259
#: ../plugin-options/premium-general-options.php:320
#: ../plugin-options/premium-general-options.php:399
#: ../plugin-options/premium-general-options.php:466
#: ../plugin-options/premium-general-options.php:544
#: ../plugin-options/vendor/vendor-options.php:76
#: ../plugin-options/vendor/vendor-options.php:141
#: ../plugin-options/vendor/vendor-options.php:205
#: ../plugin-options/vendor/vendor-options.php:269
#: ../plugin-options/vendor/vendor-options.php:351
#: ../plugin-options/vendor/vendor-options.php:421
#: ../plugin-options/vendor/vendor-options.php:502
msgid "Email content"
msgstr "Contenu du mail"

#: ../plugin-options/general-options.php:98
#: ../plugin-options/premium-general-options.php:202
msgid ""
"Hi {customer_name},\n"
"thanks for making the first purchase on {order_date} on our shop "
"{site_title}!\n"
"Because of this, we would like to offer you this coupon as a little gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"Merci pour votre premier achat le {order_date} sur notre boutique "
"{site_title}!\n"
"Grâce à cette première commande, nous aimerions vous remercier en vous "
"offrant ce coupon de réduction :\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/general-options.php:113
#: ../plugin-options/premium-general-options.php:155
#: ../plugin-options/premium-general-options.php:217
#: ../plugin-options/premium-general-options.php:278
#: ../plugin-options/premium-general-options.php:339
#: ../plugin-options/premium-general-options.php:418
#: ../plugin-options/premium-general-options.php:485
#: ../plugin-options/premium-general-options.php:563
#: ../plugin-options/vendor/vendor-options.php:95
#: ../plugin-options/vendor/vendor-options.php:160
#: ../plugin-options/vendor/vendor-options.php:224
#: ../plugin-options/vendor/vendor-options.php:288
#: ../plugin-options/vendor/vendor-options.php:370
#: ../plugin-options/vendor/vendor-options.php:440
#: ../plugin-options/vendor/vendor-options.php:521
msgid "Test email"
msgstr "Email de test"

#: ../plugin-options/mandrill-options.php:22
msgid "Enable Mandrill"
msgstr "Activer Mandrill"

#: ../plugin-options/mandrill-options.php:24
msgid "Use Mandrill to send emails"
msgstr "Utiliser Mandrill pour envoyer les emails"

#: ../plugin-options/mandrill-options.php:29
msgid "Mandrill API Key"
msgstr "Clé API Mandrill"

#: ../plugin-options/premium-general-options.php:40
msgid "Use YITH WooCommerce Email Templates"
msgstr "Utiliser YITH WooCommerce Email Templates"

#: ../plugin-options/premium-general-options.php:60
msgid "Birthday Input Date Format"
msgstr "Formate de la date d’anniversaire"

#: ../plugin-options/premium-general-options.php:79
msgid "Email template"
msgstr "Modèle d’email"

#: ../plugin-options/premium-general-options.php:81
msgid ""
"Choose which email template to send. Remember to save options before sending "
"the test email."
msgstr ""
"Choisir le modèle d’email à envoyer. Sauvegarder avant d’envoyer un mail de "
"test."

#: ../plugin-options/premium-general-options.php:83
msgid "WooCommerce Template"
msgstr "Modèle de WooCommerce"

#: ../plugin-options/premium-general-options.php:84
msgid "Template 1"
msgstr "Modèle 1"

#: ../plugin-options/premium-general-options.php:85
msgid "Template 2"
msgstr "Modèle 2"

#: ../plugin-options/premium-general-options.php:86
msgid "Template 3"
msgstr "Modèle 3"

#: ../plugin-options/premium-general-options.php:92
msgid "Deletion of Expired Coupons"
msgstr "Suppression des coupons expirés"

#: ../plugin-options/premium-general-options.php:94
msgid ""
"Delete automatically expired coupons (only those created by this plugin)"
msgstr ""
"Supprimer automatiquement les coupons expirés (seulement ceux crées par "
"l’extension)"

#: ../plugin-options/premium-general-options.php:103
#: ../plugin-options/vendor/vendor-options.php:43
msgid "On user registration"
msgstr "A l’enregistrement de l’utilisateur"

#: ../plugin-options/premium-general-options.php:129
msgid "You have received a welcome coupon from {site_title}"
msgstr "Vous avez reçu un coupon de bienvenu de la part de {site_title}"

#: ../plugin-options/premium-general-options.php:140
msgid ""
"Hi {customer_name},\n"
"thanks for your the registration on {site_title}!\n"
"We would like to offer you this coupon as a welcome gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"Merci pour votre inscription sur notre site {site_title}!\n"
"Pour fêter cela, nous vous offre ce coupon de bienvenu:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite !,\n"
"\n"
"{site_title}."

#: ../plugin-options/premium-general-options.php:227
#: ../plugin-options/vendor/vendor-options.php:173
msgid "On specific order threshold"
msgstr "Dès un seuil de commande spécifique"

#: ../plugin-options/premium-general-options.php:241
#: ../plugin-options/vendor/vendor-options.php:187
msgid "Order thresholds"
msgstr "Seuils de commande"

#: ../plugin-options/premium-general-options.php:263
msgid ""
"Hi {customer_name},\n"
"with the order made on {order_date}, you have reached {purchases_threshold} "
"orders!\n"
"Because of this, we would like to offer you this coupon as a gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"Avec le commande du {order_date}, vous atteignez le seuil des  "
"{purchases_threshold} commandes !\n"
"Grâce à cela, nous vous offrons un coupon de réduction valable sur votre "
"prochaine commande:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/premium-general-options.php:288
#: ../plugin-options/vendor/vendor-options.php:237
msgid "On specific spent threshold"
msgstr "Dès un montant (en €) de commande spécifique"

#: ../plugin-options/premium-general-options.php:302
#: ../plugin-options/vendor/vendor-options.php:251
msgid "Amount thresholds"
msgstr "Montants à dépenser"

#: ../plugin-options/premium-general-options.php:324
msgid ""
"Hi {customer_name},\n"
"with the order made on {order_date}, you have reached the amount of "
"{spending_threshold} for a total purchase amount of {customer_money_spent}!\n"
"Because of this, we would like to offer you this coupon as a gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"Avec la commande passée le {order_date}, vous avez atteint le seuil de "
"{spending_threshold} pour un montant total d’achat de\n"
" {customer_money_spent}!\n"
"Grâce à cela, nous vous offrons un coupon de réduction:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/premium-general-options.php:349
#: ../plugin-options/vendor/vendor-options.php:301
msgid "On specific product purchase"
msgstr "Sur l’achat d’un produit spécifique"

#: ../plugin-options/premium-general-options.php:362
#: ../plugin-options/vendor/vendor-options.php:314
msgid "Target products"
msgstr "Produits concernés"

#: ../plugin-options/premium-general-options.php:369
#: ../plugin-options/vendor/vendor-options.php:321
msgid "Search for a product&hellip;"
msgstr "Chercher un produit&hellip;"

#: ../plugin-options/premium-general-options.php:370
#: ../plugin-options/vendor/vendor-options.php:322
msgid "Products that will cause the sending of the coupon"
msgstr "Produits qui provoqueront l’envoi du coupon"

#: ../plugin-options/premium-general-options.php:373
#: ../plugin-options/premium-general-options.php:440
#: ../plugin-options/premium-general-options.php:518
#: ../plugin-options/vendor/vendor-options.php:325
#: ../plugin-options/vendor/vendor-options.php:395
#: ../plugin-options/vendor/vendor-options.php:476
msgid "Coupon settings"
msgstr "Réglages du coupon"

#: ../plugin-options/premium-general-options.php:403
msgid ""
"Hi {customer_name},\n"
"thanks for purchasing the following product with the order made on "
"{order_date}: {purchased_product}.\n"
"We would like to offer you this coupon as a gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"Merci d’avoir passé commande le  {order_date} et d’avoir acheté le produit "
"suivant: {purchased_product}.\n"
"Nous souhaitons vous remercier en vous offrant un coupon de réduction:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/premium-general-options.php:427
#: ../plugin-options/vendor/vendor-options.php:382
msgid "On customer birthday"
msgstr "A l’anniversaire d’un client"

#: ../plugin-options/premium-general-options.php:459
msgid "Happy birthday from {site_title}"
msgstr "Joyeux anniversaire de la part de {site_title}"

#: ../plugin-options/premium-general-options.php:470
msgid ""
"Hi {customer_name},\n"
"we would like to make you our best wishes for a happy birthday!\n"
"Please, accept our coupon as a small gift for you:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"Nous vous souhaitons un très bon anniversaire!\n"
"Pour célébrer cela, nous vous offrons un code promo pour vous faire "
"plaisir:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/premium-general-options.php:494
#: ../plugin-options/vendor/vendor-options.php:452
msgid "On a specific number of days from the last purchase"
msgstr "Après un nombre de jours spécifiques après le dernier achat effectué"

#: ../plugin-options/premium-general-options.php:507
#: ../plugin-options/vendor/vendor-options.php:465
msgid "Days to elapse"
msgstr "Jours écoulés"

#: ../plugin-options/premium-general-options.php:509
#: ../plugin-options/vendor/vendor-options.php:467
msgid ""
"The number of days that have to pass after the last order has been set as "
"\"completed\""
msgstr "Le nombre de jours écoulés après le dernier achat sur le site"

#: ../plugin-options/premium-general-options.php:548
msgid ""
"Hi {customer_name},\n"
"{days_ago} days have passed since your last order.\n"
"We would like to encourage you to purchase something more with this coupon:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{site_title}."
msgstr ""
"Bonjour {customer_name},\n"
"{days_ago} jours se sont écoulés depuis votre dernier achat.\n"
"Nous aimerions vous revoir sur la boutique et pour cela nous vous offrons un "
"coupon de réduction:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/vendor/vendor-options.php:69
msgid "You have received a welcome coupon from {vendor_name} on {site_title}"
msgstr "Vous avez reçu un coupon de bienvenu de {vendor_name} sur {site_title}"

#: ../plugin-options/vendor/vendor-options.php:80
msgid ""
"Hi {customer_name},\n"
"thanks for your the registration on {site_title}!\n"
"We would like to offer you this coupon as a welcome gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Bonjour {customer_name},\n"
"Merci pour votre inscription sur notre site {site_title}!\n"
"Pour fêter cela, nous vous offre ce coupon de bienvenu:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite !,\n"
"\n"
"{site_title}."

#: ../plugin-options/vendor/vendor-options.php:134
#: ../plugin-options/vendor/vendor-options.php:198
#: ../plugin-options/vendor/vendor-options.php:262
#: ../plugin-options/vendor/vendor-options.php:344
#: ../plugin-options/vendor/vendor-options.php:495
msgid "You have received a coupon from {vendor_name} on {site_title}"
msgstr "Vous avez reçu un coupon de {vendor_name} sur {site_title}"

#: ../plugin-options/vendor/vendor-options.php:145
msgid ""
"Hi {customer_name},\n"
"thanks for making the first purchase on {order_date} on our shop "
"{site_title}!\n"
"Because of this, we would like to offer you this coupon as a little gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Bonjour {customer_name},\n"
"Merci pour votre premier achat le {order_date} sur notre boutique "
"{site_title}!\n"
"Grâce à cette première commande, nous aimerions vous remercier en vous "
"offrant ce coupon de réduction :\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{site_title}."

#: ../plugin-options/vendor/vendor-options.php:209
msgid ""
"Hi {customer_name},\n"
"with the order made on {order_date}, you have reached {purchases_threshold} "
"orders!\n"
"Because of this, we would like to offer you this coupon as a gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Bonjour {customer_name},\n"
"Avec la commande effectuée le {order_date}, vous avez atteint "
"{purchases_threshold} commandes!\n"
"Grâce à cela, nous vous offrons un coupon de réduction à utiliser quand vous "
"voulez:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{vendor_name}."

#: ../plugin-options/vendor/vendor-options.php:273
msgid ""
"Hi {customer_name},\n"
"with the order made on {order_date}, you have reached the amount of "
"{spending_threshold} for a total purchase amount of {customer_money_spent}!\n"
"Because of this, we would like to offer you this coupon as a gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Hi {customer_name},\n"
"Avec la commande effectuée le {order_date}, vous avez atteint le montant de "
"{spending_threshold} pour un montant total d’achat de "
"{customer_money_spent}!\n"
"Pour cette raison, nous aimerions vous offrir ce coupon en cadeau:\n"
"\n"
"{coupon_description}\n"
"\n"
"A très vite,\n"
"\n"
"{vendor_name}."

#: ../plugin-options/vendor/vendor-options.php:355
msgid ""
"Hi {customer_name},\n"
"thanks for purchasing the following product with the order made on "
"{order_date}: {purchased_product}.\n"
"We would like to offer you this coupon as a gift:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Bonjour {customer_name},\n"
"Merci d’avoir acheté le produit suivant avec la commande effectuée le "
"{order_date}: {purchased_product}.\n"
"Nous aimerions vous offrir ce coupon en cadeau:\n"
"\n"
"{coupon_description}\n"
"\n"
"A bientôt sur notre boutique,\n"
"\n"
"{vendor_name}."

#: ../plugin-options/vendor/vendor-options.php:414
msgid "Happy birthday from {vendor_name} on {site_title}"
msgstr "Joyeux anniversaire de la part de {vendor_name} sur {site_title}"

#: ../plugin-options/vendor/vendor-options.php:425
msgid ""
"Hi {customer_name},\n"
"we would like to make you our best wishes for a happy birthday!\n"
"Please, accept our coupon as a small gift for you:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Bonjour {customer_name},\n"
"Nous vous souhaitons un très bon anniversaire !\n"
"Pour célébrer cela, nous vous offrons un coupon de réduction:\n"
"\n"
"{coupon_description}\n"
"\n"
"A bientôt sur notre boutique,\n"
"\n"
"{vendor_name}."

#: ../plugin-options/vendor/vendor-options.php:506
msgid ""
"Hi {customer_name},\n"
"{days_ago} days have passed since your last order.\n"
"We would like to encourage you to purchase something more with this coupon:\n"
"\n"
"{coupon_description}\n"
"\n"
"See you on our shop,\n"
"\n"
"{vendor_name}."
msgstr ""
"Bonjour {customer_name},\n"
"{days_ago} jours se sont écoulés depuis votre dernière commande.\n"
"Vous nous manquez et pour vous permettre de passer une nouvelle commande "
"voici un coupon de réduction:\n"
"\n"
"{coupon_description}\n"
"\n"
"A bientôt sur notre boutique,\n"
"\n"
"{vendor_name}."

#: ../templates/admin/class-ywces-custom-collapse.php:79
msgid "Click to collapse/expand the table"
msgstr "Cliquez pour réduire / développer la table"

#: ../templates/admin/class-ywces-custom-collapse.php:83
msgid "Expand"
msgstr "Développer"

#: ../templates/admin/class-ywces-custom-collapse.php:86
msgid "Collapse"
msgstr "Réduire"

#: ../templates/admin/class-ywces-custom-coupon-purge.php:110
msgid "Delete expired coupons"
msgstr "Supprimer les coupons expirés"

#: ../templates/admin/class-ywces-custom-coupon-purge.php:111
msgid "Deleting expired coupons..."
msgstr "Suppression des coupons expirés…"

#: ../templates/admin/class-ywces-custom-coupon.php:89
msgid "Discount type"
msgstr "Type de remise"

#: ../templates/admin/class-ywces-custom-coupon.php:118
msgid "Coupon amount"
msgstr "Montant du coupon"

#: ../templates/admin/class-ywces-custom-coupon.php:132
msgid "Expiry days after coupon release"
msgstr "Le coupon expire après combien de jours"

#: ../templates/admin/class-ywces-custom-coupon.php:140
msgid "No expiration"
msgstr "Pas d’expiration"

#: ../templates/admin/class-ywces-custom-coupon.php:147
msgid "Minimum spend"
msgstr "Dépense minimum"

#: ../templates/admin/class-ywces-custom-coupon.php:155
msgid "No minimum"
msgstr "Pas de minimum"

#: ../templates/admin/class-ywces-custom-coupon.php:161
msgid "Maximum spend"
msgstr "Dépense maximales"

#: ../templates/admin/class-ywces-custom-coupon.php:169
msgid "No maximum"
msgstr "Pas de maximum"

#: ../templates/admin/class-ywces-custom-coupon.php:183
msgid "Allow free shipping"
msgstr "Autoriser la livraison gratuite"

#: ../templates/admin/class-ywces-custom-coupon.php:195
msgid "Individual use only"
msgstr "Utiliser individuellement"

#: ../templates/admin/class-ywces-custom-coupon.php:207
msgid "Exclude sale items"
msgstr "Exclure les produits en promotion"

#: ../templates/admin/class-ywces-custom-send.php:95
msgid "Type an email address to send a test email"
msgstr "Renseigner un mail pour tester"

#: ../templates/admin/class-ywces-custom-send.php:98
msgid "Send Test Email"
msgstr "Envoyer le mail de test"

#: ../templates/admin/class-ywces-custom-table.php:88
msgid "Target value"
msgstr "Valeur"

#: ../templates/admin/class-ywces-custom-table.php:89
msgid "Coupon assigned"
msgstr "Coupon associé"

#: ../templates/admin/class-ywces-custom-table.php:150
msgid "+ Add Threshold"
msgstr "+ Ajouter le seuil"

#: ../templates/admin/class-ywces-custom-table.php:152
msgid "Remove selected threshold(s)"
msgstr "Supprimer le(s) seuil(s) sélectionné(s)"
