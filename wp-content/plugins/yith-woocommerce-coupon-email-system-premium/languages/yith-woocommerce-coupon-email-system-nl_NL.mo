��    �        �   
      �     �  )   �     �     �     �       
   %     0     K  "   e  \   �  "   �               %     B     P     h     x  w   �     �       
     
   *     5  H   D     �     �     �     �     �     �       
           +   0  A   \  "   �     �  )   �  *     )   6  "   `     �     �     �     �      �     �     �                #  1   D  �   v  �   ^  �   G  �   .  �     �   �  �   �  �   O  !    "  4  �   W  �   =  �   $  �   �     �  s   �     <   
   P   
   [      f      w      �      �   	   �      �   
   �   
   �      �   3   �      !     +!     =!     Y!     v!     �!  P   �!     �!     	"  
   "  F   *"  #   q"      �"  !   �"     �"  (   �"  2   #     D#  7   a#  "   �#  &   �#  !   �#  #   $  U   )$  -   $  $   �$  4   �$  %   %     -%  '   J%     r%     �%     �%     �%     �%     �%     �%     �%  
   �%  
   &  
   &  
   &  &   '&  U   N&  *   �&  <   �&  +   '  *   8'     c'  $   '  "   �'  "   �'  6   �'  
   !(     ,(     =(  l   R(  y   �(  
   9)  
   D)  ,   O)  =   |)  4   �)  E   �)  A   5*  D   w*  H   �*  �   +  d   �+  p   �+  X   ^,  h   �,  a    -  `   �-  Y   �-  9   =.  �  w.     1  8   1     U1     p1     �1     �1     �1     �1     �1  #   �1  t   "2  '   �2  	   �2     �2  !   �2     �2     3     )3     @3  �   M3     �3     �3  
   
4  
   4      4  X   24  "   �4  '   �4  '   �4     �4     5     5     (5  
   85     C5  /   X5  L   �5  +   �5  *   6  9   ,6  6   f6  ?   �6  1   �6     7     -7  	   K7     U7  "   e7     �7     �7     �7     �7  !   �7  2   �7  �   '8     9     (:    );  �   +<  �   �<  �   �=  �   �>  ;  U?  <  �@  �   �A  �   �B  �   �C  �   �D     _E  u   fE     �E  
   �E  
   F     F     #F     9F     JF     [F     kF     |F     �F     �F  6   �F     �F     �F  %   G  "   ,G  "   OG     rG  |   �G     	H     H     0H  E   =H  )   �H     �H     �H     �H  )   �H  ;   "I  "   ^I  9   �I  !   �I  )   �I  #   J  )   +J  [   UJ  /   �J  &   �J  8   K  "   AK  #   dK  $   �K     �K     �K     �K     �K     L     L     ,L     CL  
   WL  
   bL  
   mL  
   xL     �L  [   �L  .   �L  E   .M  A   tM  8   �M  (   �M  )   N  &   BN  '   iN  @   �N     �N     �N     �N  �   O  �   �O  
   !P  
   ,P  -   7P  C   eP  ;   �P  L   �P  J   2Q  Z   }Q  V   �Q  �   /R  }   �R  �   OS  h   �S  �   >T  �   �T  y   VU  {   �U  J   LV     �   �   (   W   G       �   e   <              �   %   �           �   C   m          H   "       q       Q          ^   B       9   �          /       N   �   v   �   R       g           z           =       ,   w   0   �       +   �   K      1   4   O   V   #   `   \   �       ~              3   
   �   U      �   [       ]   A       t            r   &   2   c   a       �   �       E   n   |               d           �   �       J       �               j         Z   o   S   ;   �   6   D   M   $       	   7            l       L       T      f       k      �          *   {   y   .       x   �       >      h   �   @   �   !   8   F   �      -   �      X   5               '       �   �                     P           s             �   i           ?       Y   �       u   �   :                  _           I         p   }   �           b      )   �    + Add Threshold Allow coupon event management for vendors Allow free shipping Allowed placeholders: Amount thresholds An error occurred: %s Birth date Birthday Input Date Format Choose the coupon to send Choose which email format to send. Choose which email template to send. Remember to save options before sending the test email. Click to collapse/expand the table Collapse Coupon Email System Coupon Email System settings Coupon amount Coupon amount: %s%s off Coupon assigned Coupon code:  Coupon management must be enabled to make YITH WooCommerce Coupon Email System work correctly for vendors. %s Enable %s Coupon not valid Coupon settings DD-MM-YYYY DD/MM/YYYY Days to elapse Delete automatically expired coupons (only those created by this plugin) Delete expired coupons Deleting expired coupons... Deletion of Expired Coupons Discount type Email content Email subject Email template Email type Enable Mandrill Enable YITH WooCommerce Coupon Email System Enable coupon on a specific number of days from the last purchase Enable coupon on customer birthday Enable coupon on first purchase Enable coupon on specific order threshold Enable coupon on specific product purchase Enable coupon on specific spent threshold Enable coupon on user registration Enable coupon sending Exclude sale items Expand Expiration date: %s Expiry days after coupon release Following products: %s Free shipping General Settings HTML Happy birthday from {site_title} Happy birthday from {vendor_name} on {site_title} Hi {customer_name},
thanks for making the first purchase on {order_date} on our shop {site_title}!
Because of this, we would like to offer you this coupon as a little gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
thanks for making the first purchase on {order_date} on our shop {site_title}!
Because of this, we would like to offer you this coupon as a little gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
thanks for purchasing the following product with the order made on {order_date}: {purchased_product}.
We would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
thanks for purchasing the following product with the order made on {order_date}: {purchased_product}.
We would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
thanks for your the registration on {site_title}!
We would like to offer you this coupon as a welcome gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
thanks for your the registration on {site_title}!
We would like to offer you this coupon as a welcome gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
we would like to make you our best wishes for a happy birthday!
Please, accept our coupon as a small gift for you:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
we would like to make you our best wishes for a happy birthday!
Please, accept our coupon as a small gift for you:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
with the order made on {order_date}, you have reached the amount of {spending_threshold} for a total purchase amount of {customer_money_spent}!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
with the order made on {order_date}, you have reached the amount of {spending_threshold} for a total purchase amount of {customer_money_spent}!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
with the order made on {order_date}, you have reached {purchases_threshold} orders!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
with the order made on {order_date}, you have reached {purchases_threshold} orders!
Because of this, we would like to offer you this coupon as a gift:

{coupon_description}

See you on our shop,

{vendor_name}. Hi {customer_name},
{days_ago} days have passed since your last order.
We would like to encourage you to purchase something more with this coupon:

{coupon_description}

See you on our shop,

{site_title}. Hi {customer_name},
{days_ago} days have passed since your last order.
We would like to encourage you to purchase something more with this coupon:

{coupon_description}

See you on our shop,

{vendor_name}. How To In order to use some of the features of YITH WooCommerce Coupon Email System you need to create at least one coupon Individual use only MM-DD-YYYY MM/DD/YYYY Mandrill API Key Mandrill Settings Maximum spend Minimum spend More info No expiration No maximum No minimum Not valid for: On a specific number of days from the last purchase On customer birthday On first purchase On specific order threshold On specific product purchase On specific spent threshold On user registration Operation completed. %d coupon trashed. Operation completed. %d coupons trashed. Order thresholds Placeholder reference Plain text Please enter Mandrill API Key for YITH WooCommerce Coupon Email System Please insert a valid email address Please select at least a product Please specify the number of days Premium Version Products of the following categories: %s Products that will cause the sending of the coupon Remove selected threshold(s) Replaced with the amount of money spent by the customer Replaced with the customer's email Replaced with the customer's last name Replaced with the customer's name Replaced with the date of the order Replaced with the description of the given coupon. This placeholder must be included. Replaced with the name of a purchased product Replaced with the name of the vendor Replaced with the number of days since last purchase Replaced with the number of purchases Replaced with the site title Replaced with the spent amount of money Search for a product&hellip; Select a coupon Selected Coupon Send Test Email Sending test email... Settings Target products Target value Template 1 Template 2 Template 3 Test email Test email has been sent successfully! The number of days that have to pass after the last order has been set as "completed" There was an error while sending the email This coupon cannot be used in conjunction with other coupons This coupon will not apply to items on sale Type an email address to send a test email Use Mandrill to send emails Use YITH WooCommerce Email Templates Valid for a maximum purchase of %s Valid for a minimum purchase of %s Valid for a minimum purchase of %s and a maximum of %s Valid for: Vendors Settings WooCommerce Template YITH WooCommerce Coupon Email System is enabled but not effective. It requires WooCommerce in order to work. YITH WooCommerce Coupon Email System offers an automatic way to send a coupon to your users according to specific events. YYYY-MM-DD YYYY/MM/DD You have received a coupon from {site_title} You have received a coupon from {vendor_name} on {site_title} You have received a welcome coupon from {site_title} You have received a welcome coupon from {vendor_name} on {site_title} You need to select a coupon to send one for a new first purchase. You need to select a coupon to send one for a new user registration. You need to select at least one product to send a coupon once purchased. You need to select at least the amount/percentage of a coupon to send it after a specific number of days following the last order. You need to select at least the amount/percentage of a coupon to send it for the birthday of a user. You need to select at least the amount/percentage of a coupon to send it for the purchase of a specific product. You need to select at least the amount/percentage of a coupon to send it in a test email You need to set a coupon for each threshold to send one when users reach a specific number of purchases. You need to set a coupon for each threshold to send one when users reach a specific spent amount. You need to set a threshold to send a coupon once a user reaches a specific number of purchases. You need to set a threshold to send a coupon once a user reaches a specific spent amount. You need to set at least a threshold to send a test email Project-Id-Version: YITH WooCommerce Coupon Email System
POT-Creation-Date: 2018-09-20 10:58+0100
PO-Revision-Date: 2018-09-20 10:58+0100
Last-Translator: 
Language-Team: Your Inspiration Themes <plugins@yithemes.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.13
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
 + drempel toevoegen Kortingsbon evenementenbeheer inschakelen voor verkopers Gratis verzending toestaan Toegestane placeholders: Bedrag drempel Er is een fout opgetreden: %s Geboortedatum Birthday Input Date Format Kies de coupon om te verzenden Kies welk email format te versturen Kies welke e-mail template je wilt versturen. Vergeet de opties niet op te slaan voor het sturen van de test e-mail. Klik voor inklappen/uitvouwen van tabel Inklappen Coupon email system Email coupon systeem instellingen Waarde kortingsbon Coupon waarde %s%s off Kortingsbon toegewezen Coupon code: Kortingsbon management moet worden ingeschakeld om YITH WooCommerce Coupon Email System juist te laten werken voor verkopers. %s inschakelen %s. Coupon niet geldig Kortingsbon instellingen DD-MM-JJJJ DD/MM/JJJJ Dagen voor afloop Verwijder verlopen kortingsbonnen automatisch (alleen die zijn gemaakt door deze plugin) Verwijder vervallen kortingsbonnen Vervallen kortingsbonnen verwijderen... Verwijderen van verlopen kortingsbonnen Type korting Email inhoud Email onderwerp E-mail template Email type Mandrill inschakelen Schakel YITH WooCommerce Coupon Email System in Kortingsbon bij een specifiek aantal dagen sinds laatste aankoop inschakelen Kortingsbon op verjaardag klant inschakelen Kortingsbon bij eerste aankoop inschakelen Kortingsbon bij specifieke bestelling drempel inschakelen Kortingsbon bij specifieke product aankoop inschakelen Kortingsbon bij specifiek uitgegeven bedrag drempel inschakelen Kortingsbon bij gebruikersregistratie inschakelen Coupon verzending inschakelen Uitverkoop artikel uitsluiten Uitvouwen Vervaldatum: %s Vervaldagen na uitgave kortingsbon Op de volgende producten: %s Gratis verzending Algemene instellingen HTML Fijne verjaardag van {site_title} Fijne verjaardag van {vendor_name} op {site_title} Hallo, {customer_name},
 bedankt voor u eerste aankoop op  {order_date} in onze online shop {site_title}!
Na aanleiding van uw aankoop, willen we u graag een coupon cadeau aanbieden. 

 {coupon_description}

Zie u in onze online shop!
{site_title}. Hoi {customer_name},
Bedankt voor het maken van de eerste aankoop op {order_date} in onze winkel {site_title}!
Daarom willen we je graag deze kortingsbon aanbieden als een klein cadeau:

{coupon_description}

We zien je graag terug in onze winkel,

{vendor_name}. Hoi {customer_name},
Bedankt voor het kopen van het volgende product, met de bestelling gemaakt op {order_date}: {purchased_product}.
Willen we je deze kortingsbon graag aanbieden als cadeau:

{coupon_description}

We zien je in onze winkel,

{site_title}. Hoi {customer_name},
Bedankt voor het kopen van het volgende product, met de bestelling gemaakt op {order_date}: {purchased_product}.
Willen we je deze kortingsbon graag aanbieden als cadeau:

{coupon_description}

We zien je in onze winkel,

{vendor_name}. Hoi {customer_name},
Bedankt voor je registratie op {site_title}!
We bieden je graag deze kortingsbon aan als welkomstgeschenk:

{coupon_description}

We zien je in onze winkel,

{site_title}. Hoi {customer_name},
Bedankt voor je registratie op {site_title}!
We bieden je graag deze kortingsbon aan als welkomstgeschenk:

{coupon_description}

We zien je in onze winkel,

{vendor_name}. Hoi {customer_name},
We willen je graag een fijne verjaardag wensen!
We bieden je hierbij een kleine attentie aan in de vorm van een kortingsbon:

{coupon_description}

We zien je in onze winkel,

{site_title}. Hoi {customer_name},
We willen je graag een fijne verjaardag wensen!
We bieden je hierbij een kleine attentie aan in de vorm van een kortingsbon:

{coupon_description}

We zien je in onze winkel,

{vendor_name}. Hoi {customer_name},
Met de bestelling geplaatst op {order_date}, heb je het bedrag van {spending_threshold} bereikt voor het totaal uitgegeven bedrag van {customer_money_spent}!
Om deze reden willen we je graag deze kortingsbon aanbieden als cadeau:

{coupon_description}

We zien je in onze winkel,

{site_title}. Hoi {customer_name},
Met de bestelling geplaatst op {order_date}, heb je het bedrag van {spending_threshold} bereikt voor het totaal uitgegeven bedrag van {customer_money_spent}!
Om deze reden willen we je graag deze kortingsbon aanbieden als cadeau:

{coupon_description}

We zien je in onze winkel,

{vendor_name}. Hoi {customer_name},
met de bestelling gemaakt op {order_date}, heb je je {purchases_threshold} bereikt!
Om deze reden bieden we je graag deze kortingsbon aan als cadeau.:

{coupon_description}

We zien je in onze winkel,

{site_title}. Hoi {customer_name},
met de bestelling gemaakt op {order_date}, heb je je {purchases_threshold} bereikt!
Om deze reden bieden we je graag deze kortingsbon aan als cadeau.:

{coupon_description}

We zien je in onze winkel,

{vendor_name}. Hoi {customer_name},
{days_ago} dagen zijn gepasseerd sinds je laatste bestelling.
We willen je graag aanmoedigen iets te kopen in onze winkel met deze kortingsbon:

{coupon_description}

We zien je in onze winkel,

{site_title}. Hoi {customer_name},
{days_ago} days have passed since your last order.
We would like to encourage you to purchase something more with this coupon:

{coupon_description}

See you on our shop,

{vendor_name}. Hoe te Om Sommige functies te gebruiken van YITH WooCommerce Coupon Email System, moet u eerst tenminste een coupon aanmaken Uitsluitend individueel gebruik MM-DD-JJJJ MM/DD/JJJJ Mandrill API Key Mandrill instellingen Maximum uitgaven Minimum uitgaven Meer informatie Geen vervaldatum Geen maximum Geen minimum Niet geldig voor: Op een specifiek aantal dagen sinds de laatste aankoop Op verjaardag klant Bij eerste aankoop Bij een specifieke bestelling drempel Bij een specifieke product aankoop Bij een specifieke uitgave drempel Bij gebruikersregistratie Operatie afgerond. %d kortingsbon verplaatst naar prullenbak Operatie afgerond. %d kortingsbonnen verplaatst naar prullenbak Andere drempels Placeholder referentie Alleen tekst Voer de Mandrill API Key in voor YITH WooCommerce Coupon Email System Vul alstublieft een geldig email adres in Selecteer minstens een product Specificeer een aantal dagen Premium versie Producten uit de volgende catogorien:  %s Producten die het versturen van een kortingsbon veroorzaken Verwijder geselecteerde drempel(s) Vervangen door het bedrag dat uitgegeven is door de klant Vangen door de email van de klant Vervangen door de achternaam van de klant Vervangen door de naam van de klant Vervangen door de datum van de bestelling Vervangen door de beschrijving van de gegeven coupon. Deze placeholder moet inclusief zijn. Vervangen door de naam van het gekochte product Vervangen door de naam van de verkoper Vervangen door het aantal dagen sinds de laatste aankoop Vervangen door het nummer aankopen Vervangen door de titel van de site Vervangen door het uitgegeven bedrag Zoeken naar een product&hellip; Selecteer een coupon Geselecteerde coupon Verstuur test email Verstuur test email Instellingen Doelstelling producten Waarde doelstelling Template 1 Template 2 Template 3 Test email Test email succesvol verstuurd! Het aantal dagen dat moet passeren voordat de laatste bestelling is gewijzigd in "afgerond" Er was een error bij het verzenden van de mail Deze coupon kan niet gebruikt worden in combinatie met andere coupons Deze coupon is niet geldig op producten die in de aanbieding zijn Type een email adres om een test email naar te versturen Gebruik Mandrill om e-mails te versturen Gebruik  YITH WooCommerce Email Templates Geldig voor een maximum aankoop van %s Geldig voor een minimum aankoop van  %s Geldig voor een minimum aankoop van %s en maximum aankoop van %s Geldig voor: Verkopers instellingen WooCommerce Template YITH WooCommerce Coupon Email System is ingeschakeld maar niet effectief. Het heeft WooCommerce nodig om te kunnen functioneren. YITH WooCommerce Coupon Email System biedt je een automatische manier om een kortingsbon te sturen aan gebruikers volgens specifieke gebeurtenissen. JJJJ-MM-DD JJJJ/MM/DD U heeft een coupon ontvangen van {site_title} Je hebt een kortingsbon ontvangen van {vendor_name} op {site_title} Je hebt een welkomst kortingsbon ontvangen van {site_title} Je hebt een welkomst kortingsbon ontvangen van {vendor_name} op {site_title} Je moet een coupon selecteren om te sturen voor een nieuwe eerste aankoop. Je moet een coupon selecteren om te versturen bij een nieuwe registratie van een gebruiker Je moet minstens een product selecteren om een kortingsbon te sturen, eenmaal gekocht. Je moet minstens het bedrag/percentage van een kortingsbon selecteren voor het versturen nadat een bepaald aantal dagen sinds laatste bestelling zijn verstreken. Je moet minstens het bedrag/percentage van een kortingsbon selecteren voor het versturen voor de verjaardag van een gebruiker Je moet minsten het bedrag/percentage selecteren voor een kortingsbon om deze te versturen voor de aankoop van een specifiek product. Je moet minstens het bedrag/percentage selecteren van een kortingsbon om te versturen in een test e-mail Je moet een kortingsbon instellen voor iedere drempel, om er een te versturen wanneer een gebruiker een specifiek aantal aankopen bereikt. Je moet een kortingsbon instellen voor iedere drempel, om er een te versturen wanneer een gebruiker een specifiek uitgegeven bedrag bereikt. Je moet een drempel instellen om een kortingsbon te versturen, zodra een gebruiker een specifiek aantal aankopen bereikt. Je moet een drempel instellen om een kortingsbon te versturen, zodra een gebruiker een specifiek uitgegeven bedrag bereikt. Je moet minstens een drempel instellen om een test e-mail te kunnen sturen 