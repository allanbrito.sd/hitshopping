<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WCGPF_VERSION' ) ) {
    exit( 'Direct access forbidden.' );
}

/**
 * @class      YITH_WCGPF_Product_Functions_Premium
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author     Your Inspiration Themes
 *
 */

if ( ! class_exists( 'YITH_WCGPF_Product_Functions_Premium' ) ) {

    class YITH_WCGPF_Product_Functions_Premium extends YITH_WCGPF_Product_Functions
    {
        /**
         * Main Instance
         *
         * @var YITH_WCGPF_Product_Functions_Premium
         * @since 1.0
         * @access protected
         */
        protected static $_instance = null;

        public $create_feed = null;

        /**
         * Main plugin Instance
         *
         * @return
         * @var YITH_WCGPF_Product_Functions_Premium instance
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         */
        public static function get_instance()
        {
            $self = __CLASS__ . (class_exists(__CLASS__ . '_Premium') ? '_Premium' : '');

            if (is_null($self::$_instance)) {
                $self::$_instance = new $self;
            }

            return $self::$_instance;
        }

        /**
         * Construct
         *
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         * @since 1.0
         */

        public function __construct()
        {
            add_filter('yith_wcgpf_get_product_condition',array($this,'add_default_option'));
            add_filter('yith_wcgpf_adult_option',array($this,'add_default_option'));
            parent::__construct();
        }

        /**
         * Get energy efficiency
         *
         * @return array
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         */
        public function energy_efficiency() {

            $energy_efficiency = array(
                'default' => __('General field','yith-google-product-feed-for-woocommerce'),
                'A+++'         => __( 'A+++', 'yith-google-product-feed-for-woocommerce' ),
                'A++'         => __( 'A++', 'yith-google-product-feed-for-woocommerce' ),
                'A+'         => __( 'A+', 'yith-google-product-feed-for-woocommerce' ),
                'A'         => __( 'A', 'yith-google-product-feed-for-woocommerce' ),
                'B'         => __( 'B', 'yith-google-product-feed-for-woocommerce' ),
                'C'         => __( 'C', 'yith-google-product-feed-for-woocommerce' ),
                'D'         => __( 'D', 'yith-google-product-feed-for-woocommerce' ),
                'E'         => __( 'E', 'yith-google-product-feed-for-woocommerce' ),
                'F'         => __( 'F', 'yith-google-product-feed-for-woocommerce' ),
                'G'         => __( 'G', 'yith-google-product-feed-for-woocommerce' ),
            );

            return apply_filters('yith_wcgpf_energy_efficiency_option',$energy_efficiency);
        }
        /**
         * Get gender
         *
         * @return array
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         */
        public function gender() {

            $energy_efficiency = array(
                'default' => __('General field','yith-google-product-feed-for-woocommerce'),
                'male'         => __( 'Male', 'yith-google-product-feed-for-woocommerce' ),
                'female'         => __( 'Female', 'yith-google-product-feed-for-woocommerce' ),
                'unisex'         => __( 'Unisex', 'yith-google-product-feed-for-woocommerce' ),
            );
            return apply_filters('yith_wcgpf_gender_option',$energy_efficiency);
        }
        /**
         * Get age group
         *
         * @return array
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         */
        public function age_group() {

            $energy_efficiency = array(
                'default' => __('General field','yith-google-product-feed-for-woocommerce'),
                'newborn'         => __( 'Newborn', 'yith-google-product-feed-for-woocommerce' ),
                'infant'         => __( 'Infant', 'yith-google-product-feed-for-woocommerce' ),
                'toddler'         => __( 'Toddler', 'yith-google-product-feed-for-woocommerce' ),
                'kids'            => __( 'Kid', 'yith-google-product-feed-for-woocommerce' ),
                'adult'           => __( 'Adult', 'yith-google-product-feed-for-woocommerce' ),
            );
            return apply_filters('yith_wcgpf_age_group_option',$energy_efficiency);
        }

        /**
         * Get size type
         *
         * @return array
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         */
        public function size_type() {

            $size_type = array(
                'default' => __('General field','yith-google-product-feed-for-woocommerce'),
                'regular'           => __( 'Regular', 'yith-google-product-feed-for-woocommerce' ),
                'petite'            => __( 'Petite', 'yith-google-product-feed-for-woocommerce' ),
                'plus'              => __( 'Plus', 'yith-google-product-feed-for-woocommerce' ),
                'big-and-tall'      => __( 'Big and tall', 'yith-google-product-feed-for-woocommerce' ),
                'maternity'         => __( 'Maternity', 'yith-google-product-feed-for-woocommerce' ),
            );
            return apply_filters('yith_wcgpf_size_type_option',$size_type);
        }
        /**
         * Get size system
         *
         * @return array
         * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
         */
        public function size_system() {

            $energy_efficiency = array(
                'default' => __('General field','yith-google-product-feed-for-woocommerce'),
                'US'         => __( 'United States sizing', 'yith-google-product-feed-for-woocommerce' ),
                'UK'         => __( 'United Kingdom sizing ', 'yith-google-product-feed-for-woocommerce' ),
                'EU'         => __( 'European sizing', 'yith-google-product-feed-for-woocommerce' ),
                'DE'            => __( 'German sizing', 'yith-google-product-feed-for-woocommerce' ),
                'FR'           => __( 'French sizing', 'yith-google-product-feed-for-woocommerce' ),
                'JP'           => __( 'Japanese sizing', 'yith-google-product-feed-for-woocommerce' ),
                'CN'           => __( 'Chinese sizing', 'yith-google-product-feed-for-woocommerce' ),
                'IT'           => __( 'Italian sizing', 'yith-google-product-feed-for-woocommerce' ),
                'BR'           => __( 'Brazilian sizing', 'yith-google-product-feed-for-woocommerce' ),
                'MEX'           => __( 'Mexican sizing', 'yith-google-product-feed-for-woocommerce' ),
                'AU'           => __( 'Australian sizing', 'yith-google-product-feed-for-woocommerce' ),

            );
            return apply_filters('yith_wcgpf_size_system_option',$energy_efficiency);
        }

        public function add_default_option($options) {
            $default_option = array(
                'default' => __('General field','yith-google-product-feed-for-woocommerce'),
            );

            $options = array_merge($default_option,$options);

            return $options;
        }
    }
}

