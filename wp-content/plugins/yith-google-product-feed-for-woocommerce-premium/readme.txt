=== YITH Google Product Feed for WooCommerce Premium  ===

== Changelog ==

= Version 1.1.0 - Released: May 21, 2018 =

* New: Spanish translation
* New: Support to WordPress 4.9.6 RC2
* New: Support to WooCommerce 3.4.0 RC1
* Update: Plugin Framework
* Update: Dutch translation

= Version 1.0.10 - Released: Apr 24, 2018 =

* New : Spanish version
* Fix : Replaced yith_wcgpf_pfd_shipping_weigth to yith_wcgpf_pfd_shipping_weight
* Fix : Get custom variable for variations product.
* Fix : Blank Spaces in the name for feed file
* Fix : Get additional images

= Version 1.0.9 - Released: Jan 30, 2018 =

* New: support to WordPress 4.9.2
* New: support to WooCommerce 3.3.0-RC2
* Update: plugin framework 3.0.11
* Dev : added yith_wcgpf_feed_url_merchant

= Version 1.0.8 - Released: Jan 15, 2018 =

* Fix : style issue in filter and conditions select
* New : Dutch translation
* Dev : added yith_wcgpf_show_brand_yith_plugin hook

= Version 1.0.7 - Released: Dic 14, 2017 =

* New : compatibility with plugin-fw 3.0
* Dev : added yith_wcgpf_get_google_categories_url hook

= Version 1.0.6 - Released: Nov 30, 2017 =

* Fix : weight label
* Fix : get the correct google categories
* Update: Language .pot

= Version 1.0.5 - Released: Oct 25, 2017 =

* New : added general field in product information and variation information
* Fix : prevent sintax error generate feed
* Update: YITH plugin framework
* Dev: added filter yith_wcgpf_product_condition
* Dev: added filter yith_wcgpf_get_sale_price
* Dev: added filter yith_wcgpf_get_sale_price_if_exists

= Version 1.0.4 - Released: Aug 28, 2017 =

* Fix : Error get a short description for product
* Fix : Error identifier_exist parameter in the feed

= Version 1.0.3 - Released: Aug 14, 2017 =

* New : Added strip tags to clean code
* New : General options to show price with tax
* New : Italian translation by Antonio Mercurio
* Fix : Error when filtering products in the feed
* Fix : Strip HTML tags from description row
* Fix : Prevent get orphan product
* Fix : Add product categories to the feed
* Dev : Added filter yith_wcgpf_enable_variation_in_feed
* Dev : Added filter yith_wcgpf_custom_fields_for_variations
* Dev : Added filter yith_wcgpf_get_attributes_wc
* Dev : Added filter yith_wcgpf_custom_fields_for_variations
* Dev : Added filter yith_wcgpf_get_variation_title
* Dev : Added filter yith_wcgpf_preg_replace_description
* Dev : Added filter yith_wcgpf_fields_value
f
= Version 1.0.2 - Released: May 22, 2017 =

* New : Compatibility with YITH WooCommerce Brands Add-On
* Fix : Error when filtering products in the feed
* Fix : Strip HTML tags from description row
* Dev : Added filter yith_wcgpf_values_in_feed

= Version 1.0.1 - Released: May 10, 2017 =
* Fix : Error generating feed.
* Update: YITH plugin framework

= Version 1.0.0 - Released: Apr 28, 2017 =

* Initial release