msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Category Accordion\n"
"POT-Creation-Date: 2016-03-24 12:33+0100\n"
"PO-Revision-Date: 2016-07-18 15:51+0100\n"
"Last-Translator: \n"
"Language-Team: Yithemes <plugins@hitoutlets.com>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;"
"_n_nooop:1,2;_c,_nc:4c,1,2;_x:1,2c\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPathExcluded-0: ../plugin-fw\n"

#: ../includes/class.yith-category-accordion-shortcode.php:28
#: ../includes/class.yith-category-accordion-widget.php:95
#: ../includes/class.yith-category-accordion-widget.php:334
msgid "WooCommerce TAGS"
msgstr "ETIQUETAS WooCommerce"

#: ../includes/class.yith-category-accordion-shortcode.php:29
#: ../includes/class.yith-category-accordion-widget.php:96
#: ../includes/class.yith-category-accordion-widget.php:335
msgid "WordPress TAGS"
msgstr "Etiquetas WordPress"

#: ../includes/class.yith-category-accordion-shortcode.php:187
msgid "No Tags"
msgstr "Sin etiquetas"

#: ../includes/class.yith-category-accordion-widget.php:13
msgid "YITH WooCommerce Category Accordion"
msgstr "YITH WooCommerce Category Accordion"

#: ../includes/class.yith-category-accordion-widget.php:14
msgid "Show your categories in an accordion!"
msgstr "¡Muestra tus categorías en un acordeón!"

#: ../includes/class.yith-category-accordion-widget.php:133
#: ../templates/admin/lightbox.php:131
msgid "Title"
msgstr "Título"

#: ../includes/class.yith-category-accordion-widget.php:134
#: ../templates/admin/lightbox.php:132
msgid "Insert a title"
msgstr "Inserta un título"

#: ../includes/class.yith-category-accordion-widget.php:137
#: ../templates/admin/lightbox.php:135
msgid "Show in Accordion"
msgstr "Mostrar en el Acordeón"

#: ../includes/class.yith-category-accordion-widget.php:139
#: ../templates/admin/lightbox.php:137
msgid "Select an option"
msgstr "Selecciona una opción"

#: ../includes/class.yith-category-accordion-widget.php:140
#: ../templates/admin/lightbox.php:138
msgid "WooCommerce Category"
msgstr "Categoría WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:141
msgid "Wordpress Category"
msgstr "Categoría de WordPress"

#: ../includes/class.yith-category-accordion-widget.php:142
#: ../templates/admin/lightbox.php:140
msgid "Tags"
msgstr "Etiquetas"

#: ../includes/class.yith-category-accordion-widget.php:143
#: ../templates/admin/lightbox.php:141
msgid "Menu"
msgstr "Menú"

#: ../includes/class.yith-category-accordion-widget.php:148
#: ../templates/admin/lightbox.php:146
msgid "Show WooCommerce Subcategories"
msgstr "Mostrar Subcategorías de WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:152
#: ../templates/admin/lightbox.php:150
msgid "Exclude WooCommerce Categories"
msgstr "Excluir Subcategorías de WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:165
#: ../includes/class.yith-category-accordion-widget.php:197
#: ../templates/admin/lightbox.php:151 ../templates/admin/lightbox.php:170
msgid "Select categories"
msgstr "Seleccionar categorías"

#: ../includes/class.yith-category-accordion-widget.php:172
#: ../templates/admin/lightbox.php:157
msgid "Show WordPress Subcategories"
msgstr "Mostrar Subcategorías WordPress"

#: ../includes/class.yith-category-accordion-widget.php:176
#: ../templates/admin/lightbox.php:161
msgid "Show Last Post"
msgstr "Mostrar Última Publicación"

#: ../includes/class.yith-category-accordion-widget.php:180
#: ../templates/admin/lightbox.php:165
msgid "Number Post (-1 for all post )"
msgstr "Número de publicación (-1 para todas las publicaciones)"

#: ../includes/class.yith-category-accordion-widget.php:184
#: ../templates/admin/lightbox.php:169
msgid "Exclude Wordpress Categories"
msgstr "Excluir Categorías de WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:206
#: ../templates/admin/lightbox.php:178
msgid "Add menu in accordion"
msgstr "Añadir menú en el acordeón"

#: ../includes/class.yith-category-accordion-widget.php:220
#: ../templates/admin/lightbox.php:196
msgid "WooCommerce Tag"
msgstr "Etiqueta WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:224
#: ../templates/admin/lightbox.php:200
msgid "WooCommerce Tag Label"
msgstr "Etiqueta Tag WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:228
#: ../templates/admin/lightbox.php:204
msgid "Wordpress Tag"
msgstr "Etiqueta de WordPress"

#: ../includes/class.yith-category-accordion-widget.php:232
#: ../templates/admin/lightbox.php:208
msgid "WordPress Tag Label"
msgstr "Etiqueta Tag WordPress"

#: ../includes/class.yith-category-accordion-widget.php:237
#: ../templates/admin/lightbox.php:213
msgid "Highlight the current category"
msgstr "Resaltar la categoría actual"

#: ../includes/class.yith-category-accordion-widget.php:242
#: ../templates/admin/lightbox.php:218
msgid "Show Count"
msgstr "Mostrar cuenta"

#: ../includes/class.yith-category-accordion-widget.php:247
#: ../templates/admin/lightbox.php:223
msgid "Style"
msgstr "Estilo"

#: ../includes/class.yith-category-accordion-widget.php:249
#: ../includes/class.yith-woocommerce-category-accordion.php:164
#: ../templates/admin/lightbox.php:225
msgid "Style 1"
msgstr "Estilo 1"

#: ../includes/class.yith-category-accordion-widget.php:250
#: ../includes/class.yith-woocommerce-category-accordion.php:165
#: ../templates/admin/lightbox.php:226
msgid "Style 2"
msgstr "Estilo 2"

#: ../includes/class.yith-category-accordion-widget.php:251
#: ../includes/class.yith-woocommerce-category-accordion.php:166
#: ../templates/admin/lightbox.php:227
msgid "Style 3"
msgstr "Estilo 3"

#: ../includes/class.yith-category-accordion-widget.php:252
#: ../includes/class.yith-woocommerce-category-accordion.php:167
#: ../templates/admin/lightbox.php:228
msgid "Style 4"
msgstr "Estilo 4"

#: ../includes/class.yith-category-accordion-widget.php:256
msgid "Hide Accordion in  pages"
msgstr "Ocultar Acordeón en páginas"

#: ../includes/class.yith-category-accordion-widget.php:269
msgid "Select page"
msgstr "Selecciona página"

#: ../includes/class.yith-category-accordion-widget.php:273
msgid "Hide Accordion in  posts"
msgstr "Ocultar Acordeón en publicaciones"

#: ../includes/class.yith-category-accordion-widget.php:286
msgid "Select post"
msgstr "Seleccionar publicación"

#: ../includes/class.yith-category-accordion-widget.php:290
#: ../templates/admin/lightbox.php:232
msgid "Order By"
msgstr "Ordenar por"

#: ../includes/class.yith-category-accordion-widget.php:292
#: ../templates/admin/lightbox.php:234
msgid "Name"
msgstr "Nombre"

#: ../includes/class.yith-category-accordion-widget.php:293
#: ../templates/admin/lightbox.php:235
msgid "Count"
msgstr "Cuenta"

#: ../includes/class.yith-category-accordion-widget.php:294
#: ../templates/admin/lightbox.php:236
msgid "ID"
msgstr "ID"

#: ../includes/class.yith-category-accordion-widget.php:296
msgid "WooCommerce Order"
msgstr "Pedido WooCommerce"

#: ../includes/class.yith-category-accordion-widget.php:300
#: ../templates/admin/lightbox.php:239
msgid "ASC"
msgstr "ASC"

#: ../includes/class.yith-category-accordion-widget.php:301
#: ../templates/admin/lightbox.php:240
msgid "DESC"
msgstr "DESC"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:135
#: ../templates/admin/lightbox.php:53
msgctxt "enhanced select"
msgid "One result is available, press enter to select it."
msgstr "Hay un resultado disponible, pulsa enter para seleccionarlo."

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:136
#: ../templates/admin/lightbox.php:54
msgctxt "enhanced select"
msgid "%qty% results are available, use up and down arrow keys to navigate."
msgstr ""
"Hay %qty% resultados disponibles, pulsa las flechas de dirección arriba y "
"abajo para navegar."

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:137
#: ../templates/admin/lightbox.php:55
msgctxt "enhanced select"
msgid "No matches found"
msgstr "No se han encontrado coincidencias."

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:138
#: ../templates/admin/lightbox.php:56
msgctxt "enhanced select"
msgid "Loading failed"
msgstr "Carga fallida"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:139
#: ../templates/admin/lightbox.php:57
msgctxt "enhanced select"
msgid "Please enter 1 or more characters"
msgstr "Por favor, introduce 1 o más caracteres"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:140
#: ../templates/admin/lightbox.php:58
msgctxt "enhanced select"
msgid "Please enter %qty% or more characters"
msgstr "Por favor, introduce %qty% o más caracteres"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:141
#: ../templates/admin/lightbox.php:59
msgctxt "enhanced select"
msgid "Please delete 1 character"
msgstr "Por favor, borra 1 caracter"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:142
#: ../templates/admin/lightbox.php:60
msgctxt "enhanced select"
msgid "Please delete %qty% characters"
msgstr "Por favor, borra %qty% caracteres"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:143
#: ../templates/admin/lightbox.php:61
msgctxt "enhanced select"
msgid "You can only select 1 item"
msgstr "Sólo puedes seleccionar 1 elemento"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:144
#: ../templates/admin/lightbox.php:62
msgctxt "enhanced select"
msgid "You can only select %qty% items"
msgstr "Sólo puedes seleccionar %qty% elementos"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:145
#: ../templates/admin/lightbox.php:63
msgctxt "enhanced select"
msgid "Loading more results&hellip;"
msgstr "Cargando más resultados&hellip;"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:146
#: ../templates/admin/lightbox.php:64
msgctxt "enhanced select"
msgid "Searching&hellip;"
msgstr "Buscando&hellip;"

#: ../includes/class.yith-woocommerce-category-accordion-premium.php:495
msgid "Add YITH WooCommerce Category Accordion shortcode"
msgstr "Añadir  shortcode YITH WooCommerce Category Accordion"

#: ../includes/class.yith-woocommerce-category-accordion.php:77
#: ../includes/class.yith-woocommerce-category-accordion.php:157
msgid "Settings"
msgstr "Ajustes"

#: ../includes/class.yith-woocommerce-category-accordion.php:79
msgid "Premium live demo"
msgstr "Live Demo Premium"

#: ../includes/class.yith-woocommerce-category-accordion.php:79
msgid "Live demo"
msgstr "Live Demo"

#: ../includes/class.yith-woocommerce-category-accordion.php:84
#: ../includes/class.yith-woocommerce-category-accordion.php:161
msgid "Premium Version"
msgstr "Versión Premium"

#: ../includes/class.yith-woocommerce-category-accordion.php:109
msgid "Plugin Documentation"
msgstr "Documentación del Plugin"

#: ../includes/class.yith-woocommerce-category-accordion.php:173
#: ../includes/class.yith-woocommerce-category-accordion.php:174
msgid "Category Accordion"
msgstr "Category Accordion"

#: ../includes/walkers/class.yith-category-accordion-walker.php:66
#, php-format
msgid "Feed for all posts filed under %s"
msgstr "Feed para todas las publicaciones por debajo de %s"

#: ../includes/walkers/class.yith-category-accordion-walker.php:182
msgid "Last Posts"
msgstr "Últimas publicaciones"

#: ../init.php:47
msgid ""
"YITH WooCommerce Category Accordion Premium is enabled but not effective. It "
"requires WooCommerce in order to work."
msgstr ""
"YITH WooCommerce Category Accordion Premium está activado pero no es "
"efectivo. Requiere WooCommerce para funcionar."

#: ../plugin-options/settings-options.php:11
msgid "General settings"
msgstr "Ajustes Generales"

#: ../plugin-options/settings-options.php:19
msgid "Hide empty"
msgstr "Ocultar vacías"

#: ../plugin-options/settings-options.php:20
msgid "Hide empty categories in accordion"
msgstr "Ocultar categorías vacías en el acordeón"

#: ../plugin-options/settings-options.php:28
msgid "Open accordion on "
msgstr "Abrir acordeón en "

#: ../plugin-options/settings-options.php:29
msgid "Select event for open accordion menu"
msgstr "Selecciona un evento para abrir el menú acordeón"

#: ../plugin-options/settings-options.php:33
msgid "On Click"
msgstr "En click"

#: ../plugin-options/settings-options.php:34
msgid "On Hover"
msgstr "En Hover"

#: ../plugin-options/settings-options.php:40
msgid "Accordion speed"
msgstr "Velocidad de Acordeón"

#: ../plugin-options/settings-options.php:41
msgid "Set the accordion speed in milliseconds"
msgstr "Establece la velocidad del acordeón en milisegundos"

#: ../plugin-options/settings-options.php:49
msgid "Closed accordion"
msgstr "Acordeón cerrado"

#: ../plugin-options/settings-options.php:50
msgid "Show your accordion with all categories closed"
msgstr "Muestra tu acordeón con todas las categorías cerradas"

#: ../plugin-options/settings-options.php:57
msgid "Open subcategories"
msgstr "Abrir subcategorías"

#: ../plugin-options/settings-options.php:58
msgid "Open subcategories when visit the parent ones"
msgstr "Abrir subcategorías al visitar las categorías padres"

#: ../plugin-options/settings-options.php:66
msgid "Max depth level"
msgstr "Máx nivel de profundidad"

#: ../plugin-options/settings-options.php:67
msgid "Set the depth of accordion, 0 for all level"
msgstr "Establece la profundidad del acordeón, 0 para todos los niveles"

#: ../plugin-options/settings-options.php:79
msgid "Limit"
msgstr "Límite"

#: ../plugin-options/settings-options.php:80
msgid "Choose how many categories to display. -1 for infinite"
msgstr "Elige cuántas categorías mostrar. -1 para infinito"

#: ../plugin-options/style1-options.php:13
#: ../plugin-options/style2-options.php:13
#: ../plugin-options/style3-options.php:13
#: ../plugin-options/style4-options.php:13
msgid "General Settings"
msgstr "Ajustes generales"

#: ../plugin-options/style1-options.php:19
#: ../plugin-options/style2-options.php:19
#: ../plugin-options/style3-options.php:19
msgid "Title Typography"
msgstr "Tipografía del título"

#: ../plugin-options/style1-options.php:40
msgid "Container Border Color"
msgstr "Color del borde del contenedor"

#: ../plugin-options/style1-options.php:41
msgid "Set border color for your container"
msgstr "Establece el color de tu contenedor"

#: ../plugin-options/style1-options.php:52
#: ../plugin-options/style2-options.php:40
#: ../plugin-options/style3-options.php:40
#: ../plugin-options/style4-options.php:19
msgid "Title Container Background"
msgstr "Fondo del Contenedor del Título"

#: ../plugin-options/style1-options.php:53
#: ../plugin-options/style2-options.php:41
#: ../plugin-options/style3-options.php:41
#: ../plugin-options/style4-options.php:40
msgid "Set background color for your title container"
msgstr "Establece el color del fondo del contenedor del título"

#: ../plugin-options/style1-options.php:64
#: ../plugin-options/style2-options.php:52
#: ../plugin-options/style3-options.php:52
#: ../plugin-options/style4-options.php:39
msgid "Title Border Color"
msgstr "Color del Borde del Título"

#: ../plugin-options/style1-options.php:65
#: ../plugin-options/style2-options.php:53
#: ../plugin-options/style3-options.php:53
msgid "Set border bottom color for your title container"
msgstr "Establece el color del borde inferior del contenedor de tu título"

#: ../plugin-options/style1-options.php:76
#: ../plugin-options/style2-options.php:64
#: ../plugin-options/style3-options.php:64
#: ../plugin-options/style4-options.php:63
msgid "Count Style"
msgstr "Estilo del recuento"

#: ../plugin-options/style1-options.php:78
#: ../plugin-options/style2-options.php:66
#: ../plugin-options/style4-options.php:65
msgid "Choose count category style"
msgstr "Elige el estilo de recuento de categorías"

#: ../plugin-options/style1-options.php:81
#: ../plugin-options/style2-options.php:69
#: ../plugin-options/style3-options.php:69
#: ../plugin-options/style4-options.php:68
msgid "Default"
msgstr "Por defecto"

#: ../plugin-options/style1-options.php:82
#: ../plugin-options/style2-options.php:70
#: ../plugin-options/style3-options.php:70
#: ../plugin-options/style4-options.php:69
msgid "Rectangle"
msgstr "Rectangular"

#: ../plugin-options/style1-options.php:83
#: ../plugin-options/style2-options.php:71
#: ../plugin-options/style3-options.php:71
#: ../plugin-options/style4-options.php:70
msgid "Round"
msgstr "Redondo"

#: ../plugin-options/style1-options.php:89
#: ../plugin-options/style1-options.php:115
#: ../plugin-options/style2-options.php:77
#: ../plugin-options/style2-options.php:103
#: ../plugin-options/style3-options.php:77
#: ../plugin-options/style3-options.php:103
#: ../plugin-options/style4-options.php:76
#: ../plugin-options/style4-options.php:102
msgid "Background color"
msgstr "Color de fondo"

#: ../plugin-options/style1-options.php:90
#: ../plugin-options/style1-options.php:116
#: ../plugin-options/style2-options.php:78
#: ../plugin-options/style2-options.php:104
#: ../plugin-options/style3-options.php:78
#: ../plugin-options/style3-options.php:104
#: ../plugin-options/style4-options.php:77
#: ../plugin-options/style4-options.php:103
msgid "Set the background for count container"
msgstr "Establece el fondo para el contenedor del recuento"

#: ../plugin-options/style1-options.php:102
#: ../plugin-options/style1-options.php:127
#: ../plugin-options/style2-options.php:90
#: ../plugin-options/style2-options.php:115
#: ../plugin-options/style3-options.php:90
#: ../plugin-options/style3-options.php:115
#: ../plugin-options/style4-options.php:89
#: ../plugin-options/style4-options.php:114
msgid "Border color"
msgstr "Color del borde"

#: ../plugin-options/style1-options.php:103
#: ../plugin-options/style1-options.php:128
#: ../plugin-options/style2-options.php:91
#: ../plugin-options/style2-options.php:116
#: ../plugin-options/style3-options.php:91
#: ../plugin-options/style3-options.php:116
#: ../plugin-options/style4-options.php:90
#: ../plugin-options/style4-options.php:115
msgid "Set the border color for count container"
msgstr "Establece el color del borde para el contenedor del recuento"

#: ../plugin-options/style1-options.php:145
msgid "Style Parent Categories"
msgstr "Estilo de categorías padre"

#: ../plugin-options/style1-options.php:151
msgid "Typography Parent Categories"
msgstr "Tipografía de categorías padre"

#: ../plugin-options/style1-options.php:175
#: ../plugin-options/style2-options.php:163
#: ../plugin-options/style3-options.php:163
#: ../plugin-options/style4-options.php:161
msgid "Parent Categories Background"
msgstr "Fondo de categorías padre"

#: ../plugin-options/style1-options.php:176
#: ../plugin-options/style2-options.php:164
#: ../plugin-options/style3-options.php:164
#: ../plugin-options/style4-options.php:162
msgid "Set background color for your parent categories"
msgstr "Establece el color de fondo de tus categorías padre"

#: ../plugin-options/style1-options.php:188
#: ../plugin-options/style1-options.php:250
#: ../plugin-options/style2-options.php:176
#: ../plugin-options/style2-options.php:239
#: ../plugin-options/style3-options.php:176
#: ../plugin-options/style3-options.php:238
#: ../plugin-options/style4-options.php:51
#: ../plugin-options/style4-options.php:174
#: ../plugin-options/style4-options.php:236
msgid "Border Color"
msgstr "Color de borde"

#: ../plugin-options/style1-options.php:189
#: ../plugin-options/style2-options.php:177
#: ../plugin-options/style3-options.php:177
#: ../plugin-options/style4-options.php:175
msgid "Set border top color for your parent categories container"
msgstr ""
"Establece el color del borde superior del contenedor de tus categorías padre"

#: ../plugin-options/style1-options.php:208
#: ../plugin-options/style2-options.php:196
#: ../plugin-options/style3-options.php:196
#: ../plugin-options/style4-options.php:194
msgid "Style for Child Categories"
msgstr "Estilo de las categorías hijas"

#: ../plugin-options/style1-options.php:214
#: ../plugin-options/style2-options.php:202
#: ../plugin-options/style3-options.php:202
#: ../plugin-options/style4-options.php:200
msgid "Typography for Child Categories"
msgstr "Tipografía de las categorías hijas"

#: ../plugin-options/style1-options.php:237
#: ../plugin-options/style2-options.php:226
#: ../plugin-options/style3-options.php:225
#: ../plugin-options/style4-options.php:223
msgid "Child Categories Background"
msgstr "Fondo de las categorías hijas"

#: ../plugin-options/style1-options.php:238
#: ../plugin-options/style2-options.php:227
#: ../plugin-options/style3-options.php:226
#: ../plugin-options/style4-options.php:224
msgid "Set background color for your child categories"
msgstr "Establece el color de fondo para tus categorías hijas"

#: ../plugin-options/style1-options.php:251
#: ../plugin-options/style2-options.php:240
#: ../plugin-options/style3-options.php:239
#: ../plugin-options/style4-options.php:237
msgid "Set border top color for your child categories container"
msgstr ""
"Establece el color del borde superior para el contenedor de tus categorías "
"hijas"

#: ../plugin-options/style2-options.php:133
#: ../plugin-options/style3-options.php:133
#: ../plugin-options/style4-options.php:132
msgid "Style for Parent Categories"
msgstr "Estilo de categorías padre"

#: ../plugin-options/style2-options.php:139
#: ../plugin-options/style3-options.php:139
#: ../plugin-options/style4-options.php:138
msgid "Typography for Parent Categories"
msgstr "Tipografía de las cate"

#: ../plugin-options/style3-options.php:66
msgid "Choose category count style"
msgstr "Elige un estilo de recuento de categorías"

#: ../plugin-options/style4-options.php:52
msgid "Set border-bottom-color for your title container"
msgstr "Establece el color del borde inferior del contenedor de tu título"

#: ../templates/admin/lightbox.php:36
msgid "Add shortcode"
msgstr "Añadir shortcode"

#: ../templates/admin/lightbox.php:139
msgid "Worpress Category"
msgstr "Categoría WordPress"

#: ../templates/admin/lightbox.php:189
msgid "Menu Label"
msgstr "Etiqueta de Menú"

#: ../templates/admin/lightbox.php:248
msgid "Insert"
msgstr "Insertar"

#: ../templates/admin/typography.php:32
msgid "px"
msgstr "px"

#: ../templates/admin/typography.php:33
msgid "em"
msgstr "em"

#: ../templates/admin/typography.php:34
msgid "pt"
msgstr "pt"

#: ../templates/admin/typography.php:35
msgid "rem"
msgstr "rem"

#: ../templates/admin/typography.php:41
msgid "Regular"
msgstr "Normal"

#: ../templates/admin/typography.php:42
msgid "Bold"
msgstr "Negrita"

#: ../templates/admin/typography.php:43
msgid "Extra bold"
msgstr "Extra negrita"

#: ../templates/admin/typography.php:44
msgid "Italic"
msgstr "Cursiva"

#: ../templates/admin/typography.php:45
msgid "Italic bold"
msgstr "Negrita cursiva"

#: ../templates/admin/typography.php:51
msgid "None"
msgstr "Ninguna"

#: ../templates/admin/typography.php:52
msgid "Lowercase"
msgstr "Minúsculas"

#: ../templates/admin/typography.php:53
msgid "Uppercase"
msgstr "Mayúsculas"

#: ../templates/admin/typography.php:54
msgid "Capitalize"
msgstr "Inicial mayúscula"
