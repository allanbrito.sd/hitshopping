��    [      �     �      �     �     �     �     �               $     ,     9  �   T  p   	  �   y	     
     

     
     &
     2
  	   @
     J
     S
  	   g
  5   q
     �
     �
     �
     �
     �
     �
     �
                 	   &     0  9   8     r     {     �     �     �     �     �     �     �     �  	   �               &     ;     D     S  "   k     �     �     �  !   �     �                ;     W     l     |     �  	   �     �     �     �     �     �     �  	   �                      	   <     F     M     ^     b     �     �  	   �  %   �     �  8   �  ;     
   R  �  ]     �          #     9     J     \     p     ~     �  e   �  D     i   L     �     �     �     �     �       
           	   8  C   B     �     �     �     �     �     �     �            	             ,  J   5  	   �     �     �  	   �     �  	   �     �     �     �     �  	             )     2     I     U  '   n     �     �     �     �  %   �          )  (   I      r     �     �     �     �  	   �     �  "   �  	             :     L     P     c     j     r  -   �  	   �     �     �     �  '   �            	   %  '   /     W     g     �     �         J      Q       L      2           Y              7           6          A   (   +           T   #   P       ,   '   N   !             G   [           @      C      R          <       ;      F   -       1   Z           U   )          =   :         I                  D             >                      "         .   &      O      *   %   H   9   4   	              E   B   S   W      X       /         5       M   ?   0         $   K   
   3   V   8                       + Add account + Add new condition Account details Account name Account number Add New Rule Add new Add new rule Admin menu nameYITH Rules Admin option description: Check this option to manage payment restriction option for shop manager roleCheck this option to manage payment restriction option for shop manager role Admin option: Show Payment Restriction option for shop managersShow Payment Restriction option for shop manager Alert Message: WooCommerce requiresYITH Payment Method Restrictions is enabled but not effective. It requires WooCommerce in order to work. All All YITH Rules BACS account BIC / Swift Back to rules Bank name Category Change BACS account Changelog Check this option to disable payment restriction rule Conditions: Delete Delete %s permanently Delete Permanently Delete permanently Disable: Disabled Does not contain Edit Edit %s Edit Rule Enabled Enter here the reason why the payment gateway is disabled Equal to General Settings Geolocalization Greater than Greater than or equal to Guest Help Center IBAN Include all Include at least one of Less than Less than or equal to Message: Move %s to the Trash New Rule No Rules found No Rules found in trash Panel: page titleGeneral settings Parent Rules Payment Method Payment Method Restrictions Payment Method Restrictions Rules Payment method gateway: Payment method restriction Payment method restriction rule Payment method restrictions Plugin documentation Premium Version Price Product Published Remove payment method Remove selected account(s) Restore Restore %s from the Trash Restriction by: Role Rule name Rules Save Search Rules Select bank transfer account: Sort code Status Support platform Tag This is where rules are stored. Trash Type of restriction: View Rule What to do with this payment gateway? current version plugin name in admin WP menuPayment Method Restrictions plugin name in admin page titlePayment Method Restrictions verbTrash Project-Id-Version: YITH Payment Method Restrictions for WooCommerce
POT-Creation-Date: 2018-05-17 20:17+0100
PO-Revision-Date: 2018-05-17 20:18+0100
Last-Translator: 
Language-Team: YITH <plugins@yithemes.com>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + Añadir cuenta + Añadir nueva condición Detalles de la cuenta Nombre de cuenta Número de cuenta Añadir nueva regla Añadir nuevo Añadir nueva regla Reglas de YITH Marca esta opción para gestionar la opción de restricción de pago para el rol de gerente de tienda Mostrar opción de restricción de pago para el gerente de la tienda YITH Payment Metido Restrictions está activado pero no es efectivo. Necesita WooCommerce para funcionar. Todo Todas las reglas de YITH Cuenta bancaria BIC / Swift Volver a las reglas Nombre del banco Categoría Cambiar cuenta bancaria Changelog Marca esta opción para desactivar la regla de restricción de pago Condiciones: Eliminar Eliminar %s permanentemente Eliminar permanentemente Eliminar permanentemente Desactivar: Desactivado No contiene Editar Editar %s Editar regla Activado Introduce aquí la razón por la que la pasarela de pago está desactivada Igual que Ajustes generales Geolocalización Mayor que Mayor o igual que Visitante Centro de ayuda IBAN Incluir todo Incluir al menos una de Menos que Menor o igual que Mensaje: Mover %s a la papelera Nueva regla No se encontraron reglas No se encontraron reglas en la papelera Ajustes generales Reglas superiores Método de pago Payment Metido Restrictions Reglas de Payment Method Restrictions Pasarela de método de pago: Restricción de método de pago Regla de restricción de método de pago Restricciones de método de pago Documentación del plugin Versión premian Precio Producto Publicado Eliminar método de pago Eliminar cuenta(s) seleccionada(s) Restaurar Restaurar %s de la papelera Restricción por: Rol Nombre de la regla Reglas Guardar Buscar reglas Seleccionar cuenta de transferencia bancaria: Sort code Estado Plataforma de soporte Etiqueta Aquí es donde se almacenan las reglas. Papelera Tipo de restricción: Ver regla ¿Qué hacer con esta pasarela de pago? versión actual Payment Method Restrictions Payment Method Restrictions Enviar a la papelera 