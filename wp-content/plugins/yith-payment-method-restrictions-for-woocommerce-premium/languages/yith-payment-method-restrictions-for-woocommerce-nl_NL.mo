��    [      �     �      �     �     �     �     �               $     ,     9  �   T  p   	  �   y	     
     

     
     &
     2
  	   @
     J
     S
  	   g
  5   q
     �
     �
     �
     �
     �
     �
     �
                 	   &     0  9   8     r     {     �     �     �     �     �     �     �     �  	   �               &     ;     D     S  "   k     �     �     �  !   �     �                ;     W     l     |     �  	   �     �     �     �     �     �     �  	   �                      	   <     F     M     ^     b     �     �  	   �  %   �     �  8   �  ;     
   R  �  ]     �          *     :  
   F     Q     g     x     �  U   �  7   �  k   (     �     �     �     �     �  	   �  	   �     �     �  D        U     b     n     �     �     �     �  
   �     �  	   �     �     �  :     
   >     I     _  
   o     z     �     �     �  
   �     �  
   �     �     �     �          &  "   ;     ^     t     �     �     �     �     �     �          +     ?     N     T     \     i  "     
   �     �     �     �  	   �     �     �     �       
   (     3     :     K     O  
   k     v     �  .   �     �     �     �              J      Q       L      2           Y              7           6          A   (   +           T   #   P       ,   '   N   !             G   [           @      C      R          <       ;      F   -       1   Z           U   )          =   :         I                  D             >                      "         .   &      O      *   %   H   9   4   	              E   B   S   W      X       /         5       M   ?   0         $   K   
   3   V   8                       + Add account + Add new condition Account details Account name Account number Add New Rule Add new Add new rule Admin menu nameYITH Rules Admin option description: Check this option to manage payment restriction option for shop manager roleCheck this option to manage payment restriction option for shop manager role Admin option: Show Payment Restriction option for shop managersShow Payment Restriction option for shop manager Alert Message: WooCommerce requiresYITH Payment Method Restrictions is enabled but not effective. It requires WooCommerce in order to work. All All YITH Rules BACS account BIC / Swift Back to rules Bank name Category Change BACS account Changelog Check this option to disable payment restriction rule Conditions: Delete Delete %s permanently Delete Permanently Delete permanently Disable: Disabled Does not contain Edit Edit %s Edit Rule Enabled Enter here the reason why the payment gateway is disabled Equal to General Settings Geolocalization Greater than Greater than or equal to Guest Help Center IBAN Include all Include at least one of Less than Less than or equal to Message: Move %s to the Trash New Rule No Rules found No Rules found in trash Panel: page titleGeneral settings Parent Rules Payment Method Payment Method Restrictions Payment Method Restrictions Rules Payment method gateway: Payment method restriction Payment method restriction rule Payment method restrictions Plugin documentation Premium Version Price Product Published Remove payment method Remove selected account(s) Restore Restore %s from the Trash Restriction by: Role Rule name Rules Save Search Rules Select bank transfer account: Sort code Status Support platform Tag This is where rules are stored. Trash Type of restriction: View Rule What to do with this payment gateway? current version plugin name in admin WP menuPayment Method Restrictions plugin name in admin page titlePayment Method Restrictions verbTrash Project-Id-Version: YITH Payment Method Restrictions for WooCommerce
POT-Creation-Date: 2018-05-09 18:33+0100
PO-Revision-Date: 2018-05-09 18:56+0100
Last-Translator: 
Language-Team: YITH <plugins@yithemes.com>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.13
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 + account toevoegen + Nieuwe voorwaarde toevoegen Account details Accountnaam Banknummer Voeg nieuwe regel toe Nieuwe toevoegen Voeg nieuwe regel toe YITH Regels Selecteer dit om de betaalmethode restrictie optie te beheren voor winkel manager rol Toon betaalmethode beperkingen optie voor winkelmanager YITH Payment Method Restrictions is ingeschakeld maar werkt niet. Het heeft WooCommerce nodig om te werken. Alle Alle YITH Regels BACS account BIC / Swift Terug naar regels Naam bank Categorie Wijzig BACS account Wijzigingenlogboek Markeer deze optie om de beperkte betaalwijze regel uit te schakelen Voorwaarden: Verwijderen Verwijder %s permanent Verwijder permanent Permanent verwijderen Uitschakelen: Uitgeschakeld Bevat niet Bewerken Bewerk %s Bewerk regel Ingeschakeld Vul hier in waarom de betaalwijze gateway is uitgeschakeld Gelijk aan Algemene Instellingen Geolocalization Groter dan Groter dan of gelijk aan Gasten Helpcentrum IBAN Bevat alle Bevat ten minste een van Minder dan Minder dan of gelijk aan Bericht: Verplaats %s naar prullenbak Nieuwe regel Geen regels gevonden Geen regels gevonden in prullenbak Algemene instellingen Bovenliggende regels Betaalwijze Beperkingen Betaalwijze Regels Beperkingen Betaalwijze Betaalwijze gateway: Beperking betaalwijze Regel beperking betaalwijze Beperkingen Betaalwijze Plugin documentatie Premium versie Prijs Product Gepubliceerd Verwijder betaalwijze Verwijder geselecteerde account(s) Herstellen Herstel %s van de prullenbak Beperking door: Rollen Regelnaam Regels Opslaan Regels zoeken Selecteer banktransfer account: Soort code Status Support platform Tag Hier zijn regels opgeslagen Prullenbak Type beperking: Bekijk regel Wat kan men doen met deze betaalwijze gateway? huidige versie Beperkingen Betaalwijze Beperkingen Betaalwijze Verplaatsen naar prullenbak 