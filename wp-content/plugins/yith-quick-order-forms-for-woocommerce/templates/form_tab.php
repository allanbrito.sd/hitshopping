<?php

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'includes/class.yith-wc-quick-order-forms-custom-post-type-table.php' );
$table = new YITH_WC_Quick_Order_Forms_Custom_Post_Type_Table();

$admin_url = admin_url( 'post-new.php' );
$params    = array(
	'post_type' => 'yith_wc_qof_1'
);

$add_new_url = esc_url( add_query_arg( $params, $admin_url ) );

?>

<div class="wrap">
	<h1><?php _e( 'Quick order forms', 'yith-quick-order-forms-for-woocommerce' ) ?>
		<a href="<?php echo $add_new_url; ?>" class="add-new-h2"><?php _e( 'Add new form', 'yith-quick-order-forms-for-woocommerce' ) ?></a>
	</h1>

	<div id="poststuff">
		<div id="post-body" class="metabox-holder">
			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">
					<form method="post">
						<input type="hidden" name="page" value="yith_quick_order_forms_for_woocommerce" />
					</form>
					<form method="post">
						<?php
						$table->views();
						$table->prepare_items();
						$table->display(); ?>
					</form>
				</div>
			</div>
		</div>
		<br class="clear">
	</div>
</div>