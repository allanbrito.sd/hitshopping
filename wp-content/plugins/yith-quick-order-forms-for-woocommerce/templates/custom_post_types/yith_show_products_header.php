<?php
/**
 * Created by PhpStorm.
 * User: dssaez
 * Date: 16/06/17
 * Time: 11:26
 */

/* ==================================== */
/* ==== Displaying header products ==== */
/* ==================================== */

?>

<div class="YITH_WC_qof_product_header">

    <?php

    if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_thumbnail_checkbox', true ) ) {
        echo "<div class='YITH_WC_qof_product_line_image_header'>" . __( 'Image', 'yith-quick-order-forms-for-woocommerce' ) . "</div>";
    }

    ?>

    <div class='YITH_WC_qof_product_line_name_header'>
        <?php _e( 'Name', 'yith-quick-order-forms-for-woocommerce' ); ?>
    </div>

    <?php

    if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_SKU_checkbox', true ) ) {
        echo "<div class='YITH_WC_qof_product_line_name_SKU_header'>" . __( 'SKU', 'yith-quick-order-forms-for-woocommerce' ) . "</div>";
    }

    ?>

    <?php

    if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_price_checkbox', true ) ) {
        echo "<div class='YITH_WC_qof_product_line_Price_header'>" . __( 'Price', 'yith-quick-order-forms-for-woocommerce' ) . "</div>";
    }

    ?>

    <div class='YITH_WC_qof_products_found'>

        <?php

        $Show_Number_Products_Found = apply_filters( 'yith_wc_qof_filters_show_number_products_found', $my_post_found . __( ' products found', 'yith-quick-order-forms-for-woocommerce'), $Yith_Post_ID, $my_post_found );

        echo $Show_Number_Products_Found;

        ?>

    </div>

</div>

