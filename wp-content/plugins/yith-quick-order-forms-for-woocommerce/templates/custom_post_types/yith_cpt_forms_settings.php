<?php
/**
 * Created by PhpStorm.
 * User: dssaez
 * Date: 14/06/17
 * Time: 15:57
 */

wp_nonce_field( 'yith_wc_quick_order_form_data', 'yith_wc_quick_order_form_meta_box_nonce' );

/* =========== DESCRIPTION =========== */

$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_description_textarea', true );

?>

<div class="YITH_QOF_Form_Line">
	<ul>
		<li>
			<label class="YITH_QOF_Form_strong" for="yith_wc_quick_order_form_description_textarea"><?php _e( 'Description', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) ); ?>: </label>
		</li>

		<li>
			<textarea class="YITH_QOF_Form_description" id="yith_wc_quick_order_form_description_textarea" name="yith_wc_quick_order_form_description_textarea"><?php echo esc_attr( $value ); ?></textarea>
		</li>

	</ul>

</div>

<?php
/* =========== PAGINATION =========== */

$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_Products_Per_Page_text', true );

?>

<div class="YITH_QOF_Form_Line">
	<ul>
		<li>
			<label class="YITH_QOF_Form_strong">
				<?php _e( 'Pagination', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>
		</li>

		<li>
			<label>
				<input class="YITH_QOF_Form_number" type="number" id="yith_wc_quick_order_form_Products_Per_Page_text" name="yith_wc_quick_order_form_Products_Per_Page_text" value="<?php echo esc_attr( $value ); ?>" />
				<?php _e( 'Products per page', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>
		</li>
	</ul>

</div>

<?php

/* =========== SHOW SKU, thumbnail, price, discount =========== */

$value       = get_post_meta( $post->ID, 'yith_wc_quick_order_form_SKU_checkbox', true );
$SKU_checked = ( $value ? 'checked="checked"' : '' );

$value             = get_post_meta( $post->ID, 'yith_wc_quick_order_form_thumbnail_checkbox', true );
$thumbnail_checked = ( $value ? 'checked="checked"' : '' );

$value         = get_post_meta( $post->ID, 'yith_wc_quick_order_form_price_checkbox', true );
$price_checked = ( $value ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_discount_checkbox', true );
$discount_checked = ( $value ? 'checked="checked"' : '' );

/* =========== SHOW Search Filter and number of products =========== */

$value         = get_post_meta( $post->ID, 'yith_wc_quick_order_form_search_filter_checkbox', true );
$search_filter_checked = ( $value != '0' ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_number_of_products_checkbox', true );
$number_of_products_checked = ( $value != '0' ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_stock_checkbox', true );
$stock_checked = ( $value != '0' ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_quantity_checkbox', true );
$quantity_checked = ( $value != '0' ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_variable_price_checkbox', true );
$variable_price = ( $value != '0' ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_default_quantity_text', true );
$default_quantity = ( is_numeric( $value ) ? $value : '1' );

?>

<div class="YITH_QOF_Form_Line">
	<ul>
		<li>
			<p class="YITH_QOF_Form_strong">
				<?php _e( 'Choose product information to show', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</p>
		</li>

		<li>
			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_SKU_checkbox" name="yith_wc_quick_order_form_SKU_checkbox" <?php echo $SKU_checked; ?>/>
				<?php _e( 'SKU', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_thumbnail_checkbox" name="yith_wc_quick_order_form_thumbnail_checkbox" <?php echo $thumbnail_checked; ?>/>
				<?php _e( 'thumbnail', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_price_checkbox" name="yith_wc_quick_order_form_price_checkbox" <?php echo $price_checked; ?>/>
				<?php _e( 'price', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_discount_checkbox" name="yith_wc_quick_order_form_discount_checkbox" <?php echo $discount_checked; ?>/>
				<?php _e( 'discount', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_search_filter_checkbox" name="yith_wc_quick_order_form_search_filter_checkbox" <?php echo $search_filter_checked; ?>/>
				<?php _e( 'show search filter', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_number_of_products_checkbox" name="yith_wc_quick_order_form_number_of_products_checkbox" <?php echo $number_of_products_checked; ?>/>
				<?php _e( 'show number of products', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

			<label>
				<input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_stock_checkbox" name="yith_wc_quick_order_form_stock_checkbox" <?php echo $stock_checked; ?>/>
				<?php _e( 'show stock', 'yith-quick-order-forms-for-woocommerce' ); ?>
			</label>

            <label>
                <input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_variable_price_checkbox" name="yith_wc_quick_order_form_variable_price_checkbox" <?php echo $variable_price; ?>/>
                <?php _e( 'show variable price', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </label>
		</li>

        <li class="yith_wc_qof_li_another_line">

            <label>
                <input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_quantity_checkbox" name="yith_wc_quick_order_form_quantity_checkbox" <?php echo $quantity_checked; ?>/>
                <?php _e( 'show quantity', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </label>

            <label>
                <input class="YITH_QOF_Form_number" type="number" id="yith_wc_quick_order_form_default_quantity_text" name="yith_wc_quick_order_form_default_quantity_text" value="<?php echo esc_attr( $default_quantity ); ?>" />
                <?php _e( 'default quantity', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </label>

        </li>

	</ul>

</div>

<?php

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_individual_add_to_cart_checkbox', true );
$individual_add_to_cart = ( $value == 'no' ? '' : 'checked="checked"' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_all_add_to_cart_top_checkbox', true );
$all_add_to_cart_top = ( $value == 'yes' ? 'checked="checked"' : '' );

$value            = get_post_meta( $post->ID, 'yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox', true );
$all_add_to_cart_bottom = ( $value == 'yes' ? 'checked="checked"' : '' );


?>

<div class="YITH_QOF_Form_Line">
    <ul>
        <li>
            <p class="YITH_QOF_Form_strong">
                <?php _e( 'Choose which add to cart buttons you want to show', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </p>
        </li>

        <li>

            <label>
                <input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_individual_add_to_cart_checkbox" name="yith_wc_quick_order_form_individual_add_to_cart_checkbox" <?php echo $individual_add_to_cart; ?>/>
                <?php _e( 'individal add to cart', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </label>

            <label>
                <input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_all_add_to_cart_top_checkbox" name="yith_wc_quick_order_form_all_add_to_cart_top_checkbox" <?php echo $all_add_to_cart_top; ?>/>
                <?php _e( 'add to car for all product at the top', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </label>

            <label>
                <input class="yith_wc_qof_checkbox" type="checkbox" id="yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox" name="yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox" <?php echo $all_add_to_cart_bottom; ?>/>
                <?php _e( 'add to car for all product at the bottom', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </label>

        </li>
    </ul>

</div>

<?php

$yith_wc_qof_sorting_select = get_post_meta( $post->ID, 'yith_wc_quick_order_form_Sorting_Select', true );

?>

<div class="YITH_QOF_Form_Line">
    <ul>
        <li>
            <p class="YITH_QOF_Form_strong">
                <?php _e( 'Choose how to sort the form by default', 'yith-quick-order-forms-for-woocommerce' ); ?>
            </p>
        </li>

        <li>

            <select id="Yith_wc_qof_Form_sorting_search_select_ID" class="Yith_wc_qof_Form_sorting_search_select" name="yith_wc_quick_order_form_Sorting_Select">

                <option value="Default"><?php _e( 'Default sorting ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

                <option value="price_YITH_ASC" <?php echo( ( $yith_wc_qof_sorting_select == "price_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by price: low to high ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
                <option value="price_YITH_DESC" <?php echo( ( $yith_wc_qof_sorting_select == "price_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by price: high to low ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

                <option value="_sku_YITH_ASC" <?php echo( ( $yith_wc_qof_sorting_select == "_sku_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by SKU: low to high ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
                <option value="_sku_YITH_DESC" <?php echo( ( $yith_wc_qof_sorting_select == "_sku_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by SKU: high to low ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

                <option value="date_YITH_ASC" <?php echo( ( $yith_wc_qof_sorting_select == "date_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by newness ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
                <option value="date_YITH_DESC" <?php echo( ( $yith_wc_qof_sorting_select == "date_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by lateness ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

                <option value="title_YITH_ASC" <?php echo( ( $yith_wc_qof_sorting_select == "title_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Name asc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
                <option value="title_YITH_DESC" <?php echo( ( $yith_wc_qof_sorting_select == "title_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Name desc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

                <option value="menu_order_YITH_ASC" <?php echo( ( $yith_wc_qof_sorting_select == "menu_order_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Menu order asc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
                <option value="menu_order_YITH_DESC" <?php echo( ( $yith_wc_qof_sorting_select == "menu_order_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Menu order desc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

            </select>

        </li>
    </ul>

</div>