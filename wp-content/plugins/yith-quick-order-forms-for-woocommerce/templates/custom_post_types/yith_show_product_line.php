<?php
/**
 * Created by PhpStorm.
 * User: dssaez
 * Date: 26/06/17
 * Time: 13:54
 */

if ( isset( $variation_Found ) ) {

	$_variation_product = $variation;
	if ( $variation_Found ) {
		$title            = $_variation_product->get_title();
		$sku              = $_variation_product->get_sku();
		$Child_Product_ID = $variation->get_id();

	}

} else {
    if( 'variation' == $_product->get_type() ) {
        $attributes = implode( ',', array_values( $_product->get_variation_attributes() ) );
        $title = $_product->get_title() . ' - ' . $attributes;
    }
	$variation_Found = true;
}

if ( ! isset( $Child_Product_ID ) )
    $Child_Product_ID = 0;

?>

<div class="YITH_WC_qof_product_line">

	<div id="YITH_WC_qof_product_line_AJAX_Selecting_Variation_ID_<?php echo $Parent_Product_ID; ?>_<?php echo $Yith_Post_ID; ?>" class="YITH_WC_qof_product_line_AJAX_Selecting_Variation"></div>

	<div class="YITH_WC_QOF_Horizontal_Line"></div>

	<?php

	if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_thumbnail_checkbox', true ) ) {
		?>

		<div class="YITH_WC_qof_product_line_image">

			<a href="<?php echo get_post_permalink( $Child_Product_ID ); ?>" class="YITH_WC_qof_product_line_a">

				<?php

				$Child_Image = get_the_post_thumbnail( $Child_Product_ID, apply_filters( 'yith_quick_order_form_image_size', 'thumbnail' ), array( 'class' => 'YITH_WC_qof_product_thumbnail' ) );
				if ( $Child_Image == "" ) {
					$Parent_Image = get_the_post_thumbnail( wp_get_post_parent_id( $Child_Product_ID ), apply_filters( 'yith_quick_order_form_image_size', 'thumbnail' ), array( 'class' => 'YITH_WC_qof_product_thumbnail' ) );
					if ( $Parent_Image == "" ) {
						$no_image = constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . '/images/no-image.jpg';
						echo "<img width='150' height='150' src='$no_image' class='YITH_WC_qof_product_thumbnail wp-post-image' alt='YITH NO IMAGE'/>";
					} else {
						echo $Parent_Image;
					}
				} else {
					echo $Child_Image;
				} ?>


			</a>

		</div>

		<?php
	}

	?>

	<div class="YITH_WC_qof_product_line_name_and_SKU">

		<div class="YITH_WC_qof_product_line_name">
			<a href="<?php echo get_post_permalink( $Child_Product_ID ); ?>" class="YITH_WC_qof_product_line_a">

				<?php

				if ( $variation_Found ) {
					echo $title;
				} else {
					_e( 'No product matching the selection found', 'yith-quick-order-forms-for-woocommerce' );
				}

				?>

			</a>
		</div>

		<?php

		if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_SKU_checkbox', true ) ) {
			?>

			<div class="YITH_WC_qof_product_line_name_SKU">
				<a href="<?php echo get_post_permalink( $Child_Product_ID ); ?>" class="YITH_WC_qof_product_line_a">

					<?php echo $sku; ?>

				</a>
			</div>

			<?php

		}

		?>

	</div>

	<?php

	if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_price_checkbox', true ) ) {
		?>

		<div class="YITH_WC_qof_product_line_Price">

			<?php

            if ( $variation_Found )
                if ( $_product->is_type( 'variable' ) ) {

                    $variations = $_product->get_available_variations();

                    $_product_for_price = ( $variation_Found ? $_variation_product : $_product );

                    echo ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_discount_checkbox', true ) ? apply_filters( 'yith_wc_quick_order_get_price', $_product_for_price->get_price_html(), $_product_for_price ) : wc_price( $_product_for_price->get_price() ) );

                } else {
                    echo ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_discount_checkbox', true ) ? apply_filters( 'yith_wc_quick_order_get_price', $_product->get_price_html(), $_product ) : wc_price( $_product->get_price() ) );
                }

			?>

		</div>

		<?php

	}

	?>

	<div id="yith_wc_qof_button_and_price_<?php echo $Parent_Product_ID; ?>_<?php echo $Yith_Post_ID; ?>" class="yith_wc_qof_button_and_price">

		<div>

			<?php

			$Stock_Quantity = ( $_product->is_type( 'variable' ) ? $_variation_product->get_stock_quantity() : $_product->get_stock_quantity() );

			$_current_product = ( $_product->is_type( 'variable' ) ? $_variation_product : $_product );

			global $woocommerce;

			if ( ! $_current_product->is_in_stock() )
				if ( version_compare( $woocommerce->version, '3.0', '<' ) )
					echo "<p class='stock out-of-stock'>Out of stock</p>";
				else
					echo wc_get_stock_html( $_current_product );
			else
				{

					$Quantity_in_Cart = 0;
					foreach ( WC()->cart->cart_contents as $Product_in_Cart ){

						if ( $Product_in_Cart['product_id'] == $Child_Product_ID || $Product_in_Cart['variation_id'] == $Child_Product_ID ) {
							$Quantity_in_Cart = $Product_in_Cart['quantity'];
							break;
						}
					}

                    $value            = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_default_quantity_text', true );
                    $default_quantity = ( is_numeric( $value ) ? $value : '1' );
                    $default_quantity = apply_filters( 'yith_wc_qof_default_quantity_' . $Child_Product_ID . "_" . $Yith_Post_ID, $default_quantity );

					$Quantity_to_Add = ( ( $Stock_Quantity != 0 ) ? ( ( $Stock_Quantity - $Quantity_in_Cart == 0 ) ? 0 : $default_quantity ) : $default_quantity );

					$show_quantity = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_quantity_checkbox', true );

					echo ( $show_quantity ? "<input type='button' value='-' class='yith_wc_button_plus_minus yith_wc_button_minus'>" : false );
			?>


                    <input <?php echo ( ! $show_quantity ? "type='hidden'" : false ); ?> id="yith_wc_qof_input_number_product_<?php echo $Child_Product_ID; ?>_<?php echo $Yith_Post_ID; ?>" class="YITH_WC_QOF_Quantity_Cart" value="<?php echo $Quantity_to_Add; ?>" data-product_id="<?php echo $Child_Product_ID; ?>" data-yith_post_id="<?php echo $Yith_Post_ID; ?>" data-stock_quantity="<?php echo $Stock_Quantity; ?>" data-backorder_allowed="<?php echo $_current_product->backorders_allowed(); ?>"
                                                                                         type="text" placeholder="1" />

					<?php

						echo ( $show_quantity ? "<input type='button' value='+' class='yith_wc_button_plus_minus yith_wc_button_plus'>" : false );

                        $value = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_individual_add_to_cart_checkbox', true );
                        $individual_add_to_cart = ( $value == 'no' ? 'display: none' : '' );

					?>

					<button type="submit" data-yith_post_id="<?php echo $Yith_Post_ID; ?>"
					        data-quantity="1" data-product_id="<?php echo $Child_Product_ID; ?>"
                            data-default_quantity="<?php echo $default_quantity; ?>"
					        class="button alt YITH_WC_qof_Button_Add_to_cart_trigger" style="<?php echo $individual_add_to_cart; ?>">
						<?php _e( 'Add to cart', 'yith-quick-order-forms-for-woocommerce' ); ?>
						<span id="yith_wc_qof_dashicon_product_<?php echo $Child_Product_ID; ?>_<?php echo $Yith_Post_ID; ?>"
						      class="dashicons dashicons-yes yith_wc_qof_dashicon_product" <?php echo ( $Quantity_in_Cart == 0 ? '' : 'style="display: inline-block"' ); ?> ></span>
					</button>
					<button id="yith_wc_qof_button_product_<?php echo $Child_Product_ID; ?>_<?php echo $Yith_Post_ID; ?>"
					        type="submit" data-yithtest="1" data-quantity_in_cart="<?php echo $Quantity_in_Cart; ?>"
					        data-quantity="<?php echo $default_quantity; ?>" data-product_id="<?php echo $Child_Product_ID; ?>"
					        class="button alt ajax_add_to_cart add_to_cart_button YITH_WC_qof_Button_Add_to_cart" style="display: none">
						<?php _e( 'Add to cart', 'yith-quick-order-forms-for-woocommerce' ); ?>
					</button>

			<?php

					if ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_stock_checkbox', true ) )
						if ( version_compare( $woocommerce->version, '3.0', '<' ) ) {
							if ( $Stock_Quantity != null )
								echo "<p class='stock in-stock'>$Stock_Quantity in stock</p>";
						}
						else
							echo wc_get_stock_html( ( $_product->is_type( 'variable' ) ? $_variation_product : $_product ) );

				}

			?>

		</div>

		<?php

		if ( $_product->is_type( 'variable' ) ) {

			$variations = $_product->get_available_variations();

			$var        = array();

			$array_variation_attributes = array();

            $product_attributes = $_product->get_attributes();

            $attribute_taxonomies = wc_get_attribute_taxonomies();

            foreach ( $_product->get_available_variations() as $attribute_name => $attribute_array ) {

                foreach ( $attribute_array["attributes"] as $attribute_name => $attribute ){
                    $array_variation_attributes[$attribute_name][] = $attribute;
                }

            }

            $array_already_variation_attributes = array();

            foreach ( $array_variation_attributes as $attribute_name => $attribute_array ) {

                if ( ! isset( $product_attributes[ $attribute_name ] ) ){
                    $attribute_name = str_replace( 'attribute_', '', $attribute_name );
                }

                if ( ! isset( $product_attributes[ $attribute_name ] ) ){
                    $attribute_name = str_replace( 'pa_', '', $attribute_name );
                }

                $print_attribute_name = $product_attributes[ $attribute_name ]->get_name();

                if ( strpos( $print_attribute_name, 'pa_' ) !== false ){

                    $print_attribute_name = wc_attribute_label( $attribute_name );

                }


				echo "<div class='Yith_WC_QOF_Variable_Product_" . $Parent_Product_ID . "_" . $Yith_Post_ID . "'>";

				echo "<span>" . esc_attr( $print_attribute_name ) . "</span>";

                $Yith_WC_QOF_Variable_Product_select_name = "Yith_WC_QOF_Variable_Product_select_name_" . $Parent_Product_ID . "_" . $Yith_Post_ID;
				echo "<select data-product_id='$Parent_Product_ID' data-yith_post_id='$Yith_Post_ID' class='Yith_WC_QOF_Variable_Product_select' name='$Yith_WC_QOF_Variable_Product_select_name'  >";

				foreach ( $attribute_array as $attribute ) {

                    $taxonomy = wc_attribute_taxonomy_name( $print_attribute_name );

                    if ( taxonomy_exists( $taxonomy ) ) {
                        $terms = get_terms($taxonomy, 'slug=' . $attribute );
                        $attribute = $terms[0]->name;
                    }

				    if ( ! isset( $array_already_variation_attributes[$attribute_name] ) )
                        $array_already_variation_attributes[$attribute_name] = array();

                    if ( ! in_array( $attribute, $array_already_variation_attributes[$attribute_name] ) )
                    {

                        $array_already_variation_attributes[$attribute_name][] = $attribute;

                        if ( isset( $data_selected[ $attribute_name ] ) ){
                            if ( $data_selected[ $attribute_name ] == wp_specialchars_decode( $attribute ) ) {
                                echo "<option value='$attribute_name' selected='selected'>" . $attribute . "</option>";
                            } else {
                                echo "<option value='$attribute_name' >" . $attribute . "</option>";
                            }
                        }
                        else
                            echo "<option value='$attribute_name' >" . $attribute . "</option>";

                    }

				}
				echo "</select>";

				echo "</div>";

			}

		}

		if ( ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_variable_price_checkbox', true ) == null || ( get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_variable_price_checkbox', true ) == 1 ) ) && $_product->is_type( 'variable' ) ) {
			?>

			<div>

				<?php echo $_product->get_price_html(); ?>

			</div>

			<?php
		}

		?>

	</div>

</div>
