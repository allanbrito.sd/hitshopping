<?php
/**
 * Created by PhpStorm.
 * User: dssaez
 * Date: 16/06/17
 * Time: 11:26
 */

/* ============================ */
/* ==== Filtering the form ==== */
/* ============================ */

global $wp;
$current_url = home_url( add_query_arg( array(), $wp->request ) );

ob_start();

?>

<div>
	<form id="Yith_wc_qof_Form_filtering_search_ID" class="Yith_wc_qof_Form_sorting_search" action="<?php echo $current_url; ?>" method="post">

		<select id="Yith_wc_qof_Form_sorting_search_select_ID" class="Yith_wc_qof_Form_sorting_search_select" name="Yith_wc_qof_Sorting">

			<option value="Default"><?php _e( 'Default sorting ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

			<option value="price_YITH_ASC" <?php echo( ( $Sorting_key == "price_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by price: low to high ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
			<option value="price_YITH_DESC" <?php echo( ( $Sorting_key == "price_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by price: high to low ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

			<option value="_sku_YITH_ASC" <?php echo( ( $Sorting_key == "_sku_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by SKU: low to high ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
			<option value="_sku_YITH_DESC" <?php echo( ( $Sorting_key == "_sku_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by SKU: high to low ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

			<option value="date_YITH_ASC" <?php echo( ( $Sorting_key == "date_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by newness ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
			<option value="date_YITH_DESC" <?php echo( ( $Sorting_key == "date_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by lateness ', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

			<option value="title_YITH_ASC" <?php echo( ( $Sorting_key == "title_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Name asc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
			<option value="title_YITH_DESC" <?php echo( ( $Sorting_key == "title_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Name desc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

            <option value="menu_order_YITH_ASC" <?php echo( ( $Sorting_key == "menu_order_YITH_ASC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Menu order asc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>
            <option value="menu_order_YITH_DESC" <?php echo( ( $Sorting_key == "menu_order_YITH_DESC" ) ? 'selected="selected"' : '' ); ?>><?php _e( 'Sort by Menu order desc', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

		</select>

		<select id="Yith_wc_qof_Form_categories_search_select_ID" class="Yith_wc_qof_Form_sorting_search_select" name="Yith_wc_qof_Categories_search">

			<?php

			/* ==== Search by category ==== */

			$category_search_id = ( ( isset( $_REQUEST['Yith_wc_qof_Categories_search'] ) ) ? $_REQUEST['Yith_wc_qof_Categories_search'] : '' );

			$taxonomy     = 'product_cat';
			$orderby      = 'name';
			$show_count   = 0;      // 1 for yes, 0 for no
			$pad_counts   = 0;      // 1 for yes, 0 for no
			$hierarchical = true;      // 1 for yes, 0 for no
			$title        = '';
			$empty        = 0;

			$args           = array(
				'taxonomy'     => $taxonomy,
				'orderby'      => $orderby,
				'show_count'   => $show_count,
				'pad_counts'   => $pad_counts,
				'hierarchical' => $hierarchical,
				'title_li'     => $title,
				'hide_empty'   => $empty,
				'parent'       => 0
			);
			$all_categories = get_categories( apply_filters( 'YITH_WC_get_categories_filter_form', $args ) );

			?>

			<option value="0"><?php _e( 'Select a category', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

			<?php

			foreach ( $all_categories as $cat ) {

				?>

				<option value="<?php echo $cat->term_id; ?>" <?php echo( ( $category_search_id == $cat->term_id ) ? 'selected="selected"' : '' ); ?>><?php echo $cat->name; ?></option>

				<?php

				$arg2              = array(
					'child_of'       => $cat->term_id,
					'menu_order'   => 'ASC',
					'hide_empty'   => 0,
					'hierarchical' => 1,
					'taxonomy'     => 'product_cat',
					'pad_counts'   => 1,
				);

				$sub_categories = get_categories( apply_filters( 'YITH_WC_get_subcategories_filter_form', $arg2 ) );

				if( !empty( $sub_categories ) && is_array( $sub_categories ) ) {

					foreach( $sub_categories as $sub_cat ){

						?>

						<option value="<?php echo $sub_cat->term_id; ?>" <?php echo( ( $category_search_id == $sub_cat->term_id ) ? 'selected="selected"' : '' ); ?>><?php echo "&nbsp;&nbsp;&nbsp;" . $sub_cat->name; ?></option>

						<?php

					}

				}

			}

			?>

		</select>

		<select id="Yith_wc_qof_Form_tags_search_select_ID" class="Yith_wc_qof_Form_sorting_search_select" name="Yith_wc_qof_tags_search">

			<?php

			/* ==== Search by tag ==== */

			$tag_search_id = ( ( isset( $_REQUEST['Yith_wc_qof_tags_search'] ) ) ? $_REQUEST['Yith_wc_qof_tags_search'] : '' );

			$taxonomy     = 'product_tag';
			$orderby      = 'name';
			$show_count   = 0;      // 1 for yes, 0 for no
			$pad_counts   = 0;      // 1 for yes, 0 for no
			$hierarchical = 1;      // 1 for yes, 0 for no
			$title        = '';
			$empty        = 0;

			$args     = array(
				'taxonomy'     => $taxonomy,
				'orderby'      => $orderby,
				'show_count'   => $show_count,
				'pad_counts'   => $pad_counts,
				'hierarchical' => $hierarchical,
				'title_li'     => $title,
				'hide_empty'   => $empty
			);
			$all_tags = get_terms( $args );

			?>

			<option value="0"><?php _e( 'Select a tag', 'yith-quick-order-forms-for-woocommerce' ); ?></option>

			<?php

			foreach ( $all_tags as $tag ) {

				?>

				<option value="<?php echo $tag->term_id; ?>" <?php echo( ( $tag_search_id == $tag->term_id ) ? 'selected="selected"' : '' ); ?>><?php echo $tag->name; ?></option>

				<?php

			}

			?>

		</select>

		<?php

		/* ==== Search by name or sku ==== */

		?>

		<input class="Yith_wc_qof_Form_sorting_search_input" type="text" name="yith_wc_qof_name" id="yith_wc_qof_name" placeholder="<?php _e( 'search by name', 'yith-quick-order-forms-for-woocommerce' ); ?>" value="<?php echo( ( isset( $_REQUEST['yith_wc_qof_name'] ) ) ? $_REQUEST['yith_wc_qof_name'] : '' ); ?>">

		<input class="Yith_wc_qof_Form_sorting_search_input" type="text" name="yith_wc_qof_sku" id="yith_wc_qof_sku" placeholder="<?php _e( 'search by SKU', 'yith-quick-order-forms-for-woocommerce' ); ?>" value="<?php echo( ( isset( $_REQUEST['yith_wc_qof_sku'] ) ) ? $_REQUEST['yith_wc_qof_sku'] : '' ); ?>">

		<input class="Yith_wc_qof_Form_sorting_search_select" type="submit" value="<?php _e( 'Search', 'yith-quick-order-forms-for-woocommerce' ); ?>">

	</form>
</div>

<?php

$filter = ob_get_clean();

$Show_filter = apply_filters( 'yith_wc_qof_filters_show_product_filter', $filter, $Yith_Post_ID, $args );

echo $Show_filter;

?>