<?php
/* =========== RADIO BUTTON =========== */

$radio = get_post_meta( $post->ID, 'yith_wc_quick_order_form_products_chosen_radio_button', true );

/* =========== SHOW PRODUCTS TO BE SELECTED =========== */

?>

<div class="YITH_QOF_Custom_Post_Type_Parent">

	<div class="YITH_QOF_Form_Line">

		<ul>
			<li>

				<p>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_products_chosen_radio_button" checked="checked" value="radio_all" <?php echo( ( $radio == "radio_all" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Choose all products ', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_products_chosen_radio_button" value="radio_allow_specific_products" <?php echo( ( $radio == "radio_allow_specific_products" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow specific products', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_products_chosen_radio_button" value="radio_disalbe_specific_products" <?php echo( ( $radio == "radio_disalbe_specific_products" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Disable products ', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

				</p>

				<?php

				$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_products_selected_select_multiple', true );

				if ( ! empty( $value ) ) {
					$products = is_array( $value ) ? $value : explode( ',', $value );
				}

				$data_selected = array();
				if ( ! empty( $products ) ) {
					foreach ( $products as $product_index ) {
						$data_selected[ $product_index ] = get_the_title( $product_index ) . "(#" . $product_index . ")";
					}
				}

				$search_product_array = array(
					'type'             => 'hidden',
					'class'            => 'wc-product-search',
					'id'               => 'yith_wc_quick_order_form_products_selected_select_multiple',
					'name'             => 'yith_wc_quick_order_form_products_selected_select_multiple',
					'data-placeholder' => __( 'Search for a product&hellip;', 'yith-quick-order-forms-for-woocommerce' ),
					'data-allow_clear' => true,
					'data-selected'    => $data_selected,
					'data-multiple'    => true,
					'data-action'      => 'woocommerce_json_search_products_and_variations',
					'value'            => empty( $value ) ? '' : $value,
					'style'            => 'width: 500px'
				);

				?>

				<div class="YITH_QOF_Form_Line_div_parent">

					<div id="YITH_QOF_Choose_Products_Displaying" class="YITH_QOF_Blank_Brightness"></div>

					<?php

					yit_add_select2_fields( $search_product_array );

					?>

				</div>

			</li>

		</ul>

	</div>

	<?php

	/* =========== SHOW CATEGORIES TO BE SELECTED =========== */

	$radio = get_post_meta( $post->ID, 'yith_wc_quick_order_form_categories_chosen_radio_button', true );

	?>

	<div class="YITH_QOF_Form_Line">

		<ul>

			<li>

				<p>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_categories_chosen_radio_button" checked="checked" value="radio_all" <?php echo( ( $radio == "radio_all" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Choose all categories ', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_categories_chosen_radio_button" value="radio_allow_specific_categories" <?php echo( ( $radio == "radio_allow_specific_categories" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow specific categories', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_categories_chosen_radio_button" value="radio_disalbe_specific_categories" <?php echo( ( $radio == "radio_disalbe_specific_categories" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Disable categories ', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

				</p>

				<?php

				$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_categories_selected_select_multiple', true );

				if ( ! empty( $value ) ) {
					$categories = is_array( $value ) ? $value : explode( ',', $value );
				}

				$data_selected = array();
				if ( ! empty( $categories ) ) {
					foreach ( $categories as $category_index ) {
						$term                             = get_term_by( 'id', $category_index, 'product_cat', 'ARRAY_A' );
						$data_selected[ $category_index ] = $term['name'];

					}
				}

				$search_cat_array = array(
					'type'             => 'hidden',
					'class'            => 'yith_wc_qof_categories_search',
					'id'               => 'yith_wc_quick_order_form_categories_selected_select_multiple',
					'name'             => 'yith_wc_quick_order_form_categories_selected_select_multiple',
					'data-placeholder' => esc_attr__( 'Search for categories&hellip;', 'yith-quick-order-forms-for-woocommerce' ),
					'data-allow_clear' => true,
					'data-selected'    => $data_selected,
					'data-multiple'    => true,
					'data-action'      => '',
					'value'            => empty( $value ) ? '' : $value,
					'style'            => 'width: 500px'
				);

				?>

				<div class="YITH_QOF_Form_Line_div_parent">

					<div id="YITH_QOF_Choose_Categories_Displaying" class="YITH_QOF_Blank_Brightness"></div>

					<?php

					yit_add_select2_fields( $search_cat_array );

					?>

				</div>

			</li>

		</ul>

	</div>

	<?php

	/* =========== SHOW TAGS TO BE SELECTED =========== */

	$radio = get_post_meta( $post->ID, 'yith_wc_quick_order_form_tags_chosen_radio_button', true );

	?>

	<div class="YITH_QOF_Form_Line">

		<ul>

			<li>

				<p>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_tags_chosen_radio_button" checked="checked" value="radio_all" <?php echo( ( $radio == "radio_all" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Choose all tags ', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_tags_chosen_radio_button" value="radio_allow_specific_tags" <?php echo( ( $radio == "radio_allow_specific_tags" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Allow specific tags', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

					<label>
						<input type="radio" name="yith_wc_quick_order_form_tags_chosen_radio_button" value="radio_disalbe_specific_tags" <?php echo( ( $radio == "radio_disalbe_specific_tags" ) ? 'checked="checked"' : '' ); ?>/>

                        <span class="Yith_wc_order_quick_forms_margin_right">
                            <?php _e( 'Disable tags ', 'yith-quick-order-forms-for-woocommerce' ); ?>
                        </span>
					</label>

				</p>

				<?php

				$value = get_post_meta( $post->ID, 'yith_wc_quick_order_form_tags_selected_select_multiple', true );

				if ( ! empty( $value ) ) {
					$tags = is_array( $value ) ? $value : explode( ',', $value );
				}

				$data_selected = array();
				if ( ! empty( $tags ) ) {
					foreach ( $tags as $tag_index ) {
						$term                        = get_term_by( 'id', $tag_index, 'product_tag', 'ARRAY_A' );
						$data_selected[ $tag_index ] = $term['name'];

					}
				}

				$search_cat_array = array(
					'type'             => 'hidden',
					'class'            => 'yith_wc_qof_tags_search',
					'id'               => 'yith_wc_quick_order_form_tags_selected_select_multiple',
					'name'             => 'yith_wc_quick_order_form_tags_selected_select_multiple',
					'data-placeholder' => esc_attr__( 'Search for tags&hellip;', 'yith-quick-order-forms-for-woocommerce' ),
					'data-allow_clear' => true,
					'data-selected'    => $data_selected,
					'data-multiple'    => true,
					'data-action'      => '',
					'value'            => empty( $value ) ? '' : $value,
					'style'            => 'width: 500px'
				);

				?>

				<div class="YITH_QOF_Form_Line_div_parent">

					<div id="YITH_QOF_Choose_Tags_Displaying" class="YITH_QOF_Blank_Brightness"></div>

					<?php

					yit_add_select2_fields( $search_cat_array );

					?>

				</div>

			</li>

		</ul>

	</div>

</div>