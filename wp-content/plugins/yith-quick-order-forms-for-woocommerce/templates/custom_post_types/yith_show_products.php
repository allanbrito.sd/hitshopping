<?php
/**
 * Created by PhpStorm.
 * User: dssaez
 * Date: 16/06/17
 * Time: 11:26
 */

/* ====================================================================== */
/* ==== Checking the products selected and making an array out of it ==== */
/* ====================================================================== */

$current_url = get_permalink();

$array_enable_products  = array();
$array_disable_products = array();

/* ==== Getting products from categories ==== */

$radio_categories = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_categories_chosen_radio_button', true );

if ( ! empty( $array_categories ) && ( $radio_categories != 'radio_all' ) ) {

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'terms'    => $array_categories,
                'operator' => 'IN',
            )
        )
    );

    $wp_query = new WP_Query( $args );

    while ( $wp_query->have_posts() ) : $wp_query->the_post();

        global $product;
        $id = $product->get_id();
        ( ( $radio_categories == 'radio_allow_specific_categories' ) ? $array_enable_products[ $id ] = $id : $array_disable_products[ $id ] = $id );

    endwhile;

    wp_reset_query();
}

/* ==== Getting products from tags ==== */

$radio_tags = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_tags_chosen_radio_button', true );

if ( ! empty( $array_tags ) && ( $radio_tags != 'radio_all' ) ) {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_tag',
                'terms'    => $array_tags,
                'operator' => 'IN',
            )
        )
    );

    $wp_query = new WP_Query( $args );

    while ( $wp_query->have_posts() ) : $wp_query->the_post();

        global $product;
        $id = $product->get_id();
        ( ( $radio_tags == 'radio_allow_specific_tags' ) ? $array_enable_products[ $id ] = $id : $array_disable_products[ $id ] = $id );

    endwhile;
    wp_reset_query();
}

/* ==== Getting products individually selected ==== */

$radio_products = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_products_chosen_radio_button', true );

if ( ! is_array( $array_products ) )
    $array_products = explode(",", $array_products);

$group_of_variations_same_parent = false;
if ( ! empty( $array_products ) && ( $radio_products != 'radio_all' ) ) {
    // Checking if the products to show are variations from the same parent
    $group_of_variations_same_parent = true;
    $parent_id = null;
    foreach ( $array_products as $Product_ID ) {

        // whenever we find a product which is not a variation or has different parent than the others
        // then the list of products will be shown as normal, otherwise we will create an array with these products
        // to give them order later on
        if ( $group_of_variations_same_parent ){

            $product_in = wc_get_product( $Product_ID );

            if ( $product_in->post_type == 'product_variation' ){
                if ( $parent_id == null )
                    $parent_id = $product_in->get_parent_id();
                else{
                    if ( $parent_id != $product_in->get_parent_id() )
                        $group_of_variations_same_parent = false;
                }
            }
            else
                $group_of_variations_same_parent = false;

        }

        ( ( $radio_products == 'radio_allow_specific_products' ) ? $array_enable_products[ $Product_ID ] = $Product_ID : $array_disable_products[ $Product_ID ] = $Product_ID );
        ( ( $radio_products == 'radio_allow_specific_products' ) ? $array_enable_Just_products[ $Product_ID ] = $Product_ID : $array_enable_Just_products = array() );
    }
}

/* ==== If we are searching by sku we have to add the variation of the variable products to the array enable products ==== */

if ( isset( $_REQUEST['yith_wc_qof_sku'] ) ) {
    if ( $_REQUEST['yith_wc_qof_sku'] != '' ) {
        foreach ( $array_enable_products as $Product_ID ) {

            $_product_by_SKU = wc_get_product( $Product_ID );

            if ( $_product_by_SKU->is_type( 'variable' ) ) {

                $childrens = $_product_by_SKU->get_children();
                foreach ( $childrens as $child ) {
                    $array_enable_products[ $child ] = $child;
                }

            }

        }
    }
}

if ( empty( $array_enable_products ) && ( $radio_products == 'radio_allow_specific_products' && $radio_categories == 'radio_allow_specific_categories' && $radio_tags == 'radio_allow_specific_tags' ) ) {
    _e( 'There are no products to show in this form', 'yith-quick-order-forms-for-woocommerce' );
} else {

    /* ===================================================================================== */
    /* ==== Adding the filter fields: order by and serach by category, tag, name or sku ==== */
    /* ===================================================================================== */

    $yith_wc_qof_sorting_select = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_Sorting_Select', true );

    $Sorting_key = ( ( isset( $_REQUEST['Yith_wc_qof_Sorting'] ) ) ? $_REQUEST['Yith_wc_qof_Sorting'] : $yith_wc_qof_sorting_select );

    $args = array(
        'Sorting_key' => $Sorting_key,
        'Yith_Post_ID' => $Yith_Post_ID
    );
    wc_get_template( 'yith_show_products_filter.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) . 'custom_post_types/' );

    /* =========================================== */
    /* ==== Showing the data after the filter ==== */
    /* =========================================== */

    $data_selected = array();

    /* ============ If all the radio buttons are set up as 'all' we show all products ============*/

    if ( $radio_products == 'radio_all' && $radio_categories == 'radio_all' && $radio_tags == 'radio_all' ) {

        /* ==== Searching by name ==== */
        if ( isset( $_REQUEST['yith_wc_qof_name'] ) ) {
            global $wpdb;
            $str                   = $_REQUEST['yith_wc_qof_name'];
            if ( $str != '' ){
                $array_enable_products = $wpdb->get_col( "select ID from $wpdb->posts where post_title like '%$str%'" );
            }

            /* ==== Initial arguments for the wp_query ==== */

            if ( $_REQUEST['yith_wc_qof_sku'] != '' || $_REQUEST['yith_wc_qof_name'] != '' || $_REQUEST['Yith_wc_qof_tags_search'] != 0 || $_REQUEST['Yith_wc_qof_Categories_search'] != 0 || ( $_REQUEST['Yith_wc_qof_Sorting'] != 'Default' && $_REQUEST['Yith_wc_qof_Sorting'] != 'menu_order_YITH_ASC' && $_REQUEST['Yith_wc_qof_Sorting'] != 'menu_order_YITH_DESC' ) ){
                $args = array(
                    'post_type'      => array( 'product', 'product_variation' ),
                    'post__in'       => $array_disable_products,
                    'posts_per_page' => - 1,
                );
            }
            else{
                $args = array(
                    'post_type'      => array( 'product' ),
                    'post__not_in'   => $array_disable_products,
                    'posts_per_page' => - 1,
                );
            }

        } else {

            /* ==== Initial arguments for the wp_query ==== */

            $args = array(
                'post_type'      => array( 'product' ),
                'posts_per_page' => - 1,
            );

        }

    } else {

        /* ============ If at least one of the radio buttons is set up as 'disable' and the rest as 'all' ============*/

        if ( $radio_products != 'radio_allow_specific_products' && $radio_categories != 'radio_allow_specific_categories' && $radio_tags != 'radio_allow_specific_tags' ) {

            /* ==== Initial arguments for the wp_query ==== */

            $args = array(
                'post_type'      => array( 'product' ),
                'post__not_in'   => $array_disable_products,
                'posts_per_page' => - 1,
            );

        } else {

            if ( ! empty( $array_enable_Just_products ) && ! empty( $array_disable_products ) ) {
                $array_disable_products = array_diff( $array_disable_products, $array_enable_Just_products );
            }

            $array_enable_products = array_diff( $array_enable_products, $array_disable_products );

            /* ==== Initial arguments for the wp_query ==== */

            if ( empty( $array_enable_products ) ) {
                $array_enable_products = array( '0' => '0' );
            }

            $args = array(
                'post_type'      => array( 'product', 'product_variation' ),
                'post__in'       => $array_enable_products,
                'posts_per_page' => - 1,
            );

        }

    }

    /* =========== Setting up the pagination in case is activated =========== */

    $products_per_page = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_Products_Per_Page_text', true );

    if ( ( $products_per_page != 0 ) && ( $products_per_page != null ) ) {

        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : ( ( isset( $_REQUEST['yithpage'] ) ) ? $_REQUEST['yithpage'] : 1 ) );

        $args['paged']          = $paged;
        $args['posts_per_page'] = $products_per_page;
    }

    /* =========== Checking if there is a different sorting search configured than default =========== */

    if ( $yith_wc_qof_sorting_select != 'Default' ) {

        $array_Sorting_key = explode( "_YITH_", $yith_wc_qof_sorting_select );

        if ( $array_Sorting_key[0] == 'price' ) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = '_price';
            $args['order']    = $array_Sorting_key[1];
        } else {
            $args['orderby'] = $array_Sorting_key[0];
            $args['order']   = $array_Sorting_key[1];
        }
    }
    else{
        $args['orderby'] = 'ID';
        $args['order'] = 'DESC';
    }

    /* ==== Searching by sorting ==== */

    if ( isset( $_REQUEST['Yith_wc_qof_Sorting'] ) ) {

        if ( $Sorting_key != 'Default' ) {

            $array_Sorting_key = explode( "_YITH_", $Sorting_key );

            if ( $array_Sorting_key[0] == 'price' ) {
                $args['orderby']  = 'meta_value_num';
                $args['meta_key'] = '_price';
                $args['order']    = $array_Sorting_key[1];
            } else {
                $args['orderby'] = $array_Sorting_key[0];
                $args['order']   = $array_Sorting_key[1];
            }
        }
        else{
            $args['orderby'] = 'ID';
            $args['order'] = 'DESC';
        }

    }

    /* ==== Searching by category ==== */

    $array_tax_query             = array();
    $array_tax_query['relation'] = 'AND';

    if ( isset( $_REQUEST['Yith_wc_qof_Categories_search'] ) ) {
        if ( $_REQUEST['Yith_wc_qof_Categories_search'] != 0 ) {
            $array_tax_query[] = array(
                'taxonomy' => 'product_cat',
                'terms'    => $_REQUEST['Yith_wc_qof_Categories_search'],
                'operator' => 'IN',
            );
        }
    }

    /* ==== Searching by tag ==== */

    if ( isset( $_REQUEST['Yith_wc_qof_tags_search'] ) ) {
        if ( $_REQUEST['Yith_wc_qof_tags_search'] != 0 ) {
            $array_tax_query[] = array(
                'taxonomy' => 'product_tag',
                'terms'    => $_REQUEST['Yith_wc_qof_tags_search'],
                'operator' => 'IN',
            );
        }
    }

    $args['tax_query'] = $array_tax_query;

    /* ==== Searching by name ==== */

    if ( isset( $_REQUEST['yith_wc_qof_name'] ) ) {
        if ( $_REQUEST['yith_wc_qof_name'] != '' ) {
            $args['s'] = $_REQUEST['yith_wc_qof_name'];
            $args['post_type'] = array( 'product' );
        }
    }

    /* ==== Searching by sku ==== */

    if ( isset( $_REQUEST['yith_wc_qof_sku'] ) ) {
        if ( $_REQUEST['yith_wc_qof_sku'] != '' && ( isset( $_REQUEST['Yith_wc_qof_tags_search'] ) && $_REQUEST[ 'Yith_wc_qof_tags_search' ] == 0 ) && ( isset( $_REQUEST['Yith_wc_qof_Categories_search'] ) && $_REQUEST['Yith_wc_qof_Categories_search'] == 0 ) ) {
            $args['meta_query'] = array(
                array(
                    'key'     => '_sku',
                    'value'   => $_REQUEST['yith_wc_qof_sku'],
                    'compare' => 'LIKE',
                )
            );
            $args['post_type'] = array( 'product', 'product_variation' );
        }
    }

    $wp_query = new WP_Query( $args );

    if ( $wp_query->have_posts() ) {

        $first_time = 0;
        $filter_by_sku = false;

        global $WC;

        $array_of_variations_same_parent = array();
        while ($wp_query->have_posts()) : $wp_query->the_post();

            global $product;

            if ( ( isset( $_REQUEST['yith_wc_qof_sku'] ) && $_REQUEST['yith_wc_qof_sku'] != '' ) && ( ( isset( $_REQUEST['Yith_wc_qof_tags_search'] ) && $_REQUEST[ 'Yith_wc_qof_tags_search' ] != 0 ) || ( isset( $_REQUEST['Yith_wc_qof_Categories_search'] ) && $_REQUEST['Yith_wc_qof_Categories_search'] != 0 ) ) ) {

                $filter_by_sku = true;

                $products_found_by_sku = array();

                $SKU_Found = false;

                if ( strpos( strtolower( $product->get_sku() ), strtolower( $_REQUEST['yith_wc_qof_sku'] ) ) !== false) {
                    $products_found_by_sku[] = $product;
                    $SKU_Found = true;
                }

                if ( $product->is_type( 'variable' ) ) {

                    $childrens = $product->get_children();
                    foreach ( $childrens as $child_id ) {

                        $child = wc_get_product( $child_id );

                        if ( strpos( strtolower( $child->get_sku() ), strtolower( $_REQUEST['yith_wc_qof_sku'] ) ) !== false) {
                            $products_found_by_sku[] = $child;
                            $SKU_Found = true;
                        }
                    }

                }

                if ( ! $SKU_Found )
                    continue;

            }


            if ( $first_time == 0 ) {

                $all_add_to_cart_top = get_post_meta($Yith_Post_ID, 'yith_wc_quick_order_form_all_add_to_cart_top_checkbox', true);

                if ($all_add_to_cart_top == 'yes') {

                    ?>

                    <div class="YITH_WC_qof_add_all_products_to_cart">

                        <button type="submit" class="button alt YITH_WC_qof_Button_Add_to_cart_trigger_all">
                            <?php _e('Add all products to cart', 'yith-quick-order-forms-for-woocommerce'); ?>
                        </button>

                    </div>

                    <?php

                }

                $args = array(
                    'my_post_found' => $wp_query->found_posts,
                    'Yith_Post_ID' => $Yith_Post_ID,
                );
                wc_get_template( 'yith_show_products_header.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) . 'custom_post_types/' );

                $first_time = 1;

            }

            if ( ( isset( $_REQUEST['yith_wc_qof_sku'] ) && $_REQUEST['yith_wc_qof_sku'] != '' ) && ( ( isset( $_REQUEST['Yith_wc_qof_tags_search'] ) && $_REQUEST[ 'Yith_wc_qof_tags_search' ] != 0 ) || ( isset( $_REQUEST['Yith_wc_qof_Categories_search'] ) && $_REQUEST['Yith_wc_qof_Categories_search'] != 0 ) ) ) {

                foreach ( $products_found_by_sku as $product_found_by_sku )
                    yith_wc_quick_order_form_show_products( $product_found_by_sku, $Yith_Post_ID, $data_selected );

            }else{

                // Checking if the products to show are variations from the same parent
                // If yes, then we create an array with the products to be shown in order as in
                // the variation product admin panel
                if ( ! $group_of_variations_same_parent )
                    yith_wc_quick_order_form_show_products( $product, $Yith_Post_ID, $data_selected );
                else
                    $array_of_variations_same_parent[] = $product;

            }

        endwhile;

        if ( $group_of_variations_same_parent && $first_time == 1 && ! $filter_by_sku ){

            // We do a for of all the possible variations the product has
            foreach ( wc_get_product( $parent_id )->get_available_variations() as $attribute_name => $attribute_array ) {

                // Getting an array of attributes
                $attributes_in_order = $attribute_array["attributes"];
                $number_of_attributes = count( $attributes_in_order );

                // We start to check inside of the products to show with the same parent which attributes match in order
                foreach ( $array_of_variations_same_parent as $variation_product ){

                    $array_attributes_variation_product = $variation_product->get_attributes();

                    $count = 0;
                    //Attributes in order
                    foreach ( $attributes_in_order as $attribute ){

                        //Checking if the attributes in order is on the attributes of the variation to show
                        foreach ( $array_attributes_variation_product as $attribute_variation ){

                            if ( $attribute == $attribute_variation){
                                $count = $count + 1;
                                break;
                            }

                        }

                    }

                    //If the attribute was found then we show it
                    if ( $count ==  $number_of_attributes){

                        yith_wc_quick_order_form_show_products( $variation_product, $Yith_Post_ID, $data_selected );
                        break;

                    }

                }

            }

        }

        if ( $first_time == 1 ){

            $all_add_to_cart_bottom = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox', true );

            if ( $all_add_to_cart_bottom == 'yes' ){

                ?>

                <div class="YITH_WC_qof_add_all_products_to_cart">

                    <button type="submit" class="button alt YITH_WC_qof_Button_Add_to_cart_trigger_all">
                        <?php _e( 'Add all products to cart', 'yith-quick-order-forms-for-woocommerce' ); ?>
                    </button>

                </div>

                <?php

            }

        }

    } else {
        echo "<p class='Yith_wc_qof_No_products_Found'>" . __( 'No products found', 'yith-quick-order-forms-for-woocommerce' ) . "</p>";
    }

    if ( ( $products_per_page != 0 ) && ( $products_per_page != null ) ) {

        echo "<div id='Yith_WC_QOF_Frontend_Pagination_ID' class='Yith_WC_QOF_Frontend_Pagination' data-current_url='$current_url'>";

        $big        = 999999999; // need an unlikely integer
        $translated = __( 'Page', 'yith-quick-order-forms-for-woocommerce' ); // Supply translatable string

        $args = array(
            'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'             => '%#%',
            'current'            => $paged,
            'total'              => $wp_query->max_num_pages,
            'before_page_number' => '<span class="screen-reader-text">' . $translated . '</span>'
        );

        echo paginate_links( $args );

        echo "</div>";

        if ( is_single() ) {

            wp_register_script( 'yith-wc-quick-order-forms-post-pagination-js', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-frontend-post-pagination.js', array(
                'jquery',
                'jquery-ui-sortable'
            ), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

            wp_enqueue_script( 'yith-wc-quick-order-forms-post-pagination-js' );

        }

    }

    wp_reset_query();
}

?>
