<?php
/**
 * Plugin Name: YITH Quick Order Forms for WooCommerce Premium
 * Plugin URI: https://hitoutlets.com/themes/plugins/yith-quick-order-forms-for-woocommerce/
 * Description: <code><strong>YITH Quick Order Forms</strong></code> allows you to create your personal "Form" to make a list of specific products, choose what to show (name, sku, price, discount) and allow specific users to have access through the permission filter. <a href="https://hitoutlets.com/" target="_blank">Find new awesome plugins on <strong>YITH</strong></a>.
 * Author: YITHEMES
 * Text Domain: yith-quick-order-forms-for-woocommerce
 * Domain Path: /languages/
 * Version: 1.0.32
 * Author URI: http://hitoutlets.com/
 * WC requires at least: 3.0.0
 * WC tested up to: 3.4.x
 **/

/*
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! function_exists( 'install_premium_woocommerce_admin_notice' ) ) {
	/**
	 * Print an admin notice if woocommerce is deactivated
	 *
	 * @author Daniel Sanchez Saez <dssaez@gmail.com>
	 * @since  1.0
	 * @return void
	 * @use    admin_notices hooks
	 */
	function install_premium_woocommerce_admin_notice() { ?>
		<div class="error">
			<p><?php _ex( 'YITH WooCommerce Quick Order Forms is enabled but not effective. It requires WooCommerce in order to work.', 'Alert Message: WooCommerce require', 'yith-quick-order-forms-for-woocommerce' ); ?></p>
		</div>
		<?php
	}
}

/*
==========
  DEFINE
==========
*/
! defined( 'YITH_WC_QOF_CONSTANT_NAME' ) && define( 'YITH_WC_QOF_CONSTANT_NAME', 'QUICK_ORDER_FORMS' );
! defined( 'YITH_WC_QOF_FILES_INCLUDE_NAME' ) && define( 'YITH_WC_QOF_FILES_INCLUDE_NAME', 'quick-order-forms' );

! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION', '1.0.32' );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_INIT' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG', 'yith-quick-order-forms-for-woocommerce' );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SECRETKEY' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SECRETKEY', 'fhwUwemR422ha6xBsXzg' );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_FILE' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_FILE', __FILE__ );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH', plugin_dir_path( __FILE__ ) );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_URL' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_URL', plugins_url( '/', __FILE__ ) );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_URL' ) . 'assets/' );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'templates/' );
! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_OPTIONS_PATH' ) && define( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_OPTIONS_PATH', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'panel' );

/*
====================================================================
 Sessions initiation and set them up to destroy when log in and out
====================================================================
*/

if ( ! function_exists( 'yith_wc_' . YITH_WC_QOF_CONSTANT_NAME . '_cyb_session_start' ) ) {

	$functionname = 'yith_wc_' . YITH_WC_QOF_CONSTANT_NAME . '_cyb_session_start';

	$$functionname = function () {
		if ( ! session_id() ) {
			session_start();
		}

	};

	add_action( 'plugins_loaded', $$functionname, 1 );

}

if ( ! function_exists( 'yith_wc_' . YITH_WC_QOF_CONSTANT_NAME . '_cyb_session_end' ) ) {

	$functionname = 'yith_wc_' . YITH_WC_QOF_CONSTANT_NAME . '_cyb_session_end';

	$$functionname = function () {
		session_destroy();
	};

	add_action( 'wp_logout', $$functionname );
	add_action( 'wp_login', $$functionname );
}

/*
================================
 Plugin Framework Version Check
================================
*/
! function_exists( 'yit_maybe_plugin_fw_loader' ) && require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'plugin-fw/init.php' );
yit_maybe_plugin_fw_loader( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) );

/* Load text domain */
load_plugin_textdomain( 'yith-quick-order-forms-for-woocommerce', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

/*
===============================
 * Instance main plugin class
===============================
*/
if ( ! function_exists( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_MAIN' ) ) {
	/**
	 * Unique access to instance of YITH_WC_FAVORITOS class
	 *
	 * @return YITH_WC_QUICK_ORDER_FORMS_MAIN Premium
	 * @since 1.0.0
	 */

	function YITH_WC_QUICK_ORDER_FORMS_MAIN() {
		// Load required classes and functions
		require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'includes/class.yith-wc-quick-order-forms.php' );

		return YITH_WC_Quick_Order_Forms_Main_Class::instance();
	}
}

/*
===================================
 * Instance main custom post type
===================================
*/
if ( ! function_exists( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_Custom_Post_Type' ) ) {
	/**
	 * Unique access to instance of YITH_WC_QUICK_ORDER_FORMS Custom_Post_Type
	 *
	 * @return YITH_WC_QUICK_ORDER_FORMS Custom_Post_Type
	 * @since 1.0.0
	 */
	function YITH_WC_QUICK_ORDER_FORMS_Custom_Post_Type() {

		if ( is_admin() && ! ( defined( 'DOING_AJAX' ) ) ) {

			// Load required classes and functions
			require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'includes/class.yith-wc-quick-order-forms-custom-post-type.php' );

			return YITH_WC_Quick_Order_Forms_Custom_Post_Type::get_instance();

		}

	}
}

if ( ! function_exists( 'yith_wc_quick_order_forms_install' ) ) {
	function yith_wc_quick_order_forms_install() {

		if ( ! function_exists( 'WC' ) ) {
			add_action( 'admin_notices', 'install_premium_woocommerce_admin_notice' );
		} else {
			/**
			 * Instance main plugin class
			 */
			YITH_WC_QUICK_ORDER_FORMS_MAIN();

			/**
			 * Instance the custom post type of product list
			 */
			YITH_WC_QUICK_ORDER_FORMS_Custom_Post_Type();

            /* Load quick order form text domain */
            load_plugin_textdomain( 'yith-quick-order-forms-for-woocommerce', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		}
	}
}

add_action( 'plugins_loaded', 'yith_wc_quick_order_forms_install', 11 );
