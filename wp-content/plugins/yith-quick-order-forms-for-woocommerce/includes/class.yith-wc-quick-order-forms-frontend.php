<?php
/*
 * This file belongs to the YITH Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_FAVORITOS_Frontend
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author
 *
 */

if ( ! class_exists( 'YITH_WC_Quick_Order_Forms_Frontend' ) ) {
	/**
	 * Class YITH_WC_Favoritos_Frontend
	 *
	 * @author
	 */
	class YITH_WC_Quick_Order_Forms_Frontend {

		const PLUGIN_QUICK_ORDER_FORMS_DB_VERSION = '1.0.0';

		/**
		 * Construct
		 *
		 * @author
		 * @since 1.0
		 */

		protected $ajax;

		public function __construct() {

			/* ====== ENQUEUE STYLES AND JS ====== */
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			/* ====== Sessions initation for ajax to include the classes with "Fronted" value ====== */
			$this->yith_woocommerce_sessions();

			add_action( 'admin_post_nopriv_yith_wc_qof_sorting', array( $this, 'yith_wc_qof_sorting_by' ) );
			add_action( 'admin_post_yith_wc_qof_sorting', array( $this, 'yith_wc_qof_sorting_by' ) );

			/* ====== AJAX FRONT FUNCTIONS ====== */

			require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'includes/class.yith-wc-' . YITH_WC_QOF_FILES_INCLUDE_NAME . '-ajax-front.php' );
			$this->ajax = YITH_WC_Quick_Order_Forms_Ajax_Front::get_instance();

			/* ====== RUNING THE SHORTCODE ====== */

			require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'includes/class.yith-wc-' . YITH_WC_QOF_FILES_INCLUDE_NAME . '-shortcodes.php' );
			Quick_Order_Forms_Shortcodes::init();

			// Filter to hide or show the cart depending on the checkbox show only in forms
			add_filter( 'woocommerce_widget_cart_is_hidden', array( $this, 'yith_checking_is_form_running' ) );

			/* ====== ADDING FILTERS ====== */

			/* Filter to show or not show the search filter */
			add_filter( 'yith_wc_qof_filters_show_product_filter', array( $this, 'yith_wc_qof_filters_show_product_filter_method' ), 10, 3 );

			/* Filter to show or not show the number of products to show */
			add_filter( 'yith_wc_qof_filters_show_number_products_found', array( $this, 'yith_wc_qof_filters_show_number_products_found_method' ), 10, 3 );

		}

		public function yith_wc_qof_filters_show_product_filter_method( $filter, $Yith_Post_ID, $args ) {

			$value         = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_search_filter_checkbox', true );
			return ( $value != '0' ? $filter : '' );

		}

		public function yith_wc_qof_filters_show_number_products_found_method( $Show_Number_Products_Found, $Yith_Post_ID, $my_post_found ) {

			$value            = get_post_meta( $Yith_Post_ID, 'yith_wc_quick_order_form_number_of_products_checkbox', true );
			return ( $value != '0' ? $Show_Number_Products_Found : '' );

		}

		public function yith_checking_is_form_running() {

			/* ========== We get all the data instances of the widget woocommerce cart =========*/
			$widget_name      = "woocommerce_widget_cart";
			$widget_instances = get_option( 'widget_' . $widget_name );

			$only_in_forms = false;

			/* ========== Finding the data only_in_forms to see if it's enable =========*/
			foreach ( $widget_instances as $widget_instance ) {
				if ( isset( $widget_instance['only_in_forms'] ) && ( $widget_instance['only_in_forms'] == '1' ) ) {
					$only_in_forms = true;
				}
			}

			$is_shortcode = $this->has_shortcode( 'yith_wc_quick_order_form' );

			return ( ( $only_in_forms && ! $is_shortcode ) || is_cart() || is_checkout() );
		}

		// check the current post for the existence of a short code
		public function has_shortcode( $shortcode = '' ) {

			$post_to_check = get_post( get_the_ID() );

			// false because we have to search through the post content first
			$found = false;

			// if no short code was provided, return false
			if ( ! $shortcode ) {
				return $found;
			}
			// check the post content for the short code
			if ( isset( $post_to_check ) )
				if ( stripos( $post_to_check->post_content, '[' . $shortcode ) !== false ) {
					// we have found the short code
					$found = true;
				}

			// return our final results
			return $found;
		}

		public function yith_woocommerce_sessions() {

			$_SESSION[ 'yith_wc_' . YITH_WC_QOF_CONSTANT_NAME . '_current_ajax' ] = "Frontend";

		}

		public function enqueue_scripts() {

			/* ====== Dashicons ====== */

			wp_enqueue_style( 'dashicons' );

			/* ====== Style ====== */

			wp_register_style( 'yith-wc-quick-order-forms-style', apply_filters( 'yith_wc_qof_frontend_css', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'css/yith-qof-frontend.css' ), array(), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) );
			wp_enqueue_style( 'yith-wc-quick-order-forms-style' );

			/* ====== Scripts ====== */

			wp_register_script( 'yith-wc-quick-order-forms-js', apply_filters( 'yith_wc_qof_frontend_js', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-frontend.js' ), array(
				'jquery',
				'jquery-ui-sortable'
			), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

			wp_localize_script( 'yith-wc-quick-order-forms-js', 'yith_wc_quick_order_forms_object', apply_filters( 'yith_wc_qof_frontend_localize', array(
				'ajax_url'    => admin_url( 'admin-ajax.php' ),
				'ajax_loader' => constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . '/images/ajax-loader.gif',
			) ) );

			wp_enqueue_script( 'yith-wc-quick-order-forms-js' );

			wp_register_script( 'yith-wc-quick-order-forms-responsive-js', apply_filters( 'yith_wc_qof_frontend_responsive', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-responsive.js' ), array(
				'jquery',
				'jquery-ui-sortable'
			), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

			wp_enqueue_script( 'yith-wc-quick-order-forms-responsive-js' );

		}

	}
}
