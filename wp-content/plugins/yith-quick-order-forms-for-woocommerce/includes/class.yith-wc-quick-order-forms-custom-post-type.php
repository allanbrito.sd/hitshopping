<?php

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_Quick_Order_Forms
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author     Your Inspiration Themes
 *
 */

if ( ! class_exists( 'YITH_WC_Quick_Order_Forms_Custom_Post_Type' ) ) {
	/**
	 * Class YITH_WC_FAVORITOS_Class
	 *
	 * @author Daniel Sánchez Sáez <dssaez@gmail.com>
	 */
	class YITH_WC_Quick_Order_Forms_Custom_Post_Type {

		/**
		 * Main Instance
		 *
		 * @var YITH_WC_Quick_Order_Forms_Custom_Post_Type
		 * @since  1.0
		 * @access protected
		 */
		protected static $_instance = null;

		/**
		 * Quick Order Post Type
		 *
		 * @var string
		 * @static
		 */
		public static $CPT_quick_order_form_id = 'yith_wc_qof_1';

		public static function get_instance() {

			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self;
			}

			return self::$_instance;
		}

		//setup the widget name, description, etc...
		public function __construct() {

			add_filter( 'woocommerce_screen_ids', array( $this, 'yith_wc_quick_order_form_add_wc_admin_page' ) );

			// Initiation of the custom post type
			add_action( 'init', array( $this, 'register_post_types' ) );

			// Adding settings meta boxes
			add_action( 'add_meta_boxes', array( $this, 'yith_wc_quick_order_form_add_meta_box_seetings' ) );

			// Adding permissions meta boxes
			add_action( 'add_meta_boxes', array( $this, 'yith_wc_quick_order_form_add_meta_box_permissions' ) );

			// Adding products to chose meta boxes
			add_action( 'add_meta_boxes', array( $this, 'yith_wc_quick_order_form_add_meta_box_products_chosen' ) );

			// Adding shortcode meta boxes
			add_action( 'add_meta_boxes', array( $this, 'yith_wc_quick_order_form_add_meta_box_shortcode' ) );

			// Saving the data of each form
			add_action( 'save_post', array( $this, 'yith_wc_quick_order_form_save_data' ) );

		}

		public function yith_wc_quick_order_form_add_wc_admin_page( $array ) {
			$array[] = self::$CPT_quick_order_form_id;

			return $array;
		}

		/**
		 * Register core post types.
		 */
		public function register_post_types() {

			if ( post_type_exists( self::$CPT_quick_order_form_id ) ) {
				return;
			}

			do_action( 'yith_wo_qof_register_post_type' );

			/*  QUICK ORDER FORMS  */

			$labels = array(
				'name'               => __( 'Quick order forms', 'yith-quick-order-forms-for-woocommerce' ),
				'singular_name'      => __( 'Quick order forms', 'yith-quick-order-forms-for-woocommerce' ),
				'add_new'            => __( 'Add new form', 'yith-quick-order-forms-for-woocommerce' ),
				'add_new_item'       => __( 'Add New Form', 'yith-quick-order-forms-for-woocommerce' ),
				'edit'               => __( 'Edit', 'yith-quick-order-forms-for-woocommerce' ),
				'edit_item'          => __( 'Edit Form', 'yith-quick-order-forms-for-woocommerce' ),
				'new_item'           => __( 'New Form', 'yith-quick-order-forms-for-woocommerce' ),
				'view'               => __( 'View Form', 'yith-quick-order-forms-for-woocommerce' ),
				'view_item'          => __( 'View Form', 'yith-quick-order-forms-for-woocommerce' ),
				'search_items'       => __( 'Search Forms', 'yith-quick-order-forms-for-woocommerce' ),
				'not_found'          => __( 'No Forms found', 'yith-quick-order-forms-for-woocommerce' ),
				'not_found_in_trash' => __( 'No Forms found in trash', 'yith-quick-order-forms-for-woocommerce' ),
				'parent'             => __( 'Parent Forms', 'yith-quick-order-forms-for-woocommerce' ),
				'menu_name'          => _x( 'YITH Forms', 'Admin menu name', 'yith-quick-order-forms-for-woocommerce' ),
				'all_items'          => __( 'All YITH Forms', 'yith-quick-order-forms-for-woocommerce' ),
			);

			$quick_order_forms_args = array(
				'label'               => __( 'List of the product forms', 'yith-quick-order-forms-for-woocommerce' ),
				'labels'              => $labels,
				'description'         => __( 'This is where the list of products is stored.', 'yith-quick-order-forms-for-woocommerce' ),
				//'public'              => true,
				'show_ui'             => true,
				'capability_type'     => 'post',
				'map_meta_cap'        => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'show_in_menu'        => false,
				'hierarchical'        => false,
				'show_in_nav_menus'   => false,
				'rewrite'             => false,
				'query_var'           => false,
				'supports'            => array( 'title', 'Date' ),
				'has_archive'         => false,
				'menu_icon'           => 'dashicons-edit',
			);

			register_post_type( self::$CPT_quick_order_form_id, apply_filters( 'yith_wc_quick_order_forms_register_post_type', $quick_order_forms_args ) );

		}

		public function yith_wc_quick_order_form_set_columns( $columns ) {

			$newColumns            = array();
			$newColumns['title']   = 'Full Name';
			$newColumns['message'] = 'Message';
			$newColumns['email']   = 'Email';
			$newColumns['date']    = 'Date';

			return $newColumns;
		}

		public function yith_wc_quick_order_form_custom_column( $column, $post_id ) {

			switch ( $column ) {

				case 'message' :
					echo get_the_excerpt();
					break;

				case 'email' :
					//email column
					echo 'email address';
					break;
			}

		}

		/* ========================================================= */
		/* ==================== META BOX SHORTCODE ================= */
		public function yith_wc_quick_order_form_add_meta_box_shortcode() {

			add_meta_box( 'yith_wc_quick_order_form_meta_boxes_shortcode_id', __( 'Shortcode and php code', 'yith-quick-order-forms-for-woocommerce' ), array(
				$this,
				'yith_wc_quick_order_form_meta_boxes_shortcode_callback'
			), self::$CPT_quick_order_form_id, 'side' );

		}

		/* ========================================================= */
		/* ===================== SHORTCODE NAME ==================== */
		public function yith_wc_quick_order_form_meta_boxes_shortcode_callback( $post ) {

			$Shortcode_Name = 'yith_wc_quick_order_form id="' . $post->ID . '"';

			echo "<p style='text-decoration: underline;'> Shortcode </p>";
			echo "<p>";
			echo "[" . $Shortcode_Name . "]";
			echo "</p>";

			echo "<p style='text-decoration: underline;'> PHP code </p>";
			echo "<p>";

			echo htmlspecialchars( "<?php yith_quick_order_form( " . $post->ID . " ); ?>" );
			//echo "yith_quick_order_form( " . $post->ID . " ) ";
			echo "</p>";


		}

		/* ========================================================= */
		/* ====================== FORM SETTINGS ==================== */
		public function yith_wc_quick_order_form_add_meta_box_seetings() {

			add_meta_box( 'yith_wc_quick_order_form_meta_boxes_settings_id', __( 'Form Settings', 'yith-quick-order-forms-for-woocommerce' ), array(
				$this,
				'yith_wc_quick_order_form_meta_boxes_settings_callback'
			), self::$CPT_quick_order_form_id, 'normal' );

		}

		public function yith_wc_quick_order_form_meta_boxes_settings_callback( $post ) {

			$args = array(
				'post' => $post,
			);
			wc_get_template( 'yith_cpt_forms_settings.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) . 'custom_post_types/' );

		}

		/* ========================================================= */
		/* ====================== PERMISSIONS ====================== */
		public function yith_wc_quick_order_form_add_meta_box_permissions() {

			add_meta_box( 'yith_wc_quick_order_form_meta_boxes_permissions_id', __( 'Form permissions', 'yith-quick-order-forms-for-woocommerce' ), array(
				$this,
				'yith_wc_quick_order_form_meta_boxes_permissions_callback'
			), self::$CPT_quick_order_form_id, 'normal' );

		}

		public function yith_wc_quick_order_form_meta_boxes_permissions_callback( $post ) {

			$args = array(
				'post' => $post,
			);
			wc_get_template( 'yith_cpt_forms_permissions.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) . 'custom_post_types/' );

		}

		/* ========================================================= */
		/* ====================== Products chosen ==================== */
		public function yith_wc_quick_order_form_add_meta_box_products_chosen() {

			add_meta_box( 'yith_wc_quick_order_form_meta_boxes_products_chosen_id', __( 'Products chosen', 'yith-quick-order-forms-for-woocommerce' ), array(
				$this,
				'yith_wc_quick_order_form_meta_boxes_products_chosen_callback'
			), self::$CPT_quick_order_form_id, 'normal' );

		}

		public function yith_wc_quick_order_form_meta_boxes_products_chosen_callback( $post ) {

			$args = array(
				'post' => $post,
			);
			wc_get_template( 'yith_cpt_forms_products_chosen.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) . 'custom_post_types/' );

		}

		public function yith_wc_quick_order_form_save_data( $post_id ) {

			if ( ! isset( $_POST['yith_wc_quick_order_form_meta_box_nonce'] ) ) {
				return;
			}

			if ( ! wp_verify_nonce( $_POST['yith_wc_quick_order_form_meta_box_nonce'], 'yith_wc_quick_order_form_data' ) ) {
				return;
			}

			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return;
			}

			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}

			/* =============== SAVING DATA ================= */

			/* ================ Description ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_description_textarea'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_description_textarea', $_POST['yith_wc_quick_order_form_description_textarea'] );
			}

			/* ================ Pagination ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_pagination_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_pagination_checkbox', true );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_pagination_checkbox', false );
			}

			if ( isset( $_POST['yith_wc_quick_order_form_Products_Per_Page_text'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_Products_Per_Page_text', $_POST['yith_wc_quick_order_form_Products_Per_Page_text'] );
			}

			/* ================ SKU ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_SKU_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_SKU_checkbox', true );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_SKU_checkbox', false );
			}

			/* ================ Thumbnail ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_thumbnail_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_thumbnail_checkbox', true );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_thumbnail_checkbox', false );
			}

			/* ================ Price ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_price_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_price_checkbox', true );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_price_checkbox', false );
			}

			/* ================ Discount ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_discount_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_discount_checkbox', true );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_discount_checkbox', false );
			}

			/* ================ SHOW OR NOT SHOW SEARCH FILTER ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_search_filter_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_search_filter_checkbox', '1' );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_search_filter_checkbox', '0' );
			}

			/* ================ SHOW OR NOT SHOW NUMBER OF PRODUCTS ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_number_of_products_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_number_of_products_checkbox', '1' );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_number_of_products_checkbox', '0' );
			}

			/* ================ SHOW OR NOT SHOW STOCK ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_stock_checkbox'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_stock_checkbox', '1' );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_stock_checkbox', '0' );
			}

            /* ================ SHOW OR NOT QUANTITY ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_quantity_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_quantity_checkbox', '1' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_quantity_checkbox', '0' );
            }

			/* ================ SHOW OR NOT VARIABLE PRICE ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_variable_price_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_variable_price_checkbox', '1' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_variable_price_checkbox', '0' );
            }

            /* ================ SHOW OR NOT SHOW QUANTITY ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_quantity_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_quantity_checkbox', '1' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_quantity_checkbox', '0' );
            }

            if ( isset( $_POST['yith_wc_quick_order_form_default_quantity_text'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_default_quantity_text', $_POST['yith_wc_quick_order_form_default_quantity_text'] );
            }

            /* ================ SHOW OR NOT SHOW INDIVIDUAL ADD TO CART ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_individual_add_to_cart_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_individual_add_to_cart_checkbox', 'yes' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_individual_add_to_cart_checkbox', 'no' );
            }

            /* ================ SHOW OR NOT SHOW ADD TO CART ALL PRODUCTS AT THE TOP ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_all_add_to_cart_top_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_all_add_to_cart_top_checkbox', 'yes' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_all_add_to_cart_top_checkbox', 'no' );
            }

            /* ================ SHOW OR NOT SHOW ADD TO CART ALL PRODUCTS AT THE BOTTOM ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox', 'yes' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_all_add_to_cart_bottom_checkbox', 'no ' );
            }

            /* ================ Choose the default sorting ================ */
            if ( isset( $_POST['yith_wc_quick_order_form_Sorting_Select'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_Sorting_Select', $_POST['yith_wc_quick_order_form_Sorting_Select'] );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_form_Sorting_Select', 'Default' );
            }

			/* ============================================================ */
			/* ================ ENABLE DISABLE USERS ROLES ================ */
			/* ============================================================ */

            /* ================ ALLOW ALL USERS ================ */
            if ( isset( $_POST['yith_wc_quick_order_all_users_checkbox'] ) ) {
                update_post_meta( $post_id, 'yith_wc_quick_order_all_users_checkbox', '1' );
            } else {
                update_post_meta( $post_id, 'yith_wc_quick_order_all_users_checkbox', '0' );
            }

			/* ================ CHECK WHAT USER ROLES LIST OPTION IS BEEN CHOSEN ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_user_roles_list_radio_button'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_user_roles_list_radio_button', $_POST['yith_wc_quick_order_form_user_roles_list_radio_button'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_user_roles_list_radio_button', '' );
			}


			/* ================ ENABLE DISABLE ROLES SELECTED ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple', $_POST['yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_enable_disable_roles_selected_select_multiple', '' );
			}

			/* ============================================================ */

			/* ============================================================ */
			/* ================ ENABLE DISABLE USERS ACCESS =============== */
			/* ============================================================ */

			/* ================ CHECK WHAT USER LIST OPTION IS BEEN CHOSEN ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_users_list_radio_button'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_users_list_radio_button', $_POST['yith_wc_quick_order_form_users_list_radio_button'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_users_list_radio_button', '' );
			}

			/* ================ ENABLE DISABLE CUSTOMERS SELECTED ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple', $_POST['yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_enable_disable_customers_selected_select_multiple', '' );
			}

			/* ============================================================ */

			/* ================ MESSAGE VISIBILITY RESTRICTED ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_visibility_restricted_textarea'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_visibility_restricted_textarea', $_POST['yith_wc_quick_order_form_visibility_restricted_textarea'] );
			}

			/* ================ CHECK IF ALL THE PRODUCTS HAS BEEN CHOSEN ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_products_chosen_radio_button'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_products_chosen_radio_button', $_POST['yith_wc_quick_order_form_products_chosen_radio_button'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_products_chosen_radio_button', '' );
			}

			/* ================ PRODUCTS ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_products_selected_select_multiple'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_products_selected_select_multiple', $_POST['yith_wc_quick_order_form_products_selected_select_multiple'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_products_selected_select_multiple', '' );
			}

			/* ================ CHECK IF ALL THE CATEGORIES HAS BEEN CHOSEN ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_categories_chosen_radio_button'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_categories_chosen_radio_button', $_POST['yith_wc_quick_order_form_categories_chosen_radio_button'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_categories_chosen_radio_button', '' );
			}

			/* ================ CATEGORIES ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_categories_selected_select_multiple'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_categories_selected_select_multiple', $_POST['yith_wc_quick_order_form_categories_selected_select_multiple'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_categories_selected_select_multiple', '' );
			}

			/* ================ CHECK IF ALL THE TAGS HAS BEEN CHOSEN ================ */

			if ( isset( $_POST['yith_wc_quick_order_form_tags_chosen_radio_button'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_tags_chosen_radio_button', $_POST['yith_wc_quick_order_form_tags_chosen_radio_button'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_tags_chosen_radio_button', '' );
			}

			/* ================ TAGS ================ */
			if ( isset( $_POST['yith_wc_quick_order_form_tags_selected_select_multiple'] ) ) {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_tags_selected_select_multiple', $_POST['yith_wc_quick_order_form_tags_selected_select_multiple'] );
			} else {
				update_post_meta( $post_id, 'yith_wc_quick_order_form_tags_selected_select_multiple', '' );
			}

		}

	}

}
