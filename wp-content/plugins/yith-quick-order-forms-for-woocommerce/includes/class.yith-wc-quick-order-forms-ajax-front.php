<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_FAVORITOS_Class
 * @package    Yithemes
 * @since      Version 2.0.0
 * @author     Your Inspiration Themes
 *
 */

if ( ! class_exists( 'YITH_WC_Quick_Order_Forms_Ajax_Front' ) ) {
	/**
	 * Class YITH_WC_FAVORITOS_Class
	 *
	 * @author Danie Sanchez Saez <dssaez@gmail.com>
	 */
	class YITH_WC_Quick_Order_Forms_Ajax_Front {
		/**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0
		 */
		public $version = YITH_WC_QUICK_ORDER_FORMS_VERSION;

		/**
		 * Main Instance
		 *
		 * @var YITH_WC_FAVORITOS_Ajax_Front
		 * @since  1.0
		 * @access protected
		 */
		protected static $_instance = null;

		protected $favorites_ddbb;

		public function __construct() {

			add_action( 'wp_ajax_nopriv_Yith_wc_qof_Ajax_Front_change_variation', array(
				$this,
				'Yith_wc_qof_Ajax_Front_change_variation'
			), 10 );
			add_action( 'wp_ajax_Yith_wc_qof_Ajax_Front_change_variation', array(
				$this,
				'Yith_wc_qof_Ajax_Front_change_variation'
			), 10 );

			add_action( 'wp_ajax_nopriv_Yith_wc_qof_Ajax_Front_loading_javascript', array(
				$this,
				'Yith_wc_qof_Ajax_Front_loading_javascript'
			), 10 );
			add_action( 'wp_ajax_Yith_wc_qof_Ajax_Front_loading_javascript', array(
				$this,
				'Yith_wc_qof_Ajax_Front_loading_javascript'
			), 10 );

		}

		public function Yith_wc_qof_Ajax_Front_loading_javascript() {

			?>

			<script src="<?php echo constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ); ?>js/yith-qof-frontend-ajax.js"></script>

			<?php

			die();

		}

		public function enqueue_scripts() {

			/* ====== Dashicons ====== */

			wp_enqueue_style( 'dashicons' );

			/* ====== Style ====== */

			wp_register_style( 'yith-wc-quick-order-forms-style', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'css/yith-qof-frontend.css', array(), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) );
			wp_enqueue_style( 'yith-wc-quick-order-forms-style' );

			/* ====== Scripts ====== */

			wp_register_script( 'yith-wc-quick-order-forms-js', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-frontend.js', array(
				'jquery',
				'jquery-ui-sortable'
			), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

			wp_localize_script( 'yith-wc-quick-order-forms-js', 'yith_wc_quick_order_forms_object', apply_filters( 'yith_wc_qof_frontend_localize', array(
				'ajax_url'    => admin_url( 'admin-ajax.php' ),
				'ajax_loader' => constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . '/images/ajax-loader.gif',
			) ) );

			wp_enqueue_script( 'yith-wc-quick-order-forms-js' );

			wp_register_script( 'yith-wc-quick-order-forms-responsive-js', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-responsive.js', array(
				'jquery',
				'jquery-ui-sortable'
			), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

			wp_enqueue_script( 'yith-wc-quick-order-forms-responsive-js' );

		}

        public function Yith_wc_qof_Ajax_Front_change_variation() {

            $Product_ID    = $_POST['Product_ID'];
            $Yith_Post_ID  = $_POST['Yith_Post_ID'];
            $data_selected = $_POST['data_selected'];

            $data_selected_aux = array();
            foreach ( $data_selected as $attribute_name => $attribute_value ) {

                $data_selected[ $attribute_name ] = wp_unslash( $attribute_value );

                $attribute_name = str_replace( 'attribute_', '', $attribute_name );
                $attribute_name = str_replace( 'pa_', '', $attribute_name );

                $attribute_name = strtolower( $attribute_name );

                $attribute_value = strtolower( $attribute_value );

                $data_selected_aux[ $attribute_name ] = wp_unslash( $attribute_value );

            }

            $_product = wc_get_product( $Product_ID );

            $childrens = $_product->get_children();

            $variation_Found = false;

            foreach ( $childrens as $child ) {

                $variation = wc_get_product( $child );

                $Total_Found          = 0;
                $Number_of_Attributes = count( $variation->get_variation_attributes() );

                foreach ( $variation->get_variation_attributes() as $attribute_name => $attribute_value ) {
                    $array_attribute_values = array();

                    $array_attribute_values[ strtolower( $attribute_value ) ] = strtolower( $attribute_value );

                    $meta = get_post_meta( $child, $attribute_name, true);

                    $attribute_name = str_replace( 'attribute_', '', $attribute_name );
                    $term = get_term_by( 'slug', $meta, $attribute_name );

                    if ( $term instanceof WP_Term ){

												$name = wp_specialchars_decode( $term->name );
												$slug = wp_specialchars_decode( $term->slug );

                        $array_attribute_values[ strtolower( $name ) ] = strtolower( $name );
                        $array_attribute_values[ strtolower( $slug ) ] = strtolower( $slug );
                    }

                    $attribute_name = str_replace( 'pa_', '', $attribute_name );

                    $attribute_name = strtolower( $attribute_name );

                    if ( in_array( $data_selected_aux[ $attribute_name ], $array_attribute_values ) ) {
                        $Total_Found = $Total_Found + 1;
                    }

                }

                if ( $Total_Found == $Number_of_Attributes ) {
                    $variation_Found = true;
                    break;
                }

            }

            $args = array(
                'Yith_Post_ID'      => $Yith_Post_ID,
                'Parent_Product_ID' => $Product_ID,
                'variation_Found'   => $variation_Found,
                'variation'         => $variation,
                '_product'          => $_product,
                'data_selected'     => $data_selected,
            );

            wc_get_template( 'custom_post_types/yith_show_product_line.php', $args, '', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH' ) );

            wp_register_script( 'yith-wc-quick-order-forms-responsive-ajax-js', constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-responsive.js', array(
                'jquery',
                'jquery-ui-sortable'
            ), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ), true );

            wp_enqueue_script( 'yith-wc-quick-order-forms-responsive-ajax-js' );

            $responsive_js = constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_ASSETS_URL' ) . 'js/yith-qof-responsive.js';

            ?>

            <script src="<?php echo $responsive_js; ?>"></script>

            <?php

            die();

        }

		public static function get_instance() {
			$self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

			if ( is_null( $self::$_instance ) ) {
				$self::$_instance = new $self;
			}

			return $self::$_instance;
		}

	}
}
