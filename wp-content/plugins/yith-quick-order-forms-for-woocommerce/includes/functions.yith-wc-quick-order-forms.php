<?php
/*
 * This file belongs to the YITh.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

function yith_quick_order_form( $id ) {
	echo do_shortcode( '[yith_wc_quick_order_form id="' . $id . '"]' );
}

function yith_wc_quick_order_form_show_products( $product, $Yith_Post_ID, $data_selected ){

    $_product = $product;

    if ( ! $_product->is_type( 'auction' ) ) {

	$Parent_Product_ID = $_product->get_id();

	$empty_children = false;

	// Prevent orphan variations
	$P = get_post(wp_get_post_parent_id($Parent_Product_ID));
	if ($P != null) {

	    $Child_Product_ID = $Parent_Product_ID;
	    $_variation_product = null;

	    if ($_product->is_type('variable')) {

		$childrens = $_product->get_children();

		// Prevent variable products with no children
		if (empty($childrens)) {
		    $empty_children = true;
		} else {

		    $Child_Product_ID = $childrens[0];
		    $_variation_product = wc_get_product($Child_Product_ID);

		    $title = $_variation_product->get_title();
		    $sku = $_variation_product->get_sku();

		    foreach ($_variation_product->get_variation_attributes() as $attribute_name => $attribute_value) {

			$attribute_name = str_replace('attribute_', '', $attribute_name);
			$attribute_name = str_replace('pa_', '', $attribute_name);

			$data_selected[$attribute_name] = $attribute_value;

		    }
		}

	    } else {
		$title = $_product->get_title();
		$sku = $_product->get_sku();
	    }

	    if (!$empty_children) {

		$ID = 'Yith_WC_QOF_Product_Line_Ajax_Result_' . $Parent_Product_ID . '_' . $Yith_Post_ID;
		echo "<div class='YITH_WC_qof_product_line' id='$ID'>";

		$args = array(
		    'Yith_Post_ID' => $Yith_Post_ID,
		    'Parent_Product_ID' => $Parent_Product_ID,
		    'Child_Product_ID' => $Child_Product_ID,
		    '_product' => $_product,
		    'title' => $title,
		    'sku' => $sku,
		    '_variation_product' => $_variation_product,
		    'data_selected' => $data_selected,
		);
		wc_get_template( 'custom_post_types/yith_show_product_line.php', $args, '', constant('YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_TEMPLATE_PATH') );

		echo "</div>";
	    }

	}

    }

}