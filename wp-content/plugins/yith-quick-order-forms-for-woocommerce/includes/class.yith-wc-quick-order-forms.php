<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_QUICK_ORDER_FORMS_Class
 * @package    Yithemes
 * @since      Version 2.0.0
 * @author     Your Inspiration Themes
 *
 */

if ( ! class_exists( 'YITH_WC_Quick_Order_Forms_Main_Class' ) ) {
	/**
	 * Class YITH_WC_Quick_Order_Main_Class
	 *
	 * @author Daniel Sánchez Sáez <dssaez@gmail.com>
	 */
	class YITH_WC_Quick_Order_Forms_Main_Class {
		/**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0
		 */
		public $version = YITH_WC_QUICK_ORDER_FORMS_VERSION;

		/**
		 * Main Instance
		 *
		 * @var YITH_WC_QUICK_ORDER_FORMS_Class
		 * @since  1.0
		 * @access protected
		 */
		protected static $_instance = null;

		/**
		 * Main Admin Instance
		 *
		 * @var YITH_QUICK_ORDER_FORMS_Admin
		 * @since 1.0
		 */
		public $admin = null;

		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_QUICK_ORDER_FORMS_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main common Instance
		 *
		 * @var YITH_QUICK_ORDER_FORMS_Common
		 * @since 1.0
		 */
		public $common = null;

		/**
		 * check if the plugin is activated or not
		 *
		 * @var bool
		 * @since 1.3.6
		 */
		public $is_plugin_enabled = true;


		/**
		 * Construct
		 *
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 * @since  1.0
		 */

		public $ajax = null;

		public function __construct() {

			/* === Require Main Files === */
			$require = apply_filters( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_require_class',
				array(
					'common'   => array(
						'includes/functions.yith-wc-' . YITH_WC_QOF_FILES_INCLUDE_NAME . '.php',
					),
					'admin'    => array(
						'includes/class.yith-wc-' . YITH_WC_QOF_FILES_INCLUDE_NAME . '-admin.php',
					),
					'frontend' => array(
						'includes/class.yith-wc-' . YITH_WC_QOF_FILES_INCLUDE_NAME . '-frontend.php',
					),
				)
			);

			$this->_require( $require );


			/* === Load Plugin Framework === */
			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_filter( 'body_class', array( $this, 'body_class' ) );

			/* == Plugins Init === */
			add_action( 'init', array( $this, 'init' ) );

			// register plugin to licence/update system
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

		}

		/**
		 * Register plugins for activation tab
		 *
		 * @return void
		 * @since 1.0.0
		 */
		public function register_plugin_for_activation() {
			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once 'plugin-fw/licence/lib/yit-licence.php';
				require_once 'plugin-fw/licence/lib/yit-plugin-licence.php';
			}

			YIT_Plugin_Licence()->register( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_INIT' ), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SECRETKEY' ), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ) );

		}

		/**
		 * Register plugins for update tab
		 *
		 * @return void
		 * @since 2.0.0
		 */
		public function register_plugin_for_updates() {

			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . 'plugin-fw/lib/yit-upgrade.php' );
			}

			YIT_Upgrade()->register( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_SLUG' ), constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_INIT' ) );
		}

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_Quick_Order_Forms Main instance
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {

					if ( file_exists( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . $class ) )
						switch ( $section ) {

							case 'common':
								require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . $class );
								break;

                            case 'frontend':
                                // We check is ajax is being runing by admin and see if the variable session is set to frontend
                                if ( is_admin() && ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
                                    require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . $class );
                                } elseif ( ! is_admin() ) {
                                    require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . $class );
                                }

                                break;

                            case 'admin':
                                // We check is ajax is being running by admin and see if the variable session is set to backend
                                if ( is_admin() ) {
                                    require_once( constant( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_PATH' ) . $class );
                                }

                                break;

						}

				}
			}
		}

		/**
		 * Load plugin framework
		 *
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 * @since  1.0
		 * @return void
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once( $plugin_fw_file );
				}
			}
		}

		/**
		 * Class Initializzation
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {

            if ( is_admin() ) {

                $this->admin = new YITH_WC_Quick_Order_Forms_Admin();

                if ( ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {

                    $this->frontend = new YITH_WC_Quick_Order_Forms_Frontend();

                }

            } else {

                $this->frontend = new YITH_WC_Quick_Order_Forms_Frontend();

            }

		}

		/**
		 * Add a body class(es)
		 *
		 * @param $classes The classes array
		 *
		 * @author Daniel Sanchez Saez <dssaez@gmail.com>
		 * @since  1.3.0
		 * @return array
		 */
		public function body_class( $classes ) {
			$classes[] = 'yith-wc-quick-order-forms';
			$classes[] = 'yes' == get_option( 'woocommerce_enable_checkout_login_reminder', 'yes' ) ? 'show_checkout_login_reminder' : 'hide_checkout_login_reminder';

			return $classes;
		}
	}
}