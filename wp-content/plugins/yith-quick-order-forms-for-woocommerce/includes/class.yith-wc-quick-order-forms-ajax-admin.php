<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_WC_' . YITH_WC_QOF_CONSTANT_NAME . '_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 *
 *
 * @class      YITH_WC_FAVORITOS_Class
 * @package    Yithemes
 * @since      Version 2.0.0
 * @author     Your Inspiration Themes
 *
 */

if ( ! class_exists( 'YITH_WC_Quick_Order_Forms_Ajax_Admin' ) ) {
	/**
	 * Class YITH_WC_FAVORITOS_Class
	 *
	 * @author Danie Sanchez Saez <dssaez@gmail.com>
	 */
	class YITH_WC_Quick_Order_Forms_Ajax_Admin {
		/**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0
		 */
		public $version = YITH_WC_QUICK_ORDER_FORMS_VERSION;

		/**
		 * Main Instance
		 *
		 * @var YITH_WC_Quick_Order_Forms_Ajax_Admin
		 * @since  1.0
		 * @access protected
		 */
		protected static $_instance = null;

		public function __construct() {

			add_action( 'wp_ajax_yith_wc_qof_ajax_search_categories', array(
				$this,
				'yith_wc_qof_ajax_search_categories'
			) );
			add_action( 'wp_ajax_yith_wc_qof_ajax_search_tags', array( $this, 'yith_wc_qof_ajax_search_tags' ) );
			add_action( 'wp_ajax_yith_wc_qof_ajax_search_roles', array( $this, 'yith_wc_qof_ajax_search_roles' ) );

		}

		public function yith_wc_qof_ajax_search_categories() {
			$this->yith_ajax_search_categories_and_tags( 'yith_wc_qof_category_search', 'product_cat' );
		}

		public function yith_wc_qof_ajax_search_tags() {
			$this->yith_ajax_search_categories_and_tags( 'yith_wc_qof_tag_search', 'product_tag' );
		}

		public function yith_ajax_search_categories_and_tags( $nonce, $type ) {

			check_ajax_referer( $nonce, 'security' );

			ob_start();

			if ( version_compare( WC()->version, '3.0', '<' ) ) {
				$term = (string) wc_clean( stripslashes( $_GET['term'] ) );
			} else {
				$term = (string) wc_clean( stripslashes( $_GET['term']['term'] ) );
			}

			if ( empty( $term ) ) {
				die();
			}
			global $wpdb;
			$terms = $wpdb->get_results( 'SELECT name, slug, wpt.term_id FROM ' . $wpdb->prefix . 'terms wpt, ' . $wpdb->prefix . 'term_taxonomy wptt WHERE wpt.term_id = wptt.term_id AND wptt.taxonomy = "' . $type . '" and wpt.name LIKE "%' . $term . '%" ORDER BY name ASC;' );

			$found_categories = array();

			if ( $terms ) {
				foreach ( $terms as $cat ) {
					$found_categories[ $cat->term_id ] = ( $cat->name ) ? $cat->name : 'ID: ' . $cat->slug;
				}
			}

			$found_categories = apply_filters( 'ywarc_json_search_categories_and_tags', $found_categories );
			wp_send_json( $found_categories );

			die();

		}

		public function yith_wc_qof_ajax_search_roles() {

			check_ajax_referer( 'yith_wc_qof_role_search', 'security' );

			ob_start();

			if ( version_compare( WC()->version, '3.0', '<' ) ) {
				$term = (string) wc_clean( stripslashes( $_GET['term'] ) );
			} else {
				$term = (string) wc_clean( stripslashes( $_GET['term']['term'] ) );
			}

			if ( empty( $term ) ) {
				die();
			}

			global $wp_roles;
			$roles = $wp_roles->roles;

			$found_roles = array();

			foreach ( $roles as $role ) {
				if ( strpos( strtolower( $role['name'] ), strtolower( $term ) ) !== false ) {
					$found_roles[ $role['name'] ] = $role['name'];
				}
			}

			$found_roles = apply_filters( 'ywarc_json_search_roles_and_tags', $found_roles );
			wp_send_json( $found_roles );

			die();
		}

		public static function get_instance() {
			$self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

			if ( is_null( $self::$_instance ) ) {
				$self::$_instance = new $self;
			}

			return $self::$_instance;
		}

	}
}