== YITH WooCommerce Quick Order Forms ==

Contributors: yithemes
Tags: quick order forms, woocommerce, products, themes, yit, e-commerce, shop
Requires at least: 4.0
Tested up to: 4.9.x
Stable tag: 1.0.32
Licence: GPLv2 or later
Licence URI: http://www.gnu.org/licences/gpl-2.0.html
Documentation: https://docs.hitoutlets.com/yith-quick-order-forms-for-woocommerce/

== Changelog ==

= Version 1.0.32 - Released: Sep 05, 2018 =

  * Tweak: Filter by sku and category or tag
  * Tweak: action links
  * Tweak: improvement of templates for customizatio
  * Update: Italian language
  * Fix: show variation with html special characters
  * Fix: changing variation on the form
  * Dev: unlash the variation selected
  * Dev: cleaning logs

= Version 1.0.31 - Released: Jun 14, 2018 =

  * Tweak: Adding some css to adapt the quantity button for some themes
  * Tweak: Allowing 0 for default quantity
  * Update: documentation link of the plugin
  * Update: Spanish translation
  * Updated Dutch translation
  * Fix: change variation on the form

= Version 1.0.30 - Released: Apr 3, 2018 =

  * Tweak - Adding field to choose default quantity
  * Tweak - Adding filed to choose whether to show single add to cart or add all products to cart
  * Fix - Show forms for user roles
  * Dev - Path of the templates
  * Dev - Loading the admin and front without PHP sessions

= Version 1.0.29 - Released: Mar 06, 2018 =

  * Fix: Allow all users to see the form
  * Tweak: Setting the sorting on the form configuration

= Version 1.0.28 - Released: Feb 08, 2018 =

  * New - support to WooCommerce 3.3.0
  * Update - Spanish translation

= Version 1.0.27 - Released: Feb 06, 2018 =

  * Tweak - Adding code to translate some strings
  * Fix - Showing capital letters of attributes
  * Fix - Showing variation correctly through ajax

= Version 1.0.26 - Released: Jan 19, 2018 =

  * Tweak -  Adding option to show or not show price of variable products
  * Update - Italian translation
  * Update - Dutch translation
  * Fix - Show attributes
  * Remove - Cleaning some logs commented

= Version 1.0.25 - Released: Jan 05, 2017 =

 * Tweak - Way of displaying the attribute name
 * Update - Dutch translation
 * Update - Italian translation
 * Fix - Capitalization of the name under YITH Plugins
 * Remove - Select one option for variation

= Version 1.0.24 - Released: Dec 29, 2017 =

 * Update - Plugin-fw

= Version 1.0.23 - Released: Nov 23, 2017 =

 * Added - Apply filters to js and css files

= Version 1.0.22 - Released: Nov 03, 2017 =

 * Added - Option to show or not show quantity

= Version 1.0.21 - Released: Oct 30, 2017 =

 * Fix - Cleaning PHP Notices

= Version 1.0.20 - Released: Oct 20, 2017 =

 * Fix - Multiple forms in the same page

= Version 1.0.19 - Released: Oct 19, 2017 =

 * Add - Showing variation products of the same parent in order as in the variation product admin panel

= Version 1.0.18 - Released: Oct 16, 2017 =

 * Fix - Compatibility with WooCommerce 2.6.14 when wc_get_stock_html and show products chosen

= Version 1.0.17 - Released: Oct 13, 2017 =

 * Fix - Showing products of tags or categories selected

= Version 1.0.16 - Released: Oct 5, 2017 =

 * Update - Adding backorders functionality when adding products to the cart

= Version 1.0.15 - Released: Oct 2, 2017 =

 * Fix - Displaying variation products order by ID DESC
 * Update - Displaying categories with subcategories in the filter
 * Update - Adding two buttons "plus" and "minus" to change the amount

= Version 1.0.14 - Released: Sep 29, 2017 =

 * Fix - Searching for products in a disable group

= Version 1.0.13 - Released: Sep 29, 2017 =

 * Update - Changing order of the select variation products

= Version 1.0.12 - Released: Sep 28, 2017 =

 * Update - Add products to cart depending on the stock

= Version 1.0.11 - Released: Sep 22, 2017 =

 * Fix - Pagination
 * Fix - Setting options

= Version 1.0.10 - Released: Sep 21, 2017 =

 * Fix - License activation

= Version 1.0.9 - Released: Sep 20, 2017 =

 * Added - Auction products not showed in forms

= Version 1.0.8 - Released: Sep 18, 2017 =

 * Added - Option to show or not show stock

= Version 1.0.7 - Released: Sep 08, 2017 =

 * Update - Multiple forms in the same page

= Version 1.0.6 - Released: Sep 06, 2017 =

 * Update - Option to show or not show the search filter
 * Update - Option to show or not show the number of products

= Version 1.0.5 - Released: Sep 01, 2017 =

 * Added - language file
 * Fix  -  load text domain

= Version 1.0.4 - Released: Aug 24, 2017 =

 * Update - Ajax selector for variation products
 * Update - Showing only parent products and not the variations in the main form. Showing all the products (variables and variations) through the search filter
 * Fix - Select the attribute of the product to change the variation

= Version 1.0.3 - Released: Aug 22, 2017 =

 * Fix - Preventing from orphan children and variable products without children

= Version 1.0.2 - Released: Aug 17, 2017 =

 * Fix - search by SKU for variable products and show the image parent if they don't have it

= Version 1.0.1 - Released: Aug 09, 2017 =

 * New - Compatibility with plugin "Customize My Account Page"
 * Fix - Pagination, search filter, parent images for variable products on my "account page"

= Version 1.0.0 - Released: Jul 10, 2017 =

* First release

== Suggestions ==

If you have suggestions about how to improve YITH Quick Order Forms for WooCommerce Premium, you can [write us](mailto:plugins@hitoutlets.com "Your Inspiration Themes") so we can bundle them into the next release of the plugin.

== Translators ==

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://hitoutlets.com/contact/ "Your Inspiration Themes") so we can bundle it into YITH Quick Order Forms for WooCommerce Premium.
 = Available Languages =
 * English
