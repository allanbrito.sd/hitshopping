function yith_wc_qof_addOnLoad(newOnLoad) {
    var prevOnload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = newOnLoad;
    }
    else {
        window.onload = function () {
            prevOnload();
            newOnLoad();
        }
    }
}