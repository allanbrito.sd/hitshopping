jQuery(function ($) {

    function yith_wc_qof_responsive_css() {

        var width = $('body #Yith_WC_QOF_Main_Product_Div').width();

        if (width < 597) {
            $(".YITH_WC_qof_product_line").removeClass("YITH_WC_qof_product_line_media_1");
            $(".YITH_WC_qof_product_line_image").removeClass("YITH_WC_qof_product_line_image_media_1");
            $(".YITH_WC_qof_product_line_name_and_SKU").removeClass("YITH_WC_qof_product_line_name_and_SKU_media_1");
            $(".YITH_WC_qof_product_line_name").removeClass("YITH_WC_qof_product_line_name_media_1");
            $(".YITH_WC_qof_product_line_name_SKU").removeClass("YITH_WC_qof_product_line_name_SKU_media_1");
            $(".yith_wc_qof_button_and_price").removeClass("yith_wc_qof_button_and_price_media_1");
            $(".YITH_WC_qof_product_line_Price").removeClass("YITH_WC_qof_product_line_Price_media_1");
        }

        if (width >= 597) {
            $(".YITH_WC_qof_product_line").addClass("YITH_WC_qof_product_line_media_1");
            $(".YITH_WC_qof_product_line_image").addClass("YITH_WC_qof_product_line_image_media_1");
            $(".YITH_WC_qof_product_line_name_and_SKU").addClass("YITH_WC_qof_product_line_name_and_SKU_media_1");
            $(".YITH_WC_qof_product_line_name").addClass("YITH_WC_qof_product_line_name_media_1");
            $(".YITH_WC_qof_product_line_name_SKU").addClass("YITH_WC_qof_product_line_name_SKU_media_1");
            $(".YITH_WC_qof_product_line_Price").addClass("YITH_WC_qof_product_line_Price_media_1");
            $(".yith_wc_qof_button_and_price").addClass("yith_wc_qof_button_and_price_media_1");
        }

        if (width < 790) {
            $(".yith_wc_qof_button_and_price").removeClass("yith_wc_qof_button_and_price_media2");
            $(".YITH_WC_qof_product_header div").removeClass("YITH_WC_qof_product_header_media_2_div");
            $(".YITH_WC_qof_product_line_image_header").removeClass("YITH_WC_qof_product_line_image_header_media_2");
            $(".YITH_WC_qof_product_line_name_header").removeClass("YITH_WC_qof_product_line_name_header_media_2");
            $(".YITH_WC_qof_product_line_name_SKU_header").removeClass("YITH_WC_qof_product_line_name_SKU_header_media_2");
            $(".YITH_WC_qof_product_line_Price_header").removeClass("YITH_WC_qof_product_line_Precio_header_media_2");
            $(".YITH_WC_qof_product_line > div").removeClass("YITH_WC_qof_product_line_media_2__div");
            $(".YITH_WC_qof_product_line_name").removeClass("YITH_WC_qof_product_line_name_media_2");
            $(".YITH_WC_qof_product_line_name_SKU").removeClass("YITH_WC_qof_product_line_name_SKU_media_2");
            $(".YITH_WC_qof_product_line_Price").removeClass("YITH_WC_qof_product_line_Price_media_2");
            $(".YITH_WC_qof_product_header .YITH_WC_qof_products_found").removeClass("YITH_WC_qof_products_found_media_2");
            $(".YITH_WC_qof_product_line").removeClass("YITH_WC_qof_product_line_media_2");
            $(".Yith_WC_QOF_Main_Product_Div_Class").removeClass("Yith_WC_QOF_Main_Product_Div_Class_media_2");
        }

        if (width >= 790) {
            $(".yith_wc_qof_button_and_price").addClass("yith_wc_qof_button_and_price_media2");
            $(".YITH_WC_qof_product_header div").addClass("YITH_WC_qof_product_header_media_2_div");
            $(".YITH_WC_qof_product_line_image_header").addClass("YITH_WC_qof_product_line_image_header_media_2");
            $(".YITH_WC_qof_product_line_name_header").addClass("YITH_WC_qof_product_line_name_header_media_2");
            $(".YITH_WC_qof_product_line_name_SKU_header").addClass("YITH_WC_qof_product_line_name_SKU_header_media_2");
            $(".YITH_WC_qof_product_line_Price_header").addClass("YITH_WC_qof_product_line_Precio_header_media_2");
            $(".YITH_WC_qof_product_line > div").addClass("YITH_WC_qof_product_line_media_2__div");
            $(".YITH_WC_qof_product_line_name").addClass("YITH_WC_qof_product_line_name_media_2");
            $(".YITH_WC_qof_product_line_name_SKU").addClass("YITH_WC_qof_product_line_name_SKU_media_2");
            $(".YITH_WC_qof_product_line_Price").addClass("YITH_WC_qof_product_line_Price_media_2");
            $(".YITH_WC_qof_product_header .YITH_WC_qof_products_found").addClass("YITH_WC_qof_products_found_media_2");
            $(".YITH_WC_qof_product_line").addClass("YITH_WC_qof_product_line_media_2");
            $(".Yith_WC_QOF_Main_Product_Div_Class").addClass("Yith_WC_QOF_Main_Product_Div_Class_media_2");

        }
    }

    $(window).resize(function () {
        yith_wc_qof_responsive_css();
    });

    yith_wc_qof_responsive_css();

});