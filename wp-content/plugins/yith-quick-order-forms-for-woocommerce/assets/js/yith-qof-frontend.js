jQuery(function ($) {

    /* ======== Adding form variables to the pagination when click in the page chosen ======== */
    $( "#Yith_WC_QOF_Main_Product_Div" ).on( "click", "#Yith_WC_QOF_Frontend_Pagination_ID a", function (e) {

        // == Dividing the url to get only the first part where the page number is
        var array_href = $(this).attr( 'href' ).split( "?" );

        var href = array_href[0];

        var array_href = href.split( "/" );

        // == Removing blank in case there is a "/" at the end of the url
        if ( array_href[array_href.length - 1] == '' )
            array_href.pop();

        // == Getting the page number and removing from the url
        if ( $.isNumeric( array_href[array_href.length - 1] ) ){
            var yithpage = array_href[array_href.length - 1];
            array_href.pop();
        }

        // == Removing the string "page" from the url
        if ( array_href[array_href.length - 1] == 'page' )
            array_href.pop();

        // == Getting back the url after removing
        href = array_href.join( '/' );

        var yith_wc_qof_name = $( "#Yith_wc_qof_Form_filtering_search_ID input:text[name='yith_wc_qof_name']" ).val();

        var yith_wc_qof_sku = $( "#Yith_wc_qof_Form_filtering_search_ID input:text[name='yith_wc_qof_sku']" ).val();

        var Yith_wc_qof_Sorting = $( "#Yith_wc_qof_Form_filtering_search_ID select#Yith_wc_qof_Form_sorting_search_select_ID option:checked" ).val();

        var Yith_wc_qof_Categories_search = $( "#Yith_wc_qof_Form_filtering_search_ID select#Yith_wc_qof_Form_categories_search_select_ID option:checked" ).val();

        var Yith_wc_qof_tags_search = $( "#Yith_wc_qof_Form_filtering_search_ID select#Yith_wc_qof_Form_tags_search_select_ID option:checked" ).val();

        $( this ).attr( "href", href + "?yithpage=" + yithpage + "&yith_wc_qof_name=" + yith_wc_qof_name + "&Yith_wc_qof_Sorting=" + Yith_wc_qof_Sorting + "&Yith_wc_qof_Categories_search=" + Yith_wc_qof_Categories_search + "&Yith_wc_qof_tags_search=" + Yith_wc_qof_tags_search + "&yith_wc_qof_sku=" + yith_wc_qof_sku );

    });

    $( "body" ).on( "click", ".YITH_WC_qof_Button_Add_to_cart_trigger_all", function () {

        $( '.Yith_WC_QOF_Main_Product_Div_Class .YITH_WC_qof_Button_Add_to_cart_trigger' ).each( function () {

            $( this ).click();

        });

    });

    /* ======== Adding the mark to product already in the cart ======== */

    $( ".Yith_WC_QOF_Main_Product_Div_Class" ).on( "click", ".YITH_WC_qof_Button_Add_to_cart_trigger", function () {

        var Product_ID = $( this ).data( 'product_id' );
        var Yith_Post_ID = $( this ).data( 'yith_post_id' );
        var default_quantity = $( this ).data( 'default_quantity' );

        if ( $( "#yith_wc_qof_input_number_product_" + Product_ID + '_' + Yith_Post_ID ).val() != 0 ){

            var Quantity_in_Cart = parseInt ( $( '.Yith_WC_QOF_Main_Product_Div_Class #yith_wc_qof_button_product_' + Product_ID + '_' + Yith_Post_ID ).data( "quantity" ) ) + parseInt( $( "#yith_wc_qof_button_product_" + Product_ID + '_' + Yith_Post_ID ).data( 'quantity_in_cart' ) );

            $( "#yith_wc_qof_button_product_" + Product_ID + '_' + Yith_Post_ID ).data( 'quantity_in_cart', Quantity_in_Cart );

            if ( parseInt( $( "#yith_wc_qof_input_number_product_" + Product_ID + '_' + Yith_Post_ID ).data( 'stock_quantity' ) ) - Quantity_in_Cart == 0 )
                $( "#yith_wc_qof_input_number_product_" + Product_ID + '_' + Yith_Post_ID ).val( 0 );
            else
                $( "#yith_wc_qof_input_number_product_" + Product_ID + '_' + Yith_Post_ID ).val( default_quantity );

            $( "#yith_wc_qof_button_product_" + Product_ID + '_' + Yith_Post_ID ).click();

            $( '#yith_wc_qof_dashicon_product_' + Product_ID + '_' + Yith_Post_ID ).css( "display", "inline-block" );

        }

    });

    /* ======== We load the attributes selected to find the variable product ======== */

    $(".Yith_WC_QOF_Main_Product_Div_Class").on( "change", ".Yith_WC_QOF_Variable_Product_select", function ( event ) {

        var Product_ID = $( this ).data( 'product_id' );
        var Yith_Post_ID = $( this ).data( 'yith_post_id' );

        $( "#YITH_WC_qof_product_line_AJAX_Selecting_Variation_ID_" + Product_ID + '_' + Yith_Post_ID ).show();

        $( "#YITH_WC_qof_product_line_AJAX_Selecting_Variation_ID_" + Product_ID + '_' + Yith_Post_ID ).append( "<img src='" + yith_wc_quick_order_forms_object.ajax_loader +"'  alt='YITH AJAX LOADER'/>" );

        var array_data_selected = {};

        var aux = "select[name^=Yith_WC_QOF_Variable_Product_select_name_" + Product_ID + "_" + Yith_Post_ID + "]";

        $( aux ).each( function() {

            var optionSelected = $( this ).find( "option:selected" );
            var valueSelected = optionSelected.val();
            var textSelected = optionSelected.text();


            array_data_selected[valueSelected] = textSelected;

        });

        var data = {
            action       : 'Yith_wc_qof_Ajax_Front_change_variation',
            data_selected: array_data_selected,
            Product_ID   : Product_ID,
            Yith_Post_ID : Yith_Post_ID
        }

        var beforeSend = "<img class='Ajax_Loader' src='" + yith_wc_quick_order_forms_object.ajax_loader + "' alt='cerrar'>"

        Yith_WC_QOF_AjaxGo( data, 'Yith_WC_QOF_Product_Line_Ajax_Result_' + Product_ID + '_' + Yith_Post_ID );

    });

    // Clicking in the button +

    $( ".Yith_WC_QOF_Main_Product_Div_Class" ).on( "click", ".yith_wc_button_plus", function () {

        $( this ).prev().val( parseInt( $( this ).prev().val() ) + 1 );

        $( this ).prev().trigger( 'change' );

    });

    // Clicking in the button -

    $( ".Yith_WC_QOF_Main_Product_Div_Class" ).on( "click", ".yith_wc_button_minus", function () {

        $( this ).next().val( parseInt( $( this ).next().val() ) - 1 );

        $( this ).next().trigger( 'change' );

    });

    /* =================== We change the quantity of the products to add to the cart ==================== */

    $( ".Yith_WC_QOF_Main_Product_Div_Class" ).on( "change", ".YITH_WC_QOF_Quantity_Cart", function (event) {

        if ( ! $.isNumeric( this.value ) )
            this.value = 0;

        if ( this.value < 0 )
            this.value = 0;
        else{
            var Product_ID = $( this ).data( 'product_id' );
            var Yith_Post_ID = $( this ).data( 'yith_post_id' );

            var Quantity_in_Cart = $( "#yith_wc_qof_button_product_" + Product_ID + '_' + Yith_Post_ID ).data( 'quantity_in_cart' );

            var Stock_Quantity = $( this ).data( 'stock_quantity' );

            if ( ! $( this ).data( 'backorder_allowed' ) )
                if ( $.isNumeric( Stock_Quantity ) )
                    if ( this.value > ( Stock_Quantity - Quantity_in_Cart ) ){
                        var message = "You cannot add that amount to the cart — we only have " + Stock_Quantity + " in stock";
                        if ( Quantity_in_Cart > 0 )
                            message = message + " and you already have " + Quantity_in_Cart + " in your cart";
                        alert( message );
                        this.value = Stock_Quantity - Quantity_in_Cart;

                        if ( this.value < 0 )
                            this.value = 0;

                    }

            $( '.Yith_WC_QOF_Main_Product_Div_Class #yith_wc_qof_button_product_' + Product_ID + '_' + Yith_Post_ID ).data( "quantity", this.value );
        }

    });

    function Yith_WC_QOF_AjaxGo( parametros, result, beforeSend ) {
        $.ajax({
            data      : parametros,
            url       : yith_wc_quick_order_forms_object.ajax_url,
            type      : 'post',
            beforeSend: function () {
                $("#" + result).html( beforeSend );
            },
            error     : function ( response ) {
                //alert(response);
            },
            success   : function ( response ) {
                $("#" + result).html( response );
            }
        });
    }

    $(document).ajaxComplete(function (event, xhr, settings) {

        if ( $( "div" ).hasClass( "YITH_WC_qof_product_line" ) )
            $("a.added_to_cart").remove();
    });

    function yith_wah() {
        var width = $('body #Yith_WC_QOF_Main_Product_Div').width();

        alert(width);
    }

    $('.yith_wah').on('click', function () {

        yith_wah();

    });

});