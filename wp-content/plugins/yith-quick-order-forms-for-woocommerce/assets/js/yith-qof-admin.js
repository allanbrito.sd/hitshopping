jQuery(function ($) {

    // Shared function by categories and tags
    var results = function (data) {
        var terms = [];
        if (data) {
            $.each(data, function (id, text) {
                terms.push({id: id, text: text});
            });
        }
        return {
            results: terms
        };
    };

    var initSelection = function (element, callback) {
        var data = $.parseJSON(element.attr('data-selected'));
        var selected = [];

        $(element.val().split(',')).each(function (i, val) {
            selected.push({
                id  : val,
                text: data[val]
            });
        });
        return callback(selected);
    };
    var formatSelection = function (data) {
        return '<div class="selected-option" data-id="' + data.id + '">' + data.text + '</div>';
    };

    $(".YITH_WC__QOF_Form_Select_Permissions").select2({
            minimumResultsForSearch: -1
        }
    );

    $(".yith_wc_qof_roles_search").select2({
        placeholder: yith_wc_quick_order_forms_object.string_choose_rol
    });

    function yith_wc_quick_order_forms_show_permissions(option) {

        switch (option) {
            case 'enable_users':
                //alert( 'Activando users' );
                $("#YITH_QOF_Form_Line_Disable_Users").hide();
                $("#YITH_QOF_Form_Line_Enable_Users").show();
                break;
            case 'disable_users':
                //alert( 'Desactivando users' );
                $("#YITH_QOF_Form_Line_Enable_Users").hide();
                $("#YITH_QOF_Form_Line_Disable_Users").show();
                break;
            case 'enable_user_roles':
                //alert( 'Activando user roles' );
                $("#YITH_QOF_Form_Line_Disable_User_Roles").hide();
                $("#YITH_QOF_Form_Line_Enable_User_Roles").show();
                break;
            case 'disable_user_roles':
                //alert( 'Dsactivando user roles' );
                $("#YITH_QOF_Form_Line_Enable_User_Roles").hide();
                $("#YITH_QOF_Form_Line_Disable_User_Roles").show();
                break;
            case 'No_user_lists':
                //alert( 'Dsactivando user lists' );
                $("#YITH_QOF_Form_Line_Disable_Users").hide();
                $("#YITH_QOF_Form_Line_Enable_Users").hide();
                break;
            case 'No_user_roles_lists':
                //alert( 'Dsactivando user roles lists' );
                $("#YITH_QOF_Form_Line_Disable_User_Roles").hide();
                $("#YITH_QOF_Form_Line_Enable_User_Roles").hide();
                break;
        }

    }

    $(".YITH_WC__QOF_Form_Select_Permissions").on("change", function () {

        yith_wc_quick_order_forms_show_permissions(this.value);

    });

    yith_wc_quick_order_forms_show_permissions($("#yith_wc_quick_order_form_option_users_list_select").val());

    yith_wc_quick_order_forms_show_permissions($("#yith_wc_quick_order_form_option_user_roles_list_select").val());

    // Arguments for categories select2
    $(':input.yith_wc_qof_categories_search').filter(':not(.enhanced)').each(function () {

        var ajax = {
            url        : yith_wc_quick_order_forms_object.ajax_url,
            dataType   : 'json',
            quietMillis: 250,
            data       : function (term) {
                return {
                    term    : term,
                    action  : 'yith_wc_qof_ajax_search_categories',
                    security: yith_wc_quick_order_forms_object.yith_wc_search_categories_nonce
                };
            },
            cache      : true
        };

        if (yith_wc_quick_order_forms_object.yith_wc_legacy) {
            ajax.results = results;
        } else {
            ajax.processResults = results;
        }
        var select2_args = {
            initSelection     : yith_wc_quick_order_forms_object.yith_wc_legacy ? initSelection : null,
            formatSelection   : yith_wc_quick_order_forms_object.yith_wc_legacy ? formatSelection : null,
            multiple          : $(this).data('multiple'),
            allowClear        : $(this).data('allow_clear') ? true : false,
            placeholder       : $(this).data('placeholder'),
            minimumInputLength: $(this).data('minimum_input_length') ? $(this).data('minimum_input_length') : '3',
            escapeMarkup      : function (m) {
                return m;
            },
            ajax              : ajax
        };
        $(this).select2(select2_args).addClass('enhanced');
    });

    // Arguments for tags select2
    $(':input.yith_wc_qof_tags_search').filter(':not(.enhanced)').each(function () {
        var ajax = {
            url        : yith_wc_quick_order_forms_object.ajax_url,
            dataType   : 'json',
            quietMillis: 250,
            data       : function (term) {
                return {
                    term    : term,
                    action  : 'yith_wc_qof_ajax_search_tags',
                    security: yith_wc_quick_order_forms_object.yith_wc_search_tags_nonce
                };
            },
            cache      : true
        };

        if (yith_wc_quick_order_forms_object.yith_wc_legacy) {
            ajax.results = results;
        } else {
            ajax.processResults = results;
        }
        var select2_args = {
            initSelection     : yith_wc_quick_order_forms_object.yith_wc_legacy ? initSelection : null,
            formatSelection   : yith_wc_quick_order_forms_object.yith_wc_legacy ? formatSelection : null,
            multiple          : $(this).data('multiple'),
            allowClear        : $(this).data('allow_clear') ? true : false,
            placeholder       : $(this).data('placeholder'),
            minimumInputLength: $(this).data('minimum_input_length') ? $(this).data('minimum_input_length') : '3',
            escapeMarkup      : function (m) {
                return m;
            },
            ajax              : ajax
        };
        $(this).select2(select2_args).addClass('enhanced');
    });

    /* ======================== Displaying permission options ========================= */

    function yith_wc_quick_order_display_permissions() {

        if ($("#yith_wc_quick_order_all_users_checkbox").is(':checked')) {
            $("#YITH_QOF_Choose_Permissions_Displaying").show();
        } else {
            $("#YITH_QOF_Choose_Permissions_Displaying").hide();
        }

    }

    $("#yith_wc_quick_order_all_users_checkbox").on("click", function () {

        yith_wc_quick_order_display_permissions();

    });

    yith_wc_quick_order_display_permissions();

    /* ================================================================================= */


    /* ====== Displaying blank brigthness after Radio button chosen all ======= */

    function yith_wc_quick_order_display_blank_brigthness_radio_button(radio_name, ID) {

        if ($('input:radio[name=' + radio_name + ']:checked').val() == "radio_all")
            $(ID).show();
        else
            $(ID).hide();

    }

    /* ================================================================================= */

    /* ======================== Displaying User Roles List ========================= */

    $("input:radio[name=yith_wc_quick_order_form_user_roles_list_radio_button]").on("click", function () {

        yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_user_roles_list_radio_button', '#YITH_QOF_Choose_User_Roles_List_Permissions_Displaying');

    });

    yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_user_roles_list_radio_button', '#YITH_QOF_Choose_User_Roles_List_Permissions_Displaying');

    /* ================================================================================= */

    /* ======================== Displaying Users List ========================= */

    $("input:radio[name=yith_wc_quick_order_form_users_list_radio_button]").on("click", function () {

        yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_users_list_radio_button', '#YITH_QOF_Choose_Users_List_Permissions_Displaying');

    });

    yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_users_list_radio_button', '#YITH_QOF_Choose_Users_List_Permissions_Displaying');

    /* ================================================================================= */

    /* ======================== Displaying products options ========================= */

    $("input:radio[name=yith_wc_quick_order_form_products_chosen_radio_button]").on("click", function () {

        yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_products_chosen_radio_button', '#YITH_QOF_Choose_Products_Displaying');

    });

    yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_products_chosen_radio_button', '#YITH_QOF_Choose_Products_Displaying');

    /* ================================================================================= */

    /* ======================== Displaying categories options ========================= */

    $("input:radio[name=yith_wc_quick_order_form_categories_chosen_radio_button]").on("click", function () {

        yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_categories_chosen_radio_button', '#YITH_QOF_Choose_Categories_Displaying');

    });

    yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_categories_chosen_radio_button', '#YITH_QOF_Choose_Categories_Displaying');

    /* ================================================================================= */

    /* ======================== Displaying products options ========================= */

    $("input:radio[name=yith_wc_quick_order_form_tags_chosen_radio_button]").on("click", function () {

        yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_tags_chosen_radio_button', '#YITH_QOF_Choose_Tags_Displaying');

    });

    yith_wc_quick_order_display_blank_brigthness_radio_button('yith_wc_quick_order_form_tags_chosen_radio_button', '#YITH_QOF_Choose_Tags_Displaying');

    /* ================================================================================= */

    $(".YITH_QOF_Form_number").on("change", function () {
        if (this.value < 0)
            this.value = 0;
    });

    $(document.body).trigger('wc-enhanced-select-init');

});