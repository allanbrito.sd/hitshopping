<?php 

    define('PUBLIC_URL', 'http://localhost/hitshopping/');
    define('PAG_HOME', PUBLIC_URL);
    define('OUT', PUBLIC_URL.'out/');
    define('RESPONSES', OUT.'responses/');
    define('IMG', PUBLIC_URL.'tim.php?src=');
    define('PAG_BUSCA', PUBLIC_URL.'busca/');
    define('PAG_DEPARTAMENTO', PUBLIC_URL.'produtos/');
    define('PAG_PRODUTOS', PUBLIC_URL.'produtos/');
    define('PAG_PRODUTOS_MASCULINOS', PUBLIC_URL.'masculino/');
    define('PAG_PRODUTOS_FEMININOS', PUBLIC_URL.'feminino/');
    define('PAG_PRODUTOS_INFANTIS', PUBLIC_URL.'infantil/');
    define('PAG_PRODUTOS_ESPORTIVOS', PUBLIC_URL.'esportes/');
    define('PAG_PRODUTOS_MODA_PRAIA', PUBLIC_URL.'moda-praia/');
    define('PAG_PRODUTO', PUBLIC_URL.'produto/');
    define('PAG_LOJAS', PUBLIC_URL.'lojas/');
    define('PAG_LOJA', PUBLIC_URL.'loja/');
    define('PAG_LOGIN', PUBLIC_URL.'login/');
    define('PAG_ESQUECI_SENHA', PUBLIC_URL.'esqueci/');
    define('PAG_RECUPERAR', PUBLIC_URL.'recuperar/');
    define('PAG_LOGOUT', PUBLIC_URL.'logout');
    define('PAG_SOBRE', PUBLIC_URL.'institucional/sobre');
    define('PAG_CONTATO', PUBLIC_URL.'contato');
    define('PAG_CARRINHO', PUBLIC_URL.'carrinho');
    define('PAG_MEUS_DADOS', PUBLIC_URL.'meus-dados/');
    define('PAG_MEUS_ENDERECOS', PUBLIC_URL.'meus-enderecos/');
    define('PAG_MEUS_PEDIDOS', PUBLIC_URL.'meus-pedidos/');
    define('PAG_PEDIDO', PUBLIC_URL.'pedido/');
    define('PAG_FAQ', PUBLIC_URL.'faq/');
    define('PAG_PROGRAMA_DE_AFILIADOS', PUBLIC_URL.'institucional/programa-de-afiliados');
    define('PAG_REGULAMENTOS', PUBLIC_URL.'institucional/regulamentos');
    define('PAG_CONDUTA', PUBLIC_URL.'institucional/conduta');
    define('PAG_AJUDA_ENTREGAS', PUBLIC_URL.'ajuda/entregas');
    define('PAG_AJUDA_ARREPENDIMENTO', PUBLIC_URL.'ajuda/arrependimento');
    define('PAG_AJUDA_CANCELAMENTOS', PUBLIC_URL.'ajuda/cancelamentos');
    define('PAG_AJUDA_PAGAMENTOS', PUBLIC_URL.'ajuda/pagamentos');
    define('PAG_AJUDA_MEUS_PEDIDOS', PUBLIC_URL.'ajuda/meus-pedidos');
    define('PAG_AJUDA_TABELA', PUBLIC_URL.'ajuda/tabela-de-tamanhos');
    define('PAG_AJUDA_TROCAS', PUBLIC_URL.'ajuda/trocas');
    define('PAG_PRIVACIDADE', PUBLIC_URL.'institucional/privacidade');
    define('PAG_MARKETPLACE', PUBLIC_URL.'marketplace');
    define('PAG_VENDAS', PUBLIC_URL.'vendas-para-empresas');
    define('PAG_PAGINA', PUBLIC_URL.'pagina/');
    define('PAG_TRABALHE_CONOSCO', PUBLIC_URL.'trabalhe-conosco');
    define('PAG_MEUS_TICKETS', PUBLIC_URL.'meus-tickets/');
    define('PAG_TICKET', PUBLIC_URL.'ticket/');
    define('PAG_NOVO_TICKET', PUBLIC_URL.'novo-ticket/');
    define('FACEBOOK_URL',  'https://www.facebook.com/Hitoutlets-118457072175331/');
    define('TWITTER_URL',  '');
    define('INSTAGRAM_URL', 'https://www.instagram.com/hitoutlets/');
    define('IMAGEM',    PUBLIC_URL.'tim.php?src=');
    define('TEMPLATE_PRODUTOS', 'Exibindo {start} - {end} de {total} produto{s}');
    define('TEMPLATE_ITENS', 'Exibindo {start} - {end} de {total} ite{ns}');

?>
<!--footer -->
<f>
<div class="container-fluid">
<div class="row newsletter">
<div class="col-sm-7">
<h2>ofertas e descontos</h2>
<h3>cadastre-se e fique por dentro</h3>
</div>
<div class="col-sm-5 form">
<form id="newsletterform" action="" method="post" class="center-block formPadrao">
<ul>
    <li>
        <div class="form-group">
            <input type="text" class="form-control input-email" placeholder="Escreva seu email" name="value" required="true">
        </div>
    </li>
    <li>
        <button type="submit" value="Enviar" class="btn-caixa">enviar</button>
    </li>
</ul>
</form>
</div>
</div>
</div>
</f>
<footer>
<div class="container-fluid">
<div class="row">
<div class="col-sm-2">
<div class="about-footer">
<img class="margin-bottom-30" src="assets/images/logo-foot.png" alt="" >
<a href="https://www.facebook.com/Hitoutlets-118457072175331/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
<a href="https://www.instagram.com/hitoutlets/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
</div>
</div>

<div class="col-sm-2">
<h6>INSTITUCIONAL</h6>
<ul class="link">
<li><a href="<?=PAG_SOBRE?>"> Sobre a hitoutlets</a></li>
<li><a href="<?=PAG_PRIVACIDADE?>"> Política de Privacidade</a></li>
<li><a href="<?='https://hitoutlets.com/vendanahit'?>"> Venda na hit</a></li>
<!-- <li><a href="<?=PAG_TRABALHE_CONOSCO?>"> Trabalhe Conosco</a></li> -->
<!-- <li><a href="<?=PAG_PROGRAMA_DE_AFILIADOS?>"> Programa de Afiliados</a></li> -->
<!-- <li><a href="<?=PAG_REGULAMENTOS?>"> Regulamentos</a></li> -->
<!-- <li><a href="<?=PAG_CONDUTA?>"> Conduta de fornecedores </a></li> -->
</ul>
</div>

<div class="col-sm-2">
<h6>ajuda</h6>
<ul class="link">
<li><a href="<?=PAG_AJUDA_ENTREGAS?>"> Entregas</a></li>
<li><a href="<?=PAG_AJUDA_PAGAMENTOS?>"> Pagamentos</a></li>
<li><a href="<?=PAG_AJUDA_TROCAS?>"> Trocas</a></li>
<li><a href="<?=PAG_AJUDA_ARREPENDIMENTO?>"> Arrependimento</a></li>
<li><a href="<?=PAG_AJUDA_CANCELAMENTOS?>"> Cancelamentos</a></li>
<!-- <li><a href="<?=PAG_AJUDA_MEUS_PEDIDOS?>"> Meus Pedidos</a></li> -->
<!-- <li><a href="<?=PAG_AJUDA_TABELA?>"> Tabela de tamanhos</a></li> -->
</ul>
</div>

<div class="col-sm-2">
<h6>CENTRAL DE RELACIONAMENTO</h6>
<ul class="link">
<li><a href="<?=PAG_CONTATO?>"> tire  suas dúvidas</a></li>
</ul>
</div>
<div class="col-sm-2">
<h6>hitoutlets empresa</h6>
<ul class="link">
<li><a href="/login"> Portal do Lojista </a></li>
<!-- <li><a href="<?=PAG_MARKETPLACE?>"> Marketplace </a></li> -->
<!-- <li><a href="<?=PAG_VENDAS?>">Vendas para Empresas</a></li> -->
<li>Segunda a sexta</li>
<li>Das 08:00 às 18:00</li>
</ul>
</div>
<div class="col-sm-2">
<h6>minha conta</h6>
<ul class="link">
<li><a href="<?=PAG_LOGIN?>"> login</a></li>
<li><a href="<?=PAG_MEUS_DADOS?>">minha conta</a></li>
<li><a href="<?=PAG_MEUS_PEDIDOS?>">meus pedidos</a></li>
<li><a href="<?=PAG_MEUS_TICKETS?>">meus tickets</a></li>
</ul>
</div>

</div>
</div>
<div class="copyright margin-top-40 padding-top-50 padding-bottom-50">
<div class="container">
<div class="row">
<div class="col-sm-12">
<img src="assets/images/cartoes.jpg?v=1.0" class="img-responsive">
<!--<?php if(!empty($copyright)) { ?>
<p class="margin-top-20"><?=$copyright[0]['content']?></p>
<?php } ?>
-->
</div>
</div>
</div>
</div>
</footer>
<!--footer -->

<script>
    window.PUBLIC_URL = 'URL DA APLICACAO AQUI!!!!!';
</script>
<script src="assets/js/jquery-1.11.3.min.js"></script> 
<script src="assets/js/bootstrap.min.js"></script> 
<script src="assets/js/own-menu.js"></script> 
<script src="assets/js/jquery.lighter.js"></script> 
<script src="assets/js/owl.carousel.min.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="assets/rs-plugin/js/jquery.tp.t.min.js"></script> 
<script type="text/javascript" src="assets/rs-plugin/js/jquery.tp.min.js"></script> 
<script type="text/javascript" src="assets/plugins/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.datetimepicker/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="assets/plugins/modal_constructor.js"></script>
<script>
    // jQuery(document).ready(function() { <?php /* Display::ModalMessages();  */ ?>});
</script>
<script src="assets/js/main.js?v=1.0"></script>
<script src="assets/js/newsletter.js"></script>
<script src="assets/plugins/multilevelmenu/js/classie.js"></script>
<script src="assets/plugins/multilevelmenu/js/dummydata.js"></script>
<script src="assets/plugins/multilevelmenu/js/main.js"></script>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'ABEc0M0ijb';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<script>
  var sTextColor = '#FFFFFF';
  var sBackColor = '#404040';
  var sLabelColor = '#000000';
  var sBorderColor = '#000000';
    ;(function(w,d,$,undefined) { /*
        var URLscheme = "<?=/*isset($searchOutputURL) ? $searchOutputURL : */PAG_PRODUTOS/*.str_repeat('*', 14);*/?>";

    */
        $("#float-search").submit(function(e) {
            e.preventDefault();
            w.location.href = URLscheme + '?busca='+$(this).find('input[type="search"]').val();
        });
    })(window, document, jQuery);
    (function() {
        var menuEl = document.getElementById('ml-menu'),
            mlmenu = new MLMenu(menuEl, {
                breadcrumbsCtrl : true,
                initialBreadcrumb : 'menu',
                backCtrl : true,
                // itemsDelayInterval : 60, // delay between each menu item sliding animation
                onItemClick: loadDummyData // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
            });

        // mobile menu toggle
        var openMenuCtrl = document.querySelector('.action--open'),
            closeMenuCtrl = document.querySelector('.action--close');

        openMenuCtrl.addEventListener('click', openMenu);
        closeMenuCtrl.addEventListener('click', closeMenu);

        function openMenu() {
            classie.add(menuEl, 'menu--open');
            closeMenuCtrl.focus();
        }

        function closeMenu() {
            classie.remove(menuEl, 'menu--open');
            openMenuCtrl.focus();
        }

        // simulate grid content loading
        var gridWrapper = document.querySelector('.content');

        function loadDummyData(ev, itemName) {
            ev.preventDefault();
            var href = ev.srcElement.href;
            if(href != undefined && href != "#" && href != "#.") {
                window.location.href = href;
            } else {
                closeMenu();
                gridWrapper.innerHTML = '';
                classie.add(gridWrapper, 'content--loading');
                setTimeout(function() {
                    classie.remove(gridWrapper, 'content--loading');
                    gridWrapper.innerHTML = '<ul class="products">' + dummyData[itemName] + '<ul>';
                }, 700);
            }
        }
    })();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118957859-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-118957859-1');
</script>
</body>
</html>

<!-- <?php /*


$footer_logo = marketo_option('footer_logo');
$footer_columns = marketo_option( 'footer_widget_layout',marketo_defaults('footer_widget_layout') );

$show_footer_logo = marketo_option( 'show_footer_logo',marketo_defaults('show_footer_logo') );
$show_back_to_top = marketo_option( 'show_back_to_top',marketo_defaults('show_back_to_top') );
$show_fixed_footer = marketo_option( 'show_fixed_footer',marketo_defaults('show_fixed_footer') );
if($footer_columns == 1 ) {
    $widget_width = 12;
}elseif($footer_columns == 2 ) {
    $widget_width = 6;
}elseif($footer_columns == 3 ) {
    $widget_width = 4;
}elseif($footer_columns == 4 ) {
    $widget_width = 3;
}elseif($footer_columns == 5 ) {
    $widget_width = 2;
}elseif($footer_columns == 6 ) {
    $widget_width = 2;
}


$fixed_footer = '';
if($show_fixed_footer){
    $fixed_footer = 'xs-fixed-footer';
}
$header_fullwidth = marketo_option('header_fullwidth');
if($header_fullwidth){
    $container = 'container container-fullwidth';
}else{
    $container = 'container';
}
$show_footer_layout = marketo_option( 'show_footer_layout' );
?>
<footer class="xs-footer-section">
    <?php if($show_footer_layout): ?>
        <div class="xs-footer-main">
            <div class="<?php echo esc_attr($container); ?>">
                <?php if(!empty($footer_logo) && $show_footer_logo): ?>
                    <div class="xs-footer-logo">
                        <a href="#">
                            <img src="<?php echo esc_url($footer_logo);  ?>" alt="<?php echo get_bloginfo(); ?>">
                        </a>
                    </div>
                <?php endif ?>
                <div class="row">
                    <?php
                    for ($i = 1; $i <= $footer_columns ;$i++):
                        $widget_width = apply_filters( "marketo_footer_widget_{$i}_width", $widget_width );
                        ?>
                        <div class="col-md-<?php echo esc_attr($widget_width); ?> footer-widget">
                            <?php
                            if(is_active_sidebar('footer-widget-'.$i)):
                                dynamic_sidebar('footer-widget-'.$i);
                            endif;
                            ?>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="xs-copyright">
        <div class="<?php echo esc_attr($container); ?>">
            <div class="row">
                <div class="col-md-6">
                    <div class="xs-copyright-text">
                        <?php echo marketo_option('copyright_text',marketo_defaults('copyright_text')); ?>
                    </div>
                </div>
                <?php $payment_methods = marketo_option('payment_methods');
                if(!empty($payment_methods)){ ?>
                    <div class="col-md-6">
                        <ul class="xs-payment-card">
                            <li class="payment-title"><?php esc_html_e('Allow payment base on','marketo');?></li>
                            <?php
                            foreach($payment_methods as $payment_method){
                                $payment_img = wp_get_attachment_url($payment_method['payment_img']);
                                ?>
                                <li>
                                    <a href="<?php echo esc_url($payment_method['payment_url']); ?>">
                                        <img src="<?php echo esc_url($payment_img); ?>" alt="<?php the_title_attribute(); ?>">
                                    </a>
                                </li>
                             <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

</footer> -->